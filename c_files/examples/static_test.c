extern void __VERIFIER_error() __attribute__ ((__noreturn__));
extern void __VERIFIER_assume(int);
extern int __VERIFIER_nondet_int();

int foo( ) { 
  return 42; 
}

int main( ) {
  int x = __VERIFIER_nondet_int();
  int y = __VERIFIER_nondet_int();

  if (x < 0) { /* This branch is questionable */
    foo(); /* Suspicious statement */
  } else {
    foo(); /* Suspicious statement */
  }

  if (y > 0) {
    __VERIFIER_error(); 
  }
}

