#include <math.h>

extern void __VERIFIER_error() __attribute__ ((__noreturn__));
extern int __VERIFIER_nondet_int();

int main( ) {
  int x = __VERIFIER_nondet_int();
  double y = x*x - pow(((double) x),2);
  if (!(y > 0)) {
    __VERIFIER_error();
  }
}
