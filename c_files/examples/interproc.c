extern void __VERIFIER_error() __attribute__ ((__noreturn__));
extern void __VERIFIER_assume(int);
extern int __VERIFIER_nondet_int();

int read() {
  int x = __VERIFIER_nondet_int();
  return x;
}

int main( ) {
  int input = read();
  if (input < 0) {
    __VERIFIER_error();
  } 
  return 0;
}

