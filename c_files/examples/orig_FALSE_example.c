extern void __VERIFIER_error() __attribute__ ((__noreturn__));
extern void __VERIFIER_assume(int);
extern int __VERIFIER_nondet_int();

int main( ) {
  int x = __VERIFIER_nondet_int();
  int y = __VERIFIER_nondet_int();
  int z = __VERIFIER_nondet_int();

  __VERIFIER_assume(x>=0); 
  __VERIFIER_assume(y>=0);
  __VERIFIER_assume(z>=0);
  int tmp = 0;

  if (x < y) {
    if (z < 0) {
      z = 0 - z;
    }

    for (int i=0; i<y; i++) {
      tmp += i;
    }
  } else {
    //tmp = y*((y+1) % x); // non-linear
    tmp = x*x - y*y; // non-linear
  }

  /* assert(tmp > 0); */
  if (!(tmp > 0)) {
    __VERIFIER_assume(!(x < y));
    __VERIFIER_error();
  }
}
