extern void __VERIFIER_error() __attribute__ ((__noreturn__));
extern void __VERIFIER_assume(int);
extern int __VERIFIER_nondet_int();

int main( ) {
  int x = __VERIFIER_nondet_int();
  int y = __VERIFIER_nondet_int();
  int z = __VERIFIER_nondet_int();

  int a = 0;

  __VERIFIER_assume(x>=0); 
  __VERIFIER_assume(y>=0);
  __VERIFIER_assume(z>=0);
  //int tmp = 0;
  int tmp = __VERIFIER_nondet_int();

  if (x < y) {
    if (z < 0) {
      z = 0 - z;
    }

    /* Original loop
    for (int i=0; i<y; i++) {
      tmp += i;
      a = z*i;
    }
    */
    int i=0;
    if (i<y) {
      // first iteration of the loop
      tmp += i;
      a = z*i;
      i++;
      if (i<y) {
        // second iteration 
        tmp += i;
        a = z*i;
        i++;
        // any remaining iterations
        for (; i<y; i++) {
          tmp += i;
          a = z*i;
        }
      } else {
        // only executed first iteration
      } 
    } else {
      // skipping the loop altogether
    }
  } else {
    tmp = y*((y+1) % x); // non-linear
    a = tmp;
  }

  /* assert(tmp > 0); */
  if (!(tmp > 0)) {
    __VERIFIER_error();
  }
  // the reads aren't getting passed within CPA
  //f(a,x,y,z);
}
