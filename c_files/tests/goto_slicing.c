extern void __VERIFIER_error() __attribute__ ((__noreturn__));
extern void __VERIFIER_assume(int);
extern int __VERIFIER_nondet_int();

int main( ) {
  
  int x = __VERIFIER_nondet_int();
  int y = __VERIFIER_nondet_int();
  int z = 0;

  if (x < 0) {
    goto out;
  } else {
    if (y < 0) {
      if (z == 0) {
        goto ERROR;
      }
    }
  } 

  out: return 0;
  ERROR: __VERIFIER_error();

}

