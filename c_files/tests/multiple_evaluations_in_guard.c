extern void __VERIFIER_error() __attribute__ ((__noreturn__));
extern void __VERIFIER_assume(int);
extern int __VERIFIER_nondet_int();

/* 
 This test is to exercise that we are parsing CPA's
 witnesses correctly according to the assumption in
 CIVL's directive interface that each guard line has
 only ONE evaluation. In CPA's witnesses, if there
 are multiple boolean tests in one guard, they are
 evaluated individually until either there is a 
 short circuit or all have been evaluated. In any
 case, we only want to grab the final evaluation
 for any set of consecutive lines in CPA's witness.
*/

int main( ) {
  int input1 = __VERIFIER_nondet_int();
  int input2 = __VERIFIER_nondet_int();

  if ((input1 != 1) && (input2 != 2)) {
    return -2;
  }

  if (input1 != 1) {
    __VERIFIER_error();
  } 
}

