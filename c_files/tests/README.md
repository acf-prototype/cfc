To run all tests:

  `ruby run_tests.rb`

To run test over *one* file, e.g. `foo.c`:

  `ruby run_tests.rb foo`