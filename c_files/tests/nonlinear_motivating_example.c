extern void __VERIFIER_error() __attribute__ ((__noreturn__));
extern void __VERIFIER_assume(int);
extern int __VERIFIER_nondet_int();

/* Running only with CPA's -sv-comp16 config */

int main( ) {
  int x = __VERIFIER_nondet_int();
  int y = __VERIFIER_nondet_int();

  int tmp = 0;

  if (x >= 0) {
    if (y < 0) {
      y = 0 - y;
    }
    for (int i=0; i<x; i++) {
      tmp += i;
    }
  } else {
    tmp = x*x; /* x < 0 -> (x*x > 0) */
  }

  if (tmp <= 0) {
    __VERIFIER_error();
  }
}
