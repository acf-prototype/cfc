extern void __VERIFIER_error() __attribute__ ((__noreturn__));
extern void __VERIFIER_assume(int);
extern int __VERIFIER_nondet_int();

int main( ) {

  int x = __VERIFIER_nondet_int();
  int p = 100;

  if (x < 0) {
    int y = __VERIFIER_nondet_int();
    if (y < 23) {
      p = 0;
    }
  } else {
    int z = __VERIFIER_nondet_int();
    if (z < 42) {
      p = 0;
    }
  }
  
  if (p == 0) {
    __VERIFIER_error();
  }

}

