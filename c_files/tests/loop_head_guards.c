extern void __VERIFIER_error() __attribute__ ((__noreturn__));
extern void __VERIFIER_assume(int);
extern int __VERIFIER_nondet_int();

/*  
 The branch directives should be: [1,0,1].
 If you only remove branches based on
 consecutive identical guard conditions---
 as I was doing before---then the witness
 would be: [0,1], and CIVL would declare
 the program safe, because CIVL would not
 enter the loop, and so the guard (x > 0)
 would violate the assumption that x==0.
*/

int main( ) {

  int x = 0;

  for (int i=0; i<1; i++) {
    x++;
  } 
  if (x > 0) {
    __VERIFIER_error();
  }

}

