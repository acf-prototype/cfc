extern void __VERIFIER_error() __attribute__ ((__noreturn__));
extern void __VERIFIER_assume(int);
extern int __VERIFIER_nondet_int();

int main( ) {

  int p = __VERIFIER_nondet_int();
  int lk = 0;

  int cond;

  while (1) {

    cond = __VERIFIER_nondet_int();
    if (cond == 0) {
      goto out;
    } else {}

    if (p != 0) {
      lk = 1; // acquire lock
    } else {}

    if (p != 0) {
      if (lk != 1) 
	goto ERROR; // assertion failure
      lk = 0;
    } else { 
      goto ERROR; 
    }

  }
 out:
  return 0;
  ERROR: 
__VERIFIER_error();

}

