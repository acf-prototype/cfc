extern void __VERIFIER_error() __attribute__ ((__noreturn__));
extern void __VERIFIER_assume(int);
extern int __VERIFIER_nondet_int();

int main( ) {
  int x = __VERIFIER_nondet_int();
  int y = __VERIFIER_nondet_int();
  int z = __VERIFIER_nondet_int();

  int tmp = 0;

  if (x < y) {
    if (z < 0) {
      z = 0 - z;
    }
    for (int i=0; i<x; i++) {
      tmp += i;
    }
  } else {
    tmp = y*y - y*y + 1;
  }

  if (tmp <= 0) {
    __VERIFIER_error();
  }
}
