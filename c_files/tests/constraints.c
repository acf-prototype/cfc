extern void __VERIFIER_error() __attribute__ ((__noreturn__));
extern void __VERIFIER_assume(int);
extern int __VERIFIER_nondet_int();

int main( ) {

  int x = __VERIFIER_nondet_int();
  int y = __VERIFIER_nondet_int();
  
  if (x != 0) {
    y = 42;
  } else {
    y = 23;
  }

  if (x != 0) {
    return 0;
  } else {
    __VERIFIER_error();
  }

}

