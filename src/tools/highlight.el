(defun goto-line-at-point ()
  (interactive)
  (setq lineNum (thing-at-point 'number))
  (other-window 1)
  (goto-line lineNum)
  (hl-line-highlight)
  (other-window 1))

(global-set-key (kbd "C-i") 'goto-line-at-point)



