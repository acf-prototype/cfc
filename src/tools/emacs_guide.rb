    if ARGV.size != 1
      puts "\n Usage: ruby emacs_guide.rb <foo.direct>\n"
      abort
    end

    file_name = ARGV[0]
    directive_file = File.readlines(file_name)

    directive_file.shift

    elems = directive_file.map {|line| line.split(' ')}

    line_guide_pairs = elems[0].zip elems[1]

    line_guide_pairs.shift

    file_string = ""
    line_guide_pairs.each_with_index do |pair, i|
      line = pair[0]
      if pair[1] == "1"
        guide = "true"
      else
        guide = "false"
      end
      file_string += "#{line}\n`-> #{guide} (index: #{i})\n"
    end

    File.open(file_name+'.debug', 'w') { |f| f.write(file_string) }

