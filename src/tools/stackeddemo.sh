#!/bin/bash
echo "unset xtics" >/tmp/gnuplot.in
echo "set term pngcairo dashed" >>/tmp/gnuplot.in
echo "set output 'test.png'" >>/tmp/gnuplot.in
echo "set style data histogram" >>/tmp/gnuplot.in
echo "set style histogram rowstacked" >>/tmp/gnuplot.in
echo "plot 'stacked1.dat' with impulses dt 0 lc 'black' lw 3 title 'sub-metric 1', 'stacked2.dat' with impulses lc 'grey' lw 4 title 'sub-metric 2', 'stacked3.dat' with impulses lc 'black' lw 4 title 'sub-metric 3'" >>/tmp/gnuplot.in
gnuplot /tmp/gnuplot.in
