#!/bin/bash
if [ $# -ne 2 ];
    then echo "Expected data file and metric name as parameters"; exit

fi
sort -nr $1 >$1.sorted
echo "unset xtics" >/tmp/gnuplot.in
echo "set term pdf" >/tmp/gnuplot.in
echo "set output \"$2.pdf\"" >>/tmp/gnuplot.in
echo "plot \"$1.sorted\" with impulses lc 'black' title '$2'" >>/tmp/gnuplot.in
gnuplot /tmp/gnuplot.in
rm $1.sorted
