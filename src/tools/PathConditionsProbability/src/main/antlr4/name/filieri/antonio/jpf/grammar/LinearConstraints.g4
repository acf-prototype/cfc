grammar LinearConstraints;

options {
  language = Java;
}

@parser::header {
import java.util.Set;
import java.util.HashSet;
import com.google.common.collect.Sets;
import name.filieri.antonio.jpf.domain.LinearPolynomial;
import name.filieri.antonio.jpf.domain.Constraint.Relation;
import name.filieri.antonio.jpf.domain.Constraints;
import name.filieri.antonio.jpf.domain.Problem;
import name.filieri.antonio.jpf.domain.VarList;
import name.filieri.antonio.jpf.utils.parserSupport.OmegaConstraintsToConstraint;
}

@lexer::header {
import name.filieri.antonio.jpf.domain.VarList;
}


@members {
    VarList variables = new VarList();
    
    //@Override
    public void reportError(RecognitionException e) {
      throw new RuntimeException(e.getMessage()+"\nOn input:\n");
    }
}

relation returns [Problem relation0]
  : constraints EOF {$relation0 = new Problem(variables,$constraints.constraints1);}
  ;

term returns [LinearPolynomial e]
  : IDENT {$e = new LinearPolynomial($IDENT.text);variables=variables.add($IDENT.text);}
  | '(' expression ')' {$e = $expression.e;}
  | INTEGER {$e = new LinearPolynomial(Long.valueOf($INTEGER.text));}
  ;
  
unary returns [LinearPolynomial e]
  : { boolean positive = true; }
    ('+' | '-' { positive = !positive; })* term
    {
      $e = $term.e;
      if (!positive)
        $e = $e.mul(LinearPolynomial.MINUS_ONE);
    }
  ;

mult returns [LinearPolynomial e]
  : op1=unary { $e = $op1.e; }
    ( '*' op2=unary { $e = $e.mul($op2.e); }
    )*
  ;
  
expression returns [LinearPolynomial e]
  : op1=mult { $e = $op1.e;}
    ( '+' op2=mult { $e = $e.add($op2.e); }
    | '-' op2=mult { $e = $e.sub($op2.e); }
    )*
  ;

constraint returns [Constraints constraints0]
  : 'FALSE' {$constraints0=Constraints.FALSE;}
  | 'false' {$constraints0=Constraints.FALSE;}
  | 'TRUE' {$constraints0=Constraints.TRUE;}
  | 'true' {$constraints0=Constraints.TRUE;} 
  |poly1=expressionsList {OmegaConstraintsToConstraint builder = new OmegaConstraintsToConstraint(); builder.addPolynomials($poly1.expressionsList0);} 
    ( '==' poly2=expressionsList {builder.addRelation(Relation.EQ);builder.addPolynomials($poly2.expressionsList0);}
    | '=' poly2=expressionsList {builder.addRelation(Relation.EQ);builder.addPolynomials($poly2.expressionsList0);}
    |'<=' poly2=expressionsList {builder.addRelation(Relation.LE);builder.addPolynomials($poly2.expressionsList0);}
    | '>=' poly2=expressionsList {builder.addRelation(Relation.GE);builder.addPolynomials($poly2.expressionsList0);}
    | '>' poly2=expressionsList {builder.addRelation(Relation.GT);builder.addPolynomials($poly2.expressionsList0);}
    | '<' poly2=expressionsList {builder.addRelation(Relation.LT);builder.addPolynomials($poly2.expressionsList0);}
    | '!=' poly2=expressionsList {builder.addRelation(Relation.NE);builder.addPolynomials($poly2.expressionsList0);}
    )+
    {$constraints0 = builder.toConstraint();}
  ;
  
constraints returns [Constraints constraints1]
  : const1=constraint {$constraints1 = $const1.constraints0;}
    ( '&&' const2=constraint {$constraints1 = $constraints1.add($const2.constraints0);})*
  ;
  
expressionsList returns [Set<LinearPolynomial> expressionsList0]
  : exp1=expression {$expressionsList0=Sets.newHashSet($exp1.e);}
  (',' exp2=expression {$expressionsList0.add($exp2.e);})*
  ;


MULTILINE_COMMENT : '/*' .* '*/' -> channel(HIDDEN) ;
	
CHAR_LITERAL
	:	'\'' . '\'' {setText(getText().substring(1,2));}
	;


fragment LETTER : ('a'..'z' | 'A'..'Z') ;
fragment DIGIT : '0'..'9';
INTEGER : DIGIT+ ;
IDENT : ('_' | LETTER) (LETTER | DIGIT | '_')*;
WS : (' ' | '\t' | '\n' | '\r' | '\f')+  -> channel(HIDDEN);
COMMENT : '//' .* ('\n'|'\r')  -> channel(HIDDEN);
