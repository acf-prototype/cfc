grammar OmegaParser;

options {
  language = Java;
}

@parser::header {
import java.util.Set;
import java.util.HashSet;
import com.google.common.collect.Sets;
import name.filieri.antonio.jpf.domain.LinearPolynomial;
import name.filieri.antonio.jpf.domain.Constraint.Relation;
import name.filieri.antonio.jpf.domain.Constraints;
import name.filieri.antonio.jpf.domain.Constraint;
import name.filieri.antonio.jpf.domain.Problem;
import name.filieri.antonio.jpf.domain.VarList;
import name.filieri.antonio.jpf.utils.parserSupport.OmegaConstraintsToConstraint;
}

@parser::members{
VarList originalVarList;

public OmegaParserParser(VarList varList, TokenStream input) {
  super(input);
  this.originalVarList=varList;           
}

private Problem forgeProblem(List<LinearPolynomial> varList,Constraints constraints){
    List<String> identifiers = originalVarList.asList();
    if(identifiers.size() != varList.size()){
      throw new RuntimeException("Lists must have the same size:\n"+identifiers+": "+identifiers.size()+"\n"+varList+": "+varList.size());
    }
    Constraints newCostraints = constraints;
    if(newCostraints==null){
      newCostraints = constraints.TRUE;
    }
    for(int i=0;i<identifiers.size();i++){
      /*try{
        Integer intValue = Integer.valueOf(varList.get(i));
        Constraint newConstraint = new Constraint(new LinearPolynomial(identifiers.get(i)), Relation.EQ, new LinearPolynomial(intValue));
        newCostraints = newCostraints.add(newConstraint);
        System.out.println("\tadded new constraintI: "+newConstraint);
      }catch(NumberFormatException e){*/
        //toUnsignedIntegerString().
        if(!varList.get(i).equals(new LinearPolynomial(identifiers.get(i)))){
          Constraint newConstraint = new Constraint(new LinearPolynomial(identifiers.get(i)), Relation.EQ, varList.get(i));
          newCostraints = newCostraints.add(newConstraint);
        //}
      }
    }
    return new Problem(originalVarList, newCostraints);
}
  //@Override
    public void reportError(RecognitionException e) {
      throw new RuntimeException(e.getMessage()+"\nOn input:\n"); //+input.toString()
    }
}

@lexer::header {
}

@lexer::members {
  //@Override
  public void reportError(RecognitionException e){
    throw new RuntimeException("ANTLR Lexer Exception: " + e.getMessage()); 
  }
}

relationEval returns [Set<Problem> problems]
	:	rel1=relation {$problems = new HashSet<Problem>();$problems.add($rel1.rel);}
	( 'union' rel2=relation {$problems.add($rel2.rel);})* 
	EOF
	;

relation returns [Problem rel]
  : '{' varList (constraints)?'}' {$rel = this.forgeProblem($varList.varList0,$constraints.constraints1);}
  ;
  
varList returns [List<LinearPolynomial> varList0]
  : '[' head=varListElement {$varList0=new ArrayList<LinearPolynomial>();$varList0.add($head.representation);} (',' trail=varListElement {$varList0.add($trail.representation);})* ']' 
  ;
  
varListElement returns [LinearPolynomial representation]
  :  expression {$representation=$expression.e;}
  ;
  


constraint returns [Constraints constraints0]
  : 'FALSE' {$constraints0=Constraints.FALSE;} 
  |poly1=expressionsList {OmegaConstraintsToConstraint builder = new OmegaConstraintsToConstraint(); builder.addPolynomials($poly1.expressionsList0);} 
    ( '==' poly2=expressionsList {builder.addRelation(Relation.EQ);builder.addPolynomials($poly2.expressionsList0);}
    | '=' poly2=expressionsList {builder.addRelation(Relation.EQ);builder.addPolynomials($poly2.expressionsList0);}
    |'<=' poly2=expressionsList {builder.addRelation(Relation.LE);builder.addPolynomials($poly2.expressionsList0);}
    | '>=' poly2=expressionsList {builder.addRelation(Relation.GE);builder.addPolynomials($poly2.expressionsList0);}
    | '>' poly2=expressionsList {builder.addRelation(Relation.GT);builder.addPolynomials($poly2.expressionsList0);}
    | '<' poly2=expressionsList {builder.addRelation(Relation.LT);builder.addPolynomials($poly2.expressionsList0);}
    | '!=' poly2=expressionsList {builder.addRelation(Relation.NE);builder.addPolynomials($poly2.expressionsList0);}
    )+
    {$constraints0 = builder.toConstraint();}
  ;
  
constraints returns [Constraints constraints1]
  : ':' const1=constraint {$constraints1 = $const1.constraints0;}
    ( '&&' const2=constraint {$constraints1 = $constraints1.add($const2.constraints0);})*
  ;
  
expressionsList returns [Set<LinearPolynomial> expressionsList0]
  : exp1=expression {$expressionsList0=Sets.newHashSet($exp1.e);}
  (',' exp2=expression {$expressionsList0.add($exp2.e);})*
  ;

term returns [LinearPolynomial e]
  : IDENT {$e = new LinearPolynomial($IDENT.text);}
  | '(' expression ')' {$e = $expression.e;}
  | INTEGER {$e = new LinearPolynomial(Long.valueOf($INTEGER.text));}
  | INTEGER IDENT {$e = new LinearPolynomial(Long.valueOf($INTEGER.text)); $e = $e.mul(new LinearPolynomial($IDENT.text));}
  ;
  
unary returns [LinearPolynomial e]
  : { boolean positive = true; }
    ('+' | '-' { positive = !positive; })* term
    {
      $e = $term.e;
      if (!positive)
        $e = $e.mul(LinearPolynomial.MINUS_ONE);
    }
  ;

mult returns [LinearPolynomial e]
  : op1=unary { $e = $op1.e; }
    ( '*' op2=unary { $e = $e.mul($op2.e); }
    )*
  ;
  
expression returns [LinearPolynomial e]
  : op1=mult { $e = $op1.e;}
    ( '+' op2=mult { $e = $e.add($op2.e); }
    | '-' op2=mult { $e = $e.sub($op2.e); }
    )*
  ;

MULTILINE_COMMENT : '/*' .* '*/' -> channel(HIDDEN) ;
	
CHAR_LITERAL
	:	'\'' . '\'' {setText(getText().substring(1,2));}
	;

fragment LETTER : ('a'..'z' | 'A'..'Z') ;
fragment DIGIT : '0'..'9';
INTEGER : DIGIT+ ;
IDENT : (LETTER | '_') (LETTER | DIGIT | '_')*;
WS : (' ' | '\t' | '\n' | '\r' | '\f')+ -> channel(HIDDEN);
COMMENT : '#' .* ('\n'|'\r') -> channel(HIDDEN);
