package name.filieri.antonio.pcp;

import com.google.common.base.Preconditions;

import name.filieri.antonio.pcp.ast.expr.ExpressionType;

public class ExpressionResult {
	public final ExpressionType type; // boolean are stored as integers
	public final Number numericResult;
	public final String stringResult;

	public ExpressionResult(ExpressionType type, Number number) {
		this.type = type;
		this.numericResult = number;
		this.stringResult = null;
	}
	
	public ExpressionResult(ExpressionType type, String string) {
		this.type = type;
		this.numericResult = null;
		this.stringResult = string;
	}
	
}
