package name.filieri.antonio.pcp.ast.expr;

import name.filieri.antonio.pcp.ast.Node;

public interface Expression extends Node {

	public <T> T accept(ExpressionVisitor<T> visitor);
	
}
