package name.filieri.antonio.pcp.ast.expr.bitvector;

import com.google.common.hash.HashCode;
import com.google.common.hash.Hasher;
import java.nio.charset.StandardCharsets;
import name.filieri.antonio.jpf.utils.ASTUtil;
import name.filieri.antonio.jpf.utils.BigRational;
import name.filieri.antonio.pcp.ast.PositionInfo;
import name.filieri.antonio.pcp.ast.expr.Constant;
import name.filieri.antonio.pcp.ast.expr.ExpressionVisitor;

/**
 * Created by mateus on 21/03/17.
 */
public class BitVectorConstant implements Constant {

  private HashCode hash = null;
  private final BigRational value;
  private final PositionInfo posInfo;
  private final int length;

  public BitVectorConstant(String bitString, boolean isHex, PositionInfo positionInfo) {
    this.posInfo = positionInfo;
    this.value = new BigRational(bitString, isHex ? 16 : 2);
    this.length = bitString.length() * (isHex ? 4 : 1);
  }

  @Override
  public <T> T accept(ExpressionVisitor<T> visitor) {
    return visitor.visit(this);
  }

  @Override
  public HashCode getHashCode() {
    if (hash == null) {
      Hasher hasher = ASTUtil.getHashFunction().newHasher();
      hasher.putString(value.toString(2), StandardCharsets.UTF_8);
      hash = hasher.hash();
    }
    return hash;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    BitVectorConstant that = (BitVectorConstant) o;

    return value.equals(that.value);
  }

  @Override
  public int hashCode() {
    return getHashCode().hashCode();
  }

  @Override
  public PositionInfo getPositionInfo() {
    return posInfo;
  }

  public BigRational getValue() {
    return value;
  }

  public int getLength() {
    return length;
  }
}
