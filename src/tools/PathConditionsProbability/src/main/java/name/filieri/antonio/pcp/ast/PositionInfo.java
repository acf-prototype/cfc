package name.filieri.antonio.pcp.ast;

import org.antlr.v4.runtime.Token;

public class PositionInfo {

	public final int startLine;
	public final int startChar;
	public final int endLine;
	public final int endChar;

	public PositionInfo(int startLine, int startChar, int endLine, int endChar) {
		super();
		this.startLine = startLine;
		this.startChar = startChar;
		this.endLine = endLine;
		this.endChar = endChar;
	}

	public static final PositionInfo DUMMY = new PositionInfo(0, 0, 0, 0);

	@Override
	public String toString() {
		return "position[start=" + startLine + "l:" + startChar + ", end=" + endLine
				+ "l:" + endChar + "]";
	}

	public static PositionInfo fromTokens(Token start, Token end) {
		return new PositionInfo(start.getLine(), start.getCharPositionInLine(), end.getLine(),
				end.getCharPositionInLine());
	}
}
