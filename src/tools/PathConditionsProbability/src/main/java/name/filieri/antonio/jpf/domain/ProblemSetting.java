package name.filieri.antonio.jpf.domain;

import java.io.IOException;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenStream;

import name.filieri.antonio.jpf.grammar.ProblemSettingsLexer;
import name.filieri.antonio.jpf.grammar.ProblemSettingsParser;

public class ProblemSetting {
	private final Domain domain;
	private final UsageProfile usageProfile;

	public ProblemSetting(Domain domain, UsageProfile usageProfile) {
		super();
		this.domain = domain;
		this.usageProfile = usageProfile;
	}

	public static ProblemSetting loadFromFile(String path) throws IOException {
		ProblemSettingsLexer psLexer = new ProblemSettingsLexer(new ANTLRInputStream(path));
		TokenStream psTokenStream = new CommonTokenStream(psLexer);
		ProblemSettingsParser psParser = new ProblemSettingsParser(psTokenStream);
		ProblemSetting output = psParser.problemSettings().ps;
		return output;
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Domain:\n" + domain.toString());
		stringBuilder.append("UsageProfile:\n" + usageProfile.toString());
		return stringBuilder.toString();
	}

	public Domain getDomain() {
		return domain;
	}

	public UsageProfile getUsageProfile() {
		return usageProfile;
	}

}
