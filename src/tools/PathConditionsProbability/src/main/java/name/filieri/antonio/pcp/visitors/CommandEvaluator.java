package name.filieri.antonio.pcp.visitors;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.Multimap;

import name.filieri.antonio.jpf.utils.BigRational;
import name.filieri.antonio.pcp.Constraint;
import name.filieri.antonio.pcp.CountDispatcher;
import name.filieri.antonio.pcp.CountResult;
import name.filieri.antonio.pcp.ExpressionResult;
import name.filieri.antonio.pcp.Options;
import name.filieri.antonio.pcp.Partitioner;
import name.filieri.antonio.pcp.ast.PositionInfo;
import name.filieri.antonio.pcp.ast.commands.AssertCommand;
import name.filieri.antonio.pcp.ast.commands.ClearCommand;
import name.filieri.antonio.pcp.ast.commands.CommandVisitor;
import name.filieri.antonio.pcp.ast.commands.CountCommand;
import name.filieri.antonio.pcp.ast.commands.DeclareConstantCommand;
import name.filieri.antonio.pcp.ast.commands.DependentVariableDeclarationCommand;
import name.filieri.antonio.pcp.ast.commands.SetOptionCommand;
import name.filieri.antonio.pcp.ast.commands.VariableDeclarationCommand;
import name.filieri.antonio.pcp.ast.expr.ExpressionType;
import name.filieri.antonio.pcp.ast.expr.Variable;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanExpression;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanNaryExpression;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanNaryExpression.Function;
import name.filieri.antonio.pcp.visitors.CommandEvaluator.Reply;
import name.filieri.antonio.pcp.visitors.SymbolTable.TypeAndExpression;

public class CommandEvaluator implements CommandVisitor<Reply> {

	public static class Reply {
		public final boolean error;
		public final CountResult result;
		public final String message;

		public Reply(boolean error, CountResult result, String message) {
			super();
			this.error = error;
			this.result = result;
			this.message = message;
		}

		@Override
		public String toString() {
			return "Reply [error=" + error + ", result=" + result + ", message=" + message + "]";
		}
		public static final Reply OK = new Reply(false, CountResult.ZERO, "ok");
	}

	private final SymbolTable stable;
	private final Set<Constraint> constraints;
	private final Partitioner partitioner;
	private final CountDispatcher dispatcher;
	private final Options options;

	public CommandEvaluator(SymbolTable stable, Partitioner partitioner, CountDispatcher dispatcher, Options options) {
		this.constraints = new LinkedHashSet<>();
		this.stable = stable;
		this.partitioner = partitioner;
		this.dispatcher = dispatcher;
		this.options = options;
	}

	@Override
	public Reply visit(SetOptionCommand cmd) {
		String key = cmd.getName();
		ExpressionResult val = cmd.getValue();
		if (val.type == ExpressionType.STRING) {
			options.processOption(key, val.stringResult);
		} else {
			options.processOption(key, val.numericResult.toString());
		}
		return Reply.OK;
	}

	@Override
	public Reply visit(AssertCommand cmd) {
		List<BooleanExpression> clauses = cmd.getExprs();
		BooleanNaryExpression conjunction = new BooleanNaryExpression(Function.AND, clauses,
				cmd.getPositionInfo());
		Constraint constraint = new Constraint(conjunction);

		if (options.isDNFConversionEnabled()) {
			List<Constraint> dnfConstraint = constraint.toDNF();
			constraints.addAll(dnfConstraint);
		} else {
			constraints.add(constraint);
		}
		return Reply.OK;
	}

	@Override
	public Reply visit(DeclareConstantCommand cmd) {
		String name = cmd.getName();
		Reply reply;
		if (stable.nameToConst.containsKey(name)) {
			TypeAndExpression texpr = stable.nameToConst.get(name);
			if (!texpr.expr.equals(cmd.getValue()) ||
					!texpr.type.equals(cmd.getType())) {
				reply = new Reply(true, CountResult.ZERO,
						"Error: trying to redefine constant " + name + " at " + cmd.getPositionInfo());
			} else {
				reply = Reply.OK;
			}
		} else {
			stable.nameToConst.put(name, new TypeAndExpression(cmd.getType(), cmd.getValue()));
			reply = Reply.OK;
		}
		return reply;
	}

	@Override
	public Reply visit(ClearCommand clearCommand) {
		stable.clear();
		partitioner.clear();
		constraints.clear();
		dispatcher.clear(false);
		return Reply.OK;
	}

	@Override
	public Reply visit(VariableDeclarationCommand cmd) {
		Variable var = cmd.getVar();
		String name = var.getName();
		PositionInfo pos = cmd.getPositionInfo();

		Reply reply = lookup(var, name, pos);
		return reply;
	}

	private Reply lookup(Variable var, String name, PositionInfo pos) {
		Reply reply;
		if (stable.nameToVar.containsKey(name)) {
			if (!stable.nameToVar.get(name).equals(var)) {
				reply = new Reply(true, CountResult.ZERO,
						"Error: trying to redefine variable " + name + " at " + pos);
			} else {
				reply = Reply.OK;
			}
		} else {
			stable.nameToVar.put(var.getName(), var);
			reply = Reply.OK;
		}
		return reply;
	}

	@Override
	public Reply visit(DependentVariableDeclarationCommand cmd) {
		Variable var = cmd.getVar();
		String name = var.getName();
		PositionInfo pos = cmd.getPositionInfo();

		Reply reply = lookup(var, name, pos);
		System.out.println("[pcp:command-eval] dependent variable processed: " + reply);
		return reply;
	}

	@Override
	public Reply visit(CountCommand countCommand) {
		partitioner.computeVarClusters(constraints);
		Multimap<Constraint, Constraint> pmap = partitioner.partition(constraints);
		try {
			Map<Constraint, CountResult> resultMap = dispatcher.count(pmap);
			Set<CountResult> constraintResults = new LinkedHashSet<>();

			// merge partial results
			for (Constraint constraint : constraints) {
				Collection<Constraint> partitions = pmap.get(constraint);
				List<CountResult> partitionResults = new ArrayList<>();

				for (Constraint partition : partitions) {
					partitionResults.add(resultMap.get(partition));
				}
				CountResult mergedResult = partitioner.mergePartitionResults(partitionResults);
				constraintResults.add(mergedResult);
			}
			// merge results for each constraint
			CountResult finalResult = merge(constraintResults);
			return new Reply(false, finalResult, "ok");
		} catch (RuntimeException e) { /*- TODO create a subclass of RuntimeException for counter errors */
			e.printStackTrace();
			return new Reply(true, CountResult.ZERO, e.getMessage());
		}
	}

	/**
	 * Use eq. 5 (Handling Disjunction) to merge partial results.
	 */

	private CountResult merge(Set<CountResult> results) {
		BigRational prob = BigRational.ZERO;
		double var = 0;

		for (CountResult partial : results) {
			BigRational partialProb = partial.probability;
			double partialVar = partial.variance;

			prob = prob.plus(partialProb);
			var = var + partialVar;
		}
		return new CountResult(prob, var);
	}

	public SymbolTable getSymbolTable() {
		return stable;
	}

	public Set<Constraint> getConstraints() {
		return constraints;
	}

	public Partitioner getPartitioner() {
		return partitioner;
	}

	public CountDispatcher getDispatcher() {
		return dispatcher;
	}

	public Options getOptions() {
		return options;
	}

}