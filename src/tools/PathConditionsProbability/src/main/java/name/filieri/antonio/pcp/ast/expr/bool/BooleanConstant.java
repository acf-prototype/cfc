package name.filieri.antonio.pcp.ast.expr.bool;

import com.google.common.hash.HashCode;

import name.filieri.antonio.jpf.utils.ASTUtil;
import name.filieri.antonio.pcp.ast.PositionInfo;
import name.filieri.antonio.pcp.ast.expr.Constant;
import name.filieri.antonio.pcp.ast.expr.Expression;
import name.filieri.antonio.pcp.ast.expr.ExpressionVisitor;

public class BooleanConstant implements Constant, BooleanExpression {

	public static final BooleanConstant TRUE = new BooleanConstant(true, PositionInfo.DUMMY);
	public static final BooleanConstant FALSE = new BooleanConstant(false, PositionInfo.DUMMY);
	
	private final boolean value;
	private final PositionInfo position; 
	private HashCode hash = null;

	public BooleanConstant(boolean value, PositionInfo position) {
		super();
		this.position = position;
		this.value = value;
	}
	
	@Override
	public <T> T accept(ExpressionVisitor<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public HashCode getHashCode() {
		if (hash == null) {
			hash = ASTUtil.getHashFunction().newHasher()
					.putBoolean(value)
					.hash(); 
		}
		return hash;
	}

	@Override
	public int hashCode() {
		return getHashCode().asInt();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BooleanConstant other = (BooleanConstant) obj;
		if (value != other.value)
			return false;
		return true;
	}

	@Override
	public PositionInfo getPositionInfo() {
		return position;
	}
	
	public boolean getValue() {
		return value;
	}

	@Override
	public Expression negate() {
		return new BooleanConstant(!value, position);
	}

	@Override
	public String toString() {
		return value + "";
	}
}
