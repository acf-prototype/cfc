package name.filieri.antonio.pcp.ast.commands;

import java.nio.charset.StandardCharsets;

import com.google.common.collect.ImmutableList;
import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;

import name.filieri.antonio.jpf.utils.ASTUtil;
import name.filieri.antonio.pcp.ast.PositionInfo;
import name.filieri.antonio.pcp.ast.expr.Variable;

public class VariableDeclarationCommand implements Command {

	private final Variable var;
	private final PositionInfo position;
	private HashCode hash = null;

	public VariableDeclarationCommand(Variable var, PositionInfo position) {
		this.var = var;
		this.position = position;
	}

	private static final HashCode commandHash = ASTUtil.getHashFunction().newHasher()
			.putString("(declare-var)", StandardCharsets.UTF_8)
			.hash();

	@Override
	public HashCode getHashCode() {
		if (hash == null) {
			hash = Hashing.combineOrdered(
					ImmutableList.of(commandHash, var.getHashCode()));
		}
		return hash;
	}

	@Override
	public PositionInfo getPositionInfo() {
		return position;
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.DECLARE_VAR;
	}

	@Override
	public <T> T accept(CommandVisitor<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public int hashCode() {
		return getHashCode().asInt();
	}
	
	public Variable getVar() {
		return var;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VariableDeclarationCommand other = (VariableDeclarationCommand) obj;

		if (this.hashCode() != other.hashCode()) {
			return false;
		}

		if (position == null) {
			if (other.position != null)
				return false;
		} else if (!position.equals(other.position))
			return false;
		if (var == null) {
			if (other.var != null)
				return false;
		} else if (!var.equals(other.var))
			return false;
		return true;
	}
}