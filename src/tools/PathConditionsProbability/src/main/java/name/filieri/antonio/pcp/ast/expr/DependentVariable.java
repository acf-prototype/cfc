package name.filieri.antonio.pcp.ast.expr;

import java.nio.charset.StandardCharsets;

import com.google.common.hash.HashCode;

import name.filieri.antonio.jpf.utils.ASTUtil;
import name.filieri.antonio.jpf.utils.BigRational;
import name.filieri.antonio.pcp.ast.PositionInfo;

/**
 * A variable which value depends on the value of another, free variable.
 * Dependent variables do not have a domain and therefore aren't considered as
 * an input variable for model counting/solution space quantification. An
 * example of Dependent Variable is a synthetic variable that holds the result
 * of accessing a concrete array with a symbolic index.
 * 
 * @author mateus
 *
 */

public class DependentVariable implements Variable {

	private final ExpressionType type;
	private final String name;
	private final PositionInfo position;
	private HashCode hash;

	public DependentVariable(ExpressionType type, String name, PositionInfo position) {
		super();
		this.type = type;
		this.name = name;
		this.position = position;
	}

	@Override
	public <T> T accept(ExpressionVisitor<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public HashCode getHashCode() {
		if (hash == null) {
			hash = ASTUtil.getHashFunction().newHasher()
					.putInt(type.ordinal())
					.putString(name, StandardCharsets.UTF_8)
					.hash();
		}
		return hash;
	}

	@Override
	public PositionInfo getPositionInfo() {
		return position;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public ExpressionType getType() {
		return type;
	}

	@Override
	public BigRational getDomainSize() {
		throw new RuntimeException("Dependent variables do not have domains!");
	}

	@Override
	public int hashCode() {
		return getHashCode().asInt();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DependentVariable other = (DependentVariable) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (position == null) {
			if (other.position != null)
				return false;
		} else if (!position.equals(other.position))
			return false;
		if (type != other.type)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return name;
	}

}
