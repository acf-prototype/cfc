package name.filieri.antonio.pcp.counters;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import com.microsoft.z3.BitVecExpr;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Context;
import com.microsoft.z3.Expr;
import com.microsoft.z3.Model;
import com.microsoft.z3.Solver;
import com.microsoft.z3.Status;

import name.filieri.antonio.jpf.utils.BigRational;
import name.filieri.antonio.pcp.Constraint;
import name.filieri.antonio.pcp.CountResult;
import name.filieri.antonio.pcp.Options;

public class Z3BlockingCaller implements ModelCounterCaller {

	private Options options;

	public Z3BlockingCaller(Options options) {
		this.options = options;
	}

	@Override
	public Map<Constraint, CountResult> count(Collection<Constraint> problems) throws ModelCounterException {
		Map<Constraint, CountResult> results = new LinkedHashMap<>();

		for (Constraint constraint : problems) {
			Context ctx = new Context();
			Solver solver = ctx.mkSolver();
			Map<String, BitVecExpr> nameToZ3BV = ModelCounterUtils
					.declareVars(constraint.getVariables(), ctx, solver);

			ModelCounterUtils.assertConstraints(constraint, ctx, solver, nameToZ3BV);
			long nSolutions = 0;

			while (solver.check() == Status.SATISFIABLE) {
				nSolutions++;
				Model m = solver.getModel();
				BoolExpr[] blockingClause = new BoolExpr[nameToZ3BV.size()];
				int i = 0;
				for (BitVecExpr bvVar : nameToZ3BV.values()) {
					Expr val = m.getConstInterp(bvVar);
					blockingClause[i] = ctx.mkNot(ctx.mkEq(bvVar, val));
					i++;
				}
				solver.add(blockingClause);
			}
			CountResult result = new CountResult(BigRational.valueOf(nSolutions).div(constraint.getDomainSize()), 0);
			results.put(constraint, result);
		}
		return results;
	}
}
