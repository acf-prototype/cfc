package name.filieri.antonio.pcp;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import com.google.common.base.Preconditions;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;

import name.filieri.antonio.pcp.ast.expr.ExpressionCategory;
import name.filieri.antonio.pcp.ast.expr.ExpressionCategory.Kind;
import name.filieri.antonio.pcp.ast.expr.ExpressionType;
import name.filieri.antonio.pcp.ast.expr.Variable;
import name.filieri.antonio.pcp.counters.BarvinokCaller;
import name.filieri.antonio.pcp.counters.ModelCounterCaller;
import name.filieri.antonio.pcp.counters.ModelCounterException;
import name.filieri.antonio.pcp.counters.QCoralCaller;
import name.filieri.antonio.pcp.counters.SharpSatCaller;
import name.filieri.antonio.pcp.counters.Z3BlockingCaller;

public class PCPDispatcher implements CountDispatcher {

	private static final int DEFAULT_CACHE_SIZE = 10000;
	private final Options options;
	private final Cache<Constraint, CountResult> cache;
	private BarvinokCaller bvCaller;
	private QCoralCaller qcoralCaller;
	private SharpSatCaller sharpsatCaller;
	private Z3BlockingCaller z3Caller;

	public PCPDispatcher(Options options) {
		this.cache = CacheBuilder.newBuilder()
				.maximumSize(DEFAULT_CACHE_SIZE)
				.recordStats()
				.build();
		this.options = options;
	}

	@Override
	public CountResult count(Constraint constraint) {
		throw new RuntimeException("Not implemented yet");
	}

	@Override
	public Map<Constraint, CountResult> count(Multimap<Constraint, Constraint> partitionMap) {
		Map<ExpressionCategory, Multimap<Constraint, Constraint>> categoryMap = classify(partitionMap);
		Map<Constraint, CountResult> partitionResults = new LinkedHashMap<>();

		for (ExpressionCategory cat : categoryMap.keySet()) {
			Multimap<Constraint, Constraint> categoryConstraints = categoryMap.get(cat);
			Map<Constraint, CountResult> categoryResults;

			Kind kind = cat.category;
			if (kind == Kind.LINEAR) {
				loop:
				for (Constraint c : categoryConstraints.values()) {
					for (Variable v : c.getVariables()) {
						if (v.getType() != ExpressionType.INT) {
							kind = Kind.NONLINEAR;
							System.out.println("[pcpDispatcher] found non-integer variable in linear constraint: moving to non-linear...");
							break loop;
						}
					}
				}
			}
			
			switch (kind) {
			case DATASTRUCTURE:
				throw new RuntimeException("Not supported");
			case LINEAR:
				try {
					categoryResults = handleLinear(categoryConstraints);
				} catch (ModelCounterException e) {
					System.err.println("Error while handling linear expression: " + e.getMessage());
					// return empty map
					categoryResults = new HashMap<>();
				}
				break;
			case NONLINEAR:
				try {
					categoryResults = handleNonLinear(categoryConstraints);
				} catch (ModelCounterException e) {
					System.err.println("Error while handling linear expression: " + e.getMessage());
					// return empty map
					categoryResults = new HashMap<>();
				}
				break;
			case STRING:
				categoryResults = handlePureString(categoryConstraints);
				break;
			case STRING_NUMERIC:
				categoryResults = handleMixedStringNumeric(categoryConstraints);
				break;
			case BITVECTOR:
				try {
					categoryResults = handleBitVector(categoryConstraints);
				} catch (ModelCounterException e) {
					System.err.println("Error while handling bv expression: " + e.getMessage());
					// return empty map
					categoryResults = new HashMap<>();
				}
				break;
			default:
				throw new RuntimeException("Not implemented yet: " + cat.category);
			}
			partitionResults.putAll(categoryResults);
		}
		return partitionResults;
	}

	private Map<Constraint, CountResult> handleBitVector(
			Multimap<Constraint, Constraint> consToPartitions) throws ModelCounterException {
		Map<Constraint, CountResult> counts = new HashMap<>();
		Set<Constraint> problems = ImmutableSet.copyOf(consToPartitions.values());
		Set<Constraint> uncountedProblems = new LinkedHashSet<>();

		for (Constraint problem : problems) {
			CountResult problemCount = cache.getIfPresent(problem);
			if (problemCount != null) {
				System.out.println("[pcpdispatcher] cache hit!");
				counts.put(problem, problemCount);
			} else {
				uncountedProblems.add(problem);
			}
		}

		ModelCounterCaller caller;

		if (options.useSharpSAT()) {
			if (sharpsatCaller == null) {
				sharpsatCaller = new SharpSatCaller(options);
			}
			caller = sharpsatCaller ;
		} else if (options.useZ3()) {
			if (z3Caller == null) {
				z3Caller = new Z3BlockingCaller(options);
			}
			caller = z3Caller;
		} else {
			throw new RuntimeException("No model counter for non-linear constraints was chosen!");
		}

		if (!uncountedProblems.isEmpty()) {
			Map<Constraint, CountResult> missingCounts = caller.count(uncountedProblems);
			cache.putAll(missingCounts);
			counts.putAll(missingCounts);
		}

		return counts;
	}

	private Map<Constraint, CountResult> handleMixedStringNumeric(
			Multimap<Constraint, Constraint> categoryConstraints) {
		throw new RuntimeException("Not implemented");
	}

	private Map<Constraint, CountResult> handlePureString(Multimap<Constraint, Constraint> consToPartitions) {
		throw new RuntimeException("Not implemented");
	}

	private Map<Constraint, CountResult> handleNonLinear(Multimap<Constraint, Constraint> consToPartitions)
			throws ModelCounterException {
		Map<Constraint, CountResult> counts = new HashMap<>();
		Set<Constraint> problems = ImmutableSet.copyOf(consToPartitions.values());
		Set<Constraint> uncountedProblems = new LinkedHashSet<>();

		for (Constraint problem : problems) {
			CountResult problemCount = cache.getIfPresent(problem);
			if (problemCount != null) {
				System.out.println("[pcpdispatcher] cache hit!");
				counts.put(problem, problemCount);
			} else {
				uncountedProblems.add(problem);
			}
		}

		ModelCounterCaller caller;
		
		if (options.useQCoral()) {
			if (qcoralCaller == null) { 
				qcoralCaller = new QCoralCaller(options);
			}
			caller = qcoralCaller;
		} else if (options.useSharpSAT()) {
			if (sharpsatCaller == null) { 
				sharpsatCaller = new SharpSatCaller(options);
			}
			caller = sharpsatCaller ;
		} else if (options.useZ3()) {
			if (z3Caller == null) { 
				z3Caller = new Z3BlockingCaller(options);
			}
			caller = z3Caller;
		} else {
			throw new RuntimeException("No model counter for non-linear constraints was chosen!");
		}

		if (!uncountedProblems.isEmpty()) {
			Map<Constraint, CountResult> missingCounts = caller.count(uncountedProblems);
			cache.putAll(missingCounts);
			counts.putAll(missingCounts);
		}

		return counts;
	}

	private Map<Constraint, CountResult> handleLinear(Multimap<Constraint, Constraint> consToPartitions)
			throws ModelCounterException {
		Map<Constraint, CountResult> counts = new HashMap<>();
		Set<Constraint> problems = ImmutableSet.copyOf(consToPartitions.values());
		Set<Constraint> uncountedProblems = new LinkedHashSet<>();

		for (Constraint problem : problems) {
			CountResult problemCount = cache.getIfPresent(problem);
			if (problemCount != null) {
				System.out.println("[pcpdispatcher] cache hit!");
				counts.put(problem, problemCount);
			} else {
				uncountedProblems.add(problem);
			}
		}

		if (bvCaller == null) { // initialize barvinok lazily
			bvCaller = new BarvinokCaller(options);
		}

		if (!uncountedProblems.isEmpty()) {
			Map<Constraint, CountResult> missingCounts = bvCaller.count(uncountedProblems);
			cache.putAll(missingCounts);
			counts.putAll(missingCounts);
		}

		return counts;
	}

	// group entries by constraint type
	private Map<ExpressionCategory, Multimap<Constraint, Constraint>> classify(
			Multimap<Constraint, Constraint> partitionMap) {
		Map<ExpressionCategory, Multimap<Constraint, Constraint>> categoryMap = new HashMap<>();

		for (java.util.Map.Entry<Constraint, Constraint> entry : partitionMap.entries()) {
			Constraint originalConstraint = entry.getKey();
			Constraint partition = entry.getValue();

			Set<ExpressionCategory> pcats = partition.getCategory();
			Preconditions.checkState(pcats.size() > 0);

			// FIXME temporary restriction until I figure out how to handle
			// multiple kinds of constraints at once
			if (pcats.size() > 1) {
				throw new RuntimeException(
						"Found constraint with multiple types. Still need to figure how to handle this :P");
			}
			for (ExpressionCategory pcat : pcats) {
				if (pcat.isConstant) {
					System.out.println("[pcp:dispatcher] Found constant expression, ignoring it...");
				} else {
					Multimap<Constraint, Constraint> pcatMap = categoryMap.get(pcat);
					if (pcatMap == null) {
						pcatMap = LinkedHashMultimap.create();
						categoryMap.put(pcat, pcatMap);
					}

					pcatMap.put(originalConstraint, partition);
				}
			}
		}
		return categoryMap;
	}

	@Override
	public void clear(boolean clearCache) {
		if (clearCache) {
			cache.invalidateAll();
		}
	}
}