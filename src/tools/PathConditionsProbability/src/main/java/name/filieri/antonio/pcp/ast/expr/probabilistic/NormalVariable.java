package name.filieri.antonio.pcp.ast.expr.probabilistic;

import java.nio.charset.StandardCharsets;

import com.google.common.base.Preconditions;
import com.google.common.hash.HashCode;

import name.filieri.antonio.jpf.utils.ASTUtil;
import name.filieri.antonio.jpf.utils.BigRational;
import name.filieri.antonio.pcp.ast.PositionInfo;
import name.filieri.antonio.pcp.ast.expr.ExpressionType;
import name.filieri.antonio.pcp.ast.expr.ExpressionVisitor;

public class NormalVariable implements ProbabilisticNumericVariable {

	private final String name;
	private final double mean;
	private final double sd;
	private final double upperBound;
	private final double lowerBound;
	private final PositionInfo position;
	private HashCode hash = null;

	public NormalVariable(String name, double lowerBound, double upperBound, double mean, double sd,
			PositionInfo position) {
		super();
		Preconditions.checkNotNull(name);
		Preconditions.checkArgument(sd > 0);
		Preconditions.checkArgument(upperBound >= lowerBound);
		this.name = name;
		this.mean = mean;
		this.sd = sd;
		this.upperBound = upperBound;
		this.lowerBound = lowerBound;
		this.position = position;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public <T> T accept(ExpressionVisitor<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public HashCode getHashCode() {
		if (hash == null) {
			hash = ASTUtil.getHashFunction().newHasher()
					.putString(name, StandardCharsets.UTF_8)
					.putDouble(mean)
					.putDouble(sd)
					.putDouble(upperBound)
					.putDouble(lowerBound)
					.hash();
		}
		return hash;
	}

	@Override
	public Number getUpperBound() {
		return upperBound;
	}

	@Override
	public Number getLowerBound() {
		return lowerBound;
	}

	@Override
	public int hashCode() {
		return getHashCode().asInt();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NormalVariable other = (NormalVariable) obj;

		if (this.hashCode() != other.hashCode()) {
			return false;
		}

		if (Double.doubleToLongBits(lowerBound) != Double.doubleToLongBits(other.lowerBound))
			return false;
		if (Double.doubleToLongBits(mean) != Double.doubleToLongBits(other.mean))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (Double.doubleToLongBits(sd) != Double.doubleToLongBits(other.sd))
			return false;
		if (Double.doubleToLongBits(upperBound) != Double.doubleToLongBits(other.upperBound))
			return false;
		return true;
	}

	public double getMean() {
		return mean;
	}

	public double getStandardDeviation() {
		return sd;
	}

	@Override
	public PositionInfo getPositionInfo() {
		return position;
	}

	@Override
	public ExpressionType getType() {
		return ExpressionType.DOUBLE;
	}

	@Override
	public BigRational getDomainSize() {
		return BigRational.valueOf(upperBound - lowerBound);
	}

}
