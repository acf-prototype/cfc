package name.filieri.antonio.jpf.utils.random;

public class TimeBasedRandomIdGenerator implements RandomIdGenerator {

	public String getRandomId() {
		return Long.valueOf(System.currentTimeMillis() + System.nanoTime()).toString();
	}

}
