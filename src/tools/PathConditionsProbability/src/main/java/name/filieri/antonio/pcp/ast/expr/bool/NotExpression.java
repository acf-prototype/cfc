package name.filieri.antonio.pcp.ast.expr.bool;

import com.google.common.collect.ImmutableList;
import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;

import name.filieri.antonio.jpf.utils.ASTUtil;
import name.filieri.antonio.pcp.ast.PositionInfo;
import name.filieri.antonio.pcp.ast.expr.Expression;
import name.filieri.antonio.pcp.ast.expr.ExpressionType;
import name.filieri.antonio.pcp.ast.expr.ExpressionVisitor;
import name.filieri.antonio.pcp.ast.expr.UnaryExpression;


public class NotExpression implements UnaryExpression, BooleanExpression {

	private final BooleanExpression arg;
	private final PositionInfo position;
	private HashCode hash = null;

	public NotExpression(BooleanExpression arg, PositionInfo position) {
		super();
		this.arg = arg;
		this.position = position;
	}

	public NotExpression(BooleanExpression arg) {
		this(arg, PositionInfo.DUMMY);
	}

	@Override
	public <T> T accept(ExpressionVisitor<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public HashCode getHashCode() {
		if (hash == null) {
			HashCode hc = ASTUtil.getHashFunction()
					.newHasher()
					.putInt(ExpressionType.BOOLEAN.ordinal())
					.hash();
			hash = Hashing.combineOrdered(ImmutableList.of(hc,arg.getHashCode()));
		}
		return hash;
	}

	@Override
	public Expression getArg() {
		return arg;
	}

	@Override
	public int hashCode() {
		return getHashCode().asInt();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NotExpression other = (NotExpression) obj;
		
		if (this.hashCode() != other.hashCode()) {
			return false;
		}
		
		if (arg == null) {
			if (other.arg != null)
				return false;
		} else if (!arg.equals(other.arg))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "NOT(" + arg.toString() + ")";
	}

	@Override
	public PositionInfo getPositionInfo() {
		return position;
	}

	@Override
	public Expression negate() {
		return arg;
	}
}
