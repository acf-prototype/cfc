package name.filieri.antonio.pcp.ast.commands;

import java.nio.charset.StandardCharsets;

import com.google.common.hash.HashCode;

import name.filieri.antonio.jpf.utils.ASTUtil;
import name.filieri.antonio.pcp.ast.PositionInfo;

public class ClearCommand implements Command {

	public final PositionInfo position;
	public static final HashCode hash = ASTUtil.getHashFunction().newHasher()
			.putString("(clear)", StandardCharsets.UTF_8).hash();

	public ClearCommand(PositionInfo position) {
		this.position = position;
	}

	@Override
	public HashCode getHashCode() {
		return hash;
	}

	@Override
	public PositionInfo getPositionInfo() {
		return position;
	}

	@Override
	public boolean equals(Object other) {
		return other instanceof ClearCommand;
	}

	@Override
	public int hashCode() {
		return getHashCode().asInt();
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.CLEAR;
	}

	@Override
	public <T> T accept(CommandVisitor<T> visitor) {
		return visitor.visit(this);
	}

}
