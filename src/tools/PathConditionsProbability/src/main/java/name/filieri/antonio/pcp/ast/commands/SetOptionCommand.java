package name.filieri.antonio.pcp.ast.commands;

import java.nio.charset.StandardCharsets;

import com.google.common.collect.ImmutableList;
import com.google.common.hash.Funnel;
import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;
import com.google.common.hash.PrimitiveSink;

import name.filieri.antonio.jpf.utils.ASTUtil;
import name.filieri.antonio.pcp.ExpressionResult;
import name.filieri.antonio.pcp.ast.PositionInfo;
import name.filieri.antonio.pcp.ast.expr.Expression;

public class SetOptionCommand implements Command {

	private final String name;
	private final ExpressionResult value;
	private final PositionInfo position;
	private HashCode hash = null; 
	
	public SetOptionCommand(String name, ExpressionResult value, PositionInfo position) {
		super();
		this.name = name;
		this.value = value;
		this.position = position;
	}	
	
	@Override
	public HashCode getHashCode() {
		if (hash == null) {
			hash = ASTUtil.getHashFunction().newHasher()
					.putString(name,StandardCharsets.UTF_8)
					.putObject(value, new Funnel<ExpressionResult>() {
						@Override
						public void funnel(ExpressionResult from, PrimitiveSink into) {
							into.putInt(from.type.ordinal());
							if (from.numericResult != null) {
								into.putDouble(from.numericResult.doubleValue());
							} else {
								into.putString(from.stringResult, StandardCharsets.UTF_8);
							}
						}
					})
					.hash();
		}
		return hash;
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.ASSERT;
	}

	@Override
	public int hashCode() {
		return getHashCode().asInt();
	}
	
	public String getName() {
		return name;
	}
	
	public ExpressionResult getValue() {
		return value;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SetOptionCommand other = (SetOptionCommand) obj;
		
		if (this.hashCode() != other.hashCode()) {
			return false;
		}
		
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	@Override
	public PositionInfo getPositionInfo() {
		return position;
	}

	@Override
	public <T> T accept(CommandVisitor<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public String toString() {
		return "set-option("+name + " : " + value + ")";
	}
}
