package name.filieri.antonio.pcp.ast;

import com.google.common.hash.HashCode;

import name.filieri.antonio.pcp.ast.PositionInfo;

public interface Node {

	public HashCode getHashCode();
	
	public PositionInfo getPositionInfo();

}
