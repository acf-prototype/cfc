package name.filieri.antonio.pcp.counters;

public class ModelCounterException extends Exception {

	public ModelCounterException(Exception e) {
		super(e);
	}

	private static final long serialVersionUID = -7294299869835672108L;

}
