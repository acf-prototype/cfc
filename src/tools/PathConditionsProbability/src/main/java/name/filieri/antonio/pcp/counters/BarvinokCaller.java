package name.filieri.antonio.pcp.counters;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.base.Preconditions;
import com.zaxxer.nuprocess.NuAbstractProcessHandler;
import com.zaxxer.nuprocess.NuProcess;
import com.zaxxer.nuprocess.NuProcessBuilder;

import name.filieri.antonio.jpf.barvinok.BarvinokException;
import name.filieri.antonio.jpf.utils.BigRational;
import name.filieri.antonio.pcp.Constraint;
import name.filieri.antonio.pcp.CountResult;
import name.filieri.antonio.pcp.Options;

public class BarvinokCaller implements ModelCounterCaller {

	private static final long WAITING_TIME = 5;
	private final Options options;
	private BarvinokProcessHandler barvinokHandler;

	public BarvinokCaller(Options options) {
		this.options = options;

		/*
		 * we need to disable buffering on input / line buffer on output for
		 * iscc. This is achieved by wrapping the call with stdbuf, a tool from
		 * coreutils. I have no idea what would be the equivalent solution for
		 * Windows.
		 */
		String OS = System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH);
		String stdbufCommand;
		if ((OS.indexOf("mac") >= 0) || (OS.indexOf("darwin") >= 0)) {
			stdbufCommand = "gstdbuf";
		} else if (OS.indexOf("nux") >= 0) {
			stdbufCommand = "stdbuf";
		} else {
			throw new RuntimeException("Only unix systems are currently supported (requires stdbuf or gstdbuf on Mac");
		}

		String[] command = new String[] { stdbufCommand, "-oL", /* "-i0", */ options.getIsccPath() };
		NuProcessBuilder builder = new NuProcessBuilder(command);
		
		// new String[] { "strace", "-o", "/home/mateus/trace", "stdbuf", "-oL",
		// options.getIsccPath() });
		/*- new String[] { "strace", "-o", "/home/mateus/trace", "stdbuf", "-oL", "-i0", options.getIsccPath() }); */
		this.barvinokHandler = new BarvinokProcessHandler();
		builder.setProcessListener(barvinokHandler);
		builder.start();
	}

	@Override
	public Map<Constraint, CountResult> count(Collection<Constraint> problems) throws ModelCounterException {
		List<QueryAndDomainSize> queries = new ArrayList<>();
		String iscc = options.getIsccPath();

		for (Constraint problem : problems) {
			String barvinokExpression = BarvinokVisitor.toBarvinok(problem);
			BigRational domainSize = problem.getDomainSize();
			queries.add(new QueryAndDomainSize(domainSize, barvinokExpression));
		}

		try {
			List<CountResult> counts = invoke(queries, iscc);
			Preconditions.checkState(counts.size() == problems.size());

			int i = 0;
			Map<Constraint, CountResult> resultMap = new HashMap<>();
			for (Constraint problem : problems) {
				resultMap.put(problem, counts.get(i++));
			}

			return resultMap;
		} catch (IOException e) {
			throw new ModelCounterException(e);
		} catch (BarvinokException e) {
			throw new ModelCounterException(e);
		}
	}

	private List<CountResult> invoke(List<QueryAndDomainSize> queries, String iscc)
			throws IOException, BarvinokException {
		List<CountResult> results = new ArrayList<>();

		System.out.println("Barvinok input:");
		for (QueryAndDomainSize queryAndSize : queries) { /*- TODO send everything at once */
			String query = queryAndSize.query;
			barvinokHandler.write(query);
			System.out.println(query);

			while (!barvinokHandler.isReplyReady()) {
				try {
					Thread.sleep(WAITING_TIME);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			BarvinokReply reply = barvinokHandler.getReply();
			String out = reply.out;
			String err = reply.err;

			if (err != null && !err.isEmpty()) {
				throw new BarvinokException("Error from barvinok: " + err);
			}

			Pattern intsOnly = Pattern.compile("\\d+");
			Matcher makeMatch = intsOnly.matcher(out);
			CountResult result;
			if (makeMatch.find()) {
				String inputInt = makeMatch.group();
				BigRational nsolutions = BigRational.valueOf(inputInt);
				BigRational probability = nsolutions.div(queryAndSize.domainSize);
				result = new CountResult(probability, 0);
			} else {
				if (out.contains("infty")) {
					throw new BarvinokException("The constraints define an unbounded set");
				} else {
					result = new CountResult(BigRational.ZERO, 0);
				}
			}
			results.add(result);
		}
		System.out.println("[barvinokCaller] CountResults: " + results);
		return results;
	}

	private static class QueryAndDomainSize {
		final BigRational domainSize;
		final String query;

		QueryAndDomainSize(BigRational domainSize, String query) {
			super();
			this.domainSize = domainSize;
			this.query = query;
		}
	}

	private static class BarvinokReply {
		final String out;
		final String err;

		public BarvinokReply(String out, String err) {
			this.out = out;
			this.err = err;
		}
	}

	private static class BarvinokProcessHandler extends NuAbstractProcessHandler {
		private volatile String input = null;
		private volatile String reply = null;
		private volatile String error = null;
		private NuProcess process = null;

		@Override
		public void onStart(NuProcess nuProcess) {
			process = nuProcess;
		};

		@Override
		public boolean onStdinReady(ByteBuffer buffer) {
			System.out.println("[barvinokCaller] stdinready");
			Preconditions.checkState(input != null, "No input available to be sent to the model counter.");
			buffer.put(input.getBytes());
			buffer.flip();
			input = null;
			return false;
		};

		@Override
		public void onStdout(ByteBuffer buffer, boolean closed) {
			System.out.println("[barvinokCaller] stdout");
			if (closed) {
				System.err.println("EOF received from barvinok!");
			} else {
				Preconditions.checkState(reply == null,
						"There is a unprocessed reply from the model counter: " + reply);
				byte[] bytes = new byte[buffer.remaining()];
				buffer.get(bytes);
				reply = new String(bytes);
				System.out.println("[barvinokCaller] reply=" + reply);
			}
		}

		@Override
		public void onStderr(ByteBuffer buffer, boolean closed) {
			System.out.println("[barvinokCaller] stderr");
			if (closed) {
				System.err.println("EOF received from barvinok!");
			} else {
				Preconditions.checkState(error == null,
						"There is a unprocessed error message from the model counter: " + error);
				byte[] bytes = new byte[buffer.remaining()];
				buffer.get(bytes);
				error = new String(bytes);
				System.out.println("[barvinokCaller] error=" + error);
			}
		}

		public void write(String s) {
			System.out.println("[barvinokCaller] write");
			Preconditions.checkState(input == null,
					"There is an unprocessed input to be sent to the model counter: " + input);
			input = s;
			process.wantWrite();
		}

		public boolean isReplyReady() {
			return reply != null || error != null;
		}

		public BarvinokReply getReply() {
			System.out.println("[barvinokCaller] getReply");
			Preconditions.checkState(reply != null || error != null,
					"No reply received so far from the model counter.");
			BarvinokReply breply = new BarvinokReply(reply, error);
			reply = null;
			error = null;
			return breply;
		}
	}
}