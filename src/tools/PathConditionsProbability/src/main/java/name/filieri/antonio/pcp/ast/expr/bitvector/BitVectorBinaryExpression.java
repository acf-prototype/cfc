package name.filieri.antonio.pcp.ast.expr.bitvector;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import com.google.common.hash.HashCode;
import com.google.common.hash.Hasher;
import com.google.common.hash.Hashing;
import java.nio.charset.StandardCharsets;
import name.filieri.antonio.jpf.utils.ASTUtil;
import name.filieri.antonio.pcp.ast.PositionInfo;
import name.filieri.antonio.pcp.ast.expr.BinaryExpression;
import name.filieri.antonio.pcp.ast.expr.Expression;
import name.filieri.antonio.pcp.ast.expr.ExpressionVisitor;

/**
 * Created by mateus on 21/03/17.
 */
public class BitVectorBinaryExpression implements BinaryExpression {

  private final Function function;
  private final Expression left;
  private final Expression right;
  private final PositionInfo posInfo;
  private HashCode hashCode;
  public BitVectorBinaryExpression(Expression left, Expression right,
      Function function, PositionInfo posInfo) {
    this.left = left;
    this.right = right;
    this.function = function;
    this.posInfo = posInfo;
  }

  public Function getFunction() {
    return function;
  }

  @Override
  public Expression getLeft() {
    return left;
  }

  @Override
  public Expression getRight() {
    return right;
  }

  @Override
  public <T> T accept(ExpressionVisitor<T> visitor) {
    return visitor.visit(this);
  }

  @Override
  public HashCode getHashCode() {
    if (hashCode == null) {
    Hasher hasher = ASTUtil.getHashFunction().newHasher();
    hasher.putString(function.name(), StandardCharsets.UTF_8);
    hashCode = Hashing.combineOrdered(ImmutableList.of(
        left.getHashCode(),right.getHashCode(),hasher.hash()));
    }
    return hashCode;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    BitVectorBinaryExpression that = (BitVectorBinaryExpression) o;

    if (function != that.function) {
      return false;
    }
    if (!left.equals(that.left)) {
      return false;
    }
    return right.equals(that.right);
  }

  @Override
  public int hashCode() {
    return hashCode.asInt();
  }

  @Override
  public PositionInfo getPositionInfo() {
    return posInfo;
  }

  public enum Function {
    CONCAT, BVAND, BVOR, BVNAND, BVNOR, BVXOR, BVXNOR,
    BVADD, BVSUB, BVMUL, BVUDIV, BVUREM, BVSDIV, BVSREM, BVSMOD,
    BVSHL, BVLSHR, BVASHR;


    public static Optional<Function> fromSymbol(String symbol) {
      try {
        Function fun = Function.valueOf(symbol);
        return Optional.of(fun);
      } catch (IllegalArgumentException e) {
        return Optional.absent();
      }
    }

  }
}
