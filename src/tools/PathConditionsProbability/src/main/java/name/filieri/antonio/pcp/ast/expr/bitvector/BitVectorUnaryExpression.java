package name.filieri.antonio.pcp.ast.expr.bitvector;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import com.google.common.hash.HashCode;
import com.google.common.hash.Hasher;
import com.google.common.hash.Hashing;
import java.nio.charset.StandardCharsets;
import name.filieri.antonio.jpf.utils.ASTUtil;
import name.filieri.antonio.pcp.ast.PositionInfo;
import name.filieri.antonio.pcp.ast.expr.Expression;
import name.filieri.antonio.pcp.ast.expr.ExpressionVisitor;
import name.filieri.antonio.pcp.ast.expr.UnaryExpression;

/**
 * Created by mateus on 21/03/17.
 */
public class BitVectorUnaryExpression implements UnaryExpression {

  private final Expression arg;
  private final PositionInfo posInfo;
  private final Function function;
  private HashCode hashCode;

  public BitVectorUnaryExpression(Expression arg, Function function,
      PositionInfo position) {
    this.arg = arg;
    this.posInfo = position;
    this.function = function;
  }

  @Override
  public Expression getArg() {
    return arg;
  }

  @Override
  public <T> T accept(ExpressionVisitor<T> visitor) {
    return visitor.visit(this);
  }

  @Override
  public HashCode getHashCode() {
    if (hashCode == null) {
      Hasher hasher = ASTUtil.getHashFunction()
          .newHasher().putString(function.name(), StandardCharsets.UTF_8);
      hashCode = Hashing.combineOrdered(ImmutableList.of(hasher.hash(), arg.getHashCode()));
    }
    return hashCode;
  }

  @Override
  public PositionInfo getPositionInfo() {
    return posInfo;
  }

  public Function getFunction() {
    return function;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    BitVectorUnaryExpression that = (BitVectorUnaryExpression) o;

    if (!arg.equals(that.arg)) {
      return false;
    }
    return function == that.function;
  }

  @Override
  public int hashCode() {
    return getHashCode().asInt();
  }

  public enum Function {
    BVNEG, BVNOT;

    public static Optional<Function> fromSymbol(String symbol) {
      try {
        Function fun = Function.valueOf(symbol);
        return Optional.of(fun);
      } catch (IllegalArgumentException e) {
        return Optional.absent();
      }
    }
  }
}
