package name.filieri.antonio.pcp;

import java.util.Map;

import com.google.common.collect.Multimap;

public interface CountDispatcher {

	public CountResult count(Constraint constraint);

	public Map<Constraint, CountResult> count(Multimap<Constraint, Constraint> partitionMap);

	public void clear(boolean clearCache);

}
