package name.filieri.antonio.pcp.counters;

import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;
import com.microsoft.z3.BitVecExpr;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Context;
import com.microsoft.z3.Expr;
import com.microsoft.z3.Solver;

import name.filieri.antonio.pcp.Constraint;
import name.filieri.antonio.pcp.ast.expr.BoundedVariable;
import name.filieri.antonio.pcp.ast.expr.ExpressionType;
import name.filieri.antonio.pcp.ast.expr.Variable;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorVariable;

public class ModelCounterUtils {

	public static HashFunction getHashFunction() {
		return Hashing.goodFastHash(32);
	}

	public static Map<String, BitVecExpr> declareVars(Set<Variable> vars, Context ctx, Solver solver) {
		Map<String, BitVecExpr> nameToZ3BV = new HashMap<>();
		for (Variable var : vars) {
			BoundedVariable nvar = (BoundedVariable) var;
			int bvlength;
			String lower;
			String higher;
			switch (nvar.getType()) {
			case BOOLEAN:
				bvlength = 1;
				lower = "0";
				higher = "1";
				break;
			case DOUBLE:
			case FLOAT:
				throw new RuntimeException("Bitblasting of floating point vars is not supported at the moment! " + nvar);
			case INT:
				bvlength = 32;
				lower = nvar.getLowerBound().intValue() + "";
				higher = nvar.getUpperBound().intValue() + "";
				break;
			case LONG:
				bvlength = 64;
				lower = nvar.getLowerBound().longValue() + "";
				higher = nvar.getUpperBound().longValue() + "";
				break;
			case BV:
				bvlength = ((BitVectorVariable) var).getLength();
				lower = nvar.getLowerBound() + "";
				higher = nvar.getUpperBound() + "";
				break;
			case DATA:
			case STRING:
			default:
				throw new RuntimeException("Unsupported var type for bitblasted model counters! " + nvar);
			}

			BitVecExpr bv = ctx.mkBVConst("v" + var.getName(), bvlength);
			nameToZ3BV.put(var.getName(), bv);
			if (nvar.getType() == ExpressionType.BV) {
				solver.add(ctx.mkBVUGE(bv, ctx.mkBV(lower, bvlength)));
				solver.add(ctx.mkBVULE(bv, ctx.mkBV(higher, bvlength)));
			} else {
				solver.add(ctx.mkBVSGE(bv, ctx.mkBV(lower, bvlength)));
				solver.add(ctx.mkBVSLE(bv, ctx.mkBV(higher, bvlength)));
			}
		}
		return nameToZ3BV;
	}

	public static List<BoolExpr> toZ3Vars(Set<Variable> vars, Context ctx, Map<String, BitVecExpr> nameToZ3BV) {
		List<BoolExpr> domainExprs = new ArrayList<>();
		for (Variable var : vars) {
			BoundedVariable nvar = (BoundedVariable) var;
			int bvlength;
			String lower;
			String higher;
			switch (nvar.getType()) {
			case BOOLEAN:
				bvlength = 1;
				lower = 0 + "";
				higher = 1 + "";
				break;
			case DOUBLE:
			case FLOAT:
				throw new RuntimeException("Bitblasting of floating point vars is not supported at the moment! " + nvar);
			case INT:
				bvlength = 32;
				lower = nvar.getLowerBound().intValue() + "";
				higher = nvar.getUpperBound().intValue() + "";
				break;
			case LONG:
				bvlength = 64;
				lower = nvar.getLowerBound().longValue() + "";
				higher = nvar.getUpperBound().longValue() + "";
				break;
			case BV:
				bvlength = ((BitVectorVariable) var).getLength();
				lower = nvar.getLowerBound().longValue() + "";
				higher = nvar.getUpperBound().longValue() + "";
				break;
			case DATA:
			case STRING:
			default:
				throw new RuntimeException("Unsupported var type for bitblasted model counters! " + nvar);
			}

			BitVecExpr bv = ctx.mkBVConst("v" + var.getName(), bvlength);
			BoolExpr lo, hi;
			if (nvar.getType() == ExpressionType.BV) {
				lo = ctx.mkBVUGE(bv, ctx.mkBV(lower, bvlength));
				hi = ctx.mkBVULE(bv, ctx.mkBV(higher, bvlength));
			} else {
				lo = ctx.mkBVSGE(bv, ctx.mkBV(lower, bvlength));
				hi = ctx.mkBVSLE(bv, ctx.mkBV(higher, bvlength));
			}

			nameToZ3BV.put(var.getName(), bv);
			domainExprs.add(lo);
			domainExprs.add(hi);
		}
		return domainExprs;
	}

	public static void assertConstraints(Constraint constraint, Context ctx, Solver solver,
			Map<String, BitVecExpr> nameToZ3BV) {
		Z3BitVecVisitor visitor = new Z3BitVecVisitor(ctx, nameToZ3BV);
		constraint.getExpr().accept(visitor);

		Deque<Expr> deque = visitor.getEvalStack();
		if (deque.size() != 1) {
			throw new RuntimeException("Error: there should be only one expression in the stack!");
		}
		solver.add((BoolExpr) deque.pop());
	}

	public static List<BoolExpr> collectZ3Constraints(Constraint constraint, Context ctx, Solver solver,
			Map<String, BitVecExpr> idToZ3BV) {
		List<BoolExpr> z3Exprs = new ArrayList<>();
		Z3BitVecVisitor visitor = new Z3BitVecVisitor(ctx, idToZ3BV);
		constraint.getExpr().accept(visitor);

		Deque<Expr> deque = visitor.getEvalStack();
		if (deque.size() != 1) {
			throw new RuntimeException("Error: there should be only one expression in the stack!");
		}
		z3Exprs.add((BoolExpr) deque.pop());
		return z3Exprs;
	}
}
