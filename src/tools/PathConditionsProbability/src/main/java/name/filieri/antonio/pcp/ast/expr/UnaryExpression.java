package name.filieri.antonio.pcp.ast.expr;

public interface UnaryExpression extends Expression {

	public Expression getArg();
	
}
