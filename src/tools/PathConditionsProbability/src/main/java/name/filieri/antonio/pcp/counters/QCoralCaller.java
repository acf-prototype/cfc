package name.filieri.antonio.pcp.counters;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.random.RandomGenerator;
import org.apache.commons.math3.random.Well44497b;

import com.google.common.collect.ImmutableList;

import coral.counters.CountingProblem;
import coral.counters.refactoring.Runner;
import coral.counters.refactoring.estimate.Estimate;
import coral.counters.refactoring.estimate.PCEstimate;
import coral.util.Config;
import coral.util.Interval;
import name.filieri.antonio.jpf.utils.BigRational;
import name.filieri.antonio.pcp.Constraint;
import name.filieri.antonio.pcp.CountResult;
import name.filieri.antonio.pcp.Options;
import name.filieri.antonio.pcp.counters.QCoralVisitor.ConstraintPlusDomain;

public class QCoralCaller implements ModelCounterCaller {

	private RandomGenerator rng;
	private Options options;
	
	public QCoralCaller(Options options) {
		this.options = options;
		
		Config.mcIterativeImprovement = true;
		Config.mcProportionalBoxSampleAllocation = true;
		Config.mcSamplesPerIncrement = 2000;
		Config.mcInitialPartitionBudget = 50000;
		Config.mcTargetVariance=Math.pow(10, -12);
		Config.mcSeed = this.options.getSeed();
		Config.realPaverLocation = this.options.getRealPaverPath();
    this.rng = new Well44497b(Config.mcSeed);
	}
	
	@Override
	public Map<Constraint, CountResult> count(Collection<Constraint> problems) throws ModelCounterException {
		List<Constraint> pcpProblems = ImmutableList.copyOf(problems);
		List<CountingProblem> qcoralProblems = new ArrayList<>();
		
		QCoralVisitor visitor = new QCoralVisitor(rng);
		for (Constraint problem : problems) {
			ConstraintPlusDomain cpd = visitor.parseConstraint(problem);
      List<Interval[]> tmp = new ArrayList<Interval[]>(1);
      tmp.add(cpd.domain);
			CountingProblem cp = new CountingProblem(cpd.constraint, tmp, visitor.lit2rvar, cpd.domain);
			qcoralProblems.add(cp);
		}
		
		Runner.runIterativeAnalysis(rng,qcoralProblems,0);

		//TODO implement a proper interface to get each PC's estimate
		Map<Constraint, CountResult> results = new LinkedHashMap<>();
		Map<CountingProblem, PCEstimate> qcoralEstimates = Runner.lastSetOfEstimates;
		
		for (int i = 0; i < pcpProblems.size(); i++) {
			Estimate estimate = qcoralEstimates.get(qcoralProblems.get(i)).pcEstimate;
			CountResult cr = new CountResult(BigRational.valueOf(estimate.estimate), estimate.variance);
			results.put(pcpProblems.get(i), cr);
		}
		return results;
	}
}
