package name.filieri.antonio.pcp.counters;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Map;

import com.microsoft.z3.BitVecExpr;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Context;
import com.microsoft.z3.Expr;
import com.microsoft.z3.IntExpr;

import name.filieri.antonio.pcp.ast.expr.DependentVariable;
import name.filieri.antonio.pcp.ast.expr.Expression;
import name.filieri.antonio.pcp.ast.expr.ExpressionVisitor;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorCompareExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorConstant;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorExtractExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorUnaryExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorVariable;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanConstant;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanNaryExpression;
import name.filieri.antonio.pcp.ast.expr.bool.NotExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericComparisonExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericConstant;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericUnaryExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericUnaryExpression.Function;
import name.filieri.antonio.pcp.ast.expr.probabilistic.BernoulliVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.NormalVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformDoubleVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformFloatVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformIntegerVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformLongVariable;
import name.filieri.antonio.pcp.ast.expr.strings.StringBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.strings.StringVariable;

public class Z3BitVecVisitor implements ExpressionVisitor<Void> {

	private final Context ctx;
	private final Map<String, BitVecExpr> nameToZ3Var;

	private final Deque<Expr> evalStack;

	public Z3BitVecVisitor(Context ctx, Map<String, BitVecExpr> idToZ3Var) {
		this.ctx = ctx;
		this.evalStack = new ArrayDeque<>();
		this.nameToZ3Var = idToZ3Var;
	}

	public Deque<Expr> getEvalStack() {
		return evalStack;
	}

	@Override
	public Void visit(BooleanNaryExpression expr) {
		switch (expr.getFunction()) {
		case AND:
			for (Expression child : expr.getArgs()) {
				child.accept(this);
			}
			break;
		case OR:
			throw new RuntimeException("Disjuctions should be handled before calling the sharpsat counter");
		default:
			throw new RuntimeException("Unknown" + expr);
		}
		return null;
	}

	@Override
	public Void visit(NumericBinaryExpression expr) {
		expr.getLeft().accept(this);
		expr.getRight().accept(this);
		Expr b = evalStack.pop();
		Expr a = evalStack.pop();
		NumericBinaryExpression.Function op = expr.getFunction();

		Expr result;

		if (a instanceof BitVecExpr && b instanceof BitVecExpr) {
			result = mkBitVecArith((BitVecExpr) a, (BitVecExpr) b, op);
		} else if (a instanceof IntExpr && b instanceof IntExpr) {
			result = mkIntArith((IntExpr) a, (IntExpr) b, op);
		} else {
			throw new RuntimeException("Unknown type: " + a + " " + b);
		}
		evalStack.push(result);
		return null;
	}

	@Override
	public Void visit(NumericComparisonExpression expr) {
		expr.getLeft().accept(this);
		expr.getRight().accept(this);
		
		Expr b = evalStack.pop();
		Expr a = evalStack.pop();
		NumericComparisonExpression.Function cmp = expr.getFunction();
		Expr result;

		if (a instanceof BitVecExpr && b instanceof BitVecExpr) {
			result = mkBitVecCmp((BitVecExpr) a, (BitVecExpr) b, cmp);
		} else if (a instanceof IntExpr && b instanceof IntExpr) {
			result = mkIntCmp((IntExpr) a, (IntExpr) b, cmp);
		} else {
			throw new RuntimeException("Unknown type: " + a + " " + b);
		}
		evalStack.push(result);
		return null;
	}

	private BoolExpr mkIntCmp(IntExpr a, IntExpr b, NumericComparisonExpression.Function cmp) {
		switch (cmp) {
		case EQ:
			return ctx.mkEq(a, b);
		case GE:
			return ctx.mkGe(a, b);
		case GT:
			return ctx.mkGt(a, b);
		case LE:
			return ctx.mkLe(a, b);
		case LT:
			return ctx.mkLt(a, b);
		case NE:
			return ctx.mkNot(ctx.mkEq(a, b));
		default:
			throw new RuntimeException("Unhandled case: " + cmp);
		}
	}

	private BoolExpr mkBitVecCmp(BitVecExpr a, BitVecExpr b, NumericComparisonExpression.Function cmp) {
		switch (cmp) {
		case EQ:
			return ctx.mkEq(a, b);
		case GE:
			return ctx.mkBVSGE(a, b);
		case GT:
			return ctx.mkBVSGT(a, b);
		case LE:
			return ctx.mkBVSLE(a, b);
		case LT:
			return ctx.mkBVSLT(a, b);
		case NE:
			return ctx.mkNot(ctx.mkEq(a, b));
		default:
			throw new RuntimeException("Unhandled case: " + cmp);
		}
	}

	private Expr mkIntArith(IntExpr a, IntExpr b, NumericBinaryExpression.Function op) {
		switch (op) {
		case DIV:
			return ctx.mkDiv(a, b);
		case SUB:
			return ctx.mkSub(a, b);
		case MUL:
			return ctx.mkMul(a, b);
		case ADD:
			return ctx.mkAdd(a, b);
		case MOD:
			return ctx.mkRem(a, b);
		case ATAN2:
		case MAX:
		case MIN:
		case POW:
		default:
			throw new RuntimeException("Unsupported operations for bit vectors (for now): " + op);
		}
	}

	private Expr mkBitVecArith(BitVecExpr a, BitVecExpr b, NumericBinaryExpression.Function op) {
		switch (op) {
		case DIV:
			return ctx.mkBVSDiv(a, b);
		case SUB:
			return ctx.mkBVSub(a, b);
		case MUL:
			return ctx.mkBVMul(a, b);
		case ADD:
			return ctx.mkBVAdd(a, b);
		case MOD:
			return ctx.mkBVSRem(a, b);
		case ATAN2:
		case MAX:
		case MIN:
		case POW:
		default:
			throw new RuntimeException("Unsupported operations for bit vectors (for now): " + op);
		}
	}

	@Override
	public Void visit(BooleanConstant expr) {
		evalStack.push(ctx.mkBV(expr.getValue() ? 1 : 0, 1));
		return null;
	}

	@Override
	public Void visit(NotExpression expr) {
		expr.getArg().accept(this);
		BoolExpr arg = (BoolExpr) evalStack.pop();
		evalStack.push(ctx.mkNot(arg));
		return null;
	}

	@Override
	public Void visit(NumericConstant expr) {
		switch (expr.getType()) {
		case INT:
			evalStack.push(ctx.mkBV(expr.getValue().intValue(), 32));
			break;
		case LONG:
			evalStack.push(ctx.mkBV(expr.getValue().longValue(), 64));
			break;
		case DOUBLE:
		case FLOAT:
		case BOOLEAN:
		case DATA:
		case STRING:
		default:
			throw new RuntimeException("Unsupported constant type for sharpsat: " + expr);
		}
		return null;
	}

	@Override
	public Void visit(NumericUnaryExpression expr) {
		expr.getArg().accept(this);
		Expr a = evalStack.pop();
		NumericUnaryExpression.Function op = expr.getFunction();

		Expr result;

		if (a instanceof BitVecExpr) {
			result = mkBitVecArith((BitVecExpr) a, op);
		} else if (a instanceof IntExpr) {
			result = mkIntArith((IntExpr) a, op);
		} else {
			throw new RuntimeException("Unknown type: " + a);
		}
		evalStack.push(result);
		return null;
	}

	private Expr mkIntArith(IntExpr a, Function op) {
		switch (op) {
		case COS:
		case COSH:
		case EXP:
		case LOG:
		case LOG10:
		case SIN:
		case SINH:
		case SQRT:
		case TAN:
		case TANH:
		default:
			throw new RuntimeException("Unsupported operation for sharpsat: " + op);
		}
	}

	private Expr mkBitVecArith(BitVecExpr a, Function op) {
		switch (op) {
		case COS:
		case COSH:
		case EXP:
		case LOG:
		case LOG10:
		case SIN:
		case SINH:
		case SQRT:
		case TAN:
		case TANH:
		default:
			throw new RuntimeException("Unsupported operation for sharpsat: " + op);
		}
	}

	@Override
	public Void visit(NormalVariable expr) {
		throw new RuntimeException("Unsupported var type for sharpsat: " + expr);
	}

	@Override
	public Void visit(name.filieri.antonio.pcp.ast.expr.strings.StringConstant expr) {
		throw new RuntimeException("Unsupported var type for sharpsat: " + expr);
	}

	@Override
	public Void visit(BernoulliVariable expr) {
		throw new RuntimeException("Unsupported var type for sharpsat: " + expr);
	}

	@Override
	public Void visit(UniformFloatVariable expr) {
		throw new RuntimeException("Unsupported var type for sharpsat: " + expr);
	}

	@Override
	public Void visit(UniformDoubleVariable expr) {
		throw new RuntimeException("Unsupported var type for sharpsat: " + expr);
	}

	@Override
	public Void visit(UniformIntegerVariable expr) {
		evalStack.push(nameToZ3Var.get(expr.getName()));
		return null;
	}

	@Override
	public Void visit(UniformLongVariable expr) {
		evalStack.push(nameToZ3Var.get(expr.getName()));
		return null;
	}

	@Override
	public Void visit(StringBinaryExpression expr) {
		throw new RuntimeException("Unsupported var type for sharpsat: " + expr);
	}

	@Override
	public Void visit(StringVariable expr) {
		throw new RuntimeException("Unsupported var type for sharpsat: " + expr);
	}

	@Override
	public Void visit(DependentVariable dependentVariable) {
		throw new RuntimeException("Unsupported var type for sharpsat: " + dependentVariable);
	}

	@Override
	public Void visit(BitVectorConstant constant) {
		evalStack.push(ctx.mkBV(constant.getValue().toString(10), constant.getLength()));
		return null;
	}

	@Override
	public Void visit(BitVectorBinaryExpression expr) {
		expr.getLeft().accept(this);
		expr.getRight().accept(this);

		Expr b = evalStack.pop();
		Expr a = evalStack.pop();
		BitVectorBinaryExpression.Function cmp = expr.getFunction();
		Expr result;

		if (a instanceof BitVecExpr && b instanceof BitVecExpr) {
			result = mkBitVecBVBin((BitVecExpr) a, (BitVecExpr) b, cmp);
		} else {
			throw new RuntimeException("Unknown type: " + a + " " + b);
		}
		evalStack.push(result);
		return null;
	}

	private Expr mkBitVecBVBin(BitVecExpr a, BitVecExpr b, BitVectorBinaryExpression.Function op) {
		switch(op) {
			case CONCAT:
				return ctx.mkConcat(a, b);
			case BVAND:
				return ctx.mkBVAND(a,b);
			case BVOR:
				return ctx.mkBVOR(a,b);
			case BVNAND:
				return ctx.mkBVNAND(a,b);
			case BVNOR:
				return ctx.mkBVNOR(a,b);
			case BVXOR:
				return ctx.mkBVXOR(a,b);
			case BVXNOR:
				return ctx.mkBVXNOR(a,b);
			case BVADD:
				return ctx.mkBVAdd(a,b);
			case BVSUB:
				return ctx.mkBVSub(a,b);
			case BVMUL:
				return ctx.mkBVMul(a,b);
			case BVUDIV:
				return ctx.mkBVUDiv(a,b);
			case BVUREM:
				return ctx.mkBVURem(a,b);
			case BVSDIV:
				return ctx.mkBVSDiv(a,b);
			case BVSREM:
				return ctx.mkBVSRem(a,b);
			case BVSMOD:
				return ctx.mkBVSMod(a,b);
			case BVSHL:
				return ctx.mkBVSHL(a,b);
			case BVLSHR:
				return ctx.mkBVLSHR(a,b);
			case BVASHR:
				return ctx.mkBVASHR(a,b);
			default:
				throw new RuntimeException("Unsupported case: " + op);
		}
	}

	@Override
	public Void visit(BitVectorExtractExpression expr) {
		expr.getArg().accept(this);
		Expr a = evalStack.pop();
		Expr extracted = ctx.mkExtract(expr.getHi(), expr.getLo(), (BitVecExpr) a);
		evalStack.push(extracted);
		return null;
	}

	@Override
	public Void visit(BitVectorCompareExpression expr) {
		expr.getLeft().accept(this);
		expr.getRight().accept(this);

		Expr b = evalStack.pop();
		Expr a = evalStack.pop();
		BitVectorCompareExpression.Function cmp = expr.getFunction();
		Expr result;

		if (a instanceof BitVecExpr && b instanceof BitVecExpr) {
			result = mkBitVecBVCmp((BitVecExpr) a, (BitVecExpr) b, cmp);
		} else {
			throw new RuntimeException("Unknown type: " + a + " " + b);
		}
		evalStack.push(result);
		return null;
	}

	private Expr mkBitVecBVCmp(BitVecExpr a, BitVecExpr b, BitVectorCompareExpression.Function cmp) {
		switch(cmp) {
			case BVULT:
				return ctx.mkBVULT(a, b);
			case BVULE:
				return ctx.mkBVULE(a, b);
			case BVUGT:
				return ctx.mkBVUGT(a, b);
			case BVUGE:
				return ctx.mkBVUGE(a, b);
			case BVSLT:
				return ctx.mkBVSLT(a, b);
			case BVSLE:
				return ctx.mkBVSLE(a, b);
			case BVSGT:
				return ctx.mkBVSGT(a, b);
			case BVSGE:
				return ctx.mkBVSGE(a, b);
			default:
				throw new RuntimeException("Unsupported operation: " + cmp);
		}
	}

	@Override
	public Void visit(BitVectorUnaryExpression expr) {
		expr.getArg().accept(this);
		Expr a = evalStack.pop();
		BitVectorUnaryExpression.Function fun = expr.getFunction();
		Expr result;

		if (a instanceof BitVecExpr) {
			switch (fun) {
				case BVNEG:
					result = ctx.mkBVNeg((BitVecExpr) a);
					break;
				case BVNOT:
					result = ctx.mkBVNeg((BitVecExpr) a);
					break;
				default:
					throw new RuntimeException("Unknown case: " + fun + " at: " + expr.getPositionInfo());
			}
		} else {
			throw new RuntimeException("Unknown type: " + a);
		}
		evalStack.push(result);
		return null;
	}

	@Override
	public Void visit(BitVectorVariable expr) {
		evalStack.push(nameToZ3Var.get(expr.getName()));
		return null;
	}
}
