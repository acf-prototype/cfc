package name.filieri.antonio.pcp.ast.expr.probabilistic;

import com.google.common.hash.HashCode;

import name.filieri.antonio.pcp.ast.PositionInfo;
import name.filieri.antonio.pcp.ast.expr.ExpressionType;
import name.filieri.antonio.pcp.ast.expr.ExpressionVisitor;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericBoundedVariable;

public class UniformFloatVariable extends NumericBoundedVariable implements ProbabilisticNumericVariable {

	public HashCode hash;

	public UniformFloatVariable(String name, float lb, float ub, PositionInfo position) {
		super(ExpressionType.FLOAT, name, lb, ub, position);
	}

	@Override
	public <T> T accept(ExpressionVisitor<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UniformFloatVariable other = (UniformFloatVariable) obj;
		
		if (this.hashCode() != other.hashCode()) {
			return false;
		}
		
		if (getLowerBound() == null) {
			if (other.getLowerBound() != null)
				return false;
		} else if (!getLowerBound().equals(other.getLowerBound()))
			return false;
		if (getName() == null) {
			if (other.getName() != null)
				return false;
		} else if (!getName().equals(other.getName()))
			return false;
		if (getType() != other.getType())
			return false;
		if (getUpperBound() == null) {
			if (other.getUpperBound() != null)
				return false;
		} else if (!getUpperBound().equals(other.getUpperBound()))
			return false;
		return true;
	}

}
