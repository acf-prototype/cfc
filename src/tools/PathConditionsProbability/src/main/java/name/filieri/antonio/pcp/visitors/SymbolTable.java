package name.filieri.antonio.pcp.visitors;

import java.util.Map;

import com.google.common.collect.Maps;

import name.filieri.antonio.pcp.ast.expr.Expression;
import name.filieri.antonio.pcp.ast.expr.ExpressionType;
import name.filieri.antonio.pcp.ast.expr.Variable;

public class SymbolTable {

	public final Map<String, Variable> nameToVar;
	// type checker will make sure that there are no variables in a constant
	// declaration
	public final Map<String, TypeAndExpression> nameToConst;

	public SymbolTable() {
		this.nameToVar = Maps.newHashMap();
		this.nameToConst = Maps.newHashMap();
	}

	public static class TypeAndExpression {
		public final ExpressionType type;
		public final Expression expr;

		public TypeAndExpression(ExpressionType type, Expression expr) {
			super();
			this.type = type;
			this.expr = expr;
		}
	}

	public void clear() {
		nameToVar.clear();
		nameToConst.clear();
	}
}
