package name.filieri.antonio.jpf.utils;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.Token;

import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;

import name.filieri.antonio.pcp.ast.PositionInfo;

public class ASTUtil {

	public static HashFunction getHashFunction() {
		return Hashing.murmur3_128();
	}

	public static PositionInfo extractPositionInfo(RuleContext ctx) {
		if (ctx instanceof ParserRuleContext) {
			ParserRuleContext prc = (ParserRuleContext) ctx;
			return new PositionInfo(prc.start.getLine(), prc.start.getCharPositionInLine(),
					prc.stop.getLine(), prc.stop.getCharPositionInLine());
		} else if (ctx.getPayload() instanceof Token ) {
			Token tok = (Token) ctx.getPayload();
			return new PositionInfo(tok.getLine(), tok.getCharPositionInLine(), -1, -1);
		} else {
			return PositionInfo.DUMMY;
		}
	}
}
