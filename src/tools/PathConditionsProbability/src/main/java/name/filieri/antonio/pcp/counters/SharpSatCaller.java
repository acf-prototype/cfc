package name.filieri.antonio.pcp.counters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.ImmutableList;
import com.microsoft.z3.BitVecExpr;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Context;
import com.microsoft.z3.Solver;

import name.filieri.antonio.jpf.utils.BigRational;
import name.filieri.antonio.pcp.Constraint;
import name.filieri.antonio.pcp.CountResult;
import name.filieri.antonio.pcp.Options;

public class SharpSatCaller implements ModelCounterCaller {

	private final String sharpSatPath;
	private final Options options;
	private final static int SIGFPE = 136;

	public SharpSatCaller(Options options) {
		this.options = options;
		this.sharpSatPath = options.getSharpSatPath();
	}

	@Override
	public Map<Constraint, CountResult> count(Collection<Constraint> problems) throws ModelCounterException {
		Map<Constraint, CountResult> results = new LinkedHashMap<>();
		for (Constraint problem : problems) {
			Path cnfFile = prepareCNF(problem);
			BigRational count = runToolAndExtract(cnfFile);
			CountResult r = new CountResult(count.div(problem.getDomainSize()), 0);
			results.put(problem, r);
		}
		return results;
	}

	public Path prepareCNF(Constraint problem) {
		Context ctx = new Context();
		Solver solver = ctx.mkSolver();
		Map<String, BitVecExpr> idToZ3BV = new HashMap<>();
		List<BoolExpr> domainExprs = ModelCounterUtils.toZ3Vars(problem.getVariables(), ctx, idToZ3BV);
		List<BoolExpr> constraintExprs = ModelCounterUtils
				.collectZ3Constraints(problem, ctx, solver, idToZ3BV);
		List<BoolExpr> formula = new ArrayList<>();
		formula.addAll(domainExprs);
		formula.addAll(constraintExprs);

		// bitblast
		Z3Bitblaster bitblaster = new Z3Bitblaster(ctx);
		String dimacs = bitblaster.generateDimacs(formula);

		try {
			Path tmpFile = Files.createTempFile("sharp", ".cnf");
			Files.write(tmpFile, ImmutableList.of(dimacs));
			return tmpFile;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private BigRational runToolAndExtract(Path cnfFile) {
		ProcessBuilder pb = new ProcessBuilder(sharpSatPath, cnfFile.toAbsolutePath().toString());
		try {
			Process process = pb.start();
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(process.getInputStream()));
			String line = null;

			while (process.isAlive()) {
				try {
					process.waitFor();
				} catch (InterruptedException e) {
				}
			}

			// workaround for division by 0 in the projection patch
			if (process.exitValue() == SIGFPE) {
				return BigRational.ONE;
			} else {
				while ((line = reader.readLine()) != null) {
					// uncomment this to see what sharpSAT is printing
					// System.out.println(line);
					if (line.startsWith("# solutions")) {
						String solutionLine = reader.readLine();
						System.out.println(solutionLine);
						return BigRational.valueOf(solutionLine);
					}
				}
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		throw new RuntimeException("Solution line not found in the output of sharpSAT!");
	}
}
