package name.filieri.antonio.pcp.ast.expr.strings;

import java.nio.charset.StandardCharsets;

import com.google.common.hash.HashCode;

import name.filieri.antonio.jpf.utils.ASTUtil;
import name.filieri.antonio.pcp.ast.PositionInfo;
import name.filieri.antonio.pcp.ast.expr.Constant;
import name.filieri.antonio.pcp.ast.expr.ExpressionVisitor;


public class StringConstant implements Constant {

	private final String value;
	private final PositionInfo position;
	private HashCode hash = null;

	public StringConstant(String value, PositionInfo position) {
		this.value = value;
		this.position = position;
	}

	@Override
	public <T> T accept(ExpressionVisitor<T> visitor) {
		return visitor.visit(this);
	}

	public String getValue() {
		return value;
	}

	@Override
	public int hashCode() {
		return getHashCode().asInt();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StringConstant other = (StringConstant) obj;
		
		if (this.hashCode() != other.hashCode())
			return false;
		
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	@Override
	public HashCode getHashCode() {
		if (hash == null) {
			HashCode hc = ASTUtil.getHashFunction().newHasher()
					.putString(value, StandardCharsets.UTF_8)
					.hash();
			hash = hc;
		}
		return hash;
	}
	
	@Override
	public String toString() {
		return "'"+value+"'";
	}
	
	@Override
	public PositionInfo getPositionInfo() {
		return position;
	}
	
	public static StringConstant valueOf(String value) {
		return new StringConstant(value, PositionInfo.DUMMY);
	}
}
