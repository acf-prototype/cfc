package name.filieri.antonio.pcp;

import java.util.List;
import java.util.Scanner;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import name.filieri.antonio.pcp.ast.commands.Command;
import name.filieri.antonio.pcp.ast.commands.CountCommand;
import name.filieri.antonio.pcp.grammar.InputFormatLexer;
import name.filieri.antonio.pcp.grammar.InputFormatParser;
import name.filieri.antonio.pcp.visitors.AstBuilder;
import name.filieri.antonio.pcp.visitors.CommandEvaluator;
import name.filieri.antonio.pcp.visitors.CommandEvaluator.Reply;
import name.filieri.antonio.pcp.visitors.SymbolTable;

public class PCPMain {

	public static void main(String[] args) {
		Options options = Options.loadFromUserHomeOrDefault();
		boolean interactive = (args.length > 0 && (args[0].equals("-i") || args[0].equals("--interactive")));
		Scanner sc = new Scanner(System.in, "UTF-8");
		SymbolTable stable = new SymbolTable();
		Partitioner partitioner = new ConstraintPartitioner();
		CountDispatcher dispatcher = new PCPDispatcher(options);
		CommandEvaluator eval = new CommandEvaluator(stable, partitioner, dispatcher, options);
		
		if (interactive) {
			System.out.println("PCP version 0.000000000000000...001");
			System.out.print("> ");
		}

		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			ANTLRInputStream input = new ANTLRInputStream(line);
			InputFormatLexer lexer = new InputFormatLexer(input);
			InputFormatParser parser = new InputFormatParser(new CommonTokenStream(lexer));

			ParseTree tree = parser.input();
			List<? extends Command> commands = AstBuilder.parseCommands(stable, tree);

			for (Command command : commands) {
				Reply reply = command.accept(eval);
				if (reply.error) {
					System.out.println("(error \"" + reply.message + "\")");
				} else {
					if (command instanceof CountCommand) {
						CountResult result = reply.result;
						System.out.println("(" + result.probability + " " + result.variance + ")");
					} else if (interactive && reply.message != null && reply.message.isEmpty()) {
						System.out.println("(" + reply.message + ")");
					}
				}
			}

			if (interactive) {
				System.out.print("> ");
			}
		}
		sc.close();
	}
}
