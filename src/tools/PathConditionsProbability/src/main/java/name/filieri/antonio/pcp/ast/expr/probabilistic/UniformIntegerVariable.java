package name.filieri.antonio.pcp.ast.expr.probabilistic;

import name.filieri.antonio.pcp.ast.PositionInfo;
import name.filieri.antonio.pcp.ast.expr.ExpressionType;
import name.filieri.antonio.pcp.ast.expr.ExpressionVisitor;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericBoundedVariable;

public class UniformIntegerVariable extends NumericBoundedVariable implements ProbabilisticNumericVariable {

	public UniformIntegerVariable(String name, int lb, int ub, PositionInfo position) {
		super(ExpressionType.INT, name, lb, ub, position);
	}
	
	public UniformIntegerVariable(String name, int lb, int ub) {
		super(ExpressionType.INT, name, lb, ub, PositionInfo.DUMMY);
	}

	@Override
	public <T> T accept(ExpressionVisitor<T> visitor) {
		return visitor.visit(this);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UniformIntegerVariable other = (UniformIntegerVariable) obj;
		
		if (this.hashCode() != other.hashCode()) {
			return false;
		}
		
		if (getLowerBound() == null) {
			if (other.getLowerBound() != null)
				return false;
		} else if (!getLowerBound().equals(other.getLowerBound()))
			return false;
		if (getName() == null) {
			if (other.getName() != null)
				return false;
		} else if (!getName().equals(other.getName()))
			return false;
		if (getType() != other.getType())
			return false;
		if (getUpperBound() == null) {
			if (other.getUpperBound() != null)
				return false;
		} else if (!getUpperBound().equals(other.getUpperBound()))
			return false;
		return true;
	}

}
