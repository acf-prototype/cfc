package name.filieri.antonio.pcp.visitors;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;

import name.filieri.antonio.pcp.ast.expr.BinaryExpression;
import name.filieri.antonio.pcp.ast.expr.DependentVariable;
import name.filieri.antonio.pcp.ast.expr.Expression;
import name.filieri.antonio.pcp.ast.expr.ExpressionVisitor;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorCompareExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorConstant;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorExtractExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorUnaryExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorVariable;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanConstant;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanExpression;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanNaryExpression;
import name.filieri.antonio.pcp.ast.expr.bool.NotExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericComparisonExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericConstant;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericUnaryExpression;
import name.filieri.antonio.pcp.ast.expr.probabilistic.BernoulliVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.NormalVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformDoubleVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformFloatVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformIntegerVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformLongVariable;
import name.filieri.antonio.pcp.ast.expr.strings.StringBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.strings.StringConstant;
import name.filieri.antonio.pcp.ast.expr.strings.StringVariable;

public class DisjunctiveNormalFormVisitor implements ExpressionVisitor<List<Expression>> {

	public static List<Expression> convertToDNF(BooleanExpression expr) {
		return expr.accept(new NegativeNormalFormVisitor())
				.accept(new DisjunctiveNormalFormVisitor());
	}

	@Override
	public List<Expression> visit(BooleanNaryExpression expr) {
		List<Expression> result;
		
		if (expr.getFunction() == BooleanNaryExpression.Function.AND) {
			List<Set<BooleanExpression>> argsDNF = new ArrayList<>(expr.getArgs().size());
			boolean lengthEqualToOne = true;
		
			for (Expression argExpr : expr.getArgs()) {
				List<Expression> argDNF = argExpr.accept(this);
				lengthEqualToOne = lengthEqualToOne && argDNF.size() == 1;

				Set<BooleanExpression> genericSet = new LinkedHashSet<>();
				for (Expression dnfExpr : argDNF) {
					genericSet.add((BooleanExpression) dnfExpr);
				}
				argsDNF.add(genericSet);
			}
			if (lengthEqualToOne) { // nothing to do here
				result = ImmutableList.<Expression> of(expr);
			} else { // compute cartesian product
				/*- TODO implement a leaner algorithm (operating directly on lists) */
				Set<List<BooleanExpression>> cartesianProduct = Sets.cartesianProduct(argsDNF);

				List<Expression> tmp = new ArrayList<>(cartesianProduct.size());
				for (List<BooleanExpression> boolList : cartesianProduct) {
					tmp.add(new BooleanNaryExpression(BooleanNaryExpression.Function.AND, boolList,
							expr.getPositionInfo()));
				}
				result = ImmutableList.copyOf(tmp);
			}
		} else if (expr.getFunction() == BooleanNaryExpression.Function.OR) {
			//combine all alternatives in a single list
			List<Expression> disjunction = new ArrayList<>();
			for (Expression argExpr : expr.getArgs()) {
				disjunction.addAll(argExpr.accept(this));
			}
			result = disjunction;
		} else {
			throw new RuntimeException("Unknown boolean function: " + expr.getFunction());
		}

		return result;
	}

	private List<Expression> handleBinaryExpression(BinaryExpression expr) {
		List<Expression> leftDNF = expr.getLeft().accept(this);
		List<Expression> rightDNF = expr.getRight().accept(this);

		if (leftDNF.size() == 1 && rightDNF.size() == 1) { // nothing to do here
			return ImmutableList.<Expression> of(expr);
		} else {
			List<Expression> dnf = new ArrayList<>(leftDNF.size() * rightDNF.size());

			for (Expression lexpr : leftDNF) {
				for (Expression rexpr : rightDNF) {
					if (expr instanceof StringBinaryExpression) {
						StringBinaryExpression castedExpr = (StringBinaryExpression) expr;
						dnf.add(new StringBinaryExpression(lexpr, rexpr, castedExpr.getFunction(),
								expr.getPositionInfo()));
					} else if (expr instanceof NumericComparisonExpression) {
						NumericComparisonExpression castedExpr = (NumericComparisonExpression) expr;
						dnf.add(new NumericComparisonExpression(lexpr, rexpr, castedExpr.getFunction(),
								expr.getPositionInfo()));
					} else if (expr instanceof NumericBinaryExpression) {
						NumericBinaryExpression castedExpr = (NumericBinaryExpression) expr;
						dnf.add(new NumericBinaryExpression(lexpr, rexpr, castedExpr.getFunction(),
								expr.getPositionInfo()));
					} else {
						throw new RuntimeException("Not implemented yet: " + expr.getClass());
					}
				}
			}
			return dnf;
		}
	}

	@Override
	public List<Expression> visit(NumericBinaryExpression expr) {
		return handleBinaryExpression(expr);
	}

	@Override
	public List<Expression> visit(NumericComparisonExpression expr) {
		return handleBinaryExpression(expr);
	}

	@Override
	public List<Expression> visit(NumericUnaryExpression expr) {
		List<Expression> argDNF = expr.getArg().accept(this);

		if (argDNF.size() == 1) {
			return ImmutableList.<Expression> of(expr);
		} else {
			List<Expression> dnf = new ArrayList<>(argDNF.size());
			for (Expression argExpr : argDNF) {
				dnf.add(new NumericUnaryExpression(expr.getFunction(), argExpr, expr.getPositionInfo()));
			}
			return dnf;
		}
	}

	@Override
	public List<Expression> visit(NotExpression expr) {
		throw new RuntimeException("Precondition violated: Negation is not allowed when converting to DNF");
	}

	@Override
	public List<Expression> visit(StringBinaryExpression expr) {
		return handleBinaryExpression(expr);
	}

	@Override
	public List<Expression> visit(BooleanConstant constant) {
		return ImmutableList.<Expression> of(constant);
	}

	@Override
	public List<Expression> visit(StringConstant constant) {
		return ImmutableList.<Expression> of(constant);
	}

	@Override
	public List<Expression> visit(NumericConstant constant) {
		return ImmutableList.<Expression> of(constant);
	}

	@Override
	public List<Expression> visit(NormalVariable var) {
		return ImmutableList.<Expression> of(var);
	}

	@Override
	public List<Expression> visit(BernoulliVariable var) {
		return ImmutableList.<Expression> of(var);
	}

	@Override
	public List<Expression> visit(UniformFloatVariable var) {
		return ImmutableList.<Expression> of(var);
	}

	@Override
	public List<Expression> visit(UniformDoubleVariable var) {
		return ImmutableList.<Expression> of(var);
	}

	@Override
	public List<Expression> visit(UniformIntegerVariable var) {
		return ImmutableList.<Expression> of(var);
	}

	@Override
	public List<Expression> visit(UniformLongVariable var) {
		return ImmutableList.<Expression> of(var);
	}

	@Override
	public List<Expression> visit(StringVariable var) {
		return ImmutableList.<Expression> of(var);
	}

	@Override
	public List<Expression> visit(DependentVariable var) {
		return ImmutableList.<Expression> of(var);
	}

	@Override
	public List<Expression> visit(BitVectorConstant constant) {
		return ImmutableList.<Expression>of(constant);
	}

	@Override
	public List<Expression> visit(BitVectorBinaryExpression binExpr) {
		return handleBinaryExpression(binExpr);
	}

	@Override
	public List<Expression> visit(BitVectorExtractExpression expr) {
		List<Expression> argDNF = expr.getArg().accept(this);

		if (argDNF.size() == 1) {
			return ImmutableList.<Expression> of(expr);
		} else {
			List<Expression> dnf = new ArrayList<>(argDNF.size());
			for (Expression argExpr : argDNF) {
				dnf.add(new BitVectorExtractExpression(argExpr, expr.getHi(), expr.getLo(),
						expr.getPositionInfo()));
			}
			return dnf;
		}
	}

	@Override
	public List<Expression> visit(BitVectorCompareExpression binExpr) {
		return handleBinaryExpression(binExpr);
	}

	@Override
	public List<Expression> visit(BitVectorUnaryExpression expr) {
		List<Expression> argDNF = expr.getArg().accept(this);

		if (argDNF.size() == 1) {
			return ImmutableList.<Expression> of(expr);
		} else {
			List<Expression> dnf = new ArrayList<>(argDNF.size());
			for (Expression argExpr : argDNF) {
				dnf.add(new BitVectorUnaryExpression(argExpr, expr.getFunction(), expr.getPositionInfo()));
			}
			return dnf;
		}
	}

	@Override
	public List<Expression> visit(BitVectorVariable var) {
		return ImmutableList.<Expression>of(var);
	}
}