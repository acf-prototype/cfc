package name.filieri.antonio.pcp.ast.expr;

import com.google.common.hash.HashCode;

import name.filieri.antonio.jpf.utils.ASTUtil;

public class ExpressionCategory {

	public enum Kind {
		LINEAR, NONLINEAR, STRING, STRING_NUMERIC, BITVECTOR, DATASTRUCTURE;

		public boolean isNumeric() {
			return this == LINEAR || this == NONLINEAR;
		}
	};

	public final Kind category;
	public final boolean isConstant;
	private HashCode hash;

	public ExpressionCategory(Kind category, boolean isConstant) {
		super();
		this.category = category;
		this.isConstant = isConstant;
	}

	@Override
	public int hashCode() {
		return getHashCode().asInt();
	}

	public HashCode getHashCode() {
		if (hash == null) {
			hash = ASTUtil.getHashFunction().newHasher()
					.putInt(category.ordinal())
					.putBoolean(isConstant)
					.hash();
		}
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExpressionCategory other = (ExpressionCategory) obj;
		if (category != other.category)
			return false;
		if (isConstant != other.isConstant)
			return false;
		return true;
	}

	@Override
	public String toString() {
		if (isConstant) {
			return "constant-" + category;
		} else {
			return "" + category;
		}
	}
};