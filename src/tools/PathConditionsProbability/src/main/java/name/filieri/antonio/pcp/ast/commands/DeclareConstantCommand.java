package name.filieri.antonio.pcp.ast.commands;

import java.nio.charset.StandardCharsets;

import com.google.common.collect.ImmutableList;
import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;

import name.filieri.antonio.jpf.utils.ASTUtil;
import name.filieri.antonio.pcp.ast.PositionInfo;
import name.filieri.antonio.pcp.ast.expr.Expression;
import name.filieri.antonio.pcp.ast.expr.ExpressionType;

public class DeclareConstantCommand implements Command {

	private final String name;
	private final ExpressionType type;
	private final Expression value;
	private final PositionInfo position;
	private HashCode hash;
	
	public DeclareConstantCommand(String name, ExpressionType type, Expression value, PositionInfo position) {
		super();
		this.name = name;
		this.type = type;
		this.value = value;
		this.position = position;
	}

	@Override
	public HashCode getHashCode() {
		if (hash == null) {
			HashCode hc = ASTUtil.getHashFunction().newHasher()
					.putString(name,StandardCharsets.UTF_8)
					.putInt(type.ordinal())
					.hash();
			hash = Hashing.combineOrdered(ImmutableList.of(hc,value.getHashCode()));
		}
		return hash;
	}

	@Override
	public PositionInfo getPositionInfo() {
		return position;
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.DECLARE_CONST;
	}

	@Override
	public <T> T accept(CommandVisitor<T> visitor) {
		return visitor.visit(this);
	}

	public String getName() {
		return name;
	}

	public ExpressionType getType() {
		return type;
	}

	public Expression getValue() {
		return value;
	}
}
