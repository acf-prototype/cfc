package name.filieri.antonio.pcp.counters;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorCompareExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorConstant;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorExtractExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorUnaryExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorVariable;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.distribution.PascalDistribution;
import org.apache.commons.math3.distribution.UniformIntegerDistribution;
import org.apache.commons.math3.distribution.UniformRealDistribution;
import org.apache.commons.math3.random.RandomGenerator;

import com.google.common.collect.ImmutableList;

import coral.PC;
import coral.counters.rvars.IntegerRandomVariable;
import coral.counters.rvars.RandomVariable;
import coral.counters.rvars.RealRandomVariable;
import coral.util.Interval;
import name.filieri.antonio.pcp.Constraint;
import name.filieri.antonio.pcp.ast.expr.DependentVariable;
import name.filieri.antonio.pcp.ast.expr.Expression;
import name.filieri.antonio.pcp.ast.expr.ExpressionVisitor;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanConstant;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanNaryExpression;
import name.filieri.antonio.pcp.ast.expr.bool.NotExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericComparisonExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericComparisonExpression.Function;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericConstant;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericUnaryExpression;
import name.filieri.antonio.pcp.ast.expr.probabilistic.BernoulliVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.NormalVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformDoubleVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformFloatVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformIntegerVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformLongVariable;
import name.filieri.antonio.pcp.ast.expr.strings.StringBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.strings.StringConstant;
import name.filieri.antonio.pcp.ast.expr.strings.StringVariable;
import symlib.FrozenSymBool;
import symlib.SymAsDouble;
import symlib.SymAsInt;
import symlib.SymBool;
import symlib.SymBoolConstant;
import symlib.SymDouble;
import symlib.SymDoubleArith;
import symlib.SymDoubleConstant;
import symlib.SymDoubleLiteral;
import symlib.SymDoubleRelational;
import symlib.SymFloat;
import symlib.SymFloatArith;
import symlib.SymFloatRelational;
import symlib.SymInt;
import symlib.SymIntArith;
import symlib.SymIntConstant;
import symlib.SymIntLiteral;
import symlib.SymIntRelational;
import symlib.SymLiteral;
import symlib.SymLong;
import symlib.SymLongArith;
import symlib.SymLongRelational;
import symlib.SymMathBinary;
import symlib.SymMathUnary;
import symlib.SymNumber;

public class QCoralVisitor implements ExpressionVisitor<List<SymBool>> {

	protected static class ConstraintPlusDomain {
		Interval[] domain;
		PC constraint;

		public ConstraintPlusDomain(Interval[] domain, PC constraint) {
			this.domain = domain;
			this.constraint = constraint;
		}
	}

	public Map<String, SymLiteral> name2lit;
	public Map<SymLiteral, RandomVariable> lit2rvar;
	public RandomGenerator rng;

	public QCoralVisitor(RandomGenerator rng) {
		lit2rvar = new LinkedHashMap<>();
		name2lit = new LinkedHashMap<>();
		this.rng = rng;
	}

	public ConstraintPlusDomain parseConstraint(Constraint problem) {
		List<SymBool> bool = problem.getExpr().accept(this);
		List<SymBool> frozenBool = new ArrayList<>(bool.size());
		for (int i = 0; i < bool.size(); i++) {
			frozenBool.add(i, new FrozenSymBool(bool.get(i)));
		}
		PC pc = new PC(frozenBool);
		Interval[] domain = new Interval[pc.getSortedVars().size()];
		int i = 0;
		for (SymLiteral lit : pc.getSortedVars()) {
			RandomVariable r = lit2rvar.get(lit);
			domain[i] = r.bounds;
		}
		return new ConstraintPlusDomain(domain, pc);
	}

	@Override
	public List<SymBool> visit(BooleanConstant expr) {
		SymBool constant;
		if (expr.getValue()) {
			constant = SymBoolConstant.TRUE;
		} else {
			constant = SymBoolConstant.FALSE;
		}
		return ImmutableList.of(constant);
	}

	@Override
	public List<SymBool> visit(BooleanNaryExpression expr) {
		List<SymBool> visited = new ArrayList<>(expr.getArgs().size());
		for (Expression child : expr.getArgs()) {
			visited.addAll(child.accept(this));
		}
		switch (expr.getFunction()) {
		case AND:
			return ImmutableList.copyOf(visited);
		case OR:
			throw new RuntimeException("Disjunctions should be handled before calling the visitor");
		default:
			throw new RuntimeException("Unknown function: " + expr.getFunction());
		}
	}

	@Override
	public List<SymBool> visit(NotExpression expr) {
		List<SymBool> visited = expr.getArg().accept(this);
		List<SymBool> neg = new ArrayList<>(visited.size());

		for (SymBool bool : visited) {
			neg.add(bool.neg());
		}

		return ImmutableList.copyOf(neg);
	}

	@Override
	public List<SymBool> visit(NumericComparisonExpression expr) {
		QCoralNumericVisitor nvisitor = new QCoralNumericVisitor();
		SymNumber left = expr.getLeft().accept(nvisitor);
		SymNumber right = expr.getRight().accept(nvisitor);

		SymBool result = handleCoralComparison(left, right, expr.getFunction());

		return ImmutableList.of(result);
	}

	private SymBool handleCoralComparison(SymNumber left, SymNumber right, Function function) {
		if ((left instanceof SymInt || left instanceof SymLong)
				&& (right instanceof SymDouble || right instanceof SymFloat)) {
			right = new SymAsInt(right);
		} else if ((left instanceof SymDouble || left instanceof SymFloat)
				&& (right instanceof SymInt || right instanceof SymLong)) {
			right = new SymAsDouble(right);
		}

		String symbol = function.toSymbol();
		if (function == Function.EQ) {
			symbol = "==";
		}
		
		if (left instanceof SymInt) {
			int i = 0;
			for (String key : SymIntRelational.symbols) {
				if (key.equals(symbol)) {
					break;
				}
				i++;
			}
			return new SymIntRelational((SymInt) left, (SymInt) right, i);
		} else if (left instanceof SymDouble) {
			int i = 0;
			for (String key : SymDoubleRelational.symbols) {
				if (key.equals(symbol)) {
					break;
				}
				i++;
			}
			return new SymDoubleRelational((SymDouble) left, (SymDouble) right, i);
		} else if (left instanceof SymFloat) {
			int i = 0;
			for (String key : SymFloatRelational.symbols) {
				if (key.equals(symbol)) {
					break;
				}
				i++;
			}
			return SymFloatRelational.create((SymFloat) left, (SymFloat) right, i);
		} else if (left instanceof SymLong) {
			int i = 0;
			for (String key : SymLongRelational.symbols) {
				if (key.equals(symbol)) {
					break;
				}
				i++;
			}
			return SymLongRelational.create((SymLong) left, (SymLong) right, i);
		} else {
			throw new RuntimeException("Unknown case: " + left);
		}
	}

	@Override
	public List<SymBool> visit(NumericBinaryExpression expr) {
		throw new RuntimeException("Parsing error: this method should not be used!");
	}

	@Override
	public List<SymBool> visit(NumericConstant expr) {
		throw new RuntimeException("Parsing error: this method should not be used!");
	}

	@Override
	public List<SymBool> visit(NumericUnaryExpression expr) {
		throw new RuntimeException("Parsing error: this method should not be used!");
	}

	@Override
	public List<SymBool> visit(NormalVariable expr) {
		throw new RuntimeException("Parsing error: this method should not be used!");
	}

	@Override
	public List<SymBool> visit(StringConstant expr) {
		throw new RuntimeException("Parsing error: this method should not be used!");
	}

	@Override
	public List<SymBool> visit(BernoulliVariable expr) {
		throw new RuntimeException("Parsing error: this method should not be used!");
	}

	@Override
	public List<SymBool> visit(UniformFloatVariable expr) {
		throw new RuntimeException("Parsing error: this method should not be used!");
	}

	@Override
	public List<SymBool> visit(UniformDoubleVariable expr) {
		throw new RuntimeException("Parsing error: this method should not be used!");
	}

	@Override
	public List<SymBool> visit(UniformIntegerVariable expr) {
		throw new RuntimeException("Parsing error: this method should not be used!");
	}

	@Override
	public List<SymBool> visit(UniformLongVariable expr) {
		throw new RuntimeException("Parsing error: this method should not be used!");
	}

	@Override
	public List<SymBool> visit(StringBinaryExpression expr) {
		throw new RuntimeException("Parsing error: this method should not be used!");
	}

	@Override
	public List<SymBool> visit(StringVariable expr) {
		throw new RuntimeException("Parsing error: this method should not be used!");
	}

	@Override
	public List<SymBool> visit(DependentVariable dependentVariable) {
		throw new RuntimeException("Parsing error: this method should not be used!");
	}

	@Override
	public List<SymBool> visit(BitVectorConstant bitVectorConstant) {
		throw new RuntimeException("Parsing error: this method should not be used!");
	}

	@Override
	public List<SymBool> visit(BitVectorBinaryExpression bitVectorBinaryExpression) {
		throw new RuntimeException("Parsing error: this method should not be used!");
	}

	@Override
	public List<SymBool> visit(BitVectorExtractExpression bitVectorExtractExpression) {
		throw new RuntimeException("Parsing error: this method should not be used!");
	}

	@Override
	public List<SymBool> visit(BitVectorCompareExpression bitVectorCompareExpression) {
		throw new RuntimeException("Parsing error: this method should not be used!");
	}

	@Override
	public List<SymBool> visit(BitVectorUnaryExpression bitVectorUnaryExpression) {
		throw new RuntimeException("Parsing error: this method should not be used!");
	}

	@Override
	public List<SymBool> visit(BitVectorVariable bitVectorVariable) {
		throw new RuntimeException("Parsing error: this method should not be used!");
	}

	private class QCoralNumericVisitor implements ExpressionVisitor<SymNumber> {

		private UniformRealDistribution urd = new UniformRealDistribution(rng, 0, 1);

		@Override
		public SymNumber visit(NumericBinaryExpression expr) {
			SymNumber left = expr.getLeft().accept(this);
			SymNumber right = expr.getRight().accept(this);

			return handleBinaryOp(left, right, expr.getFunction());
		}

		@Override
		public SymNumber visit(NumericConstant expr) {
			switch (expr.getType()) {
			case DOUBLE:
			case FLOAT:
				return new SymDoubleConstant(expr.getValue().doubleValue());
			case INT:
			case LONG:
				return new SymIntConstant(expr.getValue().intValue());
			case BOOLEAN:
			case DATA:
			case STRING:
			default:
				throw new RuntimeException("Unsupported type in this context! " + expr);
			}
		}

		@Override
		public SymNumber visit(NumericUnaryExpression expr) {
			SymNumber visited = expr.getArg().accept(this);
			if (visited instanceof SymInt || visited instanceof SymLong) {
				visited = new SymAsDouble(visited);
			}

			int op = 0;
			switch (expr.getFunction()) {
			case COS:
				op = SymMathUnary.COS;
				break;
			case COSH:
				op = SymMathUnary.ACOS;
				break;
			case EXP:
				op = SymMathUnary.EXP;
				break;
			case LOG:
				op = SymMathUnary.LOG;
				break;
			case LOG10:
				op = SymMathUnary.LOG10;
				break;
			case SIN:
				op = SymMathUnary.SIN;
				break;
			case SINH:
				op = SymMathUnary.ASIN;
				break;
			case SQRT:
				op = SymMathUnary.SQRT;
				break;
			case TAN:
				op = SymMathUnary.TAN;
				break;
			case TANH:
				op = SymMathUnary.ATAN;
				break;
			default:
				throw new RuntimeException("Unknown function: " + expr);
			}
			return new SymMathUnary((SymDouble) visited, op);
		}

		public int nextId() {
			return name2lit.size() + 1;
		}
		
		@Override
		public SymNumber visit(NormalVariable expr) {
			String name = expr.getName();
			SymLiteral lit = name2lit.get(name);
			if (lit == null) {
				int next = nextId();
				lit = SymDoubleLiteral.createForParsing(next);
				name2lit.put(name, lit);

				RandomVariable rv = new RealRandomVariable(
						new NormalDistribution(rng, expr.getMean(), expr.getStandardDeviation()),
						new Interval(expr.getLowerBound().doubleValue(), expr.getUpperBound().doubleValue()),
						urd);
				lit2rvar.put(lit, rv);
			}
			return (SymNumber) lit;
		}

		@Override
		public SymNumber visit(BernoulliVariable expr) {
			String name = expr.getName();
			SymLiteral lit = name2lit.get(name);
			if (lit == null) {
				int next = nextId();
				lit = SymIntLiteral.createForParsing(next);
				name2lit.put(name, lit);

				RandomVariable rv = new IntegerRandomVariable(
						new PascalDistribution(rng, 1, expr.getProbability().doubleValue()),
						new Interval(expr.getLowerBound().intValue(), expr.getUpperBound().intValue()),
						urd);
				lit2rvar.put(lit, rv);
			}
			return (SymNumber) lit;
		}

		@Override
		public SymNumber visit(UniformFloatVariable expr) {
			String name = expr.getName();
			SymLiteral lit = name2lit.get(name);
			if (lit == null) {
				int next = nextId();
				lit = SymDoubleLiteral.createForParsing(next);
				name2lit.put(name, lit);

				RandomVariable rv = new RealRandomVariable(
						new UniformRealDistribution(expr.getLowerBound().doubleValue(), expr.getUpperBound().doubleValue()),
						new Interval(expr.getLowerBound().doubleValue(), expr.getUpperBound().doubleValue()),
						urd);
				lit2rvar.put(lit, rv);
			}
			return (SymNumber) lit;
		}

		@Override
		public SymNumber visit(UniformDoubleVariable expr) {
			String name = expr.getName();
			SymLiteral lit = name2lit.get(name);
			if (lit == null) {
				int next = nextId();
				lit = SymDoubleLiteral.createForParsing(next);
				name2lit.put(name, lit);

				RandomVariable rv = new RealRandomVariable(
						new UniformRealDistribution(expr.getLowerBound().doubleValue(), expr.getUpperBound().doubleValue()),
						new Interval(expr.getLowerBound().doubleValue(), expr.getUpperBound().doubleValue()),
						urd);
				lit2rvar.put(lit, rv);
			}
			return (SymNumber) lit;
		}

		@Override
		public SymNumber visit(UniformIntegerVariable expr) {
			String name = expr.getName();
			SymLiteral lit = name2lit.get(name);
			if (lit == null) {
				int next = nextId();
				lit = SymIntLiteral.createForParsing(next);
				name2lit.put(name, lit);

				RandomVariable rv = new IntegerRandomVariable(
						new UniformIntegerDistribution(rng, expr.getLowerBound().intValue(), expr.getUpperBound().intValue()),
						new Interval(expr.getLowerBound().intValue(), expr.getUpperBound().intValue()),
						urd);
				lit2rvar.put(lit, rv);
			}
			return (SymNumber) lit;
		}

		@Override
		public SymNumber visit(UniformLongVariable expr) {
			String name = expr.getName();
			SymLiteral lit = name2lit.get(name);
			if (lit == null) {
				int next = nextId();
				lit = SymIntLiteral.createForParsing(next);
				name2lit.put(name, lit);

				RandomVariable rv = new IntegerRandomVariable(
						new UniformIntegerDistribution(rng, expr.getLowerBound().intValue(), expr.getUpperBound().intValue()),
						new Interval(expr.getLowerBound().intValue(), expr.getUpperBound().intValue()),
						urd);
				lit2rvar.put(lit, rv);
			}
			return (SymNumber) lit;
		}

		@Override
		public SymNumber visit(StringConstant expr) {
			throw new RuntimeException("Unsupported by qcoral: " + expr);
		}

		@Override
		public SymNumber visit(StringBinaryExpression expr) {
			throw new RuntimeException("Unsupported by qcoral: " + expr);
		}

		@Override
		public SymNumber visit(StringVariable expr) {
			throw new RuntimeException("Unsupported by qcoral: " + expr);
		}

		@Override
		public SymNumber visit(DependentVariable dependentVariable) {
			throw new RuntimeException("Dependent variables are currently unsupported in qCORAL");
		}

		@Override
		public SymNumber visit(BitVectorConstant expr) {
			throw new RuntimeException("Unsupported by qcoral: " + expr);
		}

		@Override
		public SymNumber visit(BitVectorBinaryExpression expr) {
			throw new RuntimeException("Unsupported by qcoral: " + expr);
		}

		@Override
		public SymNumber visit(BitVectorExtractExpression expr) {
			throw new RuntimeException("Unsupported by qcoral: " + expr);
		}

		@Override
		public SymNumber visit(BitVectorCompareExpression expr) {
			throw new RuntimeException("Unsupported by qcoral: " + expr);
		}

		@Override
		public SymNumber visit(BitVectorUnaryExpression expr) {
			throw new RuntimeException("Unsupported by qcoral: " + expr);
		}

		@Override
		public SymNumber visit(BitVectorVariable expr) {
			throw new RuntimeException("Unsupported by qcoral: " + expr);
		}

		private SymNumber handleBinaryOp(SymNumber left, SymNumber right,
				name.filieri.antonio.pcp.ast.expr.numeric.NumericBinaryExpression.Function function) {
			if ((left instanceof SymInt || left instanceof SymLong)
					&& (right instanceof SymDouble || right instanceof SymFloat)) {
				right = new SymAsInt(right);
			} else if ((left instanceof SymDouble || left instanceof SymFloat)
					&& (right instanceof SymInt || right instanceof SymLong)) {
				right = new SymAsDouble(right);
			}

			switch (function) {
			case ADD:
			case SUB:
			case MUL:
			case DIV:
			case MOD:
				if (left instanceof SymInt) {
					int i = 0;
					String symbol = function.toSymbol();
					for (String key : SymIntArith.symbols) {
						if (key.equals(symbol)) {
							break;
						}
						i++;
					}
					return SymIntArith.create((SymInt) left, (SymInt) right, i);
				} else if (left instanceof SymDouble) {
					int i = 0;
					String symbol = function.toSymbol();
					for (String key : SymDoubleArith.symbols) {
						if (key.equals(symbol)) {
							break;
						}
						i++;
					}
					return SymDoubleArith.create((SymDouble) left, (SymDouble) right, i);
				} else if (left instanceof SymFloat) {
					int i = 0;
					String symbol = function.toSymbol();
					for (String key : SymFloatArith.symbols) {
						if (key.equals(symbol)) {
							break;
						}
						i++;
					}
					return SymFloatArith.create((SymFloat) left, (SymFloat) right, i);
				} else if (left instanceof SymLong) {
					int i = 0;
					String symbol = function.toSymbol();
					for (String key : SymLongArith.symbols) {
						if (key.equals(symbol)) {
							break;
						}
						i++;
					}
					return SymLongArith.create((SymLong) left, (SymLong) right, i);
				} else {
					throw new RuntimeException("Unknown case: " + left);
				}
			case ATAN2:
			case MAX:
			case MIN:
			case POW:
				assert left instanceof SymDouble;

				int op;
				if (function == name.filieri.antonio.pcp.ast.expr.numeric.NumericBinaryExpression.Function.ATAN2) {
					op = SymMathBinary.ATAN2;
				} else if (function == name.filieri.antonio.pcp.ast.expr.numeric.NumericBinaryExpression.Function.MAX) {
					op = SymMathBinary.MAX;
				} else if (function == name.filieri.antonio.pcp.ast.expr.numeric.NumericBinaryExpression.Function.MIN) {
					op = SymMathBinary.MIN;
				} else if (function == name.filieri.antonio.pcp.ast.expr.numeric.NumericBinaryExpression.Function.POW) {
					op = SymMathBinary.POW;
				} else {
					throw new RuntimeException("Unknown case: " + function);
				}
				return new SymMathBinary((SymDouble) left, (SymDouble) right, op);
			default:
				throw new RuntimeException("Unknown operation: " + function);
			}
		}

		@Override
		public SymNumber visit(BooleanConstant expr) {
			throw new RuntimeException("Parsing error: this method should not be used!");
		}

		@Override
		public SymNumber visit(BooleanNaryExpression expr) {
			throw new RuntimeException("Parsing error: this method should not be used!");
		}

		@Override
		public SymNumber visit(NotExpression expr) {
			throw new RuntimeException("Parsing error: this method should not be used!");
		}

		@Override
		public SymNumber visit(NumericComparisonExpression expr) {
			throw new RuntimeException("Parsing error: this method should not be used!");
		}
	}
}