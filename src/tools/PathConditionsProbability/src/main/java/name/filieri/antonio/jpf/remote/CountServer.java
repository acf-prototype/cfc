package name.filieri.antonio.jpf.remote;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import name.filieri.antonio.jpf.barvinok.BarvinokException;
import name.filieri.antonio.jpf.barvinok.BarvinokExecutor;
import name.filieri.antonio.jpf.domain.Problem;
import name.filieri.antonio.jpf.remote.CountRequest.Command;
import name.filieri.antonio.jpf.utils.BigRational;
import name.filieri.antonio.jpf.utils.Configuration;

public class CountServer extends Thread {

	private final AtomicBoolean isStopped = new AtomicBoolean(false);
	private final ServerSocket serverSocket;
	private final Configuration configuration;

	public CountServer(Configuration configuration, int port) throws IOException {
		this.serverSocket = new ServerSocket(port);
		this.configuration = configuration;
	}

	public void stopServer() throws IOException {
		isStopped.set(true);
		this.serverSocket.close();
	}

	public void run() {
		while (!isStopped.get()) {
			Socket clientSocket = null;
			try {
				clientSocket = this.serverSocket.accept();
			} catch (IOException e) {
				if (isStopped.get()) {
					System.out.println("Server Stopped.");
					return;
				}
				throw new RuntimeException("Error accepting client connection", e);
			}

			new Thread(new Handler(clientSocket, configuration)).start();
		}

	}

	private static class Handler implements Runnable {
		private static final String DEFAULT_BARVINOK_RELATION = "Rx";
		private static final int DEFAULT_CACHES_SIZE = 10000;
		private final Socket clientSocket;
		private LoadingCache<Problem, BigRational> barvinokCache = null;

		public Handler(Socket clientSocket, final Configuration configuration) {
			this.clientSocket = clientSocket;
			this.barvinokCache = CacheBuilder.newBuilder().maximumSize(DEFAULT_CACHES_SIZE).recordStats()
					.build(new CacheLoader<Problem, BigRational>() {
						public BigRational load(Problem problem)
								throws InterruptedException, IOException, BarvinokException {
							BarvinokExecutor barvinokExecutor = new BarvinokExecutor(configuration);
							return barvinokExecutor.execute(problem.toBarvinok(DEFAULT_BARVINOK_RELATION));
						}
					});
		}

		public void run() {
			try {
				// GZIPInputStream gzipIn = new GZIPInputStream(input);
				ObjectInputStream objectIn = new ObjectInputStream(clientSocket.getInputStream());

				OutputStream output = clientSocket.getOutputStream();
				// GZIPOutputStream gzipOut = new GZIPOutputStream(output);
				ObjectOutputStream objectOut = new ObjectOutputStream(clientSocket.getOutputStream());
				// objectOut.flush();
				// gzipOut.finish();

				Object read;
				while ((read = objectIn.readObject()) != null) {
					CountRequest inputRequest = (CountRequest) read;

					if (inputRequest.getCommand() == Command.STOP) {
						break;
					}

					try {
						BigRational result = this.barvinokCache.get(inputRequest.getProblem());
						CountResponse response = new CountResponse(result);
						objectOut.writeObject(response);
						objectOut.flush();
						// gzipOut.finish();
					} catch (ExecutionException e) {
						objectOut.writeObject(CountResponse.ERROR);
						objectOut.flush();
						// gzipOut.finish();
					}
				}

				objectIn.close();
				objectOut.close();
				clientSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}

	}
	
	public static void main(String[] args) throws IOException, InterruptedException {
		Configuration configuration = new Configuration();
		configuration.setIsccExecutablePath("iscc");
		configuration.setTemporaryDirectory("/tmp/counters/");
		
		CountServer countServer = new CountServer(configuration, 9990);
		countServer.start();
		System.out.println("Started the server");
		countServer.join();
		System.out.println("Finished execution of the server");
		
	}
}
