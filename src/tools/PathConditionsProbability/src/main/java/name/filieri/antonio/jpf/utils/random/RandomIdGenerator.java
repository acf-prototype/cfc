package name.filieri.antonio.jpf.utils.random;

import com.google.inject.ImplementedBy;

@ImplementedBy(TimeBasedRandomIdGenerator.class)
public interface RandomIdGenerator {
	public String getRandomId();
}
