package name.filieri.antonio.pcp;

import com.google.common.base.Preconditions;

import name.filieri.antonio.jpf.utils.BigRational;

public class CountResult {

	public static final CountResult ZERO = new CountResult(BigRational.ZERO, 0);

	public final BigRational probability;
	public final double variance;

	public CountResult(BigRational probability, double variance) {
		super();
		Preconditions.checkArgument(probability.compareTo(1) <= 1);
		Preconditions.checkArgument(variance >= 0);
		this.probability = probability;
		this.variance = variance;
	}

	@Override
	public String toString() {
		return "prob: " + probability + " var: " + variance;
	}
}
