package name.filieri.antonio.pcp.ast.expr;

import name.filieri.antonio.jpf.utils.BigRational;

public interface Variable extends Expression {

	public String getName();
	
	public ExpressionType getType();
	
	public BigRational getDomainSize();
}
