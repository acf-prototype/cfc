package name.filieri.antonio.pcp;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;

import name.filieri.antonio.jpf.utils.ASTUtil;
import name.filieri.antonio.jpf.utils.BigRational;
import name.filieri.antonio.pcp.ast.expr.DependentVariable;
import name.filieri.antonio.pcp.ast.expr.Expression;
import name.filieri.antonio.pcp.ast.expr.ExpressionCategory;
import name.filieri.antonio.pcp.ast.expr.Variable;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanExpression;
import name.filieri.antonio.pcp.visitors.CategoryVisitor;
import name.filieri.antonio.pcp.visitors.DisjunctiveNormalFormVisitor;
import name.filieri.antonio.pcp.visitors.VariableCollector;

public class Constraint {

	private static final HashCode CONSTRAINT_HASH = ASTUtil.getHashFunction().newHasher()
			.putString(Constraint.class.getName(), StandardCharsets.UTF_8)
			.hash();

	private final BooleanExpression expr;
	private HashCode hash = null;
	private Set<Variable> vars = null;
	private Set<ExpressionCategory> category = null;
	private BigRational domainSize = null;

	public Constraint(BooleanExpression expr) {
		Preconditions.checkNotNull(expr);
		this.expr = expr;
	}

	public Set<Variable> getVariables() {
		if (vars == null) {
			vars = VariableCollector.collectVariables(expr);
		}
		return vars;
	}

	public Set<ExpressionCategory> getCategory() {
		if (category == null) {
			category = expr.accept(new CategoryVisitor());
		}
		return category;
	}

	public BigRational getDomainSize() {
		if (domainSize == null) {
			Set<Variable> vars = getVariables();
			BigRational size = BigRational.ONE;

			for (Variable var : vars) {
				if (!(var instanceof DependentVariable))
				size = size.mul(var.getDomainSize());
			}
			domainSize = size;
		}
		return domainSize;
	}

	/**
	 * return the disjunctive normal form of {@code expr}. The cost is
	 * exponential (both in time and space) with the number of disjunctions in
	 * {@code expr}.
	 * 
	 * @return
	 */

	public List<Constraint> toDNF() {
		List<Expression> exprs = DisjunctiveNormalFormVisitor.convertToDNF(expr);
		List<Constraint> dnfCons = new ArrayList<>(exprs.size());

		for (Expression expr : exprs) {
			Preconditions.checkState(expr instanceof BooleanExpression);
			BooleanExpression bExpr = (BooleanExpression) expr;
			dnfCons.add(new Constraint(bExpr));
		}
		return dnfCons;
	}

	public BooleanExpression getExpr() {
		return expr;
	}

	public HashCode getHashCode() {
		if (hash == null) {
			hash = Hashing.combineOrdered(ImmutableList.of(CONSTRAINT_HASH, expr.getHashCode()));
		}
		return hash;
	}

	@Override
	public int hashCode() {
		return getHashCode().asInt();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Constraint other = (Constraint) obj;

		if (this.hashCode() != other.hashCode()) {
			return false;
		}

		if (expr == null) {
			if (other.expr != null)
				return false;
		} else if (!expr.equals(other.expr))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return expr.toString();
	}

}
