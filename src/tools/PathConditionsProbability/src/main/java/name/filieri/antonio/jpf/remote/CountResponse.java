package name.filieri.antonio.jpf.remote;

import java.io.Serializable;

import name.filieri.antonio.jpf.utils.BigRational;

public class CountResponse implements Serializable {
	public enum Status {
		SUCCESS, ERROR
	}

	public static final CountResponse ERROR = new CountResponse(Status.ERROR, null);

	private static final long serialVersionUID = 5538121804251983358L;

	private final Status status;
	private final BigRational result;

	public CountResponse(BigRational result) {
		this(Status.SUCCESS, result);
	}

	public CountResponse(Status status, BigRational result) {
		super();
		this.status = status;
		this.result = result;
	}

	public final Status getStatus() {
		return status;
	}

	public final BigRational getResult() {
		return result;
	}

}
