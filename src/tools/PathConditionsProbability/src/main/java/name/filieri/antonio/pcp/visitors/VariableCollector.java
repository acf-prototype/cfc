package name.filieri.antonio.pcp.visitors;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.google.common.collect.ImmutableSet;

import name.filieri.antonio.pcp.ast.expr.DependentVariable;
import name.filieri.antonio.pcp.ast.expr.Expression;
import name.filieri.antonio.pcp.ast.expr.Variable;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorCompareExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorConstant;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorExtractExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorUnaryExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.BernoulliVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.NormalVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformDoubleVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformFloatVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformIntegerVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformLongVariable;
import name.filieri.antonio.pcp.ast.expr.strings.StringVariable;

public class VariableCollector extends ExpressionBaseVisitor<Void> {

	private Set<Variable> collectedVars;
	
	public VariableCollector() {
		this.collectedVars = new TreeSet<>(new Comparator<Variable>() {
			@Override
			public int compare(Variable o1, Variable o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
	}

	public static Set<Variable> collectVariables(Expression expr) {
		VariableCollector vc = new VariableCollector();
		expr.accept(vc);
		return ImmutableSet.copyOf(vc.collectedVars);
	}

	@Override
	public Void visit(NormalVariable normalVariable) {
		collectedVars.add(normalVariable);
		return emptyResult();
	}

	@Override
	public Void visit(BernoulliVariable bernoulliVariable) {
		collectedVars.add(bernoulliVariable);
		return emptyResult();
	}

	@Override
	public Void visit(UniformFloatVariable uniformFloatVariable) {
		collectedVars.add(uniformFloatVariable);
		return emptyResult();
	}

	@Override
	public Void visit(UniformDoubleVariable uniformDoubleVariable) {
		collectedVars.add(uniformDoubleVariable);
		return emptyResult();
	}

	@Override
	public Void visit(UniformIntegerVariable uniformIntegerVariable) {
		collectedVars.add(uniformIntegerVariable);
		return emptyResult();
	}

	@Override
	public Void visit(UniformLongVariable uniformLongVariable) {
		collectedVars.add(uniformLongVariable);
		return emptyResult();
	}

	@Override
	public Void visit(StringVariable stringVariable) {
		collectedVars.add(stringVariable);
		return emptyResult();
	}
	
	@Override
	public Void visit(DependentVariable dependentVariable) {
		collectedVars.add(dependentVariable);
		return emptyResult();
	}

	@Override
	public Void visit(BitVectorVariable bitVectorVariable) {
		collectedVars.add(bitVectorVariable);
		return emptyResult();
	}

	@Override
	public Void emptyResult() {
		return null; 
	}

	@Override
	public Void mergeResults(List<Void> childResults) {
		return null;
	}
}