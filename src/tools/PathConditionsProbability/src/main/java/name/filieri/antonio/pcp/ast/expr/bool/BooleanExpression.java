package name.filieri.antonio.pcp.ast.expr.bool;

import name.filieri.antonio.pcp.ast.expr.Expression;

public interface BooleanExpression extends Expression {

	public Expression negate();

}
