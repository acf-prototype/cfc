package name.filieri.antonio.pcp.ast.commands;

public interface CommandVisitor<T> {

	T visit(SetOptionCommand setOptionCommand);

	T visit(AssertCommand assertCommand);

	T visit(DeclareConstantCommand declareConstantCommand);

	T visit(ClearCommand clearCommand);

	T visit(VariableDeclarationCommand variableDeclarationCommand);

	T visit(CountCommand countCommand);

	T visit(DependentVariableDeclarationCommand dependentVariableDeclarationCommand);

}
