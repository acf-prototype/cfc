package name.filieri.antonio.pcp.ast.commands;

import name.filieri.antonio.pcp.ast.Node;

/**
 * Node interface for built-in commands (declare-var, etc.)
 */

public interface Command extends Node {

	public enum CommandType {
		SET_OPTION, DECLARE_CONST, DECLARE_VAR, DECLARE_EXPR, ASSERT, COUNT, CLEAR, DECLARE_DEP_VAR
	}
	
	public CommandType getCommandType();
	
	public <T> T accept(CommandVisitor<T> visitor);
}
