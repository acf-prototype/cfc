package name.filieri.antonio.pcp.ast.expr.probabilistic;

import name.filieri.antonio.pcp.ast.expr.BoundedVariable;

public interface ProbabilisticNumericVariable extends BoundedVariable {

}
