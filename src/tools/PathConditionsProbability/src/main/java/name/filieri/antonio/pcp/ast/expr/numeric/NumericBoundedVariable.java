package name.filieri.antonio.pcp.ast.expr.numeric;

import java.nio.charset.StandardCharsets;

import com.google.common.base.Preconditions;
import com.google.common.hash.HashCode;
import com.google.common.hash.Hasher;

import name.filieri.antonio.jpf.utils.ASTUtil;
import name.filieri.antonio.jpf.utils.BigRational;
import name.filieri.antonio.pcp.ast.PositionInfo;
import name.filieri.antonio.pcp.ast.expr.BoundedVariable;
import name.filieri.antonio.pcp.ast.expr.ExpressionType;

public abstract class NumericBoundedVariable implements BoundedVariable {

	private final ExpressionType type;
	private final String name;
	private final Number lowerBound;
	private final Number upperBound;
	private final PositionInfo position;
	private HashCode hash;

	public NumericBoundedVariable(ExpressionType type, String name, Number lowerBound, Number upperBound,
			PositionInfo position) {
		super();
		Preconditions.checkArgument(type.isNumeric(), "Only numeric types are accepted!");
		Preconditions.checkNotNull(name);
		Preconditions.checkArgument(Character.isLetter(name.charAt(0)), "First character of the name must be a letter");
		Preconditions.checkNotNull(lowerBound);
		Preconditions.checkNotNull(upperBound);
		Preconditions.checkArgument(lowerBound.doubleValue() < upperBound.doubleValue(),
				"lower bound is bigger than the upper bound!");
		this.type = type;
		this.name = name;
		this.lowerBound = lowerBound;
		this.upperBound = upperBound;
		this.position = position;
	}

	@Override
	public HashCode getHashCode() {
		if (hash == null) {
			Hasher hasher = ASTUtil.getHashFunction().newHasher()
					.putInt(type.ordinal())
					.putString(name, StandardCharsets.UTF_8);

			switch (type) {
			case DOUBLE:
				hash = hasher.putDouble(upperBound.doubleValue())
						.putDouble(lowerBound.doubleValue())
						.hash();
				break;
			case FLOAT:
				hash = hasher.putFloat(upperBound.floatValue())
						.putFloat(lowerBound.floatValue())
						.hash();
				break;
			case INT:
				hash = hasher.putInt(upperBound.intValue())
						.putInt(lowerBound.intValue())
						.hash();
				break;
			case LONG:
				hash = hasher.putLong(upperBound.longValue())
						.putLong(lowerBound.longValue())
						.hash();
				break;
			default:
				throw new RuntimeException("Not supposed to happen :(");
			}
		}
		return hash;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public int hashCode() {
		return getHashCode().asInt();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NumericBoundedVariable other = (NumericBoundedVariable) obj;

		if (this.hashCode() != other.hashCode()) {
			return false;
		}

		if (lowerBound == null) {
			if (other.lowerBound != null)
				return false;
		} else if (!lowerBound.equals(other.lowerBound))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (type != other.type)
			return false;
		if (upperBound == null) {
			if (other.upperBound != null)
				return false;
		} else if (!upperBound.equals(other.upperBound))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return name; /*- + ":" + type + "[" + lowerBound + "," + upperBound + "]"; */
	}

	@Override
	public Number getUpperBound() {
		return upperBound;
	}

	@Override
	public Number getLowerBound() {
		return lowerBound;
	}

	@Override
	public PositionInfo getPositionInfo() {
		return position;
	}

	@Override
	public ExpressionType getType() {
		return type;
	}

	@Override
	public BigRational getDomainSize() {
		if (type.isFloat()) {
			return BigRational.valueOf(upperBound.doubleValue() - lowerBound.doubleValue());
		} else {
			return BigRational.valueOf(1l + upperBound.longValue() - lowerBound.longValue());
		}
	}
}
