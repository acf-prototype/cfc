package name.filieri.antonio.pcp.ast.expr;

public interface BinaryExpression extends Expression {

	public Expression getLeft();
	public Expression getRight();
}
