package name.filieri.antonio.pcp.ast.commands;

import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;

import name.filieri.antonio.pcp.ast.PositionInfo;
import name.filieri.antonio.pcp.ast.expr.Expression;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanExpression;

public class AssertCommand implements Command {

	private final List<BooleanExpression> exprs;
	private final PositionInfo position;
	private HashCode hash;

	public AssertCommand(List<BooleanExpression> exprs, PositionInfo position) {
		super();
		this.exprs = ImmutableList.copyOf(exprs);
		this.position = position;
	}

	public List<BooleanExpression> getExprs() {
		return exprs;
	}

	@Override
	public HashCode getHashCode() {
		if (hash == null) {
			List<HashCode> hashes = new ArrayList<>(exprs.size());
			for (Expression expr : exprs) {
				hashes.add(expr.getHashCode());
			}
			hash = Hashing.combineOrdered(hashes);
		}
		return hash;
	}

	@Override
	public int hashCode() {
		return getHashCode().asInt();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AssertCommand other = (AssertCommand) obj;

		if (this.hashCode() != other.hashCode()) {
			return false;
		}

		if (exprs == null) {
			if (other.exprs != null)
				return false;
		} else if (!exprs.equals(other.exprs))
			return false;
		if (position == null) {
			if (other.position != null)
				return false;
		} else if (!position.equals(other.position))
			return false;
		return true;
	}

	@Override
	public PositionInfo getPositionInfo() {
		return position;
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.ASSERT;
	}

	@Override
	public <T> T accept(CommandVisitor<T> visitor) {
		return visitor.visit(this);
	}

}
