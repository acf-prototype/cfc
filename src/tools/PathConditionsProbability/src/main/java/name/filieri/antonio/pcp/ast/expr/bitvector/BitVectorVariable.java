package name.filieri.antonio.pcp.ast.expr.bitvector;

import com.google.common.hash.HashCode;
import java.nio.charset.StandardCharsets;
import name.filieri.antonio.jpf.utils.ASTUtil;
import name.filieri.antonio.jpf.utils.BigRational;
import name.filieri.antonio.pcp.ast.PositionInfo;
import name.filieri.antonio.pcp.ast.expr.BoundedVariable;
import name.filieri.antonio.pcp.ast.expr.ExpressionType;
import name.filieri.antonio.pcp.ast.expr.ExpressionVisitor;

/**
 * Created by mateus on 21/03/17.
 */
public class BitVectorVariable implements BoundedVariable {

  private final String name;
  private final int length;
  private final PositionInfo position;
  private HashCode hashCode;

  public BitVectorVariable(String name, int length, PositionInfo position) {
    this.name = name;
    this.length = length;
    this.position = position;
  }

  @Override
  public <T> T accept(ExpressionVisitor<T> visitor) {
    return visitor.visit(this);
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public ExpressionType getType() {
    return ExpressionType.BV;
  }

  @Override
  public BigRational getDomainSize() {
    return new BigRational(2).pow(length);
  }

  @Override
  public HashCode getHashCode() {
    if (hashCode == null) {
      hashCode = ASTUtil.getHashFunction().newHasher()
          .putString(name, StandardCharsets.UTF_8)
          .putInt(length)
          .hash();
    }
    return hashCode;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    BitVectorVariable that = (BitVectorVariable) o;

    if (length != that.length) {
      return false;
    }
    return name.equals(that.name);
  }

  @Override
  public int hashCode() {
    return hashCode.asInt();
  }

  @Override
  public PositionInfo getPositionInfo() {
    return position;
  }

  @Override
  public Number getUpperBound() {
    return getDomainSize().sub(1);
  }

  @Override
  public Number getLowerBound() {
    return 0;
  }

  public int getLength() {
    return length;
  }
}
