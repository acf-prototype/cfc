package name.filieri.antonio.pcp.visitors;

import java.util.ArrayList;
import java.util.List;

import name.filieri.antonio.pcp.ast.expr.DependentVariable;
import name.filieri.antonio.pcp.ast.expr.Expression;
import name.filieri.antonio.pcp.ast.expr.ExpressionVisitor;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorCompareExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorConstant;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorExtractExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorUnaryExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorVariable;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanConstant;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanNaryExpression;
import name.filieri.antonio.pcp.ast.expr.bool.NotExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericComparisonExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericConstant;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericUnaryExpression;
import name.filieri.antonio.pcp.ast.expr.probabilistic.BernoulliVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.NormalVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformDoubleVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformFloatVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformIntegerVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformLongVariable;
import name.filieri.antonio.pcp.ast.expr.strings.StringBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.strings.StringConstant;
import name.filieri.antonio.pcp.ast.expr.strings.StringVariable;

public abstract class ExpressionBaseVisitor<T> implements ExpressionVisitor<T> {

	public abstract T emptyResult();
	
	public abstract T mergeResults(List<T> childResults);
	
	@Override
	public T visit(BooleanConstant booleanConstant) {
		return emptyResult();
	}

	@Override
	public T visit(BooleanNaryExpression booleanNaryExpression) {
		List<T> results = new ArrayList<>();
		for (Expression expr : booleanNaryExpression.getArgs()) {
			results.add(expr.accept(this));
		}
		return mergeResults(results);
	}

	@Override
	public T visit(NotExpression notExpression) {
		return notExpression.getArg().accept(this);
	}

	@Override
	public T visit(NumericBinaryExpression numericBinaryExpression) {
		List<T> results = new ArrayList<>();
		results.add(numericBinaryExpression.getLeft().accept(this));
		results.add(numericBinaryExpression.getRight().accept(this));
		return mergeResults(results);
	}

	@Override
	public T visit(NumericComparisonExpression numericComparisonExpression) {
		List<T> results = new ArrayList<>();
		results.add(numericComparisonExpression.getLeft().accept(this));
		results.add(numericComparisonExpression.getRight().accept(this));
		return mergeResults(results);
	}

	@Override
	public T visit(NumericUnaryExpression numericUnaryExpression) {
		return numericUnaryExpression.getArg().accept(this);
	}

	@Override
	public T visit(NumericConstant numericConstant) {
		return emptyResult();
	}

	@Override
	public T visit(StringConstant stringConstant) {
		return emptyResult();
	}
	
	@Override
	public T visit(NormalVariable normalVariable) {
		return emptyResult();
	}

	@Override
	public T visit(BernoulliVariable bernoulliVariable) {
		return emptyResult();
	}

	@Override
	public T visit(UniformFloatVariable uniformFloatVariable) {
		return emptyResult();
	}

	@Override
	public T visit(UniformDoubleVariable uniformDoubleVariable) {
		return emptyResult();
	}

	@Override
	public T visit(UniformIntegerVariable uniformIntegerVariable) {
		return emptyResult();
	}

	@Override
	public T visit(UniformLongVariable uniformLongVariable) {
		return emptyResult();
	}

	@Override
	public T visit(StringVariable stringVariable) {
		return emptyResult();
	}

	@Override
	public T visit(StringBinaryExpression stringBinaryExpression) {
		List<T> results = new ArrayList<>();
		results.add(stringBinaryExpression.getLeft().accept(this));
		results.add(stringBinaryExpression.getRight().accept(this));
		return mergeResults(results);
	}

	@Override
	public T visit(DependentVariable dependentVariable) {
		return emptyResult();
	}

	@Override
	public T visit(BitVectorConstant bitVectorConstant) {
		return emptyResult();
	}

	@Override
	public T visit(BitVectorBinaryExpression bitVectorBinaryExpression) {
		List<T> results = new ArrayList<>();
		results.add(bitVectorBinaryExpression.getLeft().accept(this));
		results.add(bitVectorBinaryExpression.getRight().accept(this));
		return mergeResults(results);
	}

	@Override
	public T visit(BitVectorExtractExpression bitVectorExtractExpression) {
		return bitVectorExtractExpression.getArg().accept(this);
	}

	@Override
	public T visit(BitVectorCompareExpression bitVectorCompareExpression) {
		List<T> results = new ArrayList<>();
		results.add(bitVectorCompareExpression.getLeft().accept(this));
		results.add(bitVectorCompareExpression.getRight().accept(this));
		return mergeResults(results);
	}

	@Override
	public T visit(BitVectorUnaryExpression bitVectorUnaryExpression) {
		return bitVectorUnaryExpression.getArg().accept(this);
	}

	@Override
	public T visit(BitVectorVariable bitVectorVariable) {
		return emptyResult();
	}
}
