package name.filieri.antonio.pcp.visitors;

import java.util.HashSet;
import java.util.Set;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;

import name.filieri.antonio.pcp.ast.expr.DependentVariable;
import name.filieri.antonio.pcp.ast.expr.Expression;
import name.filieri.antonio.pcp.ast.expr.ExpressionCategory;
import name.filieri.antonio.pcp.ast.expr.ExpressionCategory.Kind;
import name.filieri.antonio.pcp.ast.expr.ExpressionVisitor;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorCompareExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorConstant;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorExtractExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorUnaryExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorVariable;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanConstant;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanNaryExpression;
import name.filieri.antonio.pcp.ast.expr.bool.NotExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericComparisonExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericConstant;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericUnaryExpression;
import name.filieri.antonio.pcp.ast.expr.probabilistic.BernoulliVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.NormalVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformDoubleVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformFloatVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformIntegerVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformLongVariable;
import name.filieri.antonio.pcp.ast.expr.strings.StringBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.strings.StringConstant;
import name.filieri.antonio.pcp.ast.expr.strings.StringVariable;

public class CategoryVisitor implements ExpressionVisitor<Set<ExpressionCategory>> {

	@Override
	public Set<ExpressionCategory> visit(BooleanConstant booleanConstant) {
		return ImmutableSet.of(new ExpressionCategory(Kind.LINEAR, true));
	}

	@Override
	public Set<ExpressionCategory> visit(BooleanNaryExpression expr) {
		Set<ExpressionCategory> argsKind = new HashSet<>();
		for (Expression arg : expr.getArgs()) {
			argsKind.addAll(arg.accept(this));
		}
		return argsKind;
	}

	@Override
	public Set<ExpressionCategory> visit(NotExpression notExpression) {
		return notExpression.getArg().accept(this);
	}

	@Override
	public Set<ExpressionCategory> visit(NumericBinaryExpression expr) {
		Set<ExpressionCategory> left = expr.getLeft().accept(this);
		Set<ExpressionCategory> right = expr.getRight().accept(this);
		Preconditions.checkArgument(left.size() == 1 && right.size() == 1);

		ExpressionCategory leftKind = left.iterator().next();
		ExpressionCategory rightKind = right.iterator().next();
		Preconditions.checkArgument(leftKind.category.isNumeric() && rightKind.category.isNumeric());

		ExpressionCategory exprKind;
		boolean isConstant = leftKind.isConstant && rightKind.isConstant;

		if (isConstant) {
			exprKind = new ExpressionCategory(Kind.LINEAR, true);
		} else {
			switch (expr.getFunction()) {
			case MOD:
			case POW:
			case MIN:
			case MAX:
			case ATAN2:
				exprKind = new ExpressionCategory(Kind.NONLINEAR, false);
				break;
			case DIV:
				if (rightKind.isConstant) {
					exprKind = new ExpressionCategory(leftKind.category, false);
				} else {
					exprKind = new ExpressionCategory(Kind.NONLINEAR, false);
				}
				break;
			case SUB:
			case ADD:
				if (leftKind.isConstant) {
					exprKind = new ExpressionCategory(rightKind.category, false);
				} else if (rightKind.isConstant) {
					exprKind = new ExpressionCategory(leftKind.category, false);
				} else {
					Kind cat;
					if (rightKind.category == Kind.NONLINEAR || leftKind.category == Kind.NONLINEAR) {
						cat = Kind.NONLINEAR;
					} else {
						cat = Kind.LINEAR;
					}
					exprKind = new ExpressionCategory(cat, false);
				}
				break;
			case MUL:
				if (leftKind.isConstant) {
					exprKind = new ExpressionCategory(rightKind.category, false);
				} else if (rightKind.isConstant) {
					exprKind = new ExpressionCategory(leftKind.category, false);
				} else {
					exprKind = new ExpressionCategory(Kind.NONLINEAR, false);
				}
				break;
			default:
				throw new RuntimeException("Unknown case: " + expr.getFunction());
			}
		}

		return ImmutableSet.of(exprKind);
	}

	@Override
	public Set<ExpressionCategory> visit(NumericComparisonExpression expr) {
		Set<ExpressionCategory> left = expr.getLeft().accept(this);
		Set<ExpressionCategory> right = expr.getRight().accept(this);
		Preconditions.checkArgument(left.size() == 1 && right.size() == 1);

		ExpressionCategory leftKind = left.iterator().next();
		ExpressionCategory rightKind = right.iterator().next();
		Preconditions.checkArgument(leftKind.category.isNumeric() && rightKind.category.isNumeric());

		ExpressionCategory exprKind;
		boolean isConstant = leftKind.isConstant && rightKind.isConstant;
		
		if (isConstant) {
			exprKind = new ExpressionCategory(Kind.LINEAR, true);
		} else if (leftKind.category == Kind.LINEAR && rightKind.category == Kind.LINEAR) {
			exprKind = new ExpressionCategory(Kind.LINEAR, false);
		} else {
			exprKind = new ExpressionCategory(Kind.NONLINEAR, false);
		}
		return ImmutableSet.of(exprKind);
	}

	@Override
	public Set<ExpressionCategory> visit(NumericUnaryExpression expr) {
		Set<ExpressionCategory> argKindSet = expr.getArg().accept(this);
		Preconditions.checkArgument(argKindSet.size() == 1);

		ExpressionCategory argKind = argKindSet.iterator().next();
		Preconditions.checkArgument(argKind.category.isNumeric());

		ExpressionCategory exprKind;
		switch (expr.getFunction()) {
		case COS:
		case COSH:
		case EXP:
		case LOG:
		case LOG10:
		case SIN:
		case SINH:
		case SQRT:
		case TAN:
		case TANH:
			exprKind = new ExpressionCategory(Kind.NONLINEAR, argKind.isConstant);
			break;
		default:
			throw new RuntimeException("Unknown case: " + expr.getFunction());
		}

		return ImmutableSet.of(exprKind);
	}

	@Override
	public Set<ExpressionCategory> visit(StringBinaryExpression expr) {
		Set<ExpressionCategory> left = expr.getLeft().accept(this);
		Set<ExpressionCategory> right = expr.getRight().accept(this);
		Preconditions.checkArgument(left.size() == 1 && right.size() == 1);

		ExpressionCategory leftKind = left.iterator().next();
		ExpressionCategory rightKind = right.iterator().next();
		Preconditions.checkArgument(leftKind.category == Kind.STRING);

		ExpressionCategory exprKind;
		// TODO add length
		switch (expr.getFunction()) {
		case CHAR_AT:
		case SUBSTRING:
			Preconditions.checkArgument(rightKind.category.isNumeric());
			if (rightKind.isConstant) {
				exprKind = new ExpressionCategory(Kind.STRING, leftKind.isConstant);
			} else {
				exprKind = new ExpressionCategory(Kind.STRING_NUMERIC, false);
			}
			break;
		case CONTAINS:
		case NOT_CONTAINS:
		case ENDS_WITH:
		case NOT_ENDS_WITH:
		case STARTS_WITH:
		case NOT_STARTS_WITH:
			Preconditions.checkArgument(rightKind.category == Kind.STRING);
			exprKind = new ExpressionCategory(Kind.STRING, leftKind.isConstant && rightKind.isConstant);
			break;
		default:
			throw new RuntimeException("Not implemented: " + expr.getFunction());
		}
		return ImmutableSet.of(exprKind);
	}

	@Override
	public Set<ExpressionCategory> visit(NumericConstant constant) {
		return ImmutableSet.of(new ExpressionCategory(Kind.LINEAR, true));
	}

	@Override
	public Set<ExpressionCategory> visit(StringConstant stringConstant) {
		return ImmutableSet.of(new ExpressionCategory(Kind.STRING, true));
	}

	@Override
	public Set<ExpressionCategory> visit(NormalVariable normalVariable) {
		return ImmutableSet.of(new ExpressionCategory(Kind.LINEAR, false));
	}

	@Override
	public Set<ExpressionCategory> visit(BernoulliVariable bernoulliVariable) {
		return ImmutableSet.of(new ExpressionCategory(Kind.LINEAR, false));
	}

	@Override
	public Set<ExpressionCategory> visit(UniformFloatVariable uniformFloatVariable) {
		return ImmutableSet.of(new ExpressionCategory(Kind.LINEAR, false));
	}

	@Override
	public Set<ExpressionCategory> visit(UniformDoubleVariable uniformDoubleVariable) {
		return ImmutableSet.of(new ExpressionCategory(Kind.LINEAR, false));
	}

	@Override
	public Set<ExpressionCategory> visit(UniformIntegerVariable uniformIntegerVariable) {
		return ImmutableSet.of(new ExpressionCategory(Kind.LINEAR, false));
	}

	@Override
	public Set<ExpressionCategory> visit(UniformLongVariable uniformLongVariable) {
		return ImmutableSet.of(new ExpressionCategory(Kind.LINEAR, false));
	}

	@Override
	public Set<ExpressionCategory> visit(StringVariable stringVariable) {
		return ImmutableSet.of(new ExpressionCategory(Kind.STRING, false));
	}

	@Override
	public Set<ExpressionCategory> visit(DependentVariable dependentVariable) {
		Kind kind;
		switch (dependentVariable.getType()) {
		case BOOLEAN:
		case DOUBLE:
		case FLOAT:
		case INT:
		case LONG:
			kind = Kind.LINEAR;
			break;
		case STRING:
			kind = Kind.STRING;
			break;
		case DATA:
			kind = Kind.DATASTRUCTURE;
			break;
		case BV:
			kind = Kind.BITVECTOR;
			break;
		default:
			throw new RuntimeException("missing case: " + dependentVariable.getType());
		}
		return ImmutableSet.of(new ExpressionCategory(kind, false));
	}

	@Override
	public Set<ExpressionCategory> visit(BitVectorConstant bitVectorConstant) {
		return ImmutableSet.of(new ExpressionCategory(Kind.BITVECTOR, true));
	}

	@Override
	public Set<ExpressionCategory> visit(BitVectorBinaryExpression expr) {
		Set<ExpressionCategory> left = expr.getLeft().accept(this);
		Set<ExpressionCategory> right = expr.getRight().accept(this);
		Preconditions.checkArgument(left.size() == 1 && right.size() == 1);

		ExpressionCategory leftKind = left.iterator().next();
		ExpressionCategory rightKind = right.iterator().next();
		Preconditions.checkArgument(leftKind.category == Kind.BITVECTOR);
		Preconditions.checkArgument(rightKind.category == Kind.BITVECTOR);

		return ImmutableSet.of(new ExpressionCategory(Kind.BITVECTOR,
				leftKind.isConstant && rightKind.isConstant));
	}

	@Override
	public Set<ExpressionCategory> visit(BitVectorExtractExpression expr) {
		Set<ExpressionCategory> arg = expr.getArg().accept(this);
		Preconditions.checkArgument(arg.size() == 1);

		ExpressionCategory leftKind = arg.iterator().next();
		Preconditions.checkArgument(leftKind.category == Kind.BITVECTOR);

		return ImmutableSet.of(new ExpressionCategory(Kind.BITVECTOR,
				leftKind.isConstant));
	}

	@Override
	public Set<ExpressionCategory> visit(BitVectorCompareExpression expr) {
		Set<ExpressionCategory> left = expr.getLeft().accept(this);
		Set<ExpressionCategory> right = expr.getRight().accept(this);
		Preconditions.checkArgument(left.size() == 1 && right.size() == 1);

		ExpressionCategory leftKind = left.iterator().next();
		ExpressionCategory rightKind = right.iterator().next();
		Preconditions.checkArgument(leftKind.category == Kind.BITVECTOR);
		Preconditions.checkArgument(rightKind.category == Kind.BITVECTOR);

		return ImmutableSet.of(new ExpressionCategory(Kind.BITVECTOR,
				leftKind.isConstant && rightKind.isConstant));
	}

	@Override
	public Set<ExpressionCategory> visit(BitVectorUnaryExpression expr) {
		Set<ExpressionCategory> arg = expr.getArg().accept(this);
		Preconditions.checkArgument(arg.size() == 1);

		ExpressionCategory leftKind = arg.iterator().next();
		Preconditions.checkArgument(leftKind.category == Kind.BITVECTOR);

		return ImmutableSet.of(new ExpressionCategory(Kind.BITVECTOR,
				leftKind.isConstant));
	}

	@Override
	public Set<ExpressionCategory> visit(BitVectorVariable bitVectorVariable) {
		return ImmutableSet.of(new ExpressionCategory(Kind.BITVECTOR, false));
	}
}