package name.filieri.antonio.pcp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;

import java.util.stream.Stream;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import name.filieri.antonio.pcp.ast.commands.Command;
import name.filieri.antonio.pcp.ast.commands.CountCommand;
import name.filieri.antonio.pcp.grammar.InputFormatLexer;
import name.filieri.antonio.pcp.grammar.InputFormatParser;
import name.filieri.antonio.pcp.visitors.AstBuilder;
import name.filieri.antonio.pcp.visitors.CommandEvaluator;
import name.filieri.antonio.pcp.visitors.CommandEvaluator.Reply;
import name.filieri.antonio.pcp.visitors.SymbolTable;

public class PCPServer extends Thread {

	private final ServerSocket serverSocket;
	private final CommandEvaluator evaluator;
	private final boolean singleSocket;
	private volatile boolean isStopped = false;

	public PCPServer(int port, boolean singleSocket) throws IOException {
		this.serverSocket = new ServerSocket(port);

		Options options = Options.loadFromUserHomeOrDefault();
		SymbolTable stable = new SymbolTable();
		Partitioner partitioner = new ConstraintPartitioner();
		CountDispatcher dispatcher = new PCPDispatcher(options);
		this.evaluator = new CommandEvaluator(stable, partitioner, dispatcher, options);
		this.singleSocket = singleSocket;
	}

	@Override
	public void run() {
		while (!isStopped) {
			try {
				System.out.println("[pcpserver] Waiting for connection...");
				Socket clientSocket = serverSocket.accept();
				System.out.println("[pcpserver] Connected!");
				BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream(), "UTF-8"));
				PrintWriter out = new PrintWriter(clientSocket.getOutputStream());

				String line;
				while ((line = in.readLine()) != null) {
					System.out.println("[pcpserver] query received: " + line);
					ANTLRInputStream input = new ANTLRInputStream(line);
					InputFormatLexer lexer = new InputFormatLexer(input);
					InputFormatParser parser = new InputFormatParser(new CommonTokenStream(lexer));

					ParseTree tree = parser.input();
					List<? extends Command> commands = AstBuilder.parseCommands(evaluator.getSymbolTable(), tree);

					for (Command command : commands) {
						Reply reply = command.accept(evaluator);
						if (reply.error) {
							out.println("(error \"" + reply.message + "\")");
							out.flush();
						} else {
							if (command instanceof CountCommand) {
								CountResult result = reply.result;
								out.println("(" + result.probability + " " + result.variance + ")");
								out.flush();
							}
						}
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (singleSocket) {
					System.out.println("[PCPServer] Socket closed; finishing server...");
					return;
				}
			}
		}
	}

	public static void main(String[] args) {
		System.out.println("PCPV2 Server started");
		PCPServer server = null;
		try {
			int port;
			if (args.length == 0) {
				port = 9991;
			} else {
				port = Integer.parseInt(args[0]);
			}
			boolean singleSocket = Stream.of(args).anyMatch(s -> s.trim().equals("-ss"));
			server = new PCPServer(port,singleSocket);
			server.start();
			server.join();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			if (server != null) {
				try {
					server.serverSocket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		System.out.println("PCPV2 Server finished");
	}
}