package name.filieri.antonio.pcp.ast.expr.bitvector;

import com.google.common.collect.ImmutableList;
import com.google.common.hash.HashCode;
import com.google.common.hash.Hasher;
import com.google.common.hash.Hashing;
import name.filieri.antonio.jpf.utils.ASTUtil;
import name.filieri.antonio.pcp.ast.PositionInfo;
import name.filieri.antonio.pcp.ast.expr.Expression;
import name.filieri.antonio.pcp.ast.expr.ExpressionVisitor;
import name.filieri.antonio.pcp.ast.expr.UnaryExpression;

/**
 * Indexes must be constant, so this is modeled as an unary expression
 *
 * Created by mateus on 21/03/17.
 */
public class BitVectorExtractExpression implements UnaryExpression {

  private final Expression src;
  private final int hi;
  private final int lo;
  private final PositionInfo posInfo;
  private HashCode hashCode;

  public BitVectorExtractExpression(Expression src, int hi, int lo,
      PositionInfo posInfo) {
    this.src = src;
    this.hi = hi;
    this.lo = lo;
    this.posInfo = posInfo;
  }

  @Override
  public Expression getArg() {
    return src;
  }

  @Override
  public <T> T accept(ExpressionVisitor<T> visitor) {
    return visitor.visit(this);
  }

  @Override
  public HashCode getHashCode() {
    if (hashCode == null) {
      Hasher hasher = ASTUtil.getHashFunction().newHasher()
          .putInt(hi)
          .putInt(lo);
      hashCode = Hashing.combineOrdered(
          ImmutableList.of(src.getHashCode(),hasher.hash()));
    }
    return hashCode;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    BitVectorExtractExpression that = (BitVectorExtractExpression) o;

    if (hi != that.hi) {
      return false;
    }
    if (lo != that.lo) {
      return false;
    }
    return src != null ? src.equals(that.src) : that.src == null;
  }

  @Override
  public int hashCode() {
    return getHashCode().asInt();
  }

  @Override
  public PositionInfo getPositionInfo() {
    return posInfo;
  }

  public int getHi() {
    return hi;
  }

  public int getLo() {
    return lo;
  }
}
