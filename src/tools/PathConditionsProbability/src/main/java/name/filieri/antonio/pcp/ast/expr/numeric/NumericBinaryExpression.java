package name.filieri.antonio.pcp.ast.expr.numeric;

import java.nio.charset.StandardCharsets;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;

import name.filieri.antonio.jpf.utils.ASTUtil;
import name.filieri.antonio.pcp.ast.PositionInfo;
import name.filieri.antonio.pcp.ast.expr.BinaryExpression;
import name.filieri.antonio.pcp.ast.expr.Expression;
import name.filieri.antonio.pcp.ast.expr.ExpressionVisitor;

public class NumericBinaryExpression implements BinaryExpression {

	public enum Function {
		ADD, SUB, MUL, DIV, MOD, POW, MAX, MIN, ATAN2;

		public static Optional<Function> fromSymbol(String text) {
			switch (text) {
			case "+":
				return Optional.of(ADD);
			case "-":
				return Optional.of(SUB);
			case "*":
				return Optional.of(MUL);
			case "/":
				return Optional.of(DIV);
			case "%":
				return Optional.of(MOD);
			case "^":
				return Optional.of(POW);
			case "max":
				return Optional.of(MAX);
			case "min":
				return Optional.of(MIN);
			case "atan2":
				return Optional.of(ATAN2);
			default:
				return Optional.absent();
			}
		}

		public String toSymbol() {
			switch (this) {
			case ADD:
				return "+";
			case ATAN2:
				return "atan2";
			case DIV:
				return "/";
			case MAX:
				return "max";
			case MIN:
				return "min";
			case MOD:
				return "%";
			case MUL:
				return "*";
			case POW:
				return "^";
			case SUB:
				return "-";
			default:
				throw new RuntimeException("Unknown type: " + this);
			}
		}

		public boolean hasSymbol() {
			return this.ordinal() <= POW.ordinal();
		}
	}

	private final Function function;
	private final Expression leftArg;
	private final Expression rightArg;
	private final PositionInfo position;
	private HashCode hash;

	public NumericBinaryExpression(Expression leftArg,
			Expression rightArg, Function function, PositionInfo position) {
		super();
		Preconditions.checkNotNull(leftArg);
		Preconditions.checkNotNull(rightArg);
		this.function = function;
		this.leftArg = leftArg;
		this.rightArg = rightArg;
		this.position = position;
	}

	public NumericBinaryExpression(Expression leftArg,
			Expression rightArg, Function function) {
		this(leftArg, rightArg, function, PositionInfo.DUMMY);
	}

	@Override
	public <T> T accept(ExpressionVisitor<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public HashCode getHashCode() {
		if (hash == null) {
			HashCode hc = ASTUtil.getHashFunction().newHasher()
					.putString(function.name(), StandardCharsets.UTF_8)
					.hash();
			hash = Hashing.combineOrdered(ImmutableList.of(
					hc, leftArg.getHashCode(), rightArg.getHashCode()));
		}
		return hash;
	}

	@Override
	public Expression getLeft() {
		return leftArg;
	}

	@Override
	public Expression getRight() {
		return rightArg;
	}

	public Function getFunction() {
		return function;
	}

	@Override
	public int hashCode() {
		return getHashCode().asInt();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NumericBinaryExpression other = (NumericBinaryExpression) obj;

		if (this.hashCode() != other.hashCode()) {
			return false;
		}

		if (function != other.function)
			return false;
		if (leftArg == null) {
			if (other.leftArg != null)
				return false;
		} else if (!leftArg.equals(other.leftArg))
			return false;
		if (rightArg == null) {
			if (other.rightArg != null)
				return false;
		} else if (!rightArg.equals(other.rightArg))
			return false;
		return true;
	}

	@Override
	public String toString() {
		if (function.hasSymbol()) {
			return "(" + leftArg.toString() + " " + function.toSymbol() + " " + rightArg.toString() + ")";
		} else {
			return function + "(" + leftArg.toString() + "," + rightArg.toString() + ")";
		}
	}

	@Override
	public PositionInfo getPositionInfo() {
		return position;
	}
}
