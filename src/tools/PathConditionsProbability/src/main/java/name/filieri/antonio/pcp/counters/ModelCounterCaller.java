package name.filieri.antonio.pcp.counters;

import java.util.Collection;
import java.util.Map;

import name.filieri.antonio.pcp.Constraint;
import name.filieri.antonio.pcp.CountResult;

public interface ModelCounterCaller {

	public Map<Constraint,CountResult> count(Collection<Constraint> problems) throws ModelCounterException;
}
