package name.filieri.antonio.jpf.analysis;

import java.util.HashSet;
import java.util.Set;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.TokenStream;

import name.filieri.antonio.jpf.domain.Domain;
import name.filieri.antonio.jpf.domain.Problem;
import name.filieri.antonio.jpf.domain.ProblemSetting;
import name.filieri.antonio.jpf.domain.UsageProfile;
import name.filieri.antonio.jpf.domain.exceptions.InvalidUsageProfileException;
import name.filieri.antonio.jpf.grammar.LinearConstraintsLexer;
import name.filieri.antonio.jpf.grammar.LinearConstraintsParser;
import name.filieri.antonio.jpf.utils.BigRational;

public class DummyUniformSettings {
	private final int lowerBound;
	private final int upperBound;

	public DummyUniformSettings(int lowerBound, int upperBound) {
		super();
		this.lowerBound = lowerBound;
		this.upperBound = upperBound;
	}

	public ProblemSetting generateFromTraces(Set<String> traces)
			throws RecognitionException, InvalidUsageProfileException {
		HashSet<String> vars = new HashSet<String>();
		for (String trace : traces) {
			try {
				LinearConstraintsLexer spfLexer = new LinearConstraintsLexer(new ANTLRInputStream(trace));
				TokenStream spfTokenStream = new CommonTokenStream(spfLexer);
				LinearConstraintsParser spfParser = new LinearConstraintsParser(spfTokenStream);
				Problem spfProblem = spfParser.relation().relation0;
				vars.addAll(spfProblem.getVarList().asList());
			} catch (Exception e) {
				System.out.println("Here is the parsing problem for dummy profile:\n" + traces + "\n" + e.getMessage());
				throw new RuntimeException("Just to stop");
			}
		}
		StringBuilder stringBuilder = new StringBuilder();
		Domain.Builder domainBuilder = new Domain.Builder();
		for (String var : vars) {
			domainBuilder.addVariable(var, lowerBound, upperBound);
			stringBuilder.append(var + ">=" + lowerBound + "&&" + var + "<=" + upperBound + "&&");
		}
		Domain domain = domainBuilder.build();
		stringBuilder.delete(stringBuilder.length() - 2, stringBuilder.length());

		UsageProfile.Builder usageProfileBuilder = new UsageProfile.Builder();
		usageProfileBuilder.addScenario(stringBuilder.toString(), BigRational.ONE);

		ProblemSetting problemSetting = new ProblemSetting(domain, usageProfileBuilder.build());
		return problemSetting;

	}
}
