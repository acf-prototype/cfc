package name.filieri.antonio.pcp.ast.expr;

import java.util.List;

public interface NAryExpression extends Expression {

	public List<Expression> getArgs();
}
