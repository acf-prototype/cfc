package name.filieri.antonio.pcp.ast.commands;

import java.nio.charset.StandardCharsets;

import com.google.common.hash.HashCode;

import name.filieri.antonio.jpf.utils.ASTUtil;
import name.filieri.antonio.pcp.ast.PositionInfo;

public class CountCommand implements Command {

	private PositionInfo position;
	private final static HashCode hash = ASTUtil.getHashFunction().newHasher()
			.putString("(count)", StandardCharsets.UTF_8)
			.hash();
	
	public CountCommand(PositionInfo position) {
		this.position = position;
	}

	@Override
	public HashCode getHashCode() {
		return hash;
	}
	
	@Override
	public boolean equals(Object other) {
		return other instanceof CountCommand;
	}
	
	@Override
	public int hashCode() {
		return getHashCode().asInt();
	}

	@Override
	public PositionInfo getPositionInfo() {
		return position;
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.COUNT;
	}

	@Override
	public <T> T accept(CommandVisitor<T> visitor) {
		return visitor.visit(this);
	}

}
