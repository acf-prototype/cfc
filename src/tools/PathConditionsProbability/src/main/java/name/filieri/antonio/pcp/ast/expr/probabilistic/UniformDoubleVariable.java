package name.filieri.antonio.pcp.ast.expr.probabilistic;

import name.filieri.antonio.pcp.ast.PositionInfo;
import name.filieri.antonio.pcp.ast.expr.ExpressionType;
import name.filieri.antonio.pcp.ast.expr.ExpressionVisitor;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericBoundedVariable;

public class UniformDoubleVariable extends NumericBoundedVariable implements ProbabilisticNumericVariable {

	public UniformDoubleVariable(String name, double lb, double ub, PositionInfo position) {
		super(ExpressionType.DOUBLE,name,lb,ub,position);
	}

	public UniformDoubleVariable(String name, double lb, double ub) {
		super(ExpressionType.DOUBLE, name, lb, ub, PositionInfo.DUMMY);
	}

	@Override
	public <T> T accept(ExpressionVisitor<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UniformDoubleVariable other = (UniformDoubleVariable) obj;
		
		if (this.hashCode() != other.hashCode()) {
			return false;
		}
		
		if (Double.doubleToLongBits(getLowerBound().doubleValue()) != Double.doubleToLongBits(other.getLowerBound().doubleValue()))
			return false;
		if (getName() == null) {
			if (other.getName() != null)
				return false;
		} else if (!getName().equals(other.getName()))
			return false;
		if (Double.doubleToLongBits(getUpperBound().doubleValue()) != Double.doubleToLongBits(other.getUpperBound().doubleValue()))
			return false;
		return true;
	}

}
