package name.filieri.antonio.pcp.ast.expr;

public enum ExpressionType {
  BOOLEAN, INT, LONG, FLOAT, DOUBLE, STRING, BV, DATA;

  public boolean isNumeric() {
    return this.equals(INT) || this.equals(LONG) || this.equals(FLOAT) || this.equals(DOUBLE);
  }

  public boolean isFloat() {
    return this.equals(FLOAT) || this.equals(DOUBLE);
  }

  public boolean isInteger() {
    return this.equals(INT) || this.equals(LONG);
  }

  public boolean isBV() {
    return this.equals(BV);
  }
}
