package name.filieri.antonio.jpf.analysis;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.zip.GZIPOutputStream;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.TokenStream;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Sets;

import name.filieri.antonio.jpf.analysis.exceptions.AnalysisException;
import name.filieri.antonio.jpf.analysis.exceptions.EmptyDomainException;
import name.filieri.antonio.jpf.domain.Constraint;
import name.filieri.antonio.jpf.domain.Constraints;
import name.filieri.antonio.jpf.domain.Domain;
import name.filieri.antonio.jpf.domain.Problem;
import name.filieri.antonio.jpf.domain.UsageProfile;
import name.filieri.antonio.jpf.domain.VarList;
import name.filieri.antonio.jpf.grammar.LinearConstraintsLexer;
import name.filieri.antonio.jpf.grammar.LinearConstraintsParser;
import name.filieri.antonio.jpf.latte.LatteException;
import name.filieri.antonio.jpf.omega.exceptions.OmegaException;
import name.filieri.antonio.jpf.remote.CountRequest;
import name.filieri.antonio.jpf.remote.CountResponse;
import name.filieri.antonio.jpf.remote.CountResponse.Status;
import name.filieri.antonio.jpf.utils.BigRational;
import name.filieri.antonio.jpf.utils.ClausesScorer;

public class RemoteAnalyzer implements Analyzer {
	private static final int DEFAULT_CACHES_SIZE = 1000;
	private Set<Problem> domain;
	private UsageProfile usageProfile;
	private LoadingCache<Problem, BigRational> cache;
	private AtomicLong countingTime = new AtomicLong(0);

	public RemoteAnalyzer(Domain domain, UsageProfile usageProfile, final ConnectionManager connectionManager)
			throws LatteException, InterruptedException, OmegaException, UnknownHostException, IOException {
		super();
		this.domain = Sets.newHashSet(domain.asProblem());
		this.usageProfile = usageProfile;
		this.cache = CacheBuilder.newBuilder().maximumSize(DEFAULT_CACHES_SIZE).recordStats()
				.build(new CacheLoader<Problem, BigRational>() {
					public BigRational load(Problem problem)
							throws InterruptedException, IOException, ClassNotFoundException {
						long countStart = System.currentTimeMillis();
						BigRational result = connectionManager.count(problem);
						long countEnd = System.currentTimeMillis();
						countingTime.addAndGet(countEnd - countStart);
						return result;
					}
				});
	}

	public RemoteAnalyzer(Domain domain, UsageProfile usageProfile, LoadingCache<Problem, BigRational> cache)
			throws LatteException, InterruptedException, OmegaException, UnknownHostException, IOException {
		super();
		this.domain = Sets.newHashSet(domain.asProblem());
		this.usageProfile = usageProfile;
		this.cache = cache;
	}

	public final LoadingCache<Problem, BigRational> getCache() {
		return cache;
	}

	public String solversStats() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Total latte time: " + countingTime + " ms (average "
				+ (countingTime.doubleValue() / cache.stats().missCount()) + " ms, on " + cache.stats().missCount()
				+ " invocations)");
		stringBuilder.append('\n');
		return stringBuilder.toString();
	}

	public BigRational analyzeSpfPC(String pc) throws AnalysisException {
		Problem spfProblem = null;
		try {
			LinearConstraintsLexer spfLexer = new LinearConstraintsLexer(new ANTLRInputStream(pc));
			TokenStream spfTokenStream = new CommonTokenStream(spfLexer);
			LinearConstraintsParser spfParser = new LinearConstraintsParser(spfTokenStream);
			spfProblem = spfParser.relation().relation0;
		} catch (RecognitionException e) {
			System.out.println("Cannot parse path condition:\n" + pc);
			e.printStackTrace();
		}

		BigRational probability = BigRational.ZERO;

		// XXX
		for (Problem usageScenario : usageProfile) {
			BigRational probabilityOfTheUsageScenario = usageProfile.getProbability(usageScenario);
			Problem usAndPC = spfProblem.addProblem(usageScenario);
			try {
				BigRational probPerUS = conditionalProbability(usAndPC, usageScenario);
				probability = probability.plus(probPerUS.times(probabilityOfTheUsageScenario));
			} catch (LatteException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}

		}
		System.out.println(solversStats());
		return probability;
	}

	public BigRational conditionalProbability(String pcPastChoices, String pcNextChoice)
			throws LatteException, InterruptedException, ExecutionException, AnalysisException {
		Problem pastChoices = null;
		try {
			LinearConstraintsLexer spfLexer = new LinearConstraintsLexer(new ANTLRInputStream(pcPastChoices));
			TokenStream spfTokenStream = new CommonTokenStream(spfLexer);
			LinearConstraintsParser spfParser = new LinearConstraintsParser(spfTokenStream);
			pastChoices = spfParser.relation().relation0;
		} catch (RecognitionException e) {
			System.out.println("Cannot parse path condition:\n" + pcPastChoices);
			e.printStackTrace();
		}
		Problem nextChoice = null;
		try {
			LinearConstraintsLexer spfLexer = new LinearConstraintsLexer(new ANTLRInputStream(pcNextChoice));
			TokenStream spfTokenStream = new CommonTokenStream(spfLexer);
			LinearConstraintsParser spfParser = new LinearConstraintsParser(spfTokenStream);
			nextChoice = spfParser.relation().relation0;
		} catch (RecognitionException e) {
			System.out.println("Cannot parse path condition:\n" + pcNextChoice);
			e.printStackTrace();
		}
		Problem jointProblem = pastChoices.addProblem(nextChoice);
		return conditionalProbability(jointProblem, pastChoices);
	}

	private BigRational conditionalProbability(Problem aAndB, Problem b)
			throws LatteException, InterruptedException, ExecutionException, AnalysisException {
		if (aAndB.isFalse()) {
			return BigRational.ZERO;
		}
		if (b.isFalse()) {
			throw new RuntimeException("Cannot condition to a false problem: " + b);
		}
		BigRational probability = BigRational.ZERO;

		for (Problem subDomain : domain) {
			BigRational probabilityOfTheSubDomain = simplifyAndCount(subDomain).div(getDomainSize());
			Problem aAndBandDomain = aAndB.addProblem(subDomain);
			Problem bAndDomain = b.addProblem(subDomain);

			BigRational countaAndB = simplifyAndCount(aAndBandDomain);
			if (!countaAndB.equals(BigRational.ZERO)) {
				BigRational countB = simplifyAndCount(bAndDomain);
				BigRational partialProb = countaAndB.divides(countB);
				probability = probability.plus(partialProb.times(probabilityOfTheSubDomain));
			}
		}

		return probability;
	}

	protected final BigRational simplifyAndCount(Problem problem) throws ExecutionException, AnalysisException {
		if (problem.isFalse()) {
			return BigRational.ZERO;
		}
		return simplifyAndCountDivideEtConquer(problem);
	}

	private final BigRational simplifyAndCountNaive(Problem problem) throws ExecutionException {
		if (problem.isFalse()) {
			return BigRational.ZERO;
		}

		BigRational count = cache.get(problem);
		return count;
	}

	private final BigRational simplifyAndCountDivideEtConquer(Problem problem) throws AnalysisException {
		if (problem.isFalse()) {
			return BigRational.ZERO;
		}
		Set<Set<String>> clustersOfVars = problem.getIndependentVarsSubSets();
		BigRational numOfPoints = BigRational.ONE;

		for (Set<String> vars : clustersOfVars) {
			VarList projectionVarList = new VarList(vars);
			HashSet<Constraint> projectionConstraints = new HashSet<Constraint>();

			for (String var : vars) {
				projectionConstraints.addAll(problem.getConstraintsRelatedTo(var));
			}

			Problem projection = new Problem(projectionVarList, new Constraints(projectionConstraints));
			BigRational projectionSize = BigRational.ZERO;
			try {
				projectionSize = simplifyAndCountNaive(projection);
			} catch (ExecutionException e) {
				throw new AnalysisException(e);
			}
			numOfPoints = numOfPoints.mul(projectionSize);
		}
		return numOfPoints;
	}

	public String chachesStats() {
		return "Not supported";
	}

	public String getLatteCache() {
		StringBuilder stringBuilder = new StringBuilder();
		Map<Problem, BigRational> problems = cache.asMap();
		for (Problem problem : problems.keySet()) {
			stringBuilder.append(problem.toString() + "\t" + problem.hashCode() + "\t" + problems.get(problem) + "\n");
		}
		return stringBuilder.toString();
	}

	public BigRational analyzeSetOfSpfPC(Set<String> pcs) throws AnalysisException {
		if (pcs.isEmpty()) {
			return BigRational.ZERO;
		}

		List<String> scoredPCs = new ArrayList<String>(pcs.size());
		scoredPCs.addAll(pcs);
		Collections.sort(scoredPCs, new ClausesScorer());

		if (pcs.size() != scoredPCs.size()) {
			throw new RuntimeException("Stopping because of size");
		}

		BigRational totalProbability = BigRational.ZERO;
		for (String pc : scoredPCs) {
			totalProbability = totalProbability.plus(analyzeSpfPC(pc));
		}
		return totalProbability;
	}

	@SuppressWarnings("unused")
	private final Comparator<Problem> problemComparator = new Comparator<Problem>() {

		public int compare(Problem o1, Problem o2) {
			double o1Score = (o1.isTrue() || o1.isFalse()) ? 1
					: o1.getIndependentVarsSubSets().size() / o1.getVarList().asList().size();
			double o2Score = (o2.isTrue() || o2.isFalse()) ? 1
					: o2.getIndependentVarsSubSets().size() / o2.getVarList().asList().size();
			if (o1Score > o2Score) {
				return -1;
			} else if (o1Score == o2Score) {
				return 0;
			} else {
				return +1;
			}
		}

	};

	@Override
	protected void finalize() throws Throwable {
		super.finalize();
	}

	public BigRational countPointsOfPC(String pc) throws AnalysisException {
		Problem spfProblem = null;
		try {
			LinearConstraintsLexer spfLexer = new LinearConstraintsLexer(new ANTLRInputStream(pc));
			TokenStream spfTokenStream = new CommonTokenStream(spfLexer);
			LinearConstraintsParser spfParser = new LinearConstraintsParser(spfTokenStream);
			spfProblem = spfParser.relation().relation0;
		} catch (RecognitionException e) {
			System.out.println("Cannot parse path condition:\n" + pc);
			e.printStackTrace();
		}

		BigRational numberOfPoints = BigRational.ZERO;

		for (Problem subDomain : domain) {
			Problem pcAndDomain = spfProblem.addProblem(subDomain);
			try {
				numberOfPoints = numberOfPoints.plus(simplifyAndCount(pcAndDomain));
			} catch (ExecutionException e) {
				throw new AnalysisException(e);
			}
		}
		return numberOfPoints;
	}

	public BigRational countPointsOfSetOfPCs(Set<String> pcs) throws AnalysisException {
		BigRational count = BigRational.ZERO;
		for (String pc : pcs) {
			count = count.plus(countPointsOfPC(pc));
		}
		return count;
	}

	public Set<Problem> excludeFromDomain(String pc) throws AnalysisException, EmptyDomainException {
		throw new RuntimeException("Not supported");
	}

	public Set<Problem> excludeFromDomain(Set<String> pcs) throws AnalysisException, EmptyDomainException {
		// TODO make more efficient
		List<String> scoredPCs = new ArrayList<String>(pcs.size());
		scoredPCs.addAll(pcs);
		Collections.sort(scoredPCs, new ClausesScorer());
		for (String pc : scoredPCs) {
			excludeFromDomain(pc);
		}
		return this.domain;
	}

	public BigRational getDomainSize() throws AnalysisException {
		return getDomainSizeNaive();
	}

	public BigRational getDomainSizeNaive() throws AnalysisException {

		BigRational numOfPoints = BigRational.ZERO;
		for (Problem subDomain : domain) {
			try {
				numOfPoints = numOfPoints.plus(simplifyAndCount(subDomain));
			} catch (ExecutionException e) {
				throw new AnalysisException(e);
			}
		}
		return numOfPoints;
	}

	public Set<Problem> complementProblem(Problem problem) throws AnalysisException {
		return complementProblemOmega(problem);
	}

	private Set<Problem> complementProblemOmega(Problem problem) throws AnalysisException {
		throw new RuntimeException("Not supported");
	}

	public static class ConnectionManager {
		private Socket countServer;
		private GZIPOutputStream gzipOut;
		private ObjectOutputStream serverOut;
		private ObjectInputStream serverIn;

		public ConnectionManager(String serverAddress, int serverPort) throws IOException {
			// Initialize connection
			this.countServer = new Socket(serverAddress, serverPort);
			// this.gzipOut = new
			// GZIPOutputStream(countServer.getOutputStream());
			this.serverOut = new ObjectOutputStream(countServer.getOutputStream());
			// serverOut.flush();
			// gzipOut.finish();

			// GZIPInputStream gzipIn = new
			// GZIPInputStream(countServer.getInputStream());
			this.serverIn = new ObjectInputStream(countServer.getInputStream());
		}

		public BigRational count(Problem problem) {
			CountRequest request;
			CountResponse response;
			try {
				request = new CountRequest(problem);
				serverOut.writeObject(request);
				serverOut.flush();
				// gzipOut.finish();
				response = (CountResponse) serverIn.readObject();
			} catch (ClassNotFoundException e) {
				throw new RuntimeException(e);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
			if (response.getStatus() == Status.ERROR) {
				throw new RuntimeException("The count server thrown and error");
			}
			return response.getResult();
		}

		public void close() throws IOException {
			CountRequest request = CountRequest.STOP;
			serverOut.writeObject(request);
			serverOut.flush();
			this.serverOut.close();
			this.serverIn.close();
			this.countServer.close();
		}

	}

	@Override
	public void terminate() {

	}
}