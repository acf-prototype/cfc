package name.filieri.antonio.pcp.ast.expr.strings;

import java.nio.charset.StandardCharsets;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;

import name.filieri.antonio.jpf.utils.ASTUtil;
import name.filieri.antonio.pcp.ast.PositionInfo;
import name.filieri.antonio.pcp.ast.expr.BinaryExpression;
import name.filieri.antonio.pcp.ast.expr.Expression;
import name.filieri.antonio.pcp.ast.expr.ExpressionVisitor;

public class StringBinaryExpression implements BinaryExpression {

	// TODO split in BooleanExpression and NumericExpression?
	public enum Function {
		CHAR_AT, STARTS_WITH, NOT_STARTS_WITH, ENDS_WITH, NOT_ENDS_WITH, SUBSTRING, CONTAINS, NOT_CONTAINS
	};

	private final Expression left;
	private final Expression right;
	private final PositionInfo position;
	private final Function fun;
	private HashCode hash;

	public StringBinaryExpression(Expression left, Expression right, Function fun, PositionInfo position) {
		Preconditions.checkNotNull(left);
		Preconditions.checkNotNull(right);
		
		this.left = left;
		this.right = right;
		this.fun = fun;
		this.position = position;
	}

	@Override
	public <T> T accept(ExpressionVisitor<T> visitor) {
		return visitor.visit(this);
	}

	public Function getFunction() {
		return fun;
	}

	@Override
	public HashCode getHashCode() {
		if (hash == null) {
			HashCode hc = ASTUtil.getHashFunction().newHasher()
					.putString(fun.name(), StandardCharsets.UTF_8)
					.hash();
			hash = Hashing.combineOrdered(ImmutableList.of(
					hc, left.getHashCode(), right.getHashCode()));
		}
		return hash;
	}

	@Override
	public PositionInfo getPositionInfo() {
		return position;
	}

	@Override
	public Expression getLeft() {
		return left;
	}

	@Override
	public Expression getRight() {
		return right;
	}

	@Override
	public int hashCode() {
		return getHashCode().asInt();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StringBinaryExpression other = (StringBinaryExpression) obj;
		
		if (this.hashCode() != other.hashCode()) {
			return false;
		}
		
		if (fun != other.fun)
			return false;
		if (left == null) {
			if (other.left != null)
				return false;
		} else if (!left.equals(other.left))
			return false;
		if (right == null) {
			if (other.right != null)
				return false;
		} else if (!right.equals(other.right))
			return false;
		return true;
	}

}
