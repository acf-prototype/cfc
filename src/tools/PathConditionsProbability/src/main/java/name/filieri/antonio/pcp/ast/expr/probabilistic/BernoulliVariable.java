package name.filieri.antonio.pcp.ast.expr.probabilistic;

import java.nio.charset.StandardCharsets;

import com.google.common.base.Preconditions;
import com.google.common.hash.HashCode;

import name.filieri.antonio.jpf.utils.ASTUtil;
import name.filieri.antonio.jpf.utils.BigRational;
import name.filieri.antonio.pcp.ast.PositionInfo;
import name.filieri.antonio.pcp.ast.expr.ExpressionType;
import name.filieri.antonio.pcp.ast.expr.ExpressionVisitor;

public class BernoulliVariable implements ProbabilisticNumericVariable {

	private final String name;
	private final BigRational probability;
	private final PositionInfo position;
	private HashCode hash;
	
	public BernoulliVariable(String name, BigRational probability, PositionInfo pos) {
		Preconditions.checkNotNull(name);
		Preconditions.checkNotNull(probability);
		this.name = name;
		this.probability = probability;
		this.position = pos;
	}

	@Override
	public Number getUpperBound() {
		return 1;
	}

	@Override
	public Number getLowerBound() {
		return 0;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public ExpressionType getType() {
		return ExpressionType.BOOLEAN;
	}

	@Override
	public <T> T accept(ExpressionVisitor<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public HashCode getHashCode() {
		if (hash == null) {
			hash = ASTUtil.getHashFunction().newHasher()
					.putString(name, StandardCharsets.UTF_8)
					.putDouble(probability.doubleValue())
					.hash();
		}
		return hash;
	}

	@Override
	public PositionInfo getPositionInfo() {
		return position;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((probability == null) ? 0 : probability.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BernoulliVariable other = (BernoulliVariable) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (probability == null) {
			if (other.probability != null)
				return false;
		} else if (!probability.equals(other.probability))
			return false;
		return true;
	}

	@Override
	public BigRational getDomainSize() {
		return BigRational.valueOf(2);
	}

	public BigRational getProbability() {
		return probability;
	}
}
