package name.filieri.antonio.pcp.ast.expr;

public interface BoundedVariable extends Variable {

	public Number getUpperBound();
	
	public Number getLowerBound();
}
