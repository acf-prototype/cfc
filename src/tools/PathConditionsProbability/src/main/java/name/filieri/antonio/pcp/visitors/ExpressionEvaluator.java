package name.filieri.antonio.pcp.visitors;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import name.filieri.antonio.pcp.ExpressionResult;
import name.filieri.antonio.pcp.ast.expr.BoundedVariable;
import name.filieri.antonio.pcp.ast.expr.Constant;
import name.filieri.antonio.pcp.ast.expr.DependentVariable;
import name.filieri.antonio.pcp.ast.expr.Expression;
import name.filieri.antonio.pcp.ast.expr.ExpressionType;
import name.filieri.antonio.pcp.ast.expr.ExpressionVisitor;
import name.filieri.antonio.pcp.ast.expr.Variable;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorCompareExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorConstant;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorExtractExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorUnaryExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorVariable;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanConstant;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanNaryExpression;
import name.filieri.antonio.pcp.ast.expr.bool.NotExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericComparisonExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericComparisonExpression.Function;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericConstant;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericUnaryExpression;
import name.filieri.antonio.pcp.ast.expr.probabilistic.BernoulliVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.NormalVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformDoubleVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformFloatVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformIntegerVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformLongVariable;
import name.filieri.antonio.pcp.ast.expr.strings.StringBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.strings.StringConstant;
import name.filieri.antonio.pcp.ast.expr.strings.StringVariable;

public class ExpressionEvaluator implements ExpressionVisitor<ExpressionResult> {

	public final Map<Variable, Constant> inputValues;

	public ExpressionEvaluator(Map<Variable, Constant> inputValues) {
		this.inputValues = inputValues;
	}

	public ExpressionEvaluator() {
		this.inputValues = new HashMap<>();
	}
	
	@Override
	public ExpressionResult visit(StringConstant node) {
		return new ExpressionResult(ExpressionType.STRING, node.getValue());
	}

	@Override
	public ExpressionResult visit(NumericConstant node) {
		return new ExpressionResult(node.getType(), node.getValue());
	}

	@Override
	public ExpressionResult visit(NotExpression node) {
		ExpressionResult childResult = node.getArg().accept(this);
		if (childResult.type != ExpressionType.BOOLEAN) {
			throw new RuntimeException("Non-boolean expression evaluated at " + node.getPositionInfo());
		} else {
			if (childResult.numericResult.intValue() == 0) {
				return new ExpressionResult(ExpressionType.BOOLEAN, 1);
			} else {
				return new ExpressionResult(ExpressionType.BOOLEAN, 0);
			}
		}
	}

	@Override
	public ExpressionResult visit(NumericUnaryExpression node) {
		ExpressionResult exprValue = node.getArg().accept(this);
		ExpressionResult wrappedResult;

		if (exprValue.type.isNumeric()) {
			// All functions in here receive 'double' values as argument
			double arg = exprValue.numericResult.doubleValue();
			double result;
			switch (node.getFunction()) {
			case COS:
				result = Math.cos(arg);
				break;
			case COSH:
				result = Math.cosh(arg);
				break;
			case EXP:
				result = Math.exp(arg);
				break;
			case LOG:
				result = Math.log(arg);
				break;
			case LOG10:
				result = Math.log10(arg);
				break;
			case SIN:
				result = Math.sin(arg);
				break;
			case SINH:
				result = Math.sinh(arg);
				break;
			case SQRT:
				result = Math.sqrt(arg);
				break;
			case TAN:
				result = Math.tan(arg);
				break;
			case TANH:
				result = Math.tanh(arg);
				break;
			default:
				throw new RuntimeException("Not implemented! " + node);
			}
			wrappedResult = new ExpressionResult(ExpressionType.DOUBLE, result);
		} else {
			throw new RuntimeException("Not Implemented! " + node);
		}

		return wrappedResult;
	}

	@Override
	public ExpressionResult visit(NumericBinaryExpression node) {
		ExpressionResult leftResult = node.getLeft().accept(this);
		ExpressionResult rightResult = node.getRight().accept(this);

		ExpressionType ltype = leftResult.type;
		ExpressionType rtype = rightResult.type;

		ExpressionResult wrappedResult;

		if (ltype.isNumeric() && rtype.isNumeric()) {
			ExpressionType finalType;

			// choose the "widest" type
			switch (node.getFunction()) {
			case ATAN2:
			case POW:
				finalType = ExpressionType.DOUBLE;
				break;
			case MOD:
				finalType = (ltype == ExpressionType.INT && rtype == ExpressionType.INT)
						? ExpressionType.INT : ExpressionType.LONG;
				break;
			default:
				finalType = ltype.ordinal() > rtype.ordinal() ? ltype : rtype;
				break;
			}

			Number lval = leftResult.numericResult;
			Number rval = rightResult.numericResult;

			switch (finalType) {
			case DOUBLE:
				wrappedResult = processDoubleExpr(node, lval, rval);
				break;
			case FLOAT:
				wrappedResult = processFloatExpr(node, lval, rval);
				break;
			case INT:
				wrappedResult = processIntExpr(node, lval, rval);
				break;
			case LONG:
				wrappedResult = processLongExpr(node, lval, rval);
				break;
			default:
				throw new RuntimeException("Not supposed to happen :(");
			}

		} else {
			throw new RuntimeException("Not implemented: " + node);
		}

		return wrappedResult;
	}

	@Override
	public ExpressionResult visit(NumericComparisonExpression node) {
		boolean result;
		ExpressionResult left = node.getLeft().accept(this);
		ExpressionResult right = node.getRight().accept(this);
		ExpressionType ltype = left.type;
		ExpressionType rtype = right.type;

		if (ltype.isNumeric() && rtype.isNumeric()) {
			ExpressionType wideningType = ltype.ordinal() > rtype.ordinal()
					? ltype : rtype;

			switch (wideningType) {
			case DOUBLE:
				result = compareDoubles(node.getFunction(), left.numericResult, right.numericResult);
				break;
			case FLOAT:
				result = compareFloats(node.getFunction(), left.numericResult, right.numericResult);
				break;
			case INT:
				result = compareInts(node.getFunction(), left.numericResult, right.numericResult);
				break;
			case LONG:
				result = compareLongs(node.getFunction(), left.numericResult, right.numericResult);
				break;
			default:
				throw new RuntimeException("Not supposed to happen :(");
			}

		} else {
			throw new RuntimeException("Non-numeric expression evaluated: " + node);
		}
		return new ExpressionResult(ExpressionType.BOOLEAN, result ? 1 : 0);
	}

	@Override
	public ExpressionResult visit(BooleanNaryExpression node) {
		List<Expression> args = node.getArgs();
		BooleanNaryExpression.Function function = node.getFunction();
		Boolean result = null;

		if (args.size() < 2) {
			throw new RuntimeException("Boolean operation need at least two arguments : " + node.toString());
		}

		for (Expression expr : args) {
			ExpressionResult r = expr.accept(this);
			if (r.type != ExpressionType.BOOLEAN) {
				throw new RuntimeException("Non-boolean expression in: " + node);
			}

			Boolean rVal = r.numericResult.intValue() == 0
					? false : true;
			if (result == null) {
				result = rVal;
			} else {
				switch (function) {
				case AND:
					result = result && rVal;
					break;
				case OR:
					result = result || rVal;
					break;
				default:
					throw new RuntimeException("Not implemented yet: " + function);
				}
			}
		}
		return new ExpressionResult(ExpressionType.BOOLEAN, result ? 1 : 0);
	}

	@Override
	public ExpressionResult visit(BooleanConstant node) {
		return new ExpressionResult(ExpressionType.BOOLEAN, node.getValue() ? 1 : 0);
	}

	@Override
	public ExpressionResult visit(NormalVariable normalVariable) {
		return handleBoundedVariables(normalVariable);
	}

	private ExpressionResult processDoubleExpr(NumericBinaryExpression node, Number lval, Number rval) {
		ExpressionResult wrappedResult;

		switch (node.getFunction()) {
		case ADD:
			wrappedResult = new ExpressionResult(ExpressionType.DOUBLE,
					lval.doubleValue() + rval.doubleValue());
			break;
		case ATAN2:
			wrappedResult = new ExpressionResult(ExpressionType.DOUBLE,
					lval.doubleValue() + rval.doubleValue());
			break;
		case DIV:
			wrappedResult = new ExpressionResult(ExpressionType.DOUBLE,
					lval.doubleValue() / rval.doubleValue());
			break;
		case MAX:
			wrappedResult = new ExpressionResult(ExpressionType.DOUBLE,
					Math.max(lval.doubleValue(), rval.doubleValue()));
			break;
		case MIN:
			wrappedResult = new ExpressionResult(ExpressionType.DOUBLE,
					Math.min(lval.doubleValue(), rval.doubleValue()));
			break;
		case MUL:
			wrappedResult = new ExpressionResult(ExpressionType.DOUBLE,
					lval.doubleValue() * rval.doubleValue());
			break;
		case POW:
			wrappedResult = new ExpressionResult(ExpressionType.DOUBLE,
					Math.pow(lval.doubleValue(), rval.doubleValue()));
			break;
		case SUB:
			wrappedResult = new ExpressionResult(ExpressionType.DOUBLE,
					lval.doubleValue() - rval.doubleValue());
			break;
		default:
			throw new RuntimeException("Not implemented: " + node);
		}
		return wrappedResult;
	}

	private ExpressionResult processFloatExpr(NumericBinaryExpression node, Number lval, Number rval) {
		ExpressionResult wrappedResult;

		switch (node.getFunction()) {
		case ADD:
			wrappedResult = new ExpressionResult(ExpressionType.FLOAT,
					lval.floatValue() + rval.floatValue());
			break;
		case DIV:
			wrappedResult = new ExpressionResult(ExpressionType.FLOAT,
					lval.floatValue() / rval.floatValue());
			break;
		case MAX:
			wrappedResult = new ExpressionResult(ExpressionType.FLOAT,
					Math.max(lval.floatValue(), rval.floatValue()));
			break;
		case MIN:
			wrappedResult = new ExpressionResult(ExpressionType.FLOAT,
					Math.min(lval.floatValue(), rval.floatValue()));
			break;
		case MUL:
			wrappedResult = new ExpressionResult(ExpressionType.FLOAT,
					lval.floatValue() * rval.floatValue());
			break;
		case SUB:
			wrappedResult = new ExpressionResult(ExpressionType.FLOAT,
					lval.floatValue() - rval.floatValue());
			break;
		default:
			throw new RuntimeException("Not implemented: " + node);
		}
		return wrappedResult;
	}

	private ExpressionResult processIntExpr(NumericBinaryExpression node, Number lval, Number rval) {
		ExpressionResult wrappedResult;

		switch (node.getFunction()) {
		case ADD:
			wrappedResult = new ExpressionResult(ExpressionType.INT,
					lval.intValue() + rval.intValue());
			break;
		case DIV:
			wrappedResult = new ExpressionResult(ExpressionType.INT,
					lval.intValue() / rval.intValue());
			break;
		case MAX:
			wrappedResult = new ExpressionResult(ExpressionType.INT,
					Math.max(lval.intValue(), rval.intValue()));
			break;
		case MIN:
			wrappedResult = new ExpressionResult(ExpressionType.INT,
					Math.min(lval.intValue(), rval.intValue()));
			break;
		case MOD:
			wrappedResult = new ExpressionResult(ExpressionType.INT,
					lval.intValue() % rval.intValue());
			break;
		case MUL:
			wrappedResult = new ExpressionResult(ExpressionType.INT,
					lval.intValue() * rval.intValue());
			break;
		case SUB:
			wrappedResult = new ExpressionResult(ExpressionType.INT,
					lval.intValue() - rval.intValue());
			break;
		default:
			throw new RuntimeException("Not implemented: " + node);
		}

		return wrappedResult;
	}

	private ExpressionResult processLongExpr(NumericBinaryExpression node, Number lval, Number rval) {
		ExpressionResult wrappedResult;

		switch (node.getFunction()) {
		case ADD:
			wrappedResult = new ExpressionResult(ExpressionType.LONG,
					lval.longValue() + rval.longValue());
			break;
		case DIV:
			wrappedResult = new ExpressionResult(ExpressionType.LONG,
					lval.longValue() / rval.longValue());
			break;
		case MAX:
			wrappedResult = new ExpressionResult(ExpressionType.LONG,
					Math.max(lval.longValue(), rval.longValue()));
			break;
		case MIN:
			wrappedResult = new ExpressionResult(ExpressionType.LONG,
					Math.min(lval.longValue(), rval.longValue()));
			break;
		case MOD:
			wrappedResult = new ExpressionResult(ExpressionType.LONG,
					lval.longValue() % rval.longValue());
			break;
		case MUL:
			wrappedResult = new ExpressionResult(ExpressionType.LONG,
					lval.longValue() * rval.longValue());
			break;
		case SUB:
			wrappedResult = new ExpressionResult(ExpressionType.LONG,
					lval.longValue() - rval.longValue());
			break;
		default:
			throw new RuntimeException("Not implemented: " + node);
		}

		return wrappedResult;
	}

	private boolean compareLongs(Function function, Number a, Number b) {
		switch (function) {
		case EQ:
			return a.longValue() == b.longValue();
		case GE:
			return a.longValue() >= b.longValue();
		case GT:
			return a.longValue() > b.longValue();
		case LE:
			return a.longValue() <= b.longValue();
		case LT:
			return a.longValue() < b.longValue();
		case NE:
			return a.longValue() != b.longValue();
		default:
			throw new RuntimeException("Not implemented yet: " + function);
		}
	}

	private boolean compareInts(Function function, Number a, Number b) {
		switch (function) {
		case EQ:
			return a.intValue() == b.intValue();
		case GE:
			return a.intValue() >= b.intValue();
		case GT:
			return a.intValue() > b.intValue();
		case LE:
			return a.intValue() <= b.intValue();
		case LT:
			return a.intValue() < b.intValue();
		case NE:
			return a.intValue() != b.intValue();
		default:
			throw new RuntimeException("Not implemented yet: " + function);
		}
	}

	private boolean compareFloats(Function function, Number a, Number b) {
		switch (function) {
		case EQ:
			return a.floatValue() == b.floatValue();
		case GE:
			return a.floatValue() >= b.floatValue();
		case GT:
			return a.floatValue() > b.floatValue();
		case LE:
			return a.floatValue() <= b.floatValue();
		case LT:
			return a.floatValue() < b.floatValue();
		case NE:
			return a.floatValue() != b.floatValue();
		default:
			throw new RuntimeException("Not implemented yet: " + function);
		}
	}

	private boolean compareDoubles(Function function, Number a, Number b) {
		switch (function) {
		case EQ:
			return a.doubleValue() == b.doubleValue();
		case GE:
			return a.doubleValue() >= b.doubleValue();
		case GT:
			return a.doubleValue() > b.doubleValue();
		case LE:
			return a.doubleValue() <= b.doubleValue();
		case LT:
			return a.doubleValue() < b.doubleValue();
		case NE:
			return a.doubleValue() != b.doubleValue();
		default:
			throw new RuntimeException("Not implemented yet: " + function);
		}
	}

	private ExpressionResult handleBoundedVariables(BoundedVariable numericVariable) {
		Constant c = inputValues.get(numericVariable);
		if (c == null) {
			throw new RuntimeException("Unknown value for variable: " + numericVariable);
		} else if (c instanceof NumericConstant) {
			return new ExpressionResult(numericVariable.getType(), ((NumericConstant) c).getValue());
		} else {
			throw new RuntimeException("Value for variable " + numericVariable +
					" is not a numeric constant! [" + c.getClass() + "]");
		}
	}

	@Override
	public ExpressionResult visit(BernoulliVariable bernoulliVariable) {
		Constant c = inputValues.get(bernoulliVariable);
		if (c == null) {
			throw new RuntimeException("Unknown value for variable: " + bernoulliVariable);
		} else if (c instanceof NumericConstant) {
			return new ExpressionResult(ExpressionType.BOOLEAN, ((BooleanConstant) c).getValue() ? 1 : 0);
		} else {
			throw new RuntimeException("Value for variable " + bernoulliVariable +
					" is not boolean! [" + c.getClass() + "]");
		}
	}

	@Override
	public ExpressionResult visit(UniformFloatVariable uniformFloatVariable) {
		return handleBoundedVariables(uniformFloatVariable);
	}

	@Override
	public ExpressionResult visit(UniformDoubleVariable uniformDoubleVariable) {
		return handleBoundedVariables(uniformDoubleVariable);
	}
	
	@Override
	public ExpressionResult visit(UniformIntegerVariable uniformIntegerVariable) {
		return handleBoundedVariables(uniformIntegerVariable);
	}
	
	@Override
	public ExpressionResult visit(UniformLongVariable uniformLongVariable) {
		return handleBoundedVariables(uniformLongVariable);
	}

	@Override
	public ExpressionResult visit(StringBinaryExpression binaryStringExpression) {
		throw new RuntimeException("Not implemented yet");
	}

	@Override
	public ExpressionResult visit(StringVariable stringVariable) {
		Constant c = inputValues.get(stringVariable);
		if (c == null) {
			throw new RuntimeException("Unknown value for variable: " + stringVariable);
		} else if (c instanceof StringConstant) {
			return new ExpressionResult(ExpressionType.STRING, ((NumericConstant) c).getValue());
		} else {
			throw new RuntimeException("Value for variable " + stringVariable +
					" is not a string constant! [" + c.getClass() + "]");
		}
	}

	@Override
	public ExpressionResult visit(DependentVariable dependentVariable) {
		throw new RuntimeException("Not implemented yet");
	}

	@Override
	public ExpressionResult visit(BitVectorConstant constant) {
		return new ExpressionResult(ExpressionType.BV, constant.getValue().toString(2));
	}

	@Override
	public ExpressionResult visit(BitVectorBinaryExpression bitVectorBinaryExpression) {
		throw new RuntimeException("Not implemented yet");
	}

	@Override
	public ExpressionResult visit(BitVectorExtractExpression bitVectorExtractExpression) {
		throw new RuntimeException("Not implemented yet");
	}

	@Override
	public ExpressionResult visit(BitVectorCompareExpression bitVectorCompareExpression) {
		throw new RuntimeException("Not implemented yet");
	}

	@Override
	public ExpressionResult visit(BitVectorUnaryExpression bitVectorUnaryExpression) {
		throw new RuntimeException("Not implemented yet");
	}

	@Override
	public ExpressionResult visit(BitVectorVariable bitVectorVariable) {
		throw new RuntimeException("Not implemented yet");
	}
}