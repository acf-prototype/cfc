package name.filieri.antonio.pcp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Reader;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Scanner;

public class Options {

	public enum Key {
		ISCC_PATH,
		SHARPSAT_PATH,
		Z3_PATH,
		SEED,
		CONVERT_TO_DNF,
		PARTITIONING,
		NON_LINEAR_COUNTER,
		REALPAVER_PATH
	}

	private static final String DEFAULT_CONFIGURATION_FILENAME = ".pcp";
	
	private Map<Key,String> optionMap;
	
	public static void main(String[] args) {
		Options options = Options.loadFromUserHomeOrDefault();
		String isccpath = options.getIsccPath();
		System.out.println(isccpath);
	}

	public Options() {
		this.optionMap = new LinkedHashMap<>();
		// Setting default values
		this.optionMap.put(Key.PARTITIONING, Boolean.TRUE.toString());
		this.optionMap.put(Key.CONVERT_TO_DNF, Boolean.TRUE.toString());
		this.optionMap.put(Key.SEED, new Long(77105613).toString());
		this.optionMap.put(Key.ISCC_PATH, "iscc");
		this.optionMap.put(Key.NON_LINEAR_COUNTER, "qcoral");
		this.optionMap.put(Key.REALPAVER_PATH, "realpaver");
	}

	public boolean isPartitioningEnabled() {
		return checkOption(Key.PARTITIONING,"true");
	}

	public boolean isDNFConversionEnabled() {
		return checkOption(Key.CONVERT_TO_DNF,"true");
	}

	public String getIsccPath() {
		return this.optionMap.get(Key.ISCC_PATH);
	}
	
	public boolean useQCoral() {
		return checkOption(Key.NON_LINEAR_COUNTER,"qcoral");
	}

	public boolean useSharpSAT() {
		return checkOption(Key.NON_LINEAR_COUNTER,"sharpsat");
	}
	
	public boolean useZ3() {
		return checkOption(Key.NON_LINEAR_COUNTER,"z3");
	}
	
	public long getSeed() {
		return Long.parseLong(this.optionMap.get(Key.SEED));
	}

	public String getSharpSatPath() {
		String ret = optionMap.get(Key.SHARPSAT_PATH);
		if (ret == null) {
			ret = "sharpSAT";
		}
		return ret;
	}
	
	public String getRealPaverPath() {
		String ret = optionMap.get(Key.REALPAVER_PATH);
		if (ret == null) {
			ret = "realpaver";
		}
		return ret;
	}

	private static Optional<Options> cachedOptions = Optional.empty();

	public static Options loadFromUserHomeOrDefault() {
	  if (cachedOptions.isPresent()) {
	    return cachedOptions.get();
    } else {
      Options options = new Options();
      String homeFolder = System.getProperty("user.home");
      File defaultConfiguration = new File(homeFolder, Options.DEFAULT_CONFIGURATION_FILENAME);
      if (defaultConfiguration.exists()) {
        options.loadFromFile(defaultConfiguration);
      }
      cachedOptions = Optional.of(options);
      return options;
    }
	}

	public static void reload() {
	  cachedOptions = Optional.empty();
  }

	public void loadFromFile(String path) {
		this.loadFromFile(new File(path));
	}

	public void loadFromFile(File file) {
		try (FileInputStream in = new FileInputStream(file)) {
			Scanner sc = new Scanner(in);
			while (sc.hasNextLine()) {
				String line = sc.nextLine();
				int commentPos = line.indexOf('#');
				if (commentPos >= 0) {
					line = line.trim().substring(0, commentPos).trim();
				}
				if (!line.isEmpty()) {
					String[] toks = line.split("=");
					String key = toks[0];
					String val = toks[1];
					processOption(key,val);	
				}
			}
			sc.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void processOption(String key, String val) {
		key = key.trim();
		key = key.replace('-', '_').toUpperCase();
		if (key.startsWith(":")) {
			key = key.substring(1);
		}
		
		Key option = Key.valueOf(key);
		optionMap.put(option, val.trim());
	}
	
	public boolean checkOption(Key key, String val) {
		if (optionMap.containsKey(key)) {
			String storedValue = optionMap.get(key);
			return storedValue.equalsIgnoreCase(val);
		}
		return false;
	}

	public boolean checkIfBarvinokIsAccessible() {
		boolean isAccessible = false;
		Runtime rt = Runtime.getRuntime();
		Process proc;
		try {
			proc = rt.exec(getIsccPath()+" --help");
			proc.waitFor();
			int exitVal = proc.exitValue();
      isAccessible = (exitVal == 0);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return isAccessible;
	}

  public boolean checkIfSharpsatIsAccessible() {
    boolean isAccessible = false;
    Runtime rt = Runtime.getRuntime();
    Process proc;
    try {
      proc = rt.exec(getSharpSatPath());
      proc.waitFor();
      int exitVal = proc.exitValue();
      // Vladmir's fork of sharpsat returns 255 if called without files,
      // and 0 if the file doesn't exists.
      isAccessible = (exitVal == 255);
    } catch (IOException e) {
      e.printStackTrace();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    return isAccessible;
  }
}
