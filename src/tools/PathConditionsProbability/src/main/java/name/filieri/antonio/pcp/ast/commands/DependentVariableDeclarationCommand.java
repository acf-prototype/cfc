package name.filieri.antonio.pcp.ast.commands;

import java.nio.charset.StandardCharsets;

import com.google.common.collect.ImmutableList;
import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;

import name.filieri.antonio.jpf.utils.ASTUtil;
import name.filieri.antonio.pcp.ast.PositionInfo;
import name.filieri.antonio.pcp.ast.expr.DependentVariable;

public class DependentVariableDeclarationCommand implements Command {

	private final DependentVariable var;
	private final PositionInfo position;
	private HashCode hash;

	public DependentVariableDeclarationCommand(DependentVariable var, PositionInfo position) {
		this.var = var;
		this.position = position;
	}

	private static final HashCode commandHash = ASTUtil.getHashFunction().newHasher()
			.putString("(declare-dependent-var)", StandardCharsets.UTF_8)
			.hash();

	@Override
	public HashCode getHashCode() {
		if (hash == null) {
			hash = Hashing.combineOrdered(ImmutableList.of(commandHash, var.getHashCode()));
		}
		return hash;
	}

	@Override
	public PositionInfo getPositionInfo() {
		return position;
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.DECLARE_DEP_VAR;
	}

	@Override
	public <T> T accept(CommandVisitor<T> visitor) {
		return visitor.visit(this);
	}

	public DependentVariable getVar() {
		return var;
	}

	public PositionInfo getPosition() {
		return position;
	}

}
