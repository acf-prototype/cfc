package name.filieri.antonio.pcp.ast.expr.bool;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;

import name.filieri.antonio.jpf.utils.ASTUtil;
import name.filieri.antonio.pcp.ast.PositionInfo;
import name.filieri.antonio.pcp.ast.expr.Expression;
import name.filieri.antonio.pcp.ast.expr.ExpressionVisitor;
import name.filieri.antonio.pcp.ast.expr.NAryExpression;

public class BooleanNaryExpression implements NAryExpression, BooleanExpression {

	public enum Function {
		AND, OR;

		private Function flipDeMorgan() {
			switch (this) {
			case AND:
				return OR;
			case OR:
				return AND;
			default:
				throw new RuntimeException("Unknown type: " + this);
			}
		}
	}

	private final Function function;
	private final List<BooleanExpression> args;
	private final PositionInfo position;
	private HashCode hash;

	public BooleanNaryExpression(Function function, List<BooleanExpression> args, PositionInfo position) {
		super();
		Preconditions.checkNotNull(args);
		this.function = function;
		this.args = ImmutableList.copyOf(args);
		this.position = position;
	}

	public BooleanNaryExpression(Function and, List<BooleanExpression> of) {
		this(and, of, PositionInfo.DUMMY);
	}

	@Override
	public <T> T accept(ExpressionVisitor<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public HashCode getHashCode() {
		if (hash == null) {
			HashCode hc = ASTUtil.getHashFunction().newHasher()
					.putInt(function.ordinal())
					.hash();
			List<HashCode> hashes = new ArrayList<>();
			hashes.add(hc);
			for (Expression exp : args) {
				hashes.add(exp.getHashCode());
			}
			hash = Hashing.combineOrdered(hashes);
		}
		return hash;
	}

	@Override
	public int hashCode() {
		return getHashCode().asInt();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BooleanNaryExpression other = (BooleanNaryExpression) obj;

		if (this.hashCode() != other.hashCode()) {
			return false;
		}

		if (args == null) {
			if (other.args != null)
				return false;
		} else if (!args.equals(other.args))
			return false;
		if (function != other.function)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return function + "(" + args + ")";
	}

	@Override
	public PositionInfo getPositionInfo() {
		return position;
	}

	@Override
	public List<Expression> getArgs() {
		return ImmutableList.<Expression>copyOf(args);
	}

	public List<BooleanExpression> getBooleanArgs() {
		return args;
	}

	public Function getFunction() {
		return function;
	}

	@Override
	public Expression negate() {
		List<BooleanExpression> negatedArgs = new ArrayList<>();
		for (BooleanExpression arg : args) {
			negatedArgs.add(new NotExpression(arg, arg.getPositionInfo()));
		}
		return new BooleanNaryExpression(function.flipDeMorgan(), negatedArgs, position);
	}
}