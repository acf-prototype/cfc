package name.filieri.antonio.pcp.visitors;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.List;
import name.filieri.antonio.jpf.utils.BigRational;
import name.filieri.antonio.pcp.ExpressionResult;
import name.filieri.antonio.pcp.ast.PositionInfo;
import name.filieri.antonio.pcp.ast.commands.AssertCommand;
import name.filieri.antonio.pcp.ast.commands.ClearCommand;
import name.filieri.antonio.pcp.ast.commands.Command;
import name.filieri.antonio.pcp.ast.commands.CountCommand;
import name.filieri.antonio.pcp.ast.commands.DeclareConstantCommand;
import name.filieri.antonio.pcp.ast.commands.DependentVariableDeclarationCommand;
import name.filieri.antonio.pcp.ast.commands.SetOptionCommand;
import name.filieri.antonio.pcp.ast.commands.VariableDeclarationCommand;
import name.filieri.antonio.pcp.ast.expr.DependentVariable;
import name.filieri.antonio.pcp.ast.expr.Expression;
import name.filieri.antonio.pcp.ast.expr.ExpressionType;
import name.filieri.antonio.pcp.ast.expr.Variable;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorCompareExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorConstant;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorExtractExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorUnaryExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorVariable;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanConstant;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanExpression;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanNaryExpression;
import name.filieri.antonio.pcp.ast.expr.bool.NotExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericComparisonExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericConstant;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericUnaryExpression;
import name.filieri.antonio.pcp.ast.expr.probabilistic.BernoulliVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.NormalVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformDoubleVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformFloatVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformIntegerVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformLongVariable;
import name.filieri.antonio.pcp.ast.expr.strings.StringBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.strings.StringConstant;
import name.filieri.antonio.pcp.ast.expr.strings.StringVariable;
import name.filieri.antonio.pcp.grammar.InputFormatBaseVisitor;
import name.filieri.antonio.pcp.grammar.InputFormatParser;
import name.filieri.antonio.pcp.grammar.InputFormatParser.CommandContext;
import name.filieri.antonio.pcp.grammar.InputFormatParser.DecBitVectorContext;
import name.filieri.antonio.pcp.grammar.InputFormatParser.ExprContext;
import name.filieri.antonio.pcp.visitors.SymbolTable.TypeAndExpression;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTree;

/**
 * Build AST from the parse tree (and populate the symbol table)
 * 
 * @author mateus
 */

public class AstBuilder {

	public static List<? extends Command> parseCommands(SymbolTable table, ParseTree tree) {
		return new CommandAstVisitor(table).visit(tree);
	}

	public static class CommandAstVisitor extends InputFormatBaseVisitor<List<Command>> {

		public final SymbolTable table;

		public CommandAstVisitor(SymbolTable symbolTable) {
			super();
			this.table = symbolTable;
		}

		// @Override
		// public List<Command> visitChildren(RuleNode n) {
		// throw new RuntimeException("AST Builder error: expected a command,
		// got '" + n + "' instead :: "
		// + ASTUtil.extractPositionInfo(n.getRuleContext()));
		// }

		@Override
		public List<Command> aggregateResult(List<Command> a, List<Command> b) {
			return ImmutableList.<Command> builder().addAll(a).addAll(b).build();
		}

		@Override
		public List<Command> defaultResult() {
			return ImmutableList.of();
		}

		@Override
		public List<Command> visitInput(@NotNull InputFormatParser.InputContext ctx) {
			List<Command> parsedCommands = new ArrayList<>();
			for (CommandContext cc : ctx.command()) {
				for (Command command : cc.accept(this)) {
					parsedCommands.add(command);
				}
			}
			return parsedCommands;
		}

		@Override
		public List<Command> visitCmd_assert(@NotNull InputFormatParser.Cmd_assertContext ctx) {
			List<BooleanExpression> exprsToAssert = new ArrayList<>(ctx.expr().size());
			for (ExprContext expCtx : ctx.expr()) {
				Expression visitResult = expCtx.accept(new ExpressionVisitor(table));
				if (visitResult instanceof BooleanExpression) {
					exprsToAssert.add((BooleanExpression)visitResult);
				} else {
					throw new RuntimeException("Non-boolean expression at " + visitResult.getPositionInfo());
				}
			}
			Command cmd = new AssertCommand(exprsToAssert,
					PositionInfo.fromTokens(ctx.start, ctx.stop));
			return ImmutableList.<Command> of(cmd);
		}

		@Override
		public List<Command> visitCmd_dec_const(@NotNull InputFormatParser.Cmd_dec_constContext ctx) {
			String name = ctx.name.getText();
			ExpressionType type = ExpressionType.valueOf(ctx.type.getText().toUpperCase());
			Expression value = ctx.val.accept(new ExpressionVisitor(table));

			table.nameToConst.put(name, new TypeAndExpression(type, value));

			Command cmd = new DeclareConstantCommand(name, type, value,
					PositionInfo.fromTokens(ctx.start, ctx.stop));
			return ImmutableList.of(cmd);
		}

		@Override
		public List<Command> visitCmd_dec_dep_var(@NotNull InputFormatParser.Cmd_dec_dep_varContext ctx) {
			String name = ctx.name.getText();
			ExpressionType type = ExpressionType.valueOf(ctx.type.getText().toUpperCase());
			DependentVariable var = new DependentVariable(type, name, PositionInfo.fromTokens(ctx.start, ctx.stop));
			table.nameToVar.put(name, var);
			Command cmd = new DependentVariableDeclarationCommand(var, PositionInfo.fromTokens(ctx.start, ctx.stop));
			return ImmutableList.of(cmd);
		}

		@Override
		public List<Command> visitCmd_clear(@NotNull InputFormatParser.Cmd_clearContext ctx) {
			Command cmd = new ClearCommand(PositionInfo.fromTokens(ctx.start, ctx.stop));
			return ImmutableList.of(cmd);
		}

		@Override
		public List<Command> visitCmd_set_option(@NotNull InputFormatParser.Cmd_set_optionContext ctx) {
			Expression valExpr = ctx.val.accept(new ExpressionVisitor(table));
			ExpressionResult value = valExpr.accept(new ExpressionEvaluator());
			Command cmd = new SetOptionCommand(ctx.key.getText(), value,
					PositionInfo.fromTokens(ctx.start, ctx.stop));
			return ImmutableList.<Command> of(cmd);
		}

		@Override
		public List<Command> visitCommand(@NotNull InputFormatParser.CommandContext ctx) {
			return ctx.getChild(1).accept(this);
		}

		@Override
		public List<Command> visitCmd_dec_var(@NotNull InputFormatParser.Cmd_dec_varContext ctx) {
			String name = ctx.name.getText();
			Variable var = ctx.type.accept(new VariableDeclarationVisitor(name, table));
			table.nameToVar.put(name, var);
			Command cmd = new VariableDeclarationCommand(var, PositionInfo.fromTokens(ctx.start, ctx.stop));
			return ImmutableList.of(cmd);
		}

		@Override
		public List<Command> visitCmd_count(@NotNull InputFormatParser.Cmd_countContext ctx) {
			Command cmd = new CountCommand(PositionInfo.fromTokens(ctx.start, ctx.stop));
			return ImmutableList.of(cmd);
		}
	}

	public static class VariableDeclarationVisitor extends InputFormatBaseVisitor<Variable> {
		public final SymbolTable table;
		public final String varName;

		public VariableDeclarationVisitor(String name, SymbolTable table) {
			this.varName = name;
			this.table = table;
		}

		// @Override
		// public Variable visitChildren(RuleNode n) {
		// throw new RuntimeException("AST Builder error: expected a variable
		// declaration, got '" + n + "' instead :: "
		// + ASTUtil.extractPositionInfo(n.getRuleContext()));
		// }

		@Override
		public Variable aggregateResult(Variable a, Variable b) {
			if (a != null && b != null) {
				throw new RuntimeException("Not supposed to happen");
			} else if (a != null) {
				return a;
			} else {
				return b;
			}
		}

		@Override
		public Variable visitDecBernoulliBoolean(@NotNull InputFormatParser.DecBernoulliBooleanContext ctx) {
			PositionInfo pos = PositionInfo.fromTokens(ctx.start, ctx.stop);
			Expression probExpr = ctx.prob.accept(new ExpressionVisitor(table));
			ExpressionResult r = probExpr.accept(new ExpressionEvaluator());

			if (r.type.isNumeric()) {
				BigRational probability = new BigRational(r.numericResult.doubleValue());
				BernoulliVariable var = new BernoulliVariable(varName, probability, pos);
				return var;
			} else {
				throw new RuntimeException(
						"Non-numeric expression used as argument at declaration of " + varName + " : " + pos);
			}
		}

		@Override
		public Variable visitDecHistogramFloat(@NotNull InputFormatParser.DecHistogramFloatContext ctx) {
			throw new RuntimeException("Not implemented yet");
		}

		@Override
		public Variable visitDecGammaFloat(@NotNull InputFormatParser.DecGammaFloatContext ctx) {
			throw new RuntimeException("Not implemented yet");
		}

		@Override
		public Variable visitTypedecP(@NotNull InputFormatParser.TypedecPContext ctx) {
			return ctx.typedec().accept(this);
		}

		@Override
		public Variable visitDecUniformFloat(@NotNull InputFormatParser.DecUniformFloatContext ctx) {
			PositionInfo pos = PositionInfo.fromTokens(ctx.start, ctx.stop);
			Expression ubExp = ctx.ub.accept(new ExpressionVisitor(table));
			Expression lbExp = ctx.lb.accept(new ExpressionVisitor(table));
			ExpressionResult ubValue = ubExp.accept(new ExpressionEvaluator());
			ExpressionResult lbValue = lbExp.accept(new ExpressionEvaluator());
			String type = ctx.type.getText().toUpperCase();
			Variable vb;

			if (ubValue.type.isNumeric() && lbValue.type.isNumeric()) {
				if (type.equals("FLOAT")) {
					vb = new UniformFloatVariable(varName, lbValue.numericResult.floatValue(),
							ubValue.numericResult.floatValue(), pos);
				} else if (type.equals("DOUBLE")) {
					vb = new UniformDoubleVariable(varName, lbValue.numericResult.doubleValue(),
							ubValue.numericResult.doubleValue(), pos);
				} else {
					throw new RuntimeException("Unknown type name: " + type + " : " + pos);
				}
			} else {
				throw new RuntimeException("Non-numeric expression for the bound of variable " + varName + " : " + pos);
			}
			return vb;
		}

		@Override
		public Variable visitDecHistogramInt(@NotNull InputFormatParser.DecHistogramIntContext ctx) {
			throw new RuntimeException("Not implemented yet");
		}

		@Override
		public Variable visitDecNormalFloat(@NotNull InputFormatParser.DecNormalFloatContext ctx) {
			PositionInfo pos = PositionInfo.fromTokens(ctx.start, ctx.stop);
			Expression ubExp = ctx.ub.accept(new ExpressionVisitor(table));
			Expression lbExp = ctx.lb.accept(new ExpressionVisitor(table));
			Expression meanExp = ctx.mean.accept(new ExpressionVisitor(table));
			Expression sdExp = ctx.sd.accept(new ExpressionVisitor(table));

			ExpressionResult ubValue = ubExp.accept(new ExpressionEvaluator());
			ExpressionResult lbValue = lbExp.accept(new ExpressionEvaluator());
			ExpressionResult meanValue = meanExp.accept(new ExpressionEvaluator());
			ExpressionResult sdValue = sdExp.accept(new ExpressionEvaluator());

			Variable vb;

			if (ubValue.type.isNumeric() && lbValue.type.isNumeric() && meanValue.type.isNumeric()
					&& sdValue.type.isNumeric()) {
				vb = new NormalVariable(varName, lbValue.numericResult.doubleValue(),
						ubValue.numericResult.doubleValue(), meanValue.numericResult.doubleValue(),
						sdValue.numericResult.doubleValue(), pos);
			} else {
				throw new RuntimeException("Non-numeric expression for the bound of variable " + varName + " : " + pos);
			}
			return vb;
		}

		@Override
		public Variable visitDecUniformInt(@NotNull InputFormatParser.DecUniformIntContext ctx) {
			PositionInfo pos = PositionInfo.fromTokens(ctx.start, ctx.stop);
			Expression ubExp = ctx.ub.accept(new ExpressionVisitor(table));
			Expression lbExp = ctx.lb.accept(new ExpressionVisitor(table));
			ExpressionResult ubValue = ubExp.accept(new ExpressionEvaluator());
			ExpressionResult lbValue = lbExp.accept(new ExpressionEvaluator());
			String type = ctx.type.getText().toUpperCase();
			Variable vb;

			if (ubValue.type.isNumeric() && lbValue.type.isNumeric()) {
				if (type.equals("INT")) {
					vb = new UniformIntegerVariable(varName, lbValue.numericResult.intValue(),
							ubValue.numericResult.intValue(), pos);
				} else if (type.equals("LONG")) {
					vb = new UniformLongVariable(varName, lbValue.numericResult.longValue(),
							ubValue.numericResult.longValue(), pos);
				} else {
					throw new RuntimeException("Unknown type name: " + type + " : " + pos);
				}
			} else {
				throw new RuntimeException("Non-numeric expression for the bound of variable " + varName + " : " + pos);
			}
			return vb;
		}

		@Override
		public Variable visitDecString(@NotNull InputFormatParser.DecStringContext ctx) {
			String regex = ctx.regex.getText();
			return new StringVariable(varName, regex.substring(1, regex.length() - 1),
					PositionInfo.fromTokens(ctx.start, ctx.stop));
		}

		@Override
		public Variable visitDecBitVector(@NotNull DecBitVectorContext ctx) {
			PositionInfo pos = PositionInfo.fromTokens(ctx.start, ctx.stop);
			Expression lengthExpr = ctx.length.accept(new ExpressionVisitor(table));
			ExpressionResult lengthValue = lengthExpr.accept(new ExpressionEvaluator());
			if (lengthValue.type.isInteger()) {
				return new BitVectorVariable(varName,lengthValue.numericResult.intValue(),
						PositionInfo.fromTokens(ctx.start, ctx.stop));
			} else {
				throw new RuntimeException("Length of bv " + varName + " is not an integer: " + pos);
			}
		}
	}

	public static class ExpressionVisitor extends InputFormatBaseVisitor<Expression> {

		public final SymbolTable table;

		public ExpressionVisitor(SymbolTable table) {
			this.table = table;
		}

		// @Override
		// public Expression visitChildren(RuleNode n) {
		// throw new RuntimeException("AST Builder error: expected an
		// expression, got '" + n + "' instead :: "
		// + ASTUtil.extractPositionInfo(n.getRuleContext()));
		// }

		@Override
		public Expression aggregateResult(Expression a, Expression b) {
			if (a != null && b != null) {
				throw new RuntimeException("Not supposed to happen");
			} else if (a != null) {
				return a;
			} else {
				return b;
			}
		}

		@Override
		public Expression visitExprBoolean(@NotNull InputFormatParser.ExprBooleanContext ctx) {
			BooleanNaryExpression.Function op = BooleanNaryExpression.Function.valueOf(
					ctx.op.getText().toUpperCase());

			List<BooleanExpression> clauses = new ArrayList<>(ctx.expr().size());
			for (ExprContext expCtx : ctx.expr()) {
				Expression subExpr = expCtx.accept(this);
				if (subExpr instanceof BooleanExpression) {
					clauses.add((BooleanExpression) subExpr);
				} else {
					throw new RuntimeException("Non-boolean expression at " + subExpr.getPositionInfo());
				}
			}

			return new BooleanNaryExpression(op, clauses, PositionInfo.fromTokens(ctx.start, ctx.stop));
		}

		@Override
		public Expression visitFloatLit(@NotNull InputFormatParser.FloatLitContext ctx) {
			Float f = Float.parseFloat(ctx.getText());
			return new NumericConstant(f, ExpressionType.FLOAT,
					PositionInfo.fromTokens(ctx.start, ctx.stop));
		}

		@Override
		public Expression visitExprMathBinary(@NotNull InputFormatParser.ExprMathBinaryContext ctx) {
			Expression left = ctx.expr(0).accept(this);
			Expression right = ctx.expr(1).accept(this);

			NumericBinaryExpression.Function fun = NumericBinaryExpression.Function
					.valueOf(ctx.fun.getText().toUpperCase());

			return new NumericBinaryExpression(left, right, fun, PositionInfo.fromTokens(ctx.start, ctx.stop));
		}

		@Override
		public Expression visitExprInfix(@NotNull InputFormatParser.ExprInfixContext ctx) {
			throw new RuntimeException("Not implemented yet");
		}

		@Override
		public Expression visitExprString(@NotNull InputFormatParser.ExprStringContext ctx) {
			Expression source = ctx.expr(0).accept(this);
			Expression arg = ctx.expr(1).accept(this);
			StringBinaryExpression.Function fun = StringBinaryExpression.Function
					.valueOf(ctx.fun.getText().replace('-', '_').toUpperCase());
			return new StringBinaryExpression(source, arg, fun, PositionInfo.fromTokens(ctx.start, ctx.stop));
		}

		@Override
		public Expression visitExprLiteral(@NotNull InputFormatParser.ExprLiteralContext ctx) {
			return ctx.getChild(0).accept(this);
		}

		@Override
		public Expression visitSymbol(@NotNull InputFormatParser.SymbolContext ctx) {
			String name = ctx.getText();
			PositionInfo position = PositionInfo.fromTokens(ctx.start, ctx.stop);

			if (table.nameToConst.containsKey(name)) { // constant
				return table.nameToConst.get(name).expr;
			} else if (table.nameToVar.containsKey(name)) { // variable
				return table.nameToVar.get(name);
			} else {
				throw new RuntimeException("Undeclared symbol: " + name + " at " + position);
			}
		}

		@Override
		public Expression visitExprNot(@NotNull InputFormatParser.ExprNotContext ctx) {
			Expression arg = ctx.expr().accept(this);
			if (arg instanceof BooleanExpression) {
				PositionInfo position = PositionInfo.fromTokens(ctx.start, ctx.stop);
				return new NotExpression((BooleanExpression) arg, position);
			} else {
				throw new RuntimeException("Non-boolean expression at " + arg.getPositionInfo());
			}
		}

		@Override
		public Expression visitExprCmp(@NotNull InputFormatParser.ExprCmpContext ctx) {
			Expression left = ctx.expr(0).accept(this);
			Expression right = ctx.expr(1).accept(this);
			PositionInfo position = PositionInfo.fromTokens(ctx.start, ctx.stop);
			Optional<NumericComparisonExpression.Function> fun = NumericComparisonExpression.Function
					.fromSymbol(ctx.op.getText());

			if (fun.isPresent()) {
				return new NumericComparisonExpression(left, right, fun.get(), position);
			} else {
				throw new RuntimeException("Unknown comparison operator: " + ctx.op);
			}
		}

		@Override
		public Expression visitDecDataStructure(@NotNull InputFormatParser.DecDataStructureContext ctx) {
			throw new RuntimeException("Not implemented");
		}

		@Override
		public Expression visitBooleanLit(@NotNull InputFormatParser.BooleanLitContext ctx) {
			return new BooleanConstant(Boolean.parseBoolean(ctx.getText()),
					PositionInfo.fromTokens(ctx.start, ctx.stop));
		}

		@Override
		public Expression visitExprArith(@NotNull InputFormatParser.ExprArithContext ctx) {
			Expression left = ctx.expr(0).accept(this);
			Expression right = ctx.expr(1).accept(this);
			PositionInfo position = PositionInfo.fromTokens(ctx.start, ctx.stop);
			Optional<NumericBinaryExpression.Function> fun = NumericBinaryExpression.Function
					.fromSymbol(ctx.op.getText().toLowerCase());

			if (fun.isPresent()) {
				return new NumericBinaryExpression(left, right, fun.get(), position);
			} else {
				throw new RuntimeException("Unknown comparison operator: " + ctx.op);
			}
		}

		// TODO handle int/longs correctly. this is just a hack
		@Override
		public Expression visitIntLit(@NotNull InputFormatParser.IntLitContext ctx) {
			long l = Long.parseLong(ctx.getText());
			Number value;
			ExpressionType type;
			if (l <= Integer.MAX_VALUE && l >= Integer.MIN_VALUE) {
				value = (int) l;
				type = ExpressionType.INT;
			} else {
				value = l;
				type = ExpressionType.LONG;
			}
			return new NumericConstant(value, type, PositionInfo.fromTokens(ctx.start, ctx.stop));
		}

		@Override
		public Expression visitExprMathFunUnary(@NotNull InputFormatParser.ExprMathFunUnaryContext ctx) {
			Expression arg = ctx.expr().accept(this);
			NumericUnaryExpression.Function fun = NumericUnaryExpression.Function
					.valueOf(ctx.fun.getText().toUpperCase());
			PositionInfo position = PositionInfo.fromTokens(ctx.start, ctx.stop);
			return new NumericUnaryExpression(fun, arg, position);
		}

		@Override
		public Expression visitNumberLit(@NotNull InputFormatParser.NumberLitContext ctx) {
			return ctx.getChild(0).accept(this);
		}

		@Override
		public Expression visitStringLit(@NotNull InputFormatParser.StringLitContext ctx) {
			String text = ctx.getText();
			return new StringConstant(text.substring(1, text.length() - 1),
					PositionInfo.fromTokens(ctx.start, ctx.stop));
		}

		@Override
		public Expression visitLiteral(@NotNull InputFormatParser.LiteralContext ctx) {
			return ctx.getChild(0).accept(this);
		}

		@Override
		public Expression visitBvHexLit(@NotNull InputFormatParser.BvHexLitContext ctx) {
			String value = ctx.value.getText().trim();
			value = value.substring(2);
			return new BitVectorConstant(value,true,PositionInfo.fromTokens(ctx.start,ctx.stop));
		}

		@Override
		public Expression visitBvBinLit(@NotNull InputFormatParser.BvBinLitContext ctx) {
			String value = ctx.value.getText().trim();
			value = value.substring(2);
			return new BitVectorConstant(value,false,PositionInfo.fromTokens(ctx.start,ctx.stop));
		}

		@Override
		public Expression visitExprBvBin(@NotNull InputFormatParser.ExprBvBinContext ctx) {
			Expression left = ctx.expr().get(0).accept(this);
			Expression right = ctx.expr().get(1).accept(this);
			PositionInfo position = PositionInfo.fromTokens(ctx.start, ctx.stop);
			Optional<BitVectorBinaryExpression.Function> function = BitVectorBinaryExpression.Function
					.fromSymbol(ctx.fun.getText().toUpperCase().trim());
			if (function.isPresent()) {
				return new BitVectorBinaryExpression(left, right, function.get(),	position);
			} else {
				throw new RuntimeException("Unrecognized function: " + ctx.fun);
			}
		}

		@Override
		public Expression visitExprBvExtract(@NotNull InputFormatParser.ExprBvExtractContext ctx) {
			Expression hiExpr = ctx.expr().get(0).accept(this);
			Expression loExpr = ctx.expr().get(1).accept(this);
			Expression src = ctx.expr().get(2).accept(this);
			ExpressionResult hi = hiExpr.accept(new ExpressionEvaluator());
			ExpressionResult lo = loExpr.accept(new ExpressionEvaluator());

			if (hi.type.isInteger() && lo.type.isInteger()) {
				PositionInfo position = PositionInfo.fromTokens(ctx.start, ctx.stop);
				return new BitVectorExtractExpression(src, hi.numericResult.intValue(),
						lo.numericResult.intValue(), position);
			} else {
				throw new RuntimeException("Non-Integer argument for extract: " + ctx.hi + " " + ctx.lo);
			}
		}

		@Override
		public Expression visitExprBvUnary(@NotNull InputFormatParser.ExprBvUnaryContext ctx) {
			Expression arg = ctx.expr().accept(this);
			PositionInfo position = PositionInfo.fromTokens(ctx.start, ctx.stop);
			Optional<BitVectorUnaryExpression.Function> function = BitVectorUnaryExpression.Function
					.fromSymbol(ctx.fun.getText().toUpperCase().trim());
			if (function.isPresent()) {
				return new BitVectorUnaryExpression(arg, function.get(), position);
			} else {
				throw new RuntimeException("Unrecognized function: " + ctx.fun);
			}

		}

		@Override
		public Expression visitExprBvCmp(@NotNull InputFormatParser.ExprBvCmpContext ctx) {
			Expression left = ctx.expr().get(0).accept(this);
			Expression right = ctx.expr().get(1).accept(this);
			PositionInfo position = PositionInfo.fromTokens(ctx.start, ctx.stop);
			Optional<BitVectorCompareExpression.Function> function = BitVectorCompareExpression.Function
					.fromSymbol(ctx.fun.getText().toUpperCase().trim());
			if (function.isPresent()) {
				return new BitVectorCompareExpression(left, right, function.get(), position);
			} else {
				throw new RuntimeException("Unrecognized function: " + ctx.fun);
			}
		}
	}
}
