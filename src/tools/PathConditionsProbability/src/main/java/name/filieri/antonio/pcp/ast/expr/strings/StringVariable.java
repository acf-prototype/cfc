package name.filieri.antonio.pcp.ast.expr.strings;

import java.nio.charset.StandardCharsets;

import com.google.common.hash.HashCode;

import name.filieri.antonio.jpf.utils.ASTUtil;
import name.filieri.antonio.jpf.utils.BigRational;
import name.filieri.antonio.pcp.ast.PositionInfo;
import name.filieri.antonio.pcp.ast.expr.ExpressionType;
import name.filieri.antonio.pcp.ast.expr.ExpressionVisitor;
import name.filieri.antonio.pcp.ast.expr.Variable;

public class StringVariable implements Variable {

	private final String name;
	private final String domain; //probably a regex, but we will see 
	private final PositionInfo position;
	private HashCode hash;
	
	public StringVariable(String name, String domain, PositionInfo position) {
		this.name = name;
		this.domain = domain;
		this.position = position;
	}
	
	@Override
	public <T> T accept(ExpressionVisitor<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public HashCode getHashCode() {
		if (hash == null) {
			hash = ASTUtil.getHashFunction().newHasher()
					.putString(name, StandardCharsets.UTF_8)
					.putString(domain, StandardCharsets.UTF_8)
					.hash();
		}
		return hash;
	}

	@Override
	public PositionInfo getPositionInfo() {
		return position;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public ExpressionType getType() {
		return ExpressionType.STRING;
	}
	
	public String getDomain() {
		return domain;
	}

	@Override
	public BigRational getDomainSize() {
		throw new RuntimeException("Not implemented yet");
	}
}
