package name.filieri.antonio.pcp.ast.expr.numeric;

import java.nio.charset.StandardCharsets;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;

import name.filieri.antonio.jpf.utils.ASTUtil;
import name.filieri.antonio.pcp.ast.PositionInfo;
import name.filieri.antonio.pcp.ast.expr.Expression;
import name.filieri.antonio.pcp.ast.expr.ExpressionVisitor;
import name.filieri.antonio.pcp.ast.expr.UnaryExpression;


public class NumericUnaryExpression implements UnaryExpression {

	public enum Function {
		SIN, COS, TAN, SINH, COSH, TANH, EXP, SQRT, LOG10, LOG
	}

	private final Expression arg;
	private final Function function;
	private PositionInfo position;
	private HashCode hash = null;

	public NumericUnaryExpression(Function function, Expression arg, PositionInfo position) {
		super();
		Preconditions.checkNotNull(arg);
		this.arg = arg;
		this.function = function;
		this.position = position;
	}

	@Override
	public <T> T accept(ExpressionVisitor<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public Expression getArg() {
		return arg;
	}

	public Function getFunction() {
		return function;
	}

	@Override
	public int hashCode() {
		return getHashCode().asInt();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NumericUnaryExpression other = (NumericUnaryExpression) obj;

		if (this.hashCode() != other.hashCode()) {
			return false;
		}

		if (arg == null) {
			if (other.arg != null)
				return false;
		} else if (!arg.equals(other.arg))
			return false;
		if (function != other.function)
			return false;
		return true;
	}

	@Override
	public HashCode getHashCode() {
		if (hash == null) {
			HashCode hc = ASTUtil.getHashFunction().newHasher()
					.putString(function.name(),StandardCharsets.UTF_8)
					.hash();
			hash = Hashing.combineOrdered(ImmutableList.of(hc,arg.getHashCode()));
		}
		return hash;
	}
	
	@Override
	public String toString() {
		return function + "(" + arg.toString() + ")";
	}

	@Override
	public PositionInfo getPositionInfo() {
		return position;
	}
}