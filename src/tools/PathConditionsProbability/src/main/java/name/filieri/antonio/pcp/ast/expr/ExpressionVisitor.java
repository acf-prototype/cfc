package name.filieri.antonio.pcp.ast.expr;

import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorCompareExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorConstant;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorExtractExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorUnaryExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorVariable;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanConstant;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanNaryExpression;
import name.filieri.antonio.pcp.ast.expr.bool.NotExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericComparisonExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericConstant;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericUnaryExpression;
import name.filieri.antonio.pcp.ast.expr.probabilistic.BernoulliVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.NormalVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformDoubleVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformFloatVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformIntegerVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformLongVariable;
import name.filieri.antonio.pcp.ast.expr.strings.StringBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.strings.StringConstant;
import name.filieri.antonio.pcp.ast.expr.strings.StringVariable;

public interface ExpressionVisitor<T> {

	T visit(BooleanConstant expr);

	T visit(BooleanNaryExpression expr);

	T visit(NotExpression expr);

	T visit(NumericBinaryExpression expr);

	T visit(NumericComparisonExpression expr);

	T visit(NumericConstant expr);

	T visit(NumericUnaryExpression expr);

	T visit(NormalVariable expr);

	T visit(StringConstant expr);

	T visit(BernoulliVariable expr);

	T visit(UniformFloatVariable expr);

	T visit(UniformDoubleVariable expr);
	
	T visit(UniformIntegerVariable expr);

	T visit(UniformLongVariable expr);

	T visit(StringBinaryExpression expr);

	T visit(StringVariable expr);

	T visit(DependentVariable dependentVariable);

  T visit(BitVectorConstant bitVectorConstant);

	T visit(BitVectorBinaryExpression bitVectorBinaryExpression);

	T visit(BitVectorExtractExpression bitVectorExtractExpression);

	T visit(BitVectorCompareExpression bitVectorCompareExpression);

	T visit(BitVectorUnaryExpression bitVectorUnaryExpression);

  T visit(BitVectorVariable bitVectorVariable);
}
