package name.filieri.antonio.pcp.counters;

import com.google.common.base.Preconditions;

import name.filieri.antonio.pcp.Constraint;
import name.filieri.antonio.pcp.ast.expr.DependentVariable;
import name.filieri.antonio.pcp.ast.expr.Expression;
import name.filieri.antonio.pcp.ast.expr.ExpressionType;
import name.filieri.antonio.pcp.ast.expr.ExpressionVisitor;
import name.filieri.antonio.pcp.ast.expr.Variable;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorCompareExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorConstant;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorExtractExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorUnaryExpression;
import name.filieri.antonio.pcp.ast.expr.bitvector.BitVectorVariable;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanConstant;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanNaryExpression;
import name.filieri.antonio.pcp.ast.expr.bool.NotExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericBoundedVariable;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericComparisonExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericConstant;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericUnaryExpression;
import name.filieri.antonio.pcp.ast.expr.probabilistic.BernoulliVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.NormalVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformDoubleVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformFloatVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformIntegerVariable;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformLongVariable;
import name.filieri.antonio.pcp.ast.expr.strings.StringBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.strings.StringConstant;
import name.filieri.antonio.pcp.ast.expr.strings.StringVariable;

public class BarvinokVisitor implements ExpressionVisitor<String> {

	public static String toBarvinok(Constraint problem) {
		BarvinokVisitor visitor = new BarvinokVisitor();
		StringBuffer prefix = new StringBuffer();
		StringBuffer suffix = new StringBuffer();
		
		prefix.append("P := { [");
		suffix.append(" : ");

		for (Variable var : problem.getVariables()) {
			if (var instanceof NumericBoundedVariable) {
			NumericBoundedVariable bvar = (NumericBoundedVariable) var;

			prefix.append(var.getName());
			prefix.append(',');

			// append domain constraints
			suffix.append(bvar.getLowerBound().longValue());
			suffix.append(" <= ");
			suffix.append(bvar.getName());
			suffix.append(" <= ");
			suffix.append(bvar.getUpperBound().longValue());
			suffix.append(" and ");
			} else if (var instanceof DependentVariable) {
				Preconditions.checkArgument(var.getType() == ExpressionType.INT
						|| var.getType() == ExpressionType.LONG);
				prefix.append(var.getName());
				prefix.append(',');
			} else {
				throw new RuntimeException("Unsupported variable type: " + var.getClass().toString());
			}
		}
		prefix.delete(prefix.length() - 1, prefix.length());
		prefix.append("] ");

		// append the actual constraint
		suffix.append(problem.getExpr().accept(visitor));

		suffix.append(" };\n card P;\n");
		
		return prefix.toString() + suffix.toString();
	}

	@Override
	public String visit(BooleanConstant expr) {
		return expr.getValue() + "";
	}

	@Override
	public String visit(BooleanNaryExpression expr) {
		StringBuffer sb = new StringBuffer();
		sb.append("(");
		String function = expr.getFunction().toString();
		for (Expression arg : expr.getArgs()) {
			sb.append(arg.accept(this));
			sb.append(' ');
			sb.append(function);
			sb.append(' ');
		}
		int len = sb.length();
		sb.delete(len - (function.length() + 1), sb.length());
		sb.append(')');
		return sb.toString();
	}

	@Override
	public String visit(NotExpression expr) {
		return "not (" + expr.getArg().accept(this) + ")";
	}

	@Override
	public String visit(NumericBinaryExpression expr) {
		return "(" + expr.getLeft().accept(this)
				+ expr.getFunction().toSymbol()
				+ expr.getRight().accept(this)
				+ ")";
	}

	@Override
	public String visit(NumericComparisonExpression expr) {
		return expr.getLeft().accept(this)
				+ expr.getFunction().toSymbol()
				+ expr.getRight().accept(this);
	}

	@Override
	public String visit(NumericConstant expr) {
		Preconditions.checkArgument(expr.getType().isInteger());
		return "" + expr.getValue().intValue();
	}

	@Override
	public String visit(NumericUnaryExpression expr) {
		throw new RuntimeException("Barvinok doesn't support complex functions! " + expr);
	}

	@Override
	public String visit(NormalVariable expr) {
		throw new RuntimeException("Barvinok doesn't support Probabilistic variables! " + expr);
	}

	@Override
	public String visit(StringConstant expr) {
		throw new RuntimeException("Barvinok doesn't support Strings!" + expr);
	}

	@Override
	public String visit(BernoulliVariable expr) {
		throw new RuntimeException("Barvinok doesn't support Probabilistic variables!" + expr);
	}

	@Override
	public String visit(UniformFloatVariable expr) {
		throw new RuntimeException("Barvinok doesn't support Floating-point variables!" + expr);
	}

	@Override
	public String visit(UniformDoubleVariable expr) {
		throw new RuntimeException("Barvinok doesn't support Floating-point variables!" + expr);
	}

	@Override
	public String visit(UniformIntegerVariable expr) {
		return expr.getName();
	}

	@Override
	public String visit(UniformLongVariable expr) {
		return expr.getName();
	}

	@Override
	public String visit(StringBinaryExpression expr) {
		throw new RuntimeException("Barvinok doesn't support String expressions!" + expr);
	}

	@Override
	public String visit(StringVariable expr) {
		throw new RuntimeException("Barvinok doesn't support String variables!" + expr);
	}

	@Override
	public String visit(DependentVariable expr) {
		return expr.getName();
	}

	@Override
	public String visit(BitVectorConstant expr) {
		throw new RuntimeException("Barvinok doesn't support BV expressions!" + expr);
	}

	@Override
	public String visit(BitVectorBinaryExpression expr) {
		throw new RuntimeException("Barvinok doesn't support BV expressions!" + expr);
	}

	@Override
	public String visit(BitVectorExtractExpression expr) {
		throw new RuntimeException("Barvinok doesn't support BV expressions!" + expr);
	}

	@Override
	public String visit(BitVectorCompareExpression expr) {
		throw new RuntimeException("Barvinok doesn't support BV expressions!" + expr);
	}

	@Override
	public String visit(BitVectorUnaryExpression expr) {
		throw new RuntimeException("Barvinok doesn't support BV expressions!" + expr);
	}

	@Override
	public String visit(BitVectorVariable variable) {
		throw new RuntimeException("Barvinok doesn't support bitVector variables!" + variable);
	}
}
