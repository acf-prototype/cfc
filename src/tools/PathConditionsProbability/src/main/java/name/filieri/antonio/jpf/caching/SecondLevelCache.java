package name.filieri.antonio.jpf.caching;

import java.util.Set;

import name.filieri.antonio.jpf.domain.Problem;
import name.filieri.antonio.jpf.utils.BigRational;

public class SecondLevelCache {
	// public final CacheAccess<String, BigRational> jcsLatte;
	// public final CacheAccess<String, Set<Problem>> jcsOmega;
	// private final IElementAttributes latteAttributes;
	// private final IElementAttributes omegaAttributes;
	// private final CacheAccess<String, BigRational> jcsBarvinok;
	// private final IElementAttributes barvinokAttributes;
	// private final boolean closeCacheWhenFinalized;
	
	public SecondLevelCache(String path, boolean closeCacheWhenFinalized) {
		// Properties cacheProperties =
		// SecondLevelCacheDefaultProperties.getDefaultProperties();
		// cacheProperties.setProperty("jcs.auxiliary.indexedDiskCache.attributes.DiskPath",
		// path);
		// //See CompositeCacheManager.ShutdownHook *in this project* for more
		// details
		// cacheProperties.setProperty("jcs.hack.disableShutdownHook", "true");
		// JCS.setConfigProperties(cacheProperties);
		// this.jcsLatte = JCS.getInstance("latte");
		// this.jcsOmega = JCS.getInstance("omega");
		// this.jcsBarvinok = JCS.getInstance("barvinok");
		//
		// IElementAttributes attributes =
		// jcsLatte.getDefaultElementAttributes();
		// attributes.setIsEternal(true);
		// latteAttributes=attributes;
		// attributes = jcsOmega.getDefaultElementAttributes();
		// attributes.setIsEternal(true);
		// omegaAttributes=attributes;
		// attributes = jcsBarvinok.getDefaultElementAttributes();
		// attributes.setIsEternal(true);
		// barvinokAttributes=attributes;
		//
		// this.closeCacheWhenFinalized = closeCacheWhenFinalized;
	}
	
	public SecondLevelCache(String path) {
		this(path,true);
	}

	public final void putLatte(Problem problem, BigRational count) {
		// jcsLatte.put("problem"+problem.toString().hashCode(), count,
		// latteAttributes);
	}

	public final BigRational getLatte(Problem problem) {
		// return jcsLatte.get("problem"+problem.toString().hashCode());
		return null;
	}

	public final void putOmega(Problem problem, Set<Problem> subProblems) {
		// jcsOmega.put("problem"+problem.toString().hashCode(),
		// subProblems,omegaAttributes);
	}

	public final Set<Problem> getOmega(Problem problem) {
		// return jcsOmega.get("problem"+problem.toString().hashCode());
		return null;
	}

	public final void putBarvinok(Problem problem, BigRational count) {
		// System.out.println("[CACHE] Storing problem:" + problem);
		// jcsBarvinok.put("problem"+problem.toString().hashCode(), count,
		// latteAttributes);
	}

	public final BigRational getBarvinok(Problem problem) {
		// BigRational result =
		// jcsBarvinok.get("problem"+problem.toString().hashCode());
		// if (result != null) {
		// System.out.println("[CACHE] Hit on problem:" + problem);
		// }
		// return result;
		return null;
	}
	
	private boolean shutdownDone = false;
	
	public final void shutdown() {
		// shutdownDone = true;
		// CompositeCacheManager cacheMgr = CompositeCacheManager.getInstance();
		// cacheMgr.shutDown();
	}

	public final String stats(){
		// StringBuilder stats = new StringBuilder();
		// stats.append("Omega stats:\n");
		// stats.append(jcsOmega.getStats()+'\n');
		// stats.append("Latte stats:\n");
		// stats.append(jcsLatte.getStats()+'\n');
		// return stats.toString();
		return null;
	}
	
	@Override
	protected void finalize() throws Throwable {
		// super.finalize();
		// if (closeCacheWhenFinalized) {
		// shutdown();
		// } else if (!shutdownDone) {
		// System.err.println("SecondLevelCache is being gc'ed without calling
		// 'shutdown'!");
		// }
	}

	public static void shutdownAllCaches() {
		// CompositeCacheManager manager = CompositeCacheManager.getInstance();
		// if (manager.isInitialized()) {
		// long start = System.nanoTime();
		// manager.shutDown();
		// System.out.printf("[CACHE] time spent flushing to disk: %f ms\n",
		// (System.nanoTime() - start) / 1000000.0);
		// }
	}
}