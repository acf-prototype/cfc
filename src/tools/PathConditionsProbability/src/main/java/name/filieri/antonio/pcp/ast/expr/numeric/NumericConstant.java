package name.filieri.antonio.pcp.ast.expr.numeric;

import com.google.common.base.Preconditions;
import com.google.common.hash.HashCode;
import com.google.common.hash.Hasher;

import name.filieri.antonio.jpf.utils.ASTUtil;
import name.filieri.antonio.pcp.ast.PositionInfo;
import name.filieri.antonio.pcp.ast.expr.Constant;
import name.filieri.antonio.pcp.ast.expr.ExpressionType;
import name.filieri.antonio.pcp.ast.expr.ExpressionVisitor;


public class NumericConstant implements Constant {

	private final Number value;
	private final ExpressionType type;
	private final PositionInfo position; //for error reporting purposes
	private HashCode hash = null;
	
	public NumericConstant(Number value, ExpressionType type, PositionInfo position) {
		super();
		Preconditions.checkArgument(type.isNumeric(), "Non-numeric type");
		Preconditions.checkNotNull(value);
		Preconditions.checkNotNull(position);
		this.value = value;
		this.type = type;
		this.position = position;
	}

	public NumericConstant(double d, ExpressionType e) {
		this(d, e, PositionInfo.DUMMY);
	}

	public ExpressionType getType() {
		return type;
	}

	public Number getValue() {
		return value;
	}

	@Override
	public <T> T accept(ExpressionVisitor<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public int hashCode() {
		return getHashCode().asInt();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NumericConstant other = (NumericConstant) obj;

		if (this.hashCode() != other.hashCode())
			return false;

		if (type != other.type)
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	public static NumericConstant valueOf(int value) {
		return new NumericConstant(value, ExpressionType.INT, PositionInfo.DUMMY);
	}

	public static NumericConstant valueOf(long value) {
		return new NumericConstant(value, ExpressionType.LONG, PositionInfo.DUMMY);
	}

	public static NumericConstant valueOf(float value) {
		return new NumericConstant(value, ExpressionType.FLOAT, PositionInfo.DUMMY);
	}

	public static NumericConstant valueOf(double value) {
		return new NumericConstant(value, ExpressionType.DOUBLE, PositionInfo.DUMMY);
	}

	@Override
	public HashCode getHashCode() {
		if (hash == null) {
			Hasher hasher = ASTUtil.getHashFunction().newHasher()
					.putInt(type.ordinal());
			switch (type) {
			case DOUBLE:
				hasher.putDouble(value.doubleValue());
				break;
			case FLOAT:
				hasher.putFloat(value.floatValue());
				break;
			case INT:
				hasher.putInt(value.intValue());
				break;
			case LONG:
				hasher.putLong(value.longValue());
				break;
			default:
				throw new RuntimeException("Not supposed to happen :(");
			}
			HashCode hc = hasher.hash();
			hash = hc;
		}
		return hash;
	}
	
	@Override
	public String toString() {
		return value+"";
	}
	
	@Override
	public PositionInfo getPositionInfo() {
		return position;
	}
}
