package name.filieri.antonio.pcp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;

import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.Test;

import name.filieri.antonio.pcp.ast.PositionInfo;
import name.filieri.antonio.pcp.ast.commands.AssertCommand;
import name.filieri.antonio.pcp.ast.commands.ClearCommand;
import name.filieri.antonio.pcp.ast.commands.Command;
import name.filieri.antonio.pcp.ast.commands.CountCommand;
import name.filieri.antonio.pcp.ast.commands.DeclareConstantCommand;
import name.filieri.antonio.pcp.ast.commands.DependentVariableDeclarationCommand;
import name.filieri.antonio.pcp.ast.commands.SetOptionCommand;
import name.filieri.antonio.pcp.ast.commands.VariableDeclarationCommand;
import name.filieri.antonio.pcp.ast.expr.Expression;
import name.filieri.antonio.pcp.ast.expr.ExpressionType;
import name.filieri.antonio.pcp.ast.expr.Variable;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanExpression;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanNaryExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericBoundedVariable;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericComparisonExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericConstant;
import name.filieri.antonio.pcp.ast.expr.strings.StringVariable;
import name.filieri.antonio.pcp.grammar.InputFormatLexer;
import name.filieri.antonio.pcp.grammar.InputFormatParser;
import name.filieri.antonio.pcp.visitors.AstBuilder;
import name.filieri.antonio.pcp.visitors.SymbolTable;

public class ParserTest {

	@Test
	public void testParsing() throws IOException {
		ANTLRFileStream input = new ANTLRFileStream("src/test/java/name/filieri/antonio/pcp/input.pcp");
		InputFormatLexer lexer = new InputFormatLexer(input);
		InputFormatParser parser = new InputFormatParser(new CommonTokenStream(lexer));

		ParseTree tree = parser.input();
		SymbolTable symtable = new SymbolTable();
		List<? extends Command> commands = AstBuilder.parseCommands(symtable, tree);

		assertTrue(commands.get(0) instanceof SetOptionCommand);
		assertEquals(((SetOptionCommand) commands.get(0)).getName(), ":seed");
		assertEquals(((SetOptionCommand) commands.get(0)).getValue().numericResult, 121314);

		assertTrue(commands.get(1) instanceof SetOptionCommand);
		assertEquals(((SetOptionCommand) commands.get(1)).getName(), ":linear-counter");
		assertEquals(((SetOptionCommand) commands.get(1)).getValue().stringResult, "barvinok");

		assertTrue(commands.get(2) instanceof SetOptionCommand);
		assertEquals(((SetOptionCommand) commands.get(2)).getName(), ":partitioning");
		assertEquals(((SetOptionCommand) commands.get(2)).getValue().numericResult, 1);

		assertTrue(commands.get(3) instanceof DeclareConstantCommand);
		assertEquals(((DeclareConstantCommand) commands.get(3)).getName(), "pi");
		assertEquals(((DeclareConstantCommand) commands.get(3)).getType(), ExpressionType.FLOAT);
		assertEquals(((DeclareConstantCommand) commands.get(3)).getValue(), NumericConstant.valueOf(3.1415f));

		assertTrue(commands.get(4) instanceof VariableDeclarationCommand);
		Variable x1 = ((VariableDeclarationCommand) commands.get(4)).getVar();
		assertEquals(x1.getType(), ExpressionType.INT);
		assertEquals(((NumericBoundedVariable) x1).getName(), "x1");
		assertEquals(((NumericBoundedVariable) x1).getLowerBound(), -10);
		assertEquals(((NumericBoundedVariable) x1).getUpperBound(), 10);

		assertTrue(commands.get(5) instanceof VariableDeclarationCommand);
		Variable x2 = ((VariableDeclarationCommand) commands.get(5)).getVar();
		assertEquals(x2.getType(), ExpressionType.LONG);
		assertEquals(((NumericBoundedVariable) x2).getName(), "x2");
		assertEquals(((NumericBoundedVariable) x2).getLowerBound(), 13l);
		assertEquals(((NumericBoundedVariable) x2).getUpperBound(), 15l);

		assertTrue(commands.get(6) instanceof VariableDeclarationCommand);
		Variable x3 = ((VariableDeclarationCommand) commands.get(6)).getVar();
		assertEquals(x3.getType(), ExpressionType.FLOAT);
		assertEquals(((NumericBoundedVariable) x3).getName(), "x3");
		assertEquals(((NumericBoundedVariable) x3).getLowerBound(), 3.1415f);
		assertEquals(((NumericBoundedVariable) x3).getUpperBound(), 2 * 3.1415f);

		assertTrue(commands.get(7) instanceof VariableDeclarationCommand);
		Variable x4 = ((VariableDeclarationCommand) commands.get(7)).getVar();
		assertEquals(x4.getType(), ExpressionType.STRING);
		assertEquals(((StringVariable) x4).getName(), "x4");
		assertEquals(((StringVariable) x4).getDomain(), "BLABLA");

		assertTrue(commands.get(8) instanceof AssertCommand);
		List<BooleanExpression> s1 = ((AssertCommand) commands.get(8)).getExprs();
		assertEquals(((NumericComparisonExpression) s1.get(0)).getFunction(), NumericComparisonExpression.Function.GT);
		assertEquals(((NumericComparisonExpression) s1.get(0)).getLeft(), x1);
		assertEquals(((NumericComparisonExpression) s1.get(0)).getRight(), x2);

		assertTrue(commands.get(9) instanceof AssertCommand);
		List<BooleanExpression> s2 = ((AssertCommand) commands.get(9)).getExprs();
		assertEquals(((NumericComparisonExpression) s2.get(0)).getFunction(), NumericComparisonExpression.Function.LE);
		assertEquals(((NumericComparisonExpression) s2.get(0)).getLeft(), x1);
		assertEquals(((NumericComparisonExpression) s2.get(0)).getRight(), x2);

		assertEquals(((NumericComparisonExpression) s2.get(1)).getFunction(), NumericComparisonExpression.Function.NE);
		assertEquals(((NumericComparisonExpression) s2.get(1)).getLeft(), x1);
		assertEquals(((NumericComparisonExpression) s2.get(1)).getRight(), NumericConstant.valueOf(0));

		assertTrue(commands.get(10) instanceof CountCommand);

		assertTrue(commands.get(11) instanceof AssertCommand);
		List<BooleanExpression> s3 = ((AssertCommand) commands.get(11)).getExprs();
		assertEquals(((BooleanNaryExpression) s3.get(0)).getFunction(), BooleanNaryExpression.Function.OR);
		Expression s3_0 = ((BooleanNaryExpression) s3.get(0)).getArgs().get(0);
		Expression s3_1 = ((BooleanNaryExpression) s3.get(0)).getArgs().get(1);
		assertEquals(((NumericComparisonExpression) s3_0).getFunction(), NumericComparisonExpression.Function.LT);
		assertEquals(((NumericComparisonExpression) s3_0).getLeft(), x1);
		assertEquals(((NumericComparisonExpression) s3_0).getRight(),
				new NumericBinaryExpression(x2,NumericConstant.valueOf(5), 
						NumericBinaryExpression.Function.ADD, PositionInfo.DUMMY));
		assertEquals(((NumericComparisonExpression) s3_1).getFunction(), NumericComparisonExpression.Function.GT);
		assertEquals(((NumericComparisonExpression) s3_1).getLeft(), x2);
		assertEquals(((NumericComparisonExpression) s3_1).getRight(), x3);
		
		assertTrue(commands.get(12) instanceof CountCommand);
		assertTrue(commands.get(13) instanceof ClearCommand);
		assertTrue(commands.get(14) instanceof AssertCommand);
	}

	@Test
	public void orWithNestedAnd() throws IOException {
		ANTLRFileStream input = new ANTLRFileStream("src/test/inputs/parser/or-with-and");
		InputFormatLexer lexer = new InputFormatLexer(input);
		InputFormatParser parser = new InputFormatParser(new CommonTokenStream(lexer));

		ParseTree tree = parser.input();
		SymbolTable symtable = new SymbolTable();
		List<? extends Command> commands = AstBuilder.parseCommands(symtable, tree);

		assertTrue(commands.get(0) instanceof VariableDeclarationCommand);
		assertTrue(commands.get(1) instanceof DependentVariableDeclarationCommand);
		assertTrue(commands.get(2) instanceof AssertCommand);

		List<BooleanExpression> s1 = ((AssertCommand) commands.get(2)).getExprs();
		assertEquals(((BooleanNaryExpression) s1.get(3)).getFunction(), BooleanNaryExpression.Function.OR);
	}
}