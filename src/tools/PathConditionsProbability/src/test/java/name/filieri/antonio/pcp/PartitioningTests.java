package name.filieri.antonio.pcp;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.Test;

import com.google.common.collect.Sets;

import name.filieri.antonio.pcp.ast.commands.AssertCommand;
import name.filieri.antonio.pcp.ast.commands.Command;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanNaryExpression;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanNaryExpression.Function;
import name.filieri.antonio.pcp.grammar.InputFormatLexer;
import name.filieri.antonio.pcp.grammar.InputFormatParser;
import name.filieri.antonio.pcp.visitors.AstBuilder;
import name.filieri.antonio.pcp.visitors.SymbolTable;

public class PartitioningTests {

	@Test
	public void testJtopasPartitioning() throws IOException {
		ANTLRFileStream input = new ANTLRFileStream("src/test/inputs/jtopas.pcp");
		InputFormatLexer lexer = new InputFormatLexer(input);
		InputFormatParser parser = new InputFormatParser(new CommonTokenStream(lexer));

		ParseTree tree = parser.input();
		SymbolTable symtable = new SymbolTable();
		List<? extends Command> commands = AstBuilder.parseCommands(symtable, tree);

		Constraint cons = new Constraint(
				new BooleanNaryExpression(Function.AND,
						((AssertCommand) commands.get(20)).getExprs()));

		ConstraintPartitioner part = new ConstraintPartitioner();
		part.computeVarClusters(Sets.newHashSet(cons));
		List<Constraint> partitions = part.partition(cons);
		System.out.println(partitions);
		assertEquals(partitions.size(), 20);
	}
}
