package name.filieri.antonio.jpf.analysis;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Set;

import name.filieri.antonio.jpf.analysis.RemoteAnalyzer.ConnectionManager;
import name.filieri.antonio.jpf.analysis.exceptions.AnalysisException;
import name.filieri.antonio.jpf.analysis.exceptions.EmptyDomainException;
import name.filieri.antonio.jpf.domain.Domain;
import name.filieri.antonio.jpf.domain.Problem;
import name.filieri.antonio.jpf.domain.UsageProfile;
import name.filieri.antonio.jpf.domain.exceptions.InvalidUsageProfileException;
import name.filieri.antonio.jpf.latte.LatteException;
import name.filieri.antonio.jpf.omega.exceptions.OmegaException;
import name.filieri.antonio.jpf.remote.CountServer;
import name.filieri.antonio.jpf.utils.BigRational;
import name.filieri.antonio.jpf.utils.Configuration;

public class SequentialAnalyzerRemoteTest {

	//@Test
	public static void main(String[] args) throws LatteException, InterruptedException, OmegaException, AnalysisException, EmptyDomainException, InvalidUsageProfileException, UnknownHostException, IOException {
		Domain.Builder domainBuilder = new Domain.Builder();
		domainBuilder.addVariable("x", 1, 10);
		Domain domain = domainBuilder.build();
		
		UsageProfile.Builder usageProfileBuilder = new UsageProfile.Builder();
		
		usageProfileBuilder.addScenario("x>=0", BigRational.valueOf("100/100"));
		//usageProfileBuilder.addScenario("x<=5", BigRational.valueOf("5/100"));
		
		UsageProfile usageProfile = usageProfileBuilder.build();
		
		Configuration configuration = new Configuration();
		configuration.setIsccExecutablePath("iscc");
		configuration.setTemporaryDirectory("/tmp/counters/");
		
		CountServer countServer = new CountServer(configuration, 9999);
		countServer.start();
		
		ConnectionManager connectionManager = new ConnectionManager("127.0.0.1", 9999);
		
		RemoteAnalyzer analyzer = new RemoteAnalyzer(domain, usageProfile, connectionManager);
		
		
		
		BigRational probabilityOf = analyzer.analyzeSpfPC("x>9");
		
		System.out.println(probabilityOf+"\t"+probabilityOf.doubleValue());
		
		
		connectionManager.close();
		countServer.stopServer();
		countServer.interrupt();
	}

}
