package name.filieri.antonio.pcp;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.nio.ByteBuffer;

import org.junit.Assume;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.common.base.Preconditions;
import com.zaxxer.nuprocess.NuAbstractProcessHandler;
import com.zaxxer.nuprocess.NuProcess;
import com.zaxxer.nuprocess.NuProcessBuilder;

public class ExternalProcessTests {

	static boolean barvinokInPath = false;
	static Options options = Options.loadFromUserHomeOrDefault();

	@BeforeClass
	public static void checkForCommandInPath() {
		Runtime rt = Runtime.getRuntime();
		Process proc;
		try {
			proc = rt.exec(options.getIsccPath()+" --help");
			proc.waitFor();
			int exitVal = proc.exitValue();
			if (exitVal == 0) {
				barvinokInPath = true;
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	static class DummyProcessHandler extends NuAbstractProcessHandler {

		private volatile String input = null;
		private volatile String reply = null;
		private NuProcess process = null;

		@Override
		public void onStart(NuProcess nuProcess) {
			process = nuProcess;
		};

		@Override
		public boolean onStdinReady(ByteBuffer buffer) {
			System.out.println("stdinready");
			Preconditions.checkState(input != null, "No input available to be sent to the model counter.");
			buffer.put(input.getBytes());
			buffer.flip();
			input = null;
			return false;
		};

		@Override
		public void onStdout(ByteBuffer buffer, boolean closed) {
			System.out.println("stdout");
			if (closed) {
				System.err.println("EOF received from barvinok!");
			} else {
				Preconditions.checkState(reply == null, "There is a unprocessed reply from the model counter");
				byte[] bytes = new byte[buffer.remaining()];
				buffer.get(bytes);
				reply = new String(bytes);
			}
		}

		@Override
		public void onStderr(ByteBuffer buffer, boolean closed) {
			System.out.println("stderr");
			if (closed) {
				System.err.println("EOF received from barvinok!");
			} else {
				Preconditions.checkState(reply == null, "There is a unprocessed error message from the model counter");
				byte[] bytes = new byte[buffer.remaining()];
				buffer.get(bytes);
				reply = new String(bytes);
			}
		}

		public void write(String s) {
			System.out.println("write");
			Preconditions.checkState(input == null, "There is an unprocessed input to be sent to the model counter.");
			input = s;
			process.wantWrite();
		}

		public boolean isReplyReady() {
			return reply != null;
		}

		public String getReply() {
			System.out.println("getReply");
			Preconditions.checkState(reply != null, "No reply received so far from the model counter.");
			String tmp = reply;
			reply = null;
			return tmp;
		}
	}

	@Test
	public void barvinokCallTest() throws Exception {
		Assume.assumeTrue(barvinokInPath);

		// we need to disable buffering on input for iscc. This is achieved by
		// wrapping the call with stdbuf, a tool from coreutils. I have no idea
		// what would be the equivalent solution for Windows.

		NuProcessBuilder pb = new NuProcessBuilder(new String[] { "stdbuf", "-o0", options.getIsccPath() });
		DummyProcessHandler handler = new DummyProcessHandler();
		pb.setProcessListener(handler);
		NuProcess process = pb.start();
		handler.write("P := { [i,j] : 0 <= i <= 10 and i <= j <= 10 };\ncard P;\n");

		while (!handler.isReplyReady()) {
			Thread.sleep(100);
		}
		String reply = handler.getReply();
		System.out.println(reply);
		assertTrue(reply.contains("{ 66 }"));

		handler.write("pure nonsense;\n\n");
		while (!handler.isReplyReady()) {
			Thread.sleep(100);
		}
		reply = handler.getReply();
		System.out.println(reply);
		assertTrue(reply.contains("syntax error"));

		handler.write("P := { [i,j] : 0 <= i <= 100 and i <= j <= 100 };\ncard P;\n");
		while (!handler.isReplyReady()) {
			Thread.sleep(100);
		}
		reply = handler.getReply();
		System.out.println(reply);
		assertTrue(reply.contains("5151"));

	}
}
