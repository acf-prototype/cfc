package name.filieri.antonio.pcp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import name.filieri.antonio.pcp.ast.expr.Expression;
import name.filieri.antonio.pcp.ast.expr.ExpressionType;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanExpression;
import name.filieri.antonio.pcp.ast.expr.bool.BooleanNaryExpression;
import name.filieri.antonio.pcp.ast.expr.bool.NotExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericBinaryExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericComparisonExpression;
import name.filieri.antonio.pcp.ast.expr.numeric.NumericConstant;
import name.filieri.antonio.pcp.ast.expr.probabilistic.UniformDoubleVariable;
import name.filieri.antonio.pcp.visitors.DisjunctiveNormalFormVisitor;

public class ConstraintTests {

	@Test
	public void testGetVars() {
		UniformDoubleVariable v1 = new UniformDoubleVariable("v1", 10, 20);
		UniformDoubleVariable v2 = new UniformDoubleVariable("v2", 10, 20);
		UniformDoubleVariable v3 = new UniformDoubleVariable("v3", 10, 20);

		NumericBinaryExpression e1 = new NumericBinaryExpression(v1, v2,
				NumericBinaryExpression.Function.ADD);
		NumericBinaryExpression e2 = new NumericBinaryExpression(v2, v3,
				NumericBinaryExpression.Function.ADD);
		NumericComparisonExpression e12 = new NumericComparisonExpression(e1, e2,
				NumericComparisonExpression.Function.LE);

		Constraint c = new Constraint(e12);
		assertTrue(c.getVariables().containsAll(ImmutableSet.of(v1, v2, v3)));
	}

	@Test
	public void testDNF_Simple() {
		UniformDoubleVariable v1 = new UniformDoubleVariable("v1", 10, 20);
		UniformDoubleVariable v2 = new UniformDoubleVariable("v2", 10, 20);
		UniformDoubleVariable v3 = new UniformDoubleVariable("v3", 10, 20);

		NumericConstant c1 = new NumericConstant(15.0, ExpressionType.DOUBLE);

		NumericBinaryExpression e1 = new NumericBinaryExpression(v1, v2,
				NumericBinaryExpression.Function.ADD);
		NumericComparisonExpression e1c1 = new NumericComparisonExpression(e1, c1,
				NumericComparisonExpression.Function.LE);
		BooleanExpression ne1c1 = new NotExpression(e1c1);

		// not(x + y <= c) -----> x + y > c
		List<Expression> dnf1 = DisjunctiveNormalFormVisitor.convertToDNF(ne1c1);
		assertEquals(dnf1.size(), 1);
		assertTrue(dnf1.get(0) instanceof NumericComparisonExpression);
		NumericComparisonExpression tmp1 = ((NumericComparisonExpression) dnf1.get(0));
		assertEquals(tmp1.getFunction(), NumericComparisonExpression.Function.GT);
		assertEquals(tmp1.getLeft(), e1);
		assertEquals(tmp1.getRight(), c1);

		NumericBinaryExpression e2 = new NumericBinaryExpression(v2, v3,
				NumericBinaryExpression.Function.SUB);
		NumericComparisonExpression e2c1 = new NumericComparisonExpression(e2, c1,
				NumericComparisonExpression.Function.EQ);
		BooleanExpression ne2c1 = new NotExpression(e2c1);

		// not(y - z = c) -----> y - z != c
		List<Expression> dnf2 = DisjunctiveNormalFormVisitor.convertToDNF(ne2c1);
		assertEquals(dnf2.size(), 1);
		assertTrue(dnf2.get(0) instanceof NumericComparisonExpression);
		NumericComparisonExpression tmp2 = ((NumericComparisonExpression) dnf2.get(0));
		assertEquals(tmp2.getFunction(), NumericComparisonExpression.Function.NE);
		assertEquals(tmp2.getLeft(), e2);
		assertEquals(tmp2.getRight(), c1);

		// not(x + y <= c and y - z = c) ------> x + y > c or y - z != c)

		BooleanExpression e2andc1 = new BooleanNaryExpression(BooleanNaryExpression.Function.AND,
				ImmutableList.<BooleanExpression> of(e1c1, e2c1));
		BooleanExpression n_e2andc1 = new NotExpression(e2andc1);

		List<Expression> dnf3 = DisjunctiveNormalFormVisitor.convertToDNF(n_e2andc1);
		assertEquals(dnf3.size(), 2);
		NumericComparisonExpression tmp3_0 = (NumericComparisonExpression) dnf3.get(0);
		NumericComparisonExpression tmp3_1 = (NumericComparisonExpression) dnf3.get(1);
		assertEquals(tmp3_0.getFunction(), NumericComparisonExpression.Function.GT);
		assertEquals(tmp3_0.getLeft(), e1);
		assertEquals(tmp3_0.getRight(), c1);
		assertEquals(tmp3_1.getFunction(), NumericComparisonExpression.Function.NE);
		assertEquals(tmp3_1.getLeft(), e2);
		assertEquals(tmp3_1.getRight(), c1);
		
		
		// not(x + y <= c or y - z = c) ------> x + y > c and y - z != c)
		BooleanExpression e2orc1 = new BooleanNaryExpression(BooleanNaryExpression.Function.OR,
				ImmutableList.<BooleanExpression> of(e1c1, e2c1));
		BooleanExpression n_e2orc1 = new NotExpression(e2orc1);

		List<Expression> dnf4 = DisjunctiveNormalFormVisitor.convertToDNF(n_e2orc1);
		assertEquals(dnf4.size(), 1);
		BooleanNaryExpression tmp4 = (BooleanNaryExpression) dnf4.get(0);
		assertEquals(tmp4.getFunction(), BooleanNaryExpression.Function.AND);
		NumericComparisonExpression tmp4_0 = (NumericComparisonExpression) tmp4.getArgs().get(0);
		NumericComparisonExpression tmp4_1 = (NumericComparisonExpression) tmp4.getArgs().get(1);
		assertEquals(tmp4_0.getFunction(), NumericComparisonExpression.Function.GT);
		assertEquals(tmp4_0.getLeft(), e1);
		assertEquals(tmp4_0.getRight(), c1);
		assertEquals(tmp4_1.getFunction(), NumericComparisonExpression.Function.NE);
		assertEquals(tmp4_1.getLeft(), e2);
		assertEquals(tmp4_1.getRight(), c1);
	}

	@Test
	public void testDoubleNegation() {
		UniformDoubleVariable v1 = new UniformDoubleVariable("v1", 10, 20);
		UniformDoubleVariable v2 = new UniformDoubleVariable("v2", 10, 20);

		NumericConstant c1 = new NumericConstant(15.0, ExpressionType.DOUBLE);

		NumericBinaryExpression e1 = new NumericBinaryExpression(v1, v2,
				NumericBinaryExpression.Function.ADD);
		NumericComparisonExpression e1c1 = new NumericComparisonExpression(e1, c1,
				NumericComparisonExpression.Function.LE);
		BooleanExpression n_e1c1 = new NotExpression(e1c1);
		BooleanExpression n_n_e1c1 = new NotExpression(n_e1c1);

		// not(not(x + y <= c)) -----> x + y <= c
		List<Expression> dnf = DisjunctiveNormalFormVisitor.convertToDNF(n_n_e1c1);
		assertEquals(dnf.size(), 1);
		assertEquals(dnf.get(0), e1c1);
	}

	@Test
	public void testDNF_NestedORs() {
		UniformDoubleVariable v1 = new UniformDoubleVariable("v1", 10, 20);
		UniformDoubleVariable v2 = new UniformDoubleVariable("v2", 10, 20);
		UniformDoubleVariable v3 = new UniformDoubleVariable("v3", 10, 20);
		NumericConstant c1 = new NumericConstant(15.0, ExpressionType.DOUBLE);

		NumericBinaryExpression e1 = new NumericBinaryExpression(v1, v2,
				NumericBinaryExpression.Function.ADD);
		NumericComparisonExpression e1c1 = new NumericComparisonExpression(e1, c1,
				NumericComparisonExpression.Function.LE);
		NumericBinaryExpression e2 = new NumericBinaryExpression(v2, v3,
				NumericBinaryExpression.Function.SUB);
		NumericComparisonExpression e2c1 = new NumericComparisonExpression(e2, c1,
				NumericComparisonExpression.Function.EQ);
		NumericComparisonExpression e1e2 = new NumericComparisonExpression(e1, e2,
				NumericComparisonExpression.Function.GE);
		NumericComparisonExpression c1c1 = new NumericComparisonExpression(c1, c1,
				NumericComparisonExpression.Function.EQ);

		BooleanExpression or = new BooleanNaryExpression(BooleanNaryExpression.Function.OR,
				ImmutableList.<BooleanExpression> of(e1c1, e2c1));
		BooleanExpression or_or = new BooleanNaryExpression(BooleanNaryExpression.Function.OR,
				ImmutableList.<BooleanExpression> of(or, e1e2));
		BooleanExpression and_or_or = new BooleanNaryExpression(BooleanNaryExpression.Function.AND,
				ImmutableList.<BooleanExpression> of(c1c1, or_or));

		List<Expression> dnf1 = DisjunctiveNormalFormVisitor.convertToDNF(and_or_or);
		System.out.println(dnf1);
		assertEquals(dnf1.size(), 3);
		BooleanNaryExpression tmp0_0 = (BooleanNaryExpression) dnf1.get(0);
		BooleanNaryExpression tmp0_1 = (BooleanNaryExpression) dnf1.get(1);
		BooleanNaryExpression tmp0_2 = (BooleanNaryExpression) dnf1.get(2);

		assertEquals(tmp0_0.getArgs().size(), 2);
		assertEquals(tmp0_1.getArgs().size(), 2);
		assertEquals(tmp0_2.getArgs().size(), 2);

		assertEquals(tmp0_0.getFunction(), BooleanNaryExpression.Function.AND);
		assertEquals(tmp0_1.getFunction(), BooleanNaryExpression.Function.AND);
		assertEquals(tmp0_2.getFunction(), BooleanNaryExpression.Function.AND);

		assertEquals(((NumericComparisonExpression) tmp0_0.getArgs().get(0)), c1c1);
		assertEquals(((NumericComparisonExpression) tmp0_0.getArgs().get(1)), e1c1);
		assertEquals(((NumericComparisonExpression) tmp0_1.getArgs().get(0)), c1c1);
		assertEquals(((NumericComparisonExpression) tmp0_1.getArgs().get(1)), e2c1);
		assertEquals(((NumericComparisonExpression) tmp0_2.getArgs().get(0)), c1c1);
		assertEquals(((NumericComparisonExpression) tmp0_2.getArgs().get(1)), e1e2);

	}
}
