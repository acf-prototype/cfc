package name.filieri.antonio.jpf.domain;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.TokenStream;

import name.filieri.antonio.jpf.analysis.exceptions.AnalysisException;
import name.filieri.antonio.jpf.domain.exceptions.InvalidUsageProfileException;
import name.filieri.antonio.jpf.grammar.LinearConstraintsLexer;
import name.filieri.antonio.jpf.grammar.LinearConstraintsParser;
import name.filieri.antonio.jpf.latte.LatteException;
import name.filieri.antonio.jpf.omega.exceptions.OmegaException;
import name.filieri.antonio.jpf.utils.BigRational;
import name.filieri.antonio.jpf.utils.Configuration;

public class ProblemTest {

	// @Test
	public void testComplement() throws InvalidUsageProfileException, AnalysisException, LatteException,
			InterruptedException, OmegaException {
		String pc = "x!=2";
		Problem spfProblem = null;
		try {
			LinearConstraintsLexer spfLexer = new LinearConstraintsLexer(new ANTLRInputStream(pc));
			TokenStream spfTokenStream = new CommonTokenStream(spfLexer);
			LinearConstraintsParser spfParser = new LinearConstraintsParser(spfTokenStream);
			spfProblem = spfParser.relation().relation0;
		} catch (RecognitionException e) {
			System.out.println("Cannot parse path condition:\n" + pc);
			e.printStackTrace();
		}

		Domain.Builder domainBuilder = new Domain.Builder();
		domainBuilder.addVariable("x", 1, 10);
		Domain domain = domainBuilder.build();

		UsageProfile.Builder usageProfileBuilder = new UsageProfile.Builder();

		usageProfileBuilder.addScenario("x>5", BigRational.valueOf("95/100"));
		usageProfileBuilder.addScenario("x<=5", BigRational.valueOf("5/100"));

		UsageProfile usageProfile = usageProfileBuilder.build();

		Configuration configuration = new Configuration();

		// SequentialAnalyzer analyzer = new SequentialAnalyzer(configuration,
		// domain, usageProfile, 1);

		// System.out.println("Problem: " + spfProblem);
		// System.out.println("\nComplement: " +
		// analyzer.complementProblem(spfProblem));

		System.out.println("\nOmega:\n" + spfProblem.toExecutableOmega("Rx"));

		System.out.println("\n\n\nComplement Omega:\n" + spfProblem.toComplementedExecutableOmega("Rc"));
	}

}
