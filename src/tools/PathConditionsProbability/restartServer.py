import os, sys, signal, subprocess, time

project_root = os.path.realpath('.')

f = open(project_root+'/classpath', 'r')
deps = f.read()
f.close()

classes=project_root+'/target/classes'

#main class
main='name.filieri.antonio.pcp.PCPServer'
log=project_root+'/pcpserver.log'


if len(sys.argv)>1:
	port = int(sys.argv[1])

	if len(sys.argv)>2:
		pid = int(sys.argv[2])
		os.kill(int(pid), signal.SIGTERM) #or signal.SIGKILL
		time.sleep(1) # wait for the old process to terminate

	command = 'java -Xmx1G -classpath "'+str(classes)+':'+deps+'" '+str(main)+' '+str(port)+' &> '+str(log)+'.'+str(port) +' & echo "PID: "$!'
	proc = subprocess.Popen(command, shell=True, executable='/bin/bash')
else:
	print 'Usage: restartServer port [old_pid]'

