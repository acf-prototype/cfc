#!/bin/bash
if [ $# -ne 4 ];
    then echo "Expected four data files as parameters: gen_poss_def_slice.dat first, then gen_poss_def.dat, then gen_poss.dat, then gen.dat"; exit

fi

echo "set xtics" >/tmp/gnuplot.in
echo "set term pdfcairo dashed" >>/tmp/gnuplot.in
echo "set output 'stacked_times.pdf'" >>/tmp/gnuplot.in
echo "set style data histogram" >>/tmp/gnuplot.in
echo "set style histogram rowstacked" >>/tmp/gnuplot.in
echo "plot \"$1\" with impulses lc 'blue' lw 3 title 'Slice time', \"$2\" with impulses lc 'yellow' lw 4 title 'Definite failure time', \"$3\" with impulses lc 'red' lw 4 title 'Possible failure time', \"$4\" with impulses lc 'light-gray' lw 4 title 'Generalization time'" >>/tmp/gnuplot.in
gnuplot /tmp/gnuplot.in
