def get_time ( key, str )
  time = str.match(/#{key} time: (\d+)/).captures.first.to_i
  return time
end

def poss_time ( str )
  get_time("Possible failure", str)
end

def def_time ( str )
  get_time("Definite failure", str)
end

def slice_time ( str )
  get_time("Slice", str)
end

file_name = ARGV[0]
file = File.read(file_name)

unless file.include? "Generalizations: 0"

  analysis_time = get_time("Analysis", file)
  other_time = poss_time(file) + def_time(file) + slice_time(file)
  gen_time = analysis_time - other_time
  new_file = file.gsub(/Generalization time: \d+/, "Generalization time: #{gen_time}")

  File.open(file_name, "w") {|f| f.puts new_file}
  
end
