#!/bin/bash
if [ $# -ne 2 ];
    then echo "Expected two data files as parameters: total_partitions.dat first, then partitions_without_gap.dat."; exit

fi

echo "unset xtics" >/tmp/gnuplot.in
echo "set term pdfcairo dashed" >>/tmp/gnuplot.in
echo "set output 'stacked_partitions.pdf'" >>/tmp/gnuplot.in
echo "set style data histogram" >>/tmp/gnuplot.in
echo "set style histogram rowstacked" >>/tmp/gnuplot.in
echo "plot \"$1\" with impulses lc 'black' lw 3 title 'Gap in bounds', \"$2\" with impulses lc 'grey' lw 4 title 'Equivalent bounds'" >>/tmp/gnuplot.in
gnuplot /tmp/gnuplot.in
