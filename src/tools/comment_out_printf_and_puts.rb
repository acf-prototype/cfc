# To run in some directory, dir:
# > for file in dir/*.c; do ruby comment_out_printf_and_puts.rb "$file"; done

if ARGV.size != 1
 puts "Expecting (single) file argument."; abort
end 

#path = Dir.pwd+'/'+ARGV[0]
path = ARGV[0]

def comment_out ( line )
  # Must escape two backslashes
  return "//"+line
end

lines = IO.readlines(path).map do |line|
  if (line.include? 'printf' or line.include? 'puts')
    comment_out(line)
  else
    line
  end
end
File.open(path, 'w') do |file|
  file.puts lines
end
