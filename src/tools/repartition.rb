bench_dir = "/home/mgerrard/work/bench_subset"
new_dir = "/home/mgerrard/work/sv_benchmark_subset"
Dir.mkdir new_dir unless File.exist? new_dir

all_file_paths = []

Dir.foreach(bench_dir) do |subdir|
  next if subdir.start_with? '.'
  subdir_path = bench_dir+'/'+subdir
  Dir.foreach(subdir_path) do |file|
    next if file.start_with? '.'
    file_path = "#{subdir_path}/#{file}"
    all_file_paths << file_path
  end
end

all_file_paths.shuffle!
file_groups = all_file_paths.each_slice(5).to_a

file_groups.each_with_index do |group, i|
  group_dir = "#{new_dir}/#{i}"
  Dir.mkdir(group_dir) unless File.exist? group_dir
  group.each do |file|
    bare_file = file.split('/').last
    `cp #{file} #{group_dir}/#{bare_file}`
  end
end

