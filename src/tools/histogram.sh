#!/bin/bash
if [ $# -ne 2 ];
    then echo "Expected data file and metric name as parameters"; exit

fi
sort -nr $1 >$1.sorted
echo "unset xtics" >/tmp/gnuplot.in
echo "set term png" >/tmp/gnuplot.in
echo "set output \"$2.png\"" >>/tmp/gnuplot.in
echo "set style data histogram" >>/tmp/gnuplot.in
echo "set style histogram rowstacked" >>/tmp/gnuplot.in
echo "plot \"$1.sorted\" title '$2'" >>/tmp/gnuplot.in
gnuplot /tmp/gnuplot.in
rm $1.sorted
