    require 'colorize'

    require 'optparse'

    require 'fileutils'
    require 'parallel'

    require 'open3'

    require 'nokogiri'

    require 'pp'

    CIVL_DIR = File.expand_path('../analyzers', File.dirname(__FILE__))
    UA_DIR = File.expand_path('../analyzers/uautomizer', File.dirname(__FILE__))
    CPA_DIR = File.expand_path('../analyzers/cpachecker', File.dirname(__FILE__))

    unknown = 0
    safe = 1
    error_found = 2
    cached = 3
    iteration = 0

    # UA_DIR defined in ./system_dependencies.rb
    ua_script = "#{UA_DIR}/Ultimate.py"
    UA_SCRIPT = "#{UA_DIR}/Ultimate.py"
    ua_specification = "#{UA_DIR}/PropertyUnreachCall.prp"
    UA_SPEC = ua_specification
    ua_timeouts = 0
    TIMEOUT_STATUS = 124 # the exit status when `timeout` times out

    # CPA_DIR defined in ./system_dependencies.rb 
    cpa_script = "#{CPA_DIR}/scripts/cpa.sh"
    CPA_SCRIPT = cpa_script
    cpa_specification = "#{CPA_DIR}/config/specification/sv-comp-reachability.spc"
    CPA_SPEC = cpa_specification
    cpa_debug = true

    cpa_timeouts = 0

    guidance_file = ""

    # CIVL_DIR defined in ./system_dependencies.rb
    civl_jar = "java -jar -ea #{CIVL_DIR}/civl.jar"
    civl_debug = true
    civl_timeouts = 0

    spurious_error_count = 0

    trace_id = 0
    svcomp_error_summary = ""

    $input_counts = {}
    $input_types = {}
    $symvars_to_lines = {}
    $lines_to_vars = {}
    $raw_to_pretty_reads = {}

    spurious_errors = []

    $bounds_map = {}
    $evidence_to_subspace = {}

    cfc_pairs = []

    $conjuncts_sliced = 0
    $equiv_partition_stack = []
    $last_injected_clauses_queue = Queue.new

    always_fails = false

    top_of_lattice = ["1"]   # logical 'true' in C
    generalization_count = 0
    analysis_moved_to_top = false
    just_generalized = false

  #  Input  : upper_C, stalling_c
  #  Output : generalized_blocking_clause

  #  1. max_{c \in upper_C} #(c \cap stalling_c)
  #  2. base = c \cap upper_C
  #  3. foreach x \in {stalling_c - base}:
  #       does over_C \cup {stalling_c - x}
  #       successfully block?

  #  Data structure implementation:
  #    upper_C     : an array of array of strings
  #    stalling_c : an array of strings 

    class Stopwatch

    attr_reader :time_spent

    def initialize
      @time_spent = 0
      @clock_is_running = false
    end
    
    def start
      assert { not @clock_is_running }
      @clock_is_running = true
      @start_time = Time.now
    end

    def stop
      assert { @clock_is_running }
      @clock_is_running = false
      @end_time = Time.now
      @time_spent += (@start_time - @end_time).to_i.abs    
    end

    def reset
      @time_spent = 0
    end

  end


    class OverPortfolio

      attr_accessor :timeout
      attr_reader :analyzers
    
      def initialize ( analyzers, timeout )
        @analyzers = analyzers
        @timeout = timeout
      end
    
    end

    class OverapproximateComputation
      attr_reader :name
      attr_reader :result
      attr_reader :evidence
      attr_accessor :safety_clause
      attr_accessor :generalized_clause

      def initialize ( n, r, e=nil )
        @name = n
        @result = r
        @evidence = e
      end
    end

    class GeneralizationResult
      attr_reader :pieces_of_evidence
      attr_reader :generalized_clause

      def initialize ( es, gc )
        @pieces_of_evidence = es
        @generalized_clause = gc
      end
    end

    class UnderapproximateComputation
      attr_reader :result
      attr_reader :subspace
      attr_reader :under_update

      def initialize ( r, s=nil, u=nil )
        @result = r
        @subspace = s
        @under_update = u
      end

      def to_s
        if @result == "FALSE"
          "FALSE. Subspace: #{@subspace}"
        else
          @result
        end
      end

      def ==(other)
        if self.result == "FALSE"
          self.under_update == other.under_update
        else
          self.result == other.result
        end
      end

      def hash
        if self.result == "FALSE"
          under_update.sliced_pc.hash
        else
          result.hash
        end
      end
      alias eql? ==

    end
    
    class Overapproximator
    
      attr_reader :tool
      attr_reader :config
    
      def initialize ( tool, config="" )
        @tool = tool; @config = config
      end
    
      def run_analysis ( input_file, output_dir, timeout )
    
  #      FileUtils.mkdir_p output_dir unless File.exist? output_dir
        bare_cmd = build_cmd(input_file, output_dir)
        cmd = "timeout #{timeout} #{bare_cmd}"
        out, stderr, status = Open3.capture3(cmd)
    
        result = ""
        if $?.exitstatus == TIMEOUT_STATUS
          result = "UNKNOWN"
        else
          result = get_result(out)
        end

        name = @tool+@config
        if result == "FALSE"
          witness = witness(output_dir)
          return OverapproximateComputation.new(name, result, witness)
        else
          return OverapproximateComputation.new(name, result)
        end
    
      end
    
      def build_cmd ( input_file, output_dir )
    
        case @tool
        when "CPA"
          cmd = "#{CPA_SCRIPT} -#{@config} "\
                "-timelimit 900 "\
                "-spec #{CPA_SPEC} "\
                "-setprop output.path=\"#{output_dir}\" "\
                "#{input_file}"
        when "UA"
          cmd = "#{UA_SCRIPT} "\
                "--full-output "\
                "--spec #{UA_SPEC} "\
                "--architecture 32bit "\
                "--file #{input_file} "\
                "--witness-dir #{output_dir}"       
        else
          puts "The tool: #{@tool} is not implemented."; abort
        end
    
      end
    
      def get_result ( output )
    
        case @tool
        when "CPA"
          if output.include? "Verification result: TRUE."
            return "TRUE"
          elsif output.include? "Verification result: FALSE."
            return "FALSE"
          else
            return "UNKNOWN"
          end
        when "UA"
          if output.include? "Result:\nTRUE"
            return "TRUE"
          elsif output.include? "Result:\nFALSE"
            return "FALSE"
          else
            return "UNKNOWN"
          end
        else
          puts "The tool: #{@tool} is not implemented."; abort
        end
    
      end
    
      def witness ( output_dir )

        witness_file = ""

        case @tool
        when "CPA"
          witness_file = get_cpa_witness(output_dir)
        when "UA"
          witness_file = get_ua_witness(output_dir)
        else
          puts "The witness finder for #{@tool} is not implemented."; abort
        end

        if @tool == "CPA"
          get_abstract_witness(witness_file, true)
        else
          get_abstract_witness(witness_file, false)
        end

      end

        def get_cpa_witness ( output_dir )
          if @config == "sv-comp17"
            witness_file = output_dir+"/violation-witness.graphml"
            assert { File.exist? witness_file }
            return witness_file
          else
            gz_file = output_dir+"/witness.graphml.gz"
            assert { File.exist? gz_file }
            witness_file = output_dir+"/witness.graphml"
            `rm #{witness_file}` if File.exist? witness_file
            `gunzip #{gz_file}`
            assert { File.exist? witness_file }
            return witness_file
          end
        end

        def get_ua_witness ( output_dir )
          witness_file = output_dir+"/witness.graphml"
          assert { File.exist? witness_file }
          return witness_file
        end

    
    end

    class Branch
      attr_accessor :conditional, :branch_taken, :line_number, :is_loop_head

      def initialize( branch, loop_head_flag )
        @conditional = extract_conditional(branch)
        @branch_taken = discover_branch_taken(branch)
        @line_number = extract_line_number(branch)
        @is_loop_head = loop_head_flag
      end

      def extract_line_number( branch )
        line_number = branch.css("data[key='startline']").text
        return line_number.to_i
      end

      def discover_branch_taken( branch )
        if branch.css("data[key='control']").text == "condition-true"
          return 1
        else
          return 0
        end
      end

      def extract_conditional( branch )
        raw_cond = select_conditional_source(branch)
        conditional = ''
        if conditional[1] == '!'
          conditional = raw_cond[3..-3]
        else 
          conditional = raw_cond[1..-2] # Strip enclosing braces
        end
        return conditional
      end

      def select_conditional_source( branch )
        return branch.css("data[key='sourcecode']").text
      end
    end

    class Guidance

      attr_reader :branch_lines
      attr_reader :outcomes 

      def initialize ( bls, os )
        @branch_lines = bls
        @outcomes = os
      end

      def canonical_string
        string = ""
        @branch_lines.each_with_index do |branch, i|
          outcome = @outcomes[i]
          string += "(#{branch},#{outcome}) "
        end
        return string
      end
    
      def ==(another_guidance)
        self.canonical_string == another_guidance.canonical_string
      end

      def hash
        canonical_string.hash
      end
      alias eql? ==

    end

    class Directive
      attr_reader :evidence
      attr_reader :file

      def initialize ( e, f )
        @evidence = e; @file = f
      end
    end

    class UnderUpdates

      attr_reader :input_counts
      attr_reader :input_types
      attr_reader :symvars_to_lines
      attr_reader :lines_to_vars
      attr_reader :number_sliced_away
      attr_reader :sliced_pc
      attr_reader :bounds_image
      attr_reader :evidence

      def initialize ( slice_output, symvars_to_lines, evidence )
        @input_counts = parse_input_frequency(slice_output)
        @input_types = parse_input_types(slice_output)
        @symvars_to_lines = parse_read_calls(slice_output)
     
        temp_symvars_to_lines = symvars_to_lines.dup
        temp_symvars_to_lines = update_symvars_to_lines(temp_symvars_to_lines, @symvars_to_lines)
    
        @lines_to_vars = make_line_to_var_map(slice_output)
        @number_sliced_away = get_number_sliced_away(slice_output)
        @sliced_pc = parse_assumptions(slice_output, temp_symvars_to_lines)
        @bounds_image = parse_smt_bounds(slice_output, @sliced_pc)
        @evidence = evidence
      end

      def print
        puts "Printing UnderUpdate structure:".blue
        puts " Input counts:"; pp @input_counts
        puts " Input types:"; pp @input_types
        puts " Symvars to lines:"; pp @symvars_to_lines
        puts " Lines to vars:"; pp @lines_to_vars
        puts " Number of conjuncts sliced: #{@number_sliced_away}"
        puts " Sliced PC: #{@sliced_pc}"
      end

      def ==(other)
        self.sliced_pc == other.sliced_pc
      end

      def hash
        sliced_pc.hash
      end
      alias eql? ==

    end

    class Expression
      attr_reader :c_expr
      attr_reader :smt_expr

      def initialize ( c_ex, smt_ex )
        @c_expr = c_ex; @smt_expr = smt_ex     
      end
    end

    class CfcPair
      attr_accessor :upper
      attr_reader :lower
      attr_reader :declarations

      def initialize ( exprs, decls )
        @upper = exprs; @lower = [exprs]
        @declarations = decls
      end
    end

    class Instrumentation
      attr_accessor :program, :assumptions, :symvars_to_lines, :input_counts, :input_types

      def initialize (program, assumptions, sym_map, count_map, type_map)
        @program = program
        @assumptions = assumptions
        @symvars_to_lines = sym_map
        @input_counts = count_map
        @input_types = type_map
      end

      def display
        puts "Global instrumentation object:"; puts
        puts "  Program: #{@program}"
        puts "  Assumptions: #{@assumptions}"
      end
    end

      class Stopwatch

      attr_reader :time_spent

      def initialize
        @time_spent = 0
        @clock_is_running = false
      end
      
      def start
        assert { not @clock_is_running }
        @clock_is_running = true
        @start_time = Time.now
      end

      def stop
        assert { @clock_is_running }
        @clock_is_running = false
        @end_time = Time.now
        @time_spent += (@start_time - @end_time).to_i.abs    
      end

      def reset
        @time_spent = 0
      end

    end


      class OverPortfolio

        attr_accessor :timeout
        attr_reader :analyzers
      
        def initialize ( analyzers, timeout )
          @analyzers = analyzers
          @timeout = timeout
        end
      
      end

      class OverapproximateComputation
        attr_reader :name
        attr_reader :result
        attr_reader :evidence
        attr_accessor :safety_clause
        attr_accessor :generalized_clause

        def initialize ( n, r, e=nil )
          @name = n
          @result = r
          @evidence = e
        end
      end

      class GeneralizationResult
        attr_reader :pieces_of_evidence
        attr_reader :generalized_clause

        def initialize ( es, gc )
          @pieces_of_evidence = es
          @generalized_clause = gc
        end
      end

      class UnderapproximateComputation
        attr_reader :result
        attr_reader :subspace
        attr_reader :under_update

        def initialize ( r, s=nil, u=nil )
          @result = r
          @subspace = s
          @under_update = u
        end

        def to_s
          if @result == "FALSE"
            "FALSE. Subspace: #{@subspace}"
          else
            @result
          end
        end

        def ==(other)
          if self.result == "FALSE"
            self.under_update == other.under_update
          else
            self.result == other.result
          end
        end

        def hash
          if self.result == "FALSE"
            under_update.sliced_pc.hash
          else
            result.hash
          end
        end
        alias eql? ==

      end
      
      class Overapproximator
      
        attr_reader :tool
        attr_reader :config
      
        def initialize ( tool, config="" )
          @tool = tool; @config = config
        end
      
        def run_analysis ( input_file, output_dir, timeout )
      
    #      FileUtils.mkdir_p output_dir unless File.exist? output_dir
          bare_cmd = build_cmd(input_file, output_dir)
          cmd = "timeout #{timeout} #{bare_cmd}"
          out, stderr, status = Open3.capture3(cmd)
      
          result = ""
          if $?.exitstatus == TIMEOUT_STATUS
            result = "UNKNOWN"
          else
            result = get_result(out)
          end

          name = @tool+@config
          if result == "FALSE"
            witness = witness(output_dir)
            return OverapproximateComputation.new(name, result, witness)
          else
            return OverapproximateComputation.new(name, result)
          end
      
        end
      
        def build_cmd ( input_file, output_dir )
      
          case @tool
          when "CPA"
            cmd = "#{CPA_SCRIPT} -#{@config} "\
                  "-timelimit 900 "\
                  "-spec #{CPA_SPEC} "\
                  "-setprop output.path=\"#{output_dir}\" "\
                  "#{input_file}"
          when "UA"
            cmd = "#{UA_SCRIPT} "\
                  "--full-output "\
                  "--spec #{UA_SPEC} "\
                  "--architecture 32bit "\
                  "--file #{input_file} "\
                  "--witness-dir #{output_dir}"       
          else
            puts "The tool: #{@tool} is not implemented."; abort
          end
      
        end
      
        def get_result ( output )
      
          case @tool
          when "CPA"
            if output.include? "Verification result: TRUE."
              return "TRUE"
            elsif output.include? "Verification result: FALSE."
              return "FALSE"
            else
              return "UNKNOWN"
            end
          when "UA"
            if output.include? "Result:\nTRUE"
              return "TRUE"
            elsif output.include? "Result:\nFALSE"
              return "FALSE"
            else
              return "UNKNOWN"
            end
          else
            puts "The tool: #{@tool} is not implemented."; abort
          end
      
        end
      
        def witness ( output_dir )

          witness_file = ""

          case @tool
          when "CPA"
            witness_file = get_cpa_witness(output_dir)
          when "UA"
            witness_file = get_ua_witness(output_dir)
          else
            puts "The witness finder for #{@tool} is not implemented."; abort
          end

          if @tool == "CPA"
            get_abstract_witness(witness_file, true)
          else
            get_abstract_witness(witness_file, false)
          end

        end

          def get_cpa_witness ( output_dir )
            if @config == "sv-comp17"
              witness_file = output_dir+"/violation-witness.graphml"
              assert { File.exist? witness_file }
              return witness_file
            else
              gz_file = output_dir+"/witness.graphml.gz"
              assert { File.exist? gz_file }
              witness_file = output_dir+"/witness.graphml"
              `rm #{witness_file}` if File.exist? witness_file
              `gunzip #{gz_file}`
              assert { File.exist? witness_file }
              return witness_file
            end
          end

          def get_ua_witness ( output_dir )
            witness_file = output_dir+"/witness.graphml"
            assert { File.exist? witness_file }
            return witness_file
          end

      
      end

      class Branch
        attr_accessor :conditional, :branch_taken, :line_number, :is_loop_head

        def initialize( branch, loop_head_flag )
          @conditional = extract_conditional(branch)
          @branch_taken = discover_branch_taken(branch)
          @line_number = extract_line_number(branch)
          @is_loop_head = loop_head_flag
        end

        def extract_line_number( branch )
          line_number = branch.css("data[key='startline']").text
          return line_number.to_i
        end

        def discover_branch_taken( branch )
          if branch.css("data[key='control']").text == "condition-true"
            return 1
          else
            return 0
          end
        end

        def extract_conditional( branch )
          raw_cond = select_conditional_source(branch)
          conditional = ''
          if conditional[1] == '!'
            conditional = raw_cond[3..-3]
          else 
            conditional = raw_cond[1..-2] # Strip enclosing braces
          end
          return conditional
        end

        def select_conditional_source( branch )
          return branch.css("data[key='sourcecode']").text
        end
      end

      class Guidance

        attr_reader :branch_lines
        attr_reader :outcomes 

        def initialize ( bls, os )
          @branch_lines = bls
          @outcomes = os
        end

        def canonical_string
          string = ""
          @branch_lines.each_with_index do |branch, i|
            outcome = @outcomes[i]
            string += "(#{branch},#{outcome}) "
          end
          return string
        end
      
        def ==(another_guidance)
          self.canonical_string == another_guidance.canonical_string
        end

        def hash
          canonical_string.hash
        end
        alias eql? ==

      end

      class Directive
        attr_reader :evidence
        attr_reader :file

        def initialize ( e, f )
          @evidence = e; @file = f
        end
      end

      class UnderUpdates

        attr_reader :input_counts
        attr_reader :input_types
        attr_reader :symvars_to_lines
        attr_reader :lines_to_vars
        attr_reader :number_sliced_away
        attr_reader :sliced_pc
        attr_reader :bounds_image
        attr_reader :evidence

        def initialize ( slice_output, symvars_to_lines, evidence )
          @input_counts = parse_input_frequency(slice_output)
          @input_types = parse_input_types(slice_output)
          @symvars_to_lines = parse_read_calls(slice_output)
       
          temp_symvars_to_lines = symvars_to_lines.dup
          temp_symvars_to_lines = update_symvars_to_lines(temp_symvars_to_lines, @symvars_to_lines)
      
          @lines_to_vars = make_line_to_var_map(slice_output)
          @number_sliced_away = get_number_sliced_away(slice_output)
          @sliced_pc = parse_assumptions(slice_output, temp_symvars_to_lines)
          @bounds_image = parse_smt_bounds(slice_output, @sliced_pc)
          @evidence = evidence
        end

        def print
          puts "Printing UnderUpdate structure:".blue
          puts " Input counts:"; pp @input_counts
          puts " Input types:"; pp @input_types
          puts " Symvars to lines:"; pp @symvars_to_lines
          puts " Lines to vars:"; pp @lines_to_vars
          puts " Number of conjuncts sliced: #{@number_sliced_away}"
          puts " Sliced PC: #{@sliced_pc}"
        end

        def ==(other)
          self.sliced_pc == other.sliced_pc
        end

        def hash
          sliced_pc.hash
        end
        alias eql? ==

      end

      class Expression
        attr_reader :c_expr
        attr_reader :smt_expr

        def initialize ( c_ex, smt_ex )
          @c_expr = c_ex; @smt_expr = smt_ex     
        end
      end

      class CfcPair
        attr_accessor :upper
        attr_reader :lower
        attr_reader :declarations

        def initialize ( exprs, decls )
          @upper = exprs; @lower = [exprs]
          @declarations = decls
        end
      end

      class Instrumentation
        attr_accessor :program, :assumptions, :symvars_to_lines, :input_counts, :input_types

        def initialize (program, assumptions, sym_map, count_map, type_map)
          @program = program
          @assumptions = assumptions
          @symvars_to_lines = sym_map
          @input_counts = count_map
          @input_types = type_map
        end

        def display
          puts "Global instrumentation object:"; puts
          puts "  Program: #{@program}"
          puts "  Assumptions: #{@assumptions}"
        end
      end


    class AssertionError < RuntimeError
    end

    def assert &block
      raise AssertionError unless yield
    end

    def get_bare_name ( program )
      # Drop the extension after the *final* period
      name_without_ext = program.split('.')[0..-2].join('.')
      bare_name = name_without_ext.split('/').last
      return bare_name
    end

    def run_in_parallel ( portfolio, input_file, dir_base )

      computations = Parallel.map(portfolio.analyzers) do |analyzer|
        output_dir = dir_base+"/#{analyzer.tool+analyzer.config}"
        `mkdir #{output_dir}` unless File.exist? output_dir
        analyzer.run_analysis(input_file, output_dir, portfolio.timeout)
      end

      computations.each do |c|
        if c.result == "FALSE"
          message = "#{c.name} result: FALSE. Evidence: #{c.evidence.canonical_string}"
          $log.write message
          puts message.red
        else
          message = "#{c.name} result: #{c.result}"
          $log.write message
          puts message.blue
        end
      end

      return computations  

    end

    def run_civl_in_parallel ( directives, dir_base, input_file, disjoint_covering )
      
      civl_jar = "java -jar -ea #{CIVL_DIR}/civl.jar"
      computations = Parallel.map_with_index(directives) do |directive, i|
   
        program_copy = "#{input_file}.#{i}.c"
        `cp #{input_file} #{program_copy}`       

        civl_cmd = construct_civl_verify_cmd( program_copy, directive.file, civl_jar)
        civl_output = `#{civl_cmd}`

        if svcomp_error_found(civl_output)
          trace_id = 0
              summary_regex = 'Violation \d encountered at depth .*?(?=\R\R|\z)'
              violation_summaries = civl_output.scan(/#{summary_regex}/m).flatten

            violation_summaries.each do |violation|
              if is_svcomp_error(violation)
                trace_id = extract_trace_id(violation)
                svcomp_error_summary = violation
                break
              end
            end

          slice_cmd = "#{civl_jar} replay -id=#{trace_id} -sliceAnalysis #{program_copy}"
          slice_output = `#{slice_cmd}`
          under_updates = UnderUpdates.new(slice_output, $symvars_to_lines, directive.evidence)

          UnderapproximateComputation.new("FALSE", under_updates.sliced_pc, under_updates)

        elsif timed_out(civl_output)
          UnderapproximateComputation.new("UNKNOWN")
        else
          puts "Didn't find an error".green
          UnderapproximateComputation.new("TRUE")
        end
      end

      computations.uniq!
      puts "Parallel computations:".blue
      puts computations

      $last_injected_clauses_queue.clear if computations.select {|c| c.result == "FALSE"}.any?
      computations.select {|c| c.result == "FALSE"}.each do |c|
        update_global_maps(c.under_update)
        
        $last_injected_clauses_queue.enq c.subspace
        
        $bounds_map[c.subspace] = c.under_update.bounds_image
        sliced_pc = c.subspace
          disjoint_covering[sliced_pc] = [sliced_pc]
            message = "\n  Adding new entry to disjoint covering:\n"\
                        "    #{sliced_pc}\n"
            $log.write message


        $evidence_to_subspace[c.under_update.evidence] = c.subspace
      end

      if computations.select {|c| c.result == "FALSE"}.any?
        $underapprox_result = 2 # error_found
      else
        $underapprox_result = 0 # unknown
      end

    end

    def update_global_maps ( updates )
      $input_counts = update_input_counts($input_counts, updates.input_counts)    
      $input_types = update_input_types($input_types, updates.input_types) 
      $symvars_to_lines = update_symvars_to_lines($symvars_to_lines, updates.symvars_to_lines)    
      $lines_to_vars = update_lines_to_vars($lines_to_vars, updates.lines_to_vars)
      $conjuncts_sliced += updates.number_sliced_away
    end

    def new_evidence ( pieces_of_evidence, cache )
      return (pieces_of_evidence - cache)
    end

    def has_new_evidence ( evidence, cache )
      return cache.include? evidence
    end

    def prepend_error_at_line ( file, target_line )

      curr_line = 1
      lines = IO.readlines(file).map do |line|
        if curr_line == target_line
          line = '__VERIFIER_error(); //' + line
        end
        curr_line += 1
        line
      end
       
      File.open(file, 'w') do |f|
        f.puts lines
      end

    end

    def rewrite_line_with_string ( file, target_line, string )

      curr_line = 1
      lines = IO.readlines(file).map do |line|
        if curr_line == target_line
          line = string
        end
        curr_line += 1
        line
      end
       
      File.open(file, 'w') do |f|
        f.puts lines
      end

    end

    # The canonical string for an abstract witness will
    # be space delimited tuples of:
    #   (branch, branch_outcome)
    def get_canonical_string (witness)

      string = ""

      witness.branch_lines.each_with_index do |branch, i|
        outcome = witness.outcomes[i]
        string += "(#{branch},#{outcome}) "
      end

      return string

    end

    def get_abstract_witness (witness_graphml, is_cpa)

      graphml_file = File.read(witness_graphml)
      witness = Nokogiri::XML.parse(graphml_file)

      branches = get_branches_in_error_path(witness, is_cpa)
      branch_lines = branches.map { |b| b.line_number }
      outcomes = branches.map { |b| b.branch_taken }

      return Guidance.new(branch_lines, outcomes)

    end

    def is_relevant_control_edge (edge)
      sourcecode = "data[key='sourcecode']"

      if edge.values.include? "sink"
        return false
      elsif edge.css(sourcecode).text.include? "instrumentation_index"
        return false
      elsif edge.css(sourcecode).text.include? "input_at_"
        return false
      elsif edge.css(sourcecode).text.include? "__VERIFIER_assume("
        return false
      elsif edge.css(sourcecode).text.include? "INPUTS_at_"
        return false
      elsif edge.css(sourcecode).text.include? "__CPAchecker_TMP"
        return false
      else
        return true
      end
    end

    def get_branches_in_error_path ( witness, is_cpa )

      branch_trace = []

      is_loop_head = false
      witness.css('edge').each do |edge|
        if edge.at("data[key='enterLoopHead']")
          is_loop_head = true
          next
        end
        if edge.at("data[key='control']")
          if is_relevant_control_edge(edge)
            branch_trace << Branch.new(edge, is_loop_head)
          end
        end
        is_loop_head = false
      end

      if is_cpa
          branches_to_remove = []
          
          branch_trace.each_cons(2) do |curr_b, next_b|
            if ((curr_b.line_number == next_b.line_number) &&
                (!curr_b.is_loop_head))
              branches_to_remove << curr_b
            end
          end  

          branch_trace = branch_trace - branches_to_remove

      end

      return branch_trace

    end

    def make_guidance_file ( guidance, dir, program_name )

      guidance_file = "#{dir}/directives"
      program_name = program_name.split('/')[-1]

      File.open(guidance_file, 'w') do |f|
        f.puts program_name
        f.puts "lines #{guidance.branch_lines.join(' ')}"
        f.puts "guide #{guidance.outcomes.join(' ')}"
      end

      return guidance_file

    end

    def make_directive ( guidance, dir, program_name, tag )
      evidence = guidance.canonical_string
      file = make_directive_file(guidance, dir, program_name, tag)

      return Directive.new(evidence, file)
    end

    def make_directive_file ( guidance, dir, program_name, tag )

      guidance_file = "#{dir}/directives_#{tag}"
      program_name = program_name.split('/')[-1]+".#{tag}.c"

      File.open(guidance_file, 'w') do |f|
        f.puts program_name
        f.puts "lines #{guidance.branch_lines.join(' ')}"
        f.puts "guide #{guidance.outcomes.join(' ')}"
      end

      return guidance_file

    end

    def make_directives ( witness_array, dir, c_file )
      directives = []
      witness_array.each_with_index do |witness, i|
        directives << make_directive(witness, dir, c_file, i)
      end
      return directives
    end

    def make_directive_files ( witness_array, dir, c_file )
      files = []
      witness_array.each_with_index do |witness, i|
        files << make_directive_file(witness, dir, c_file, i)
      end
      return files
    end

    def result_string ( result )
      case result
      when 0 then "unknown"
      when 1 then "safe"
      when 2 then "error found"
      when 3 then "cached"
      else assert { puts 'Illegal result'.red; false }
      end
    end

    def construct_civl_verify_cmd ( program, guidance_file, civl_jar )
      bound = 42
      civl_verify_cmd = "#{civl_jar} verify "\
                        "-svcomp16 "\
                        "-timeout=#{$options[:civl_timeout]} "\
                        "-errorBound=#{bound} "\
                        "-direct=#{guidance_file} "\
                        "#{program}"
    end

    def svcomp_error_found ( civl_output )
      return civl_output.include? '$assert(0, "__VERIFIER_error() is called.\n")'
    end

    def timed_out ( civl_output )
      return civl_output.include? 'Time out.'
    end

    def no_relevant_error_found ( civl_output )
      if civl_output.include? 'The standard properties hold for all executions.'
        return true
      elsif civl_output.include? 'The program MAY NOT be correct.'
        # If we got here and 'VERIFIER_error' was not in the results, then either only picky CIVL errors were caught, the error bound was exceeded, or both.
        return true
      else
        return false
      end
    end

    def is_svcomp_error( violation_summary )
      return violation_summary.include? "__VERIFIER_error() is called."
    end

    def extract_trace_id ( violation_summary )
      trace_id_regex = 'Violation (\d) encountered at depth .*'
      return violation_summary[/#{trace_id_regex}/,1]
    end

    def update_symvars_to_lines ( curr_map, new_map )
      new_map.each do |line, var|
        curr_map[line] = var
      end
      return curr_map
    end

    def update_input_counts ( curr_map, new_map )
      instr_line = "1"
      new_map.each do |line, count|
        next if line == instr_line
        if curr_map.keys.include? line
          curr_map[line] += count
        else
          curr_map[line] = count
        end
      end
      return curr_map
    end

    def update_input_types ( curr_map, new_map )
      instr_line = "1"
      new_map.each do |line, type|
        next if line == instr_line
        curr_map[line] = type
      end
      return curr_map
    end

    def update_lines_to_vars ( curr_map, new_map )
      new_map.each do |line, var|
        curr_map[line] = var
      end
      return curr_map
    end

    def grab_section ( name, output )
      start = "BEGIN #{name}\n"
      finish = "END #{name}\n"
      return output[/#{start}(.*?)#{finish}/m, 1]
    end

    def parse_read_calls ( output )

      map_string = grab_section("SymVar -> Line -> Var", output)
      symvars_to_line = []

      map_string.each_line do |l|

        next if l == "\n"

        triple = l.strip.split(' ',3)
        line = triple[1]; lhs = triple[2]
     
        instrumentation_line = "1"
        next if line == instrumentation_line

        symvars_to_line << triple[0..1] 
        
      end

      symvars_to_line = Hash[symvars_to_line]
      return symvars_to_line

    end

    def get_number_sliced_away ( output )
      return grab_section("Number of Conjuncts Sliced Away", output).to_i
    end

    def make_line_to_var_map ( output )

      map_string = grab_section("SymVar -> Line -> Var", output)
      lines_to_vars = []
      map_string.each_line do |l|
        next if l == "\n"

        triple = l.strip.split(' ',3)
        lhs = triple[2]
        unless lhs =~ /.*INPUTS_at_(\d+).*/
          lines_to_vars << triple[1..2] 
        end
      end
      lines_to_vars = Hash[lines_to_vars]

      return lines_to_vars

    end

    def parse_instrument_lines ( output )
      lines_string = grab_section("Guidance Lines", output)
      lines = []
      lines_string.each_line {|l| next if l == "\n"; lines << l.strip}
      puts "Guidance Lines: #{lines}" if $debug
      return lines
    end

    def parse_input_frequency ( output )

      inputs_string = grab_section("Line -> Number of Input Reads", output)
      inputs = {}
      inputs_string.each_line do |l|
        next if l == "\n"
        a = l.split(' ')
        inputs[a[0]] = a[1].to_i    
      end
      # TODO: reinsert globals line logic
      # We don't want to instrument the instrumentation
      #inputs.delete($globals_line.to_s)
      return inputs

    end

    def parse_input_types ( output )

      types_string = grab_section("Line -> Type of Input Read", output)
      types = {}
      types_string.each_line do |l|
        next if l == "\n"
        a = l.split(' ')
        types[a[0]] = a[1]
      end
      return types

    end

    def relevant_constraint ( constraint, symvars )

      is_relevant = false
      symvars.each do |v|
        if constraint.include? v
          is_relevant = true
          break
        end
      end

      return is_relevant

    end

    def parse_assumptions ( output, symvars_to_line )

      assumptions = grab_section("Control Dependent Slice", output)
      symvars = symvars_to_line.keys
      slice = []
      assumptions.each_line do |a|
        next if (a == "\n" || a.include?("true") || a.include?("!(false)"))
        next unless relevant_constraint(a, symvars)
        slice << a.strip
      end

      symvars_to_array_elem = map_symvars_to_array_elem(symvars_to_line)
      
      symvars_to_array_elem.each do |symvar,array_string|
        slice.each do |a|
          a = transform_power_subexprs(a)
          a.gsub!(/(#{symvar}\D|#{symvar}$)/, array_string)
        end
      end

      assumptions = []
      slice.each do |a|
        #assume = a.split(' ',3)[2].strip
        assumptions << a.strip
      end

      return assumptions
      puts "Assumptions: #{assumptions}".red
    end

    def map_symvars_to_array_elem (symvars_to_line_map)
      line_map = symvars_to_line_map.group_by{|k,v| v}
      line_to_symvars = {}
      line_map.each do |line, pairs|
        sym_vars = []
        pairs.each do |p|
          sym_vars << p.first
        end
        line_to_symvars[line] = sym_vars
      end
      map = {}
      
      line_to_symvars.each do |line, symvars|
        symvars.each_with_index do |sym_var, i|
          if sym_var.start_with? '['
            sym_var = sym_var[1..-2] # strip array braces
          end
          raw_read = "INPUTS_at_#{line}[#{i}]"
          map[sym_var] = raw_read

          var = $lines_to_vars[line]
          if var
            pretty_read = "#{var.upcase}_#{i}"
            $raw_to_pretty_reads[raw_read] = pretty_read
          end
        end
      end
      return map

    end

    def expand_power_expr ( var, exp )
      "("+Array.new(exp.to_i, var).join(" *")+")"
    end

    def transform_power_subexprs ( expr )
      regexp = '((Y\d+)\^(\d+))'

      expr_copy = expr.dup
      expr_copy.scan(/#{regexp}/).each do |m|
        power_expr, var, exp = m[0], m[1], m[2]
        transformation = expand_power_expr(var, exp)
        expr.gsub! power_expr, transformation
      end

      return expr
    end

    $instrumentation_line = 1

    def instrument ( program, assumptions, symvars_to_lines, input_counts, input_types )

      program = ensure_assume_declaration(program)

      globals = make_globals(input_counts, input_types)
      init_reads_function = make_init_reads_function(input_counts, input_types)
      program = inject_read_instrumentation(program, globals + init_reads_function)
      program = replace_read_expressions(program, input_counts, input_types)
      
      negated_assumptions = negate_assumptions(assumptions)
      program = inject_assumes(program, negated_assumptions)

    end

    def replace_read_expressions ( program, input_table, input_types )
      lines = input_table.keys
      lines.map! { |l| l.to_i }
      lines.each { |line| write_input_read(program,line,input_types) unless line == $instrumentation_line } 
      
      return program
    end

    def parse_smt_bounds ( slice_output, sliced_pc )
      expression_lines = grab_section("SMT TRANSLATIONS", slice_output)
      exprs = []

      expression_lines.each_line do |l|
        tuple = l.split(" maps to ")
        c_expr = tuple[0]
        smt_expr = tuple[1]
        exprs << Expression.new(c_expr, smt_expr)
      end

      declarations = grab_section("SMT DECLARATIONS", slice_output)
      bounds_image = [Hash[sliced_pc.zip exprs], declarations]
      return bounds_image
    end

    def write_input_read(fname, at_line, input_types)
      line = at_line
      type = input_types[line.to_s]
      open(fname, 'r+') do |f|
        while (at_line-=1) > 0 # read up to the line you want to write after
          f.readline
        end
        pos = f.pos # save your position (after line to be rewritten) in the file
        line_to_rewrite = f.readline
        # rewrite line using gsub
        input_array_read = "INPUTS_at_#{line}[input_at_#{line}_counter++]"
        raw_read = "__VERIFIER_nondet_#{type}()"
        array_bounds_check = "(input_at_#{line}_counter < num_of_inputs_at_#{line})"
        ternary_expr = "#{array_bounds_check} ? #{input_array_read} : #{raw_read} ;"
        line_to_rewrite.gsub!(/=.*__VERIFIER_nondet_#{Regexp.quote(type)}(\s)*\((\s)*\)(\s)*;/, "= "+ternary_expr)
        rest = f.read # save the rest of the file
        f.seek pos # go back to the old position
        f.write (line_to_rewrite) # write new data
        f.write rest # write rest of file
      end
    end

    def make_globals ( input_counts, input_types )
      globals = ""
      input_counts.each do |line, count|
        unless line == $instrumentation_line 
          type_decl = get_type_declaration(input_types[line])
          globals += "int num_of_inputs_at_#{line} = #{count}; #{type_decl} INPUTS_at_#{line}[#{count}]; int input_at_#{line}_counter = 0;"
        end
      end 

      return globals
    end

    def make_init_reads_function ( input_counts, input_types )
      readlines = ""
      input_counts.each do |line, count|
        unless line == $instrumentation_line
          type = input_types[line]
          readlines += "  for (int instrumentation_index=0; instrumentation_index<#{count}; instrumentation_index++) INPUTS_at_#{line}[instrumentation_index] = __VERIFIER_nondet_#{type}();"
        end
      end
      init_reads_function = "int initialize_reads() {"+readlines+"}"

      return init_reads_function
    end

    def inject_init_reads_setup ( file, str )
      # This is a hack to not have to create a frame file.
      # We just look for the regular expression hardcoded as
      # the beginning of the globals string and end of the
      # init_reads function
      reads_regex = '(int num_of_inputs_.*_index\] = __VERIFIER_nondet_\w+\(\);}|int initialize_reads\(\) {})'
      file_string = File.read(file)
      if file_string =~ /#{reads_regex}/      
        file_string.gsub!(/#{reads_regex}/, str)
      else
        file_string = str + file_string
      end
      File.open(file, 'w') { |f| f.write(file_string) } 

      return file

    end

    def get_type_declaration ( type )

      case type
      when "int"
        return "int"
      when "char"
        return "char"
      when "bool"
        return "_Bool"
      when "float"
        return "float"
      when "double"
        return "double"
      when "long"
        return "long"
      when "short"
        return "short"
      when "size_t"
        return "size_t"
      when "uchar"
        return "unsigned char"
      when "uint"
        return "unsigned int"
      when "ulong"
        return "unsigned long"
      when "unsigned"
        return "unsigned"
      when "ushort"
        return "unsigned short"
      else
        message = "Unexpected type: #{type}. Aborting."
        assert { puts message; $log.write message; false }
      end

    end

    def ensure_assume_declaration ( file )
      assume_declaration = "extern void __VERIFIER_assume(int);"
      assume_regex = 'extern\s*void\s*__VERIFIER_assume\s*\(\s*int\s*\)\s*;'
      file_string = File.read(file)
      unless file_string =~ /#{assume_regex}/
        file_string = assume_declaration + file_string
        File.open(file, 'w') { |f| f.write(file_string) } 
      end

      return file
    end

    def inject_read_instrumentation ( file, str )
      file = inject_init_reads_setup(file, str)
      file = inject_call_to_init_reads( file )
    end

    def inject_call_to_init_reads ( file )
      main_regex = '((int\s)*main\s*\(\s*(void)*\s*\)\s*{)'
      file_string = File.read(file)

      unless file_string.include? 'initialize_reads();'
        file_string.gsub!(/#{main_regex}/, '\1'+'initialize_reads();')
        File.open(file, 'w') { |f| f.write(file_string) } 
      end

      return file
    end

    def negate_assumptions ( pc_list )

      if pc_list.any?
        assume_statement = ""
        pc_list.each do |pc|

          sanitized_pc = []
          pc.each do |expr|

            if contains_subtraction(expr)
              expr = rewrite_subtract_expression(expr)
            end
        
            if contains_geq(expr)
              expr = rewrite_geq_expression(expr) 
            end

            if contains_leq(expr)
              expr = rewrite_leq_expression(expr) 
            end

            sanitized_pc << expr
          end
       
          conjuncted_pc = "(#{sanitized_pc.to_a.join(" && ")})"
          puts "A blocking clause: #{conjuncted_pc}".red if $debug
          assume_statement += "__VERIFIER_assume(!(#{conjuncted_pc}));"
        end
      end

      return assume_statement

    end

    def inject_assumes ( file, assumptions )
      error_regex = '^(?!.*extern void(\s)*__VERIFIER_error)(?!.*ERROR:)(^[^\__VERIFIER]*).*__VERIFIER_error'
      error_regex_with_numbered_label = '^(?!.*extern void(\s)*__VERIFIER_error)(\s*error_(\d+):).*__VERIFIER_error'
      error_regex_with_error_label = '^(?!.*extern void(\s)*__VERIFIER_error)(\s*ERROR:).*__VERIFIER_error'
      assume_file_string = File.read(file)

      if assume_file_string =~ /\s*ERROR:/
        assume_file_string.gsub!(/#{error_regex_with_error_label}/, '\2'+assumptions+"__VERIFIER_error")
      end

      if assume_file_string =~ /(\s*)error_(\d+):/
        assume_file_string.gsub!(/#{error_regex_with_numbered_label}/, '\2'+assumptions+"__VERIFIER_error")
      else
        assume_file_string.gsub!(/#{error_regex}/, '\2'+assumptions+"__VERIFIER_error")
      end

      File.open(file, 'w') { |f| f.write(assume_file_string) }
      return file
    end

    # Input:  SARL-generated expression
    # Output: Equivalent expression without
    #         subtraction subexpressions
    def rewrite_subtract_expression ( orig_expr ) 
      expr = orig_expr.dup
      
      disjuncts = []
      expr.split(' || ').each do |orig_sub_expr|
        sub_expr = orig_sub_expr.dup
        if sub_expr.start_with? '('
          sub_expr = sub_expr[1..-2]
        end
        right_operands = right_subtract_operands(sub_expr)
        to_remove = subtract_subexpressions_to_remove(sub_expr)
        to_remove.each {|s| sub_expr.slice! s}
        sub_expr.strip!
      
        addition_subexpression = right_operands.join(' + ')

        if (sub_expr.start_with? '0')
          disjuncts << "(#{addition_subexpression} + #{sub_expr})"
        elsif (sub_expr.end_with? '0')
          disjuncts << "(#{sub_expr} + #{addition_subexpression})"
        else
          puts "** Found: #{sub_expr}".red
          puts 'Not an expected SARL expression. Aborting.'
          abort
        end
      end
      return '('+disjuncts.join(' || ')+')'
    end

    def rewrite_geq_expression ( orig_expr )
      return orig_expr.gsub(/>=/,"+ 1 >")
    end

    def rewrite_leq_expression ( orig_expr )
      return orig_expr.gsub(/<=/,"< 1 +")
    end

    def first_char (str)
      return str[0]
    end

    def last_char (str)
      return str[-1]
    end

    # Input:  SARL-generated expression
    # Output: Array of strings of the right
    #         operand of a subtract expression
    def right_subtract_operands ( expr )
      match_strings = expr.scan(/(- (\S*)|- (\S*)$)/)
      right_operands = []
      match_strings.map {|s| right_operands << s[1]}
      return right_operands
    end

    # Input:  SARL-generated expression
    # Output: Array of strings of minus OP symbol
    #         and its right operand
    def subtract_subexpressions_to_remove ( expr )
      match_strings = expr.scan(/(- (\S*)|- (\S*)$)/)
      subexpressions_to_delete = []
      match_strings.map {|s| subexpressions_to_delete << s[0]}
      return subexpressions_to_delete 
    end

    def contains_subtraction ( expr )
      return expr.include? ' - '
    end

    def contains_geq ( expr )
      return expr.include? ' >= '
    end

    def contains_leq ( expr )
      return expr.include? ' <= '
    end

    def binary_search ( portfolio, output_dir, program, clauses, upper )

      height_of_singleton_layer = clauses.size - 1
      low, high = 1, height_of_singleton_layer
      successful_computations = []
      puts "Starting clauses:"
      puts "  #{clauses}"
      puts
      while low <= high
        mid = (low + high) / 2
        successful_computations = run_layer_in_parallel(portfolio, output_dir, program, clauses, upper, mid)
        if successful_computations.any?
  #       puts "Found some successes: #{successful_computations}"
          computations = successful_computations
          if low == mid
            puts "Low equals mid at layer: #{low}"
            break
          else
            high = mid - 1
            bottom = greatest_lower_bound_layer(successful_computations)
            if bottom > low
              low = bottom
            end
          end
        else
          puts "No successes; moving low up"
          low = mid + 1
        end
      end
    
      return computations
      
    end

    def run_layer_in_parallel ( portfolio, output_dir, program, clauses, upper, layer )
    
      layer_dir = "#{output_dir}/gen/layer#{layer}"
      FileUtils.mkdir_p layer_dir unless File.exist? layer_dir
    
      cardinality = clauses.size - layer
  #   Stub...
  #    return [ [], [clauses.combination(cardinality).to_a.sample], clauses.combination(cardinality).to_a.sample(2) ].sample

      over_computations = Parallel.map_with_index(clauses.combination(cardinality).to_a) do |elem, id|
        dir_base = make_dir_base(layer_dir, id)
        input_file = make_input_file(program, elem, upper, dir_base)
        over_computation = run_in_parallel(portfolio, input_file, dir_base)
        over_computation.select {|c| c.result == "TRUE"}.each do |c|
          c.safety_clause = elem
        end
        over_computation.each {|c| c.generalized_clause = elem}
        over_computation
      end
      over_computations.flatten!

      over_computations.select {|c| c.result == "TRUE"}.each do |c|
        $clauses_that_prove_safety << c.safety_clause
      end
    
      # Gather computations that found FALSE result
      computations_that_found_false = over_computations.select {|c| c.result == "FALSE"}
      # Filter on computations that have new evidence
      puts "Current witness cache: #{$witness_cache}"
      computations_with_new_evidence = computations_that_found_false.select {|c| has_new_evidence(c.evidence, $witness_cache)}

      return computations_with_new_evidence
   
      # The following is DED
      # Find the "FALSE" computations that aren't already in the witness cache
      pieces_of_evidence = over_computations.select {|c| c.result == "FALSE"}.map {|c| c.evidence}.uniq
      return new_evidence(pieces_of_evidence, $witness_cache)
  #    return [ [], [clauses.combination(cardinality).to_a.sample], clauses.combination(cardinality).to_a.sample(2) ].sample
    
    end

    def greatest_lower_bound_layer ( computations )
      lattice_elements = computations.map {|c| c.generalized_clause}.first
      if lattice_elements.size > 1
        lattice_elements.flatten.uniq.size
      else
        return -1
      end
    end

    def make_dir_base ( layer_dir, id )
      dir = "#{layer_dir}/id#{id}"
      FileUtils.mkdir_p dir unless File.exist? dir
      return dir  
    end
    
    def make_input_file ( program, clause, upper, dir_base )
      program_base_name = program.split('/').last
      input_file = dir_base+'/'+program_base_name
      `cp #{program} #{input_file}`
      clauses_to_block = upper + [clause]
      puts "Clauses to block: #{clauses_to_block}".green
      input_file = instrument(input_file, clauses_to_block, $symvars_to_lines, $input_counts, $input_types)
      return input_file
    end

    def get_candidate_powerset ( clauses )
      p = powerset(clauses.clone)
      puts "Reached past powerset call; the powerset is: #{p}".blue
      # Sort list; begin by dropping single
      # clauses, then pairwise, etc.
      p.sort_by!(&:length)
      p.shift # Drop the empty set at the head
      return p
    end

    # Solution take from:
    # http://stackoverflow.com/questions/2779094/
    # what-algorithm-can-calculate-the-power-set-of-a-given-set
    def powerset(set)
      puts "Calling powerset function".red
      return [set] if set.empty?

      p = set.pop
      subset = powerset(set)  
      subset | subset.map { |x| x | [p] }
    end

    def find_maximally_intersecting_set(set_of_sets, set)
      intersections = set_of_sets.map {|s| s & set}
      if intersections.empty?
        return []
      else
        return intersections.group_by(&:size).max.last.flatten.uniq
      end
    end

    class Array
      def is_a_subset_of ( other_array )
        return (self - other_array).empty?
      end
    end

    def update_disjoint_covering ( disjoint_covering, generalized_clause, original_clause=nil )

      $log.write "\n  ++ Updating disjoint covering map ++\n"

      elements_covered_by_generalization = []
      disjoint_covering.each do |u, image_of_u|
        if generalized_clause.is_a_subset_of u
          elements_covered_by_generalization << u
        end
      end

      equiv_map_value = []
      image_of_generalized_clause = []
      if $options[:parallel]
        equiv_map_value = $bounds_map[original_clause]
        image_of_generalized_clause = [original_clause]
      else
        equiv_map_value = $bounds_map[$equiv_partition_stack.last]
        image_of_generalized_clause = [$equiv_partition_stack.last]
      end
      $bounds_map[generalized_clause] = equiv_map_value

      elements_covered_by_generalization.each do |e|
        image_of_generalized_clause.concat disjoint_covering[e]
      end

      elements_covered_by_generalization.each do |u|
        $log.write "  Removing #{u} from the upper approximation"
        disjoint_covering.delete u
      end
      
      image_of_generalized_clause.uniq!
      disjoint_covering[generalized_clause] = image_of_generalized_clause.clone
      $log.write "  The image of #{generalized_clause} is:\n"\
                "    #{image_of_generalized_clause}\n"

    end

    def make_bracketed_pairs ( disjoint_covering )
      str = ""
      disjoint_covering.each do |upper, lower|
        str += "  < #{upper} , #{lower} >\n"
      end
      return str
    end

    # Solution taken from:
    # http://stackoverflow.com/questions/19595840/rails-get-the-time-difference-in-hours-minutes-and-seconds
    def time_diff(start_time, end_time)
      seconds_diff = (start_time - end_time).to_i.abs

      hours = seconds_diff / 3600
      seconds_diff -= hours * 3600

      minutes = seconds_diff / 60
      seconds_diff -= minutes * 60

      seconds = seconds_diff

      "#{hours.to_s.rjust(2, '0')}:#{minutes.to_s.rjust(2, '0')}:#{seconds.to_s.rjust(2, '0')}"
  end

    def prettify ( str )
      pretty_string = str.dup
      $raw_to_pretty_reads.each do |raw_read, pretty_read|
        if pretty_string.include? raw_read
          pretty_string.gsub! raw_read, pretty_read
        end
      end
      pretty_string.gsub! "1*", "" # Remove multiplication by unity
      pretty_string.gsub! "(int)", "" # Remove int casts
      pretty_string.gsub! "(real)", "" # Remove real casts
      return pretty_string
    end

    def conjoin ( exprs )
      smt_exprs = exprs.map {|e| e.smt_expr.strip}
      return "(and #{smt_exprs.join(' ')})"
    end

    def syntactic_to_smt ( clauses, map )
      clause_to_smt_map = map[clauses].first
      smt_clauses = []
      clauses.each do |c|
        smt_clauses << clause_to_smt_map[c] if clause_to_smt_map[c]
      end
      return smt_clauses
    end

    def wrap_in_assert ( str )
      return "(assert #{str})"
    end

    def make_smt_string ( upper, lower, b_map )
      decls = b_map[upper][1]
      up_smt = conjoin(syntactic_to_smt(upper, b_map))
      up_smt = wrap_in_assert(up_smt)
      
      low_smts = []
      lower.each do |b|
        low_smt = conjoin(syntactic_to_smt(b, b_map))
        low_smt = wrap_in_assert(low_smt)
        low_smts << low_smt
        decls = decls + b_map[b][1]
      end

      decls = transform_declarations(decls)

      str = "(set-option :seed 121314)\n"
      str += "(set-option :partitioning true)\n\n"
      str += decls.lines.uniq.join
      # upper bound
      str += "\n\n"
      str += up_smt
      # lower bound
      str += "\n\n"
      str += low_smts.join("\n")
      str += "\n(count)\n"

      return str
    end

    def transform_declarations ( decls )
      decls.gsub!("declare-const", "declare-var")
      int_low = -10; int_high = 10
      decls.gsub!("Int", "(Int #{int_low} #{int_high})")
    end

      class AssertionError < RuntimeError
      end

      def assert &block
        raise AssertionError unless yield
      end

      def get_bare_name ( program )
        # Drop the extension after the *final* period
        name_without_ext = program.split('.')[0..-2].join('.')
        bare_name = name_without_ext.split('/').last
        return bare_name
      end

      def run_in_parallel ( portfolio, input_file, dir_base )

        computations = Parallel.map(portfolio.analyzers) do |analyzer|
          output_dir = dir_base+"/#{analyzer.tool+analyzer.config}"
          `mkdir #{output_dir}` unless File.exist? output_dir
          analyzer.run_analysis(input_file, output_dir, portfolio.timeout)
        end

        computations.each do |c|
          if c.result == "FALSE"
            message = "#{c.name} result: FALSE. Evidence: #{c.evidence.canonical_string}"
            $log.write message
            puts message.red
          else
            message = "#{c.name} result: #{c.result}"
            $log.write message
            puts message.blue
          end
        end

        return computations  

      end

      def run_civl_in_parallel ( directives, dir_base, input_file, disjoint_covering )
        
        civl_jar = "java -jar -ea #{CIVL_DIR}/civl.jar"
        computations = Parallel.map_with_index(directives) do |directive, i|
     
          program_copy = "#{input_file}.#{i}.c"
          `cp #{input_file} #{program_copy}`       

          civl_cmd = construct_civl_verify_cmd( program_copy, directive.file, civl_jar)
          civl_output = `#{civl_cmd}`

          if svcomp_error_found(civl_output)
            trace_id = 0
                summary_regex = 'Violation \d encountered at depth .*?(?=\R\R|\z)'
                violation_summaries = civl_output.scan(/#{summary_regex}/m).flatten

              violation_summaries.each do |violation|
                if is_svcomp_error(violation)
                  trace_id = extract_trace_id(violation)
                  svcomp_error_summary = violation
                  break
                end
              end

            slice_cmd = "#{civl_jar} replay -id=#{trace_id} -sliceAnalysis #{program_copy}"
            slice_output = `#{slice_cmd}`
            under_updates = UnderUpdates.new(slice_output, $symvars_to_lines, directive.evidence)

            UnderapproximateComputation.new("FALSE", under_updates.sliced_pc, under_updates)

          elsif timed_out(civl_output)
            UnderapproximateComputation.new("UNKNOWN")
          else
            puts "Didn't find an error".green
            UnderapproximateComputation.new("TRUE")
          end
        end

        computations.uniq!
        puts "Parallel computations:".blue
        puts computations

        $last_injected_clauses_queue.clear if computations.select {|c| c.result == "FALSE"}.any?
        computations.select {|c| c.result == "FALSE"}.each do |c|
          update_global_maps(c.under_update)
          
          $last_injected_clauses_queue.enq c.subspace
          
          $bounds_map[c.subspace] = c.under_update.bounds_image
          sliced_pc = c.subspace
            disjoint_covering[sliced_pc] = [sliced_pc]
              message = "\n  Adding new entry to disjoint covering:\n"\
                          "    #{sliced_pc}\n"
              $log.write message


          $evidence_to_subspace[c.under_update.evidence] = c.subspace
        end

        if computations.select {|c| c.result == "FALSE"}.any?
          $underapprox_result = 2 # error_found
        else
          $underapprox_result = 0 # unknown
        end

      end

      def update_global_maps ( updates )
        $input_counts = update_input_counts($input_counts, updates.input_counts)    
        $input_types = update_input_types($input_types, updates.input_types) 
        $symvars_to_lines = update_symvars_to_lines($symvars_to_lines, updates.symvars_to_lines)    
        $lines_to_vars = update_lines_to_vars($lines_to_vars, updates.lines_to_vars)
        $conjuncts_sliced += updates.number_sliced_away
      end

      def new_evidence ( pieces_of_evidence, cache )
        return (pieces_of_evidence - cache)
      end

      def has_new_evidence ( evidence, cache )
        return cache.include? evidence
      end

      def prepend_error_at_line ( file, target_line )

        curr_line = 1
        lines = IO.readlines(file).map do |line|
          if curr_line == target_line
            line = '__VERIFIER_error(); //' + line
          end
          curr_line += 1
          line
        end
         
        File.open(file, 'w') do |f|
          f.puts lines
        end

      end

      def rewrite_line_with_string ( file, target_line, string )

        curr_line = 1
        lines = IO.readlines(file).map do |line|
          if curr_line == target_line
            line = string
          end
          curr_line += 1
          line
        end
         
        File.open(file, 'w') do |f|
          f.puts lines
        end

      end

      # The canonical string for an abstract witness will
      # be space delimited tuples of:
      #   (branch, branch_outcome)
      def get_canonical_string (witness)

        string = ""

        witness.branch_lines.each_with_index do |branch, i|
          outcome = witness.outcomes[i]
          string += "(#{branch},#{outcome}) "
        end

        return string

      end

      def get_abstract_witness (witness_graphml, is_cpa)

        graphml_file = File.read(witness_graphml)
        witness = Nokogiri::XML.parse(graphml_file)

        branches = get_branches_in_error_path(witness, is_cpa)
        branch_lines = branches.map { |b| b.line_number }
        outcomes = branches.map { |b| b.branch_taken }

        return Guidance.new(branch_lines, outcomes)

      end

      def is_relevant_control_edge (edge)
        sourcecode = "data[key='sourcecode']"

        if edge.values.include? "sink"
          return false
        elsif edge.css(sourcecode).text.include? "instrumentation_index"
          return false
        elsif edge.css(sourcecode).text.include? "input_at_"
          return false
        elsif edge.css(sourcecode).text.include? "__VERIFIER_assume("
          return false
        elsif edge.css(sourcecode).text.include? "INPUTS_at_"
          return false
        elsif edge.css(sourcecode).text.include? "__CPAchecker_TMP"
          return false
        else
          return true
        end
      end

      def get_branches_in_error_path ( witness, is_cpa )

        branch_trace = []

        is_loop_head = false
        witness.css('edge').each do |edge|
          if edge.at("data[key='enterLoopHead']")
            is_loop_head = true
            next
          end
          if edge.at("data[key='control']")
            if is_relevant_control_edge(edge)
              branch_trace << Branch.new(edge, is_loop_head)
            end
          end
          is_loop_head = false
        end

        if is_cpa
            branches_to_remove = []
            
            branch_trace.each_cons(2) do |curr_b, next_b|
              if ((curr_b.line_number == next_b.line_number) &&
                  (!curr_b.is_loop_head))
                branches_to_remove << curr_b
              end
            end  

            branch_trace = branch_trace - branches_to_remove

        end

        return branch_trace

      end

      def make_guidance_file ( guidance, dir, program_name )

        guidance_file = "#{dir}/directives"
        program_name = program_name.split('/')[-1]

        File.open(guidance_file, 'w') do |f|
          f.puts program_name
          f.puts "lines #{guidance.branch_lines.join(' ')}"
          f.puts "guide #{guidance.outcomes.join(' ')}"
        end

        return guidance_file

      end

      def make_directive ( guidance, dir, program_name, tag )
        evidence = guidance.canonical_string
        file = make_directive_file(guidance, dir, program_name, tag)

        return Directive.new(evidence, file)
      end

      def make_directive_file ( guidance, dir, program_name, tag )

        guidance_file = "#{dir}/directives_#{tag}"
        program_name = program_name.split('/')[-1]+".#{tag}.c"

        File.open(guidance_file, 'w') do |f|
          f.puts program_name
          f.puts "lines #{guidance.branch_lines.join(' ')}"
          f.puts "guide #{guidance.outcomes.join(' ')}"
        end

        return guidance_file

      end

      def make_directives ( witness_array, dir, c_file )
        directives = []
        witness_array.each_with_index do |witness, i|
          directives << make_directive(witness, dir, c_file, i)
        end
        return directives
      end

      def make_directive_files ( witness_array, dir, c_file )
        files = []
        witness_array.each_with_index do |witness, i|
          files << make_directive_file(witness, dir, c_file, i)
        end
        return files
      end

      def result_string ( result )
        case result
        when 0 then "unknown"
        when 1 then "safe"
        when 2 then "error found"
        when 3 then "cached"
        else assert { puts 'Illegal result'.red; false }
        end
      end

      def construct_civl_verify_cmd ( program, guidance_file, civl_jar )
        bound = 42
        civl_verify_cmd = "#{civl_jar} verify "\
                          "-svcomp16 "\
                          "-timeout=#{$options[:civl_timeout]} "\
                          "-errorBound=#{bound} "\
                          "-direct=#{guidance_file} "\
                          "#{program}"
      end

      def svcomp_error_found ( civl_output )
        return civl_output.include? '$assert(0, "__VERIFIER_error() is called.\n")'
      end

      def timed_out ( civl_output )
        return civl_output.include? 'Time out.'
      end

      def no_relevant_error_found ( civl_output )
        if civl_output.include? 'The standard properties hold for all executions.'
          return true
        elsif civl_output.include? 'The program MAY NOT be correct.'
          # If we got here and 'VERIFIER_error' was not in the results, then either only picky CIVL errors were caught, the error bound was exceeded, or both.
          return true
        else
          return false
        end
      end

      def is_svcomp_error( violation_summary )
        return violation_summary.include? "__VERIFIER_error() is called."
      end

      def extract_trace_id ( violation_summary )
        trace_id_regex = 'Violation (\d) encountered at depth .*'
        return violation_summary[/#{trace_id_regex}/,1]
      end

      def update_symvars_to_lines ( curr_map, new_map )
        new_map.each do |line, var|
          curr_map[line] = var
        end
        return curr_map
      end

      def update_input_counts ( curr_map, new_map )
        instr_line = "1"
        new_map.each do |line, count|
          next if line == instr_line
          if curr_map.keys.include? line
            curr_map[line] += count
          else
            curr_map[line] = count
          end
        end
        return curr_map
      end

      def update_input_types ( curr_map, new_map )
        instr_line = "1"
        new_map.each do |line, type|
          next if line == instr_line
          curr_map[line] = type
        end
        return curr_map
      end

      def update_lines_to_vars ( curr_map, new_map )
        new_map.each do |line, var|
          curr_map[line] = var
        end
        return curr_map
      end

      def grab_section ( name, output )
        start = "BEGIN #{name}\n"
        finish = "END #{name}\n"
        return output[/#{start}(.*?)#{finish}/m, 1]
      end

      def parse_read_calls ( output )

        map_string = grab_section("SymVar -> Line -> Var", output)
        symvars_to_line = []

        map_string.each_line do |l|

          next if l == "\n"

          triple = l.strip.split(' ',3)
          line = triple[1]; lhs = triple[2]
       
          instrumentation_line = "1"
          next if line == instrumentation_line

          symvars_to_line << triple[0..1] 
          
        end

        symvars_to_line = Hash[symvars_to_line]
        return symvars_to_line

      end

      def get_number_sliced_away ( output )
        return grab_section("Number of Conjuncts Sliced Away", output).to_i
      end

      def make_line_to_var_map ( output )

        map_string = grab_section("SymVar -> Line -> Var", output)
        lines_to_vars = []
        map_string.each_line do |l|
          next if l == "\n"

          triple = l.strip.split(' ',3)
          lhs = triple[2]
          unless lhs =~ /.*INPUTS_at_(\d+).*/
            lines_to_vars << triple[1..2] 
          end
        end
        lines_to_vars = Hash[lines_to_vars]

        return lines_to_vars

      end

      def parse_instrument_lines ( output )
        lines_string = grab_section("Guidance Lines", output)
        lines = []
        lines_string.each_line {|l| next if l == "\n"; lines << l.strip}
        puts "Guidance Lines: #{lines}" if $debug
        return lines
      end

      def parse_input_frequency ( output )

        inputs_string = grab_section("Line -> Number of Input Reads", output)
        inputs = {}
        inputs_string.each_line do |l|
          next if l == "\n"
          a = l.split(' ')
          inputs[a[0]] = a[1].to_i    
        end
        # TODO: reinsert globals line logic
        # We don't want to instrument the instrumentation
        #inputs.delete($globals_line.to_s)
        return inputs

      end

      def parse_input_types ( output )

        types_string = grab_section("Line -> Type of Input Read", output)
        types = {}
        types_string.each_line do |l|
          next if l == "\n"
          a = l.split(' ')
          types[a[0]] = a[1]
        end
        return types

      end

      def relevant_constraint ( constraint, symvars )

        is_relevant = false
        symvars.each do |v|
          if constraint.include? v
            is_relevant = true
            break
          end
        end

        return is_relevant

      end

      def parse_assumptions ( output, symvars_to_line )

        assumptions = grab_section("Control Dependent Slice", output)
        symvars = symvars_to_line.keys
        slice = []
        assumptions.each_line do |a|
          next if (a == "\n" || a.include?("true") || a.include?("!(false)"))
          next unless relevant_constraint(a, symvars)
          slice << a.strip
        end

        symvars_to_array_elem = map_symvars_to_array_elem(symvars_to_line)
        
        symvars_to_array_elem.each do |symvar,array_string|
          slice.each do |a|
            a = transform_power_subexprs(a)
            a.gsub!(/(#{symvar}\D|#{symvar}$)/, array_string)
          end
        end

        assumptions = []
        slice.each do |a|
          #assume = a.split(' ',3)[2].strip
          assumptions << a.strip
        end

        return assumptions
        puts "Assumptions: #{assumptions}".red
      end

      def map_symvars_to_array_elem (symvars_to_line_map)
        line_map = symvars_to_line_map.group_by{|k,v| v}
        line_to_symvars = {}
        line_map.each do |line, pairs|
          sym_vars = []
          pairs.each do |p|
            sym_vars << p.first
          end
          line_to_symvars[line] = sym_vars
        end
        map = {}
        
        line_to_symvars.each do |line, symvars|
          symvars.each_with_index do |sym_var, i|
            if sym_var.start_with? '['
              sym_var = sym_var[1..-2] # strip array braces
            end
            raw_read = "INPUTS_at_#{line}[#{i}]"
            map[sym_var] = raw_read

            var = $lines_to_vars[line]
            if var
              pretty_read = "#{var.upcase}_#{i}"
              $raw_to_pretty_reads[raw_read] = pretty_read
            end
          end
        end
        return map

      end

      def expand_power_expr ( var, exp )
        "("+Array.new(exp.to_i, var).join(" *")+")"
      end

      def transform_power_subexprs ( expr )
        regexp = '((Y\d+)\^(\d+))'

        expr_copy = expr.dup
        expr_copy.scan(/#{regexp}/).each do |m|
          power_expr, var, exp = m[0], m[1], m[2]
          transformation = expand_power_expr(var, exp)
          expr.gsub! power_expr, transformation
        end

        return expr
      end

      $instrumentation_line = 1

      def instrument ( program, assumptions, symvars_to_lines, input_counts, input_types )

        program = ensure_assume_declaration(program)

        globals = make_globals(input_counts, input_types)
        init_reads_function = make_init_reads_function(input_counts, input_types)
        program = inject_read_instrumentation(program, globals + init_reads_function)
        program = replace_read_expressions(program, input_counts, input_types)
        
        negated_assumptions = negate_assumptions(assumptions)
        program = inject_assumes(program, negated_assumptions)

      end

      def replace_read_expressions ( program, input_table, input_types )
        lines = input_table.keys
        lines.map! { |l| l.to_i }
        lines.each { |line| write_input_read(program,line,input_types) unless line == $instrumentation_line } 
        
        return program
      end

      def parse_smt_bounds ( slice_output, sliced_pc )
        expression_lines = grab_section("SMT TRANSLATIONS", slice_output)
        exprs = []

        expression_lines.each_line do |l|
          tuple = l.split(" maps to ")
          c_expr = tuple[0]
          smt_expr = tuple[1]
          exprs << Expression.new(c_expr, smt_expr)
        end

        declarations = grab_section("SMT DECLARATIONS", slice_output)
        bounds_image = [Hash[sliced_pc.zip exprs], declarations]
        return bounds_image
      end

      def write_input_read(fname, at_line, input_types)
        line = at_line
        type = input_types[line.to_s]
        open(fname, 'r+') do |f|
          while (at_line-=1) > 0 # read up to the line you want to write after
            f.readline
          end
          pos = f.pos # save your position (after line to be rewritten) in the file
          line_to_rewrite = f.readline
          # rewrite line using gsub
          input_array_read = "INPUTS_at_#{line}[input_at_#{line}_counter++]"
          raw_read = "__VERIFIER_nondet_#{type}()"
          array_bounds_check = "(input_at_#{line}_counter < num_of_inputs_at_#{line})"
          ternary_expr = "#{array_bounds_check} ? #{input_array_read} : #{raw_read} ;"
          line_to_rewrite.gsub!(/=.*__VERIFIER_nondet_#{Regexp.quote(type)}(\s)*\((\s)*\)(\s)*;/, "= "+ternary_expr)
          rest = f.read # save the rest of the file
          f.seek pos # go back to the old position
          f.write (line_to_rewrite) # write new data
          f.write rest # write rest of file
        end
      end

      def make_globals ( input_counts, input_types )
        globals = ""
        input_counts.each do |line, count|
          unless line == $instrumentation_line 
            type_decl = get_type_declaration(input_types[line])
            globals += "int num_of_inputs_at_#{line} = #{count}; #{type_decl} INPUTS_at_#{line}[#{count}]; int input_at_#{line}_counter = 0;"
          end
        end 

        return globals
      end

      def make_init_reads_function ( input_counts, input_types )
        readlines = ""
        input_counts.each do |line, count|
          unless line == $instrumentation_line
            type = input_types[line]
            readlines += "  for (int instrumentation_index=0; instrumentation_index<#{count}; instrumentation_index++) INPUTS_at_#{line}[instrumentation_index] = __VERIFIER_nondet_#{type}();"
          end
        end
        init_reads_function = "int initialize_reads() {"+readlines+"}"

        return init_reads_function
      end

      def inject_init_reads_setup ( file, str )
        # This is a hack to not have to create a frame file.
        # We just look for the regular expression hardcoded as
        # the beginning of the globals string and end of the
        # init_reads function
        reads_regex = '(int num_of_inputs_.*_index\] = __VERIFIER_nondet_\w+\(\);}|int initialize_reads\(\) {})'
        file_string = File.read(file)
        if file_string =~ /#{reads_regex}/      
          file_string.gsub!(/#{reads_regex}/, str)
        else
          file_string = str + file_string
        end
        File.open(file, 'w') { |f| f.write(file_string) } 

        return file

      end

      def get_type_declaration ( type )

        case type
        when "int"
          return "int"
        when "char"
          return "char"
        when "bool"
          return "_Bool"
        when "float"
          return "float"
        when "double"
          return "double"
        when "long"
          return "long"
        when "short"
          return "short"
        when "size_t"
          return "size_t"
        when "uchar"
          return "unsigned char"
        when "uint"
          return "unsigned int"
        when "ulong"
          return "unsigned long"
        when "unsigned"
          return "unsigned"
        when "ushort"
          return "unsigned short"
        else
          message = "Unexpected type: #{type}. Aborting."
          assert { puts message; $log.write message; false }
        end

      end

      def ensure_assume_declaration ( file )
        assume_declaration = "extern void __VERIFIER_assume(int);"
        assume_regex = 'extern\s*void\s*__VERIFIER_assume\s*\(\s*int\s*\)\s*;'
        file_string = File.read(file)
        unless file_string =~ /#{assume_regex}/
          file_string = assume_declaration + file_string
          File.open(file, 'w') { |f| f.write(file_string) } 
        end

        return file
      end

      def inject_read_instrumentation ( file, str )
        file = inject_init_reads_setup(file, str)
        file = inject_call_to_init_reads( file )
      end

      def inject_call_to_init_reads ( file )
        main_regex = '((int\s)*main\s*\(\s*(void)*\s*\)\s*{)'
        file_string = File.read(file)

        unless file_string.include? 'initialize_reads();'
          file_string.gsub!(/#{main_regex}/, '\1'+'initialize_reads();')
          File.open(file, 'w') { |f| f.write(file_string) } 
        end

        return file
      end

      def negate_assumptions ( pc_list )

        if pc_list.any?
          assume_statement = ""
          pc_list.each do |pc|

            sanitized_pc = []
            pc.each do |expr|

              if contains_subtraction(expr)
                expr = rewrite_subtract_expression(expr)
              end
          
              if contains_geq(expr)
                expr = rewrite_geq_expression(expr) 
              end

              if contains_leq(expr)
                expr = rewrite_leq_expression(expr) 
              end

              sanitized_pc << expr
            end
         
            conjuncted_pc = "(#{sanitized_pc.to_a.join(" && ")})"
            puts "A blocking clause: #{conjuncted_pc}".red if $debug
            assume_statement += "__VERIFIER_assume(!(#{conjuncted_pc}));"
          end
        end

        return assume_statement

      end

      def inject_assumes ( file, assumptions )
        error_regex = '^(?!.*extern void(\s)*__VERIFIER_error)(?!.*ERROR:)(^[^\__VERIFIER]*).*__VERIFIER_error'
        error_regex_with_numbered_label = '^(?!.*extern void(\s)*__VERIFIER_error)(\s*error_(\d+):).*__VERIFIER_error'
        error_regex_with_error_label = '^(?!.*extern void(\s)*__VERIFIER_error)(\s*ERROR:).*__VERIFIER_error'
        assume_file_string = File.read(file)

        if assume_file_string =~ /\s*ERROR:/
          assume_file_string.gsub!(/#{error_regex_with_error_label}/, '\2'+assumptions+"__VERIFIER_error")
        end

        if assume_file_string =~ /(\s*)error_(\d+):/
          assume_file_string.gsub!(/#{error_regex_with_numbered_label}/, '\2'+assumptions+"__VERIFIER_error")
        else
          assume_file_string.gsub!(/#{error_regex}/, '\2'+assumptions+"__VERIFIER_error")
        end

        File.open(file, 'w') { |f| f.write(assume_file_string) }
        return file
      end

      # Input:  SARL-generated expression
      # Output: Equivalent expression without
      #         subtraction subexpressions
      def rewrite_subtract_expression ( orig_expr ) 
        expr = orig_expr.dup
        
        disjuncts = []
        expr.split(' || ').each do |orig_sub_expr|
          sub_expr = orig_sub_expr.dup
          if sub_expr.start_with? '('
            sub_expr = sub_expr[1..-2]
          end
          right_operands = right_subtract_operands(sub_expr)
          to_remove = subtract_subexpressions_to_remove(sub_expr)
          to_remove.each {|s| sub_expr.slice! s}
          sub_expr.strip!
        
          addition_subexpression = right_operands.join(' + ')

          if (sub_expr.start_with? '0')
            disjuncts << "(#{addition_subexpression} + #{sub_expr})"
          elsif (sub_expr.end_with? '0')
            disjuncts << "(#{sub_expr} + #{addition_subexpression})"
          else
            puts "** Found: #{sub_expr}".red
            puts 'Not an expected SARL expression. Aborting.'
            abort
          end
        end
        return '('+disjuncts.join(' || ')+')'
      end

      def rewrite_geq_expression ( orig_expr )
        return orig_expr.gsub(/>=/,"+ 1 >")
      end

      def rewrite_leq_expression ( orig_expr )
        return orig_expr.gsub(/<=/,"< 1 +")
      end

      def first_char (str)
        return str[0]
      end

      def last_char (str)
        return str[-1]
      end

      # Input:  SARL-generated expression
      # Output: Array of strings of the right
      #         operand of a subtract expression
      def right_subtract_operands ( expr )
        match_strings = expr.scan(/(- (\S*)|- (\S*)$)/)
        right_operands = []
        match_strings.map {|s| right_operands << s[1]}
        return right_operands
      end

      # Input:  SARL-generated expression
      # Output: Array of strings of minus OP symbol
      #         and its right operand
      def subtract_subexpressions_to_remove ( expr )
        match_strings = expr.scan(/(- (\S*)|- (\S*)$)/)
        subexpressions_to_delete = []
        match_strings.map {|s| subexpressions_to_delete << s[0]}
        return subexpressions_to_delete 
      end

      def contains_subtraction ( expr )
        return expr.include? ' - '
      end

      def contains_geq ( expr )
        return expr.include? ' >= '
      end

      def contains_leq ( expr )
        return expr.include? ' <= '
      end

      def binary_search ( portfolio, output_dir, program, clauses, upper )

        height_of_singleton_layer = clauses.size - 1
        low, high = 1, height_of_singleton_layer
        successful_computations = []
        puts "Starting clauses:"
        puts "  #{clauses}"
        puts
        while low <= high
          mid = (low + high) / 2
          successful_computations = run_layer_in_parallel(portfolio, output_dir, program, clauses, upper, mid)
          if successful_computations.any?
    #       puts "Found some successes: #{successful_computations}"
            computations = successful_computations
            if low == mid
              puts "Low equals mid at layer: #{low}"
              break
            else
              high = mid - 1
              bottom = greatest_lower_bound_layer(successful_computations)
              if bottom > low
                low = bottom
              end
            end
          else
            puts "No successes; moving low up"
            low = mid + 1
          end
        end
      
        return computations
        
      end

      def run_layer_in_parallel ( portfolio, output_dir, program, clauses, upper, layer )
      
        layer_dir = "#{output_dir}/gen/layer#{layer}"
        FileUtils.mkdir_p layer_dir unless File.exist? layer_dir
      
        cardinality = clauses.size - layer
    #   Stub...
    #    return [ [], [clauses.combination(cardinality).to_a.sample], clauses.combination(cardinality).to_a.sample(2) ].sample

        over_computations = Parallel.map_with_index(clauses.combination(cardinality).to_a) do |elem, id|
          dir_base = make_dir_base(layer_dir, id)
          input_file = make_input_file(program, elem, upper, dir_base)
          over_computation = run_in_parallel(portfolio, input_file, dir_base)
          over_computation.select {|c| c.result == "TRUE"}.each do |c|
            c.safety_clause = elem
          end
          over_computation.each {|c| c.generalized_clause = elem}
          over_computation
        end
        over_computations.flatten!

        over_computations.select {|c| c.result == "TRUE"}.each do |c|
          $clauses_that_prove_safety << c.safety_clause
        end
      
        # Gather computations that found FALSE result
        computations_that_found_false = over_computations.select {|c| c.result == "FALSE"}
        # Filter on computations that have new evidence
        puts "Current witness cache: #{$witness_cache}"
        computations_with_new_evidence = computations_that_found_false.select {|c| has_new_evidence(c.evidence, $witness_cache)}

        return computations_with_new_evidence
     
        # The following is DED
        # Find the "FALSE" computations that aren't already in the witness cache
        pieces_of_evidence = over_computations.select {|c| c.result == "FALSE"}.map {|c| c.evidence}.uniq
        return new_evidence(pieces_of_evidence, $witness_cache)
    #    return [ [], [clauses.combination(cardinality).to_a.sample], clauses.combination(cardinality).to_a.sample(2) ].sample
      
      end

      def greatest_lower_bound_layer ( computations )
        lattice_elements = computations.map {|c| c.generalized_clause}.first
        if lattice_elements.size > 1
          lattice_elements.flatten.uniq.size
        else
          return -1
        end
      end

      def make_dir_base ( layer_dir, id )
        dir = "#{layer_dir}/id#{id}"
        FileUtils.mkdir_p dir unless File.exist? dir
        return dir  
      end
      
      def make_input_file ( program, clause, upper, dir_base )
        program_base_name = program.split('/').last
        input_file = dir_base+'/'+program_base_name
        `cp #{program} #{input_file}`
        clauses_to_block = upper + [clause]
        puts "Clauses to block: #{clauses_to_block}".green
        input_file = instrument(input_file, clauses_to_block, $symvars_to_lines, $input_counts, $input_types)
        return input_file
      end

      def get_candidate_powerset ( clauses )
        p = powerset(clauses.clone)
        puts "Reached past powerset call; the powerset is: #{p}".blue
        # Sort list; begin by dropping single
        # clauses, then pairwise, etc.
        p.sort_by!(&:length)
        p.shift # Drop the empty set at the head
        return p
      end

      # Solution take from:
      # http://stackoverflow.com/questions/2779094/
      # what-algorithm-can-calculate-the-power-set-of-a-given-set
      def powerset(set)
        puts "Calling powerset function".red
        return [set] if set.empty?

        p = set.pop
        subset = powerset(set)  
        subset | subset.map { |x| x | [p] }
      end

      def find_maximally_intersecting_set(set_of_sets, set)
        intersections = set_of_sets.map {|s| s & set}
        if intersections.empty?
          return []
        else
          return intersections.group_by(&:size).max.last.flatten.uniq
        end
      end

      class Array
        def is_a_subset_of ( other_array )
          return (self - other_array).empty?
        end
      end

      def update_disjoint_covering ( disjoint_covering, generalized_clause, original_clause=nil )

        $log.write "\n  ++ Updating disjoint covering map ++\n"

        elements_covered_by_generalization = []
        disjoint_covering.each do |u, image_of_u|
          if generalized_clause.is_a_subset_of u
            elements_covered_by_generalization << u
          end
        end

        equiv_map_value = []
        image_of_generalized_clause = []
        if $options[:parallel]
          equiv_map_value = $bounds_map[original_clause]
          image_of_generalized_clause = [original_clause]
        else
          equiv_map_value = $bounds_map[$equiv_partition_stack.last]
          image_of_generalized_clause = [$equiv_partition_stack.last]
        end
        $bounds_map[generalized_clause] = equiv_map_value

        elements_covered_by_generalization.each do |e|
          image_of_generalized_clause.concat disjoint_covering[e]
        end

        elements_covered_by_generalization.each do |u|
          $log.write "  Removing #{u} from the upper approximation"
          disjoint_covering.delete u
        end
        
        image_of_generalized_clause.uniq!
        disjoint_covering[generalized_clause] = image_of_generalized_clause.clone
        $log.write "  The image of #{generalized_clause} is:\n"\
                  "    #{image_of_generalized_clause}\n"

      end

      def make_bracketed_pairs ( disjoint_covering )
        str = ""
        disjoint_covering.each do |upper, lower|
          str += "  < #{upper} , #{lower} >\n"
        end
        return str
      end

      # Solution taken from:
      # http://stackoverflow.com/questions/19595840/rails-get-the-time-difference-in-hours-minutes-and-seconds
      def time_diff(start_time, end_time)
        seconds_diff = (start_time - end_time).to_i.abs

        hours = seconds_diff / 3600
        seconds_diff -= hours * 3600

        minutes = seconds_diff / 60
        seconds_diff -= minutes * 60

        seconds = seconds_diff

        "#{hours.to_s.rjust(2, '0')}:#{minutes.to_s.rjust(2, '0')}:#{seconds.to_s.rjust(2, '0')}"
    end

      def prettify ( str )
        pretty_string = str.dup
        $raw_to_pretty_reads.each do |raw_read, pretty_read|
          if pretty_string.include? raw_read
            pretty_string.gsub! raw_read, pretty_read
          end
        end
        pretty_string.gsub! "1*", "" # Remove multiplication by unity
        pretty_string.gsub! "(int)", "" # Remove int casts
        pretty_string.gsub! "(real)", "" # Remove real casts
        return pretty_string
      end

      def conjoin ( exprs )
        smt_exprs = exprs.map {|e| e.smt_expr.strip}
        return "(and #{smt_exprs.join(' ')})"
      end

      def syntactic_to_smt ( clauses, map )
        clause_to_smt_map = map[clauses].first
        smt_clauses = []
        clauses.each do |c|
          smt_clauses << clause_to_smt_map[c] if clause_to_smt_map[c]
        end
        return smt_clauses
      end

      def wrap_in_assert ( str )
        return "(assert #{str})"
      end

      def make_smt_string ( upper, lower, b_map )
        decls = b_map[upper][1]
        up_smt = conjoin(syntactic_to_smt(upper, b_map))
        up_smt = wrap_in_assert(up_smt)
        
        low_smts = []
        lower.each do |b|
          low_smt = conjoin(syntactic_to_smt(b, b_map))
          low_smt = wrap_in_assert(low_smt)
          low_smts << low_smt
          decls = decls + b_map[b][1]
        end

        decls = transform_declarations(decls)

        str = "(set-option :seed 121314)\n"
        str += "(set-option :partitioning true)\n\n"
        str += decls.lines.uniq.join
        # upper bound
        str += "\n\n"
        str += up_smt
        # lower bound
        str += "\n\n"
        str += low_smts.join("\n")
        str += "\n(count)\n"

        return str
      end

      def transform_declarations ( decls )
        decls.gsub!("declare-const", "declare-var")
        int_low = -10; int_high = 10
        decls.gsub!("Int", "(Int #{int_low} #{int_high})")
      end


        # This hash will hold all of the options
        # parsed from the command-line by
        # OptionParser.
        $options = {}

        optparse = OptionParser.new do|opts|
          # Set a banner, displayed at the top
          # of the help screen.
          opts.banner = "\n  Usage: cfc.rb [options] foo.c\n\n"

          # Define the options, and what they do
          $options[:verbose] = false
          opts.on( '-v', '--verbose', 'Output more information' ) do
            $options[:verbose] = true
          end

          $options[:full] = false 
          opts.on( '-f', '--full', 'Allow all CPA configs to run to completion' ) do
            $options[:full] = true
          end

          # This displays the help screen, all programs are
          # assumed to have this option.
          opts.on( '-h', '--help', 'Display this screen' ) do
            puts opts
            exit
          end

          $options[:only_cpa] = false
          opts.on( '--only_cpa', 'Only run CPA as the overapproximator' ) do
            $options[:only_cpa] = true
          end

          $options[:single_cpa] = false
          opts.on( '--single_cpa', 'Only run CPA config svcomp17 as the overapproximator' ) do
            $options[:only_cpa] = true
            $options[:single_cpa] = true
          end

          $options[:only_ua] = false
          opts.on( '--only_ua', 'Only run UA as the overapproximator' ) do
            $options[:only_ua] = true
          end

          $options[:civl_timeout] = 120 # Two minute default
          opts.on( '--civl_timeout BOUND', 'Set CIVL time bound (in seconds)' ) do |b|
            $options[:civl_timeout] = b
          end

          $options[:cpa_timeout] = 60 # One minute default
          opts.on( '--cpa_timeout BOUND', 'Set CPA time bound (in seconds)' ) do |b|
            $options[:cpa_timeout] = b
          end

          $options[:ua_timeout] = 60 # One minute default
          opts.on( '--ua_timeout BOUND', 'Set UA time bound (in seconds)' ) do |b|
            $options[:ua_timeout] = b
          end

          $options[:first_tool] = "" # Empty default
          opts.on( '--first_tool TOOL', 'Force one tool to be run first' ) do |b|
            $options[:first_tool] = b
          end

          $options[:prettify] = false
          opts.on( '--prettify', 'Prettify the LOG output? (Output displayed is not guaranteed to preseve semantic meaning)' ) do
            $options[:prettify] = true
          end

          $options[:smt] = true
          opts.on( '--smt-off', 'Suppress SMT-LIB output of CFC pairs to separate output files?' ) do
            $options[:smt] = false
          end

          $options[:parallel] = false
          opts.on( '--parallel', 'Run analyses in parallel?' ) do
            $options[:parallel] = true
          end

        end

        # Parse the command-line. There are two forms
        # of the parse method. The 'parse' method simply parses
        # ARGV, while the 'parse!' method parses ARGV and removes
        # any options found there, as well as any parameters for
        # the options. 
        optparse.parse!

          $adaptive_timebound_map = {}
          $adaptive_timebound_map["sv-comp17"] = $options[:cpa_timeout]
          $adaptive_timebound_map["sv-comp16-bam"] = $options[:cpa_timeout]
          $adaptive_timebound_map["sv-comp16--k-induction"] = $options[:cpa_timeout]
          $adaptive_timebound_map["UltimateAutomizer"] = $options[:ua_timeout]
          $timebound_padding = 300 # seconds

          cpa_stopwatch = Stopwatch.new
          ua_stopwatch = Stopwatch.new


      program = ARGV[0] # C program we are analyzing

        cfc_dir = "#{Dir.pwd}/analysis_logs"
        `mkdir #{cfc_dir}` unless File.exist? cfc_dir

        program_name = get_bare_name(program)
        program_dir = "#{cfc_dir}/#{program_name}"
        `mkdir #{program_dir}` unless File.exist? program_dir
        `rm -rf #{program_dir}/*` # Clean out possible previous files

        dump_dir = "#{Dir.pwd}/dump"
        `mkdir #{dump_dir}` unless File.exist? dump_dir

      #  instrumented_program = "#{program_dir}/#{program_name}.instrument.c"
      #  `cp #{program} #{instrumented_program}`
         over_instr_program = "#{program_dir}/#{program_name}.over.instr.c"
         under_instr_program = "#{program_dir}/#{program_name}.under.instr.c"
        `cp #{program} #{over_instr_program}`
        `cp #{program} #{under_instr_program}` 

        cpa_output_dir = "#{program_dir}/cpa_output"
        `mkdir #{cpa_output_dir}` unless File.exist? cpa_output_dir


      log_file = "#{program_dir}/LOG"

      class Log
        attr_accessor :log_file

        def initialize ( log_file )
          @log_file = log_file
          `rm -rf #{@log_file}` if File.exist? log_file
        end

        def write ( str )
          str = prettify(str) if $options[:prettify]
          File.open(@log_file,'a') {|f| f.write(str+"\n")}
        end

        def write_banner ( iteration )
          banner = "\n================= ITERATION #{iteration} ===================\n"
          write(banner)
        end
      end

      $log = Log.new(log_file)

        disjoint_covering = {}
        configurations = []
        @analyzers = []
        
        if $options[:single_cpa]
          @cpa17 = Overapproximator.new("CPA", "sv-comp17")

          @analyzers = [@cpa17]
        else
          @cpa17 = Overapproximator.new("CPA", "sv-comp17")
          @cpa16bam = Overapproximator.new("CPA", "sv-comp16-bam")
          @cpa16kind = Overapproximator.new("CPA", "sv-comp16--k-induction")
          @ua = Overapproximator.new("UA")

          @analyzers = [@ua,@cpa17,@cpa16bam,@cpa16kind]
        end

        @portfolio = OverPortfolio.new(@analyzers, 300)

        if $options[:first_tool].empty?
          configurations = [
            ["UltimateAutomizer"],
            ["CPA", "sv-comp17"],
            ["CPA", "sv-comp16-bam"],
            ["CPA", "sv-comp16--k-induction"]
          ]
        else
          case $options[:first_tool]
          when "uautomizer"
            configurations = [
              ["UltimateAutomizer"],
              ["CPA", "sv-comp17"],
              ["CPA", "sv-comp16-bam"],
              ["CPA", "sv-comp16--k-induction"]
            ]
          when "cpa-seq"
            configurations = [
              ["CPA", "sv-comp17"],
              ["CPA", "sv-comp16-bam"],
              ["CPA", "sv-comp16--k-induction"],
              ["UltimateAutomizer"]
            ]
          when "cpa-bam-bnb"
            configurations = [
              ["CPA", "sv-comp16-bam"],
              ["CPA", "sv-comp17"],
              ["CPA", "sv-comp16--k-induction"],
              ["UltimateAutomizer"]
            ]
          when "cpa-kind"
            configurations = [
              ["CPA", "sv-comp16--k-induction"],
              ["CPA", "sv-comp17"],
              ["CPA", "sv-comp16-bam"],
              ["UltimateAutomizer"]
            ]
          end
        end
        $witness_cache = []

        overapprox_result = unknown
        $underapprox_result = unknown

        analysis_stopwatch = Stopwatch.new
        possible_failure_stopwatch = Stopwatch.new
        generalization_stopwatch = Stopwatch.new
        definite_failure_stopwatch = Stopwatch.new
        slice_stopwatch = Stopwatch.new

        iteration = 1
        $log.write_banner(iteration)
        analysis_stopwatch.start

      loop do
          if just_generalized
            just_generalized = false
          else
            possible_failure_stopwatch.start
            
              goto_beginning_of_analysis_loop = false

             if $options[:parallel]

              over_computations = run_in_parallel(@portfolio, over_instr_program, program_dir)

              if over_computations.select {|c| c.result == "TRUE"}.any?
                message = "Over approximator found a TRUE result. Exiting."
                $log.write message
                overapprox_result = safe
              else
                pieces_of_evidence = over_computations.select {|c| c.result == "FALSE"}.map {|c| c.evidence}.uniq  
                puts "Pieces of evidence: #{pieces_of_evidence}".yellow
                if new_evidence(pieces_of_evidence, $witness_cache).any?
                  directives = make_directives(pieces_of_evidence, program_dir, over_instr_program)
                  puts "Directives:".blue
                  directives.each {|d| puts "#{d.evidence} #{d.file} \n\n"}
                  overapprox_result = error_found
            #      string_evidence = pieces_of_evidence.map {|e| e.canonical_string}.uniq
            #      $witness_cache = $witness_cache + string_evidence
                  $witness_cache = $witness_cache + pieces_of_evidence.uniq
                else
                  overapprox_result = cached
                end
              end

             else


              configurations.each do |conf|
                if conf.first == "UltimateAutomizer"
                  if $options[:only_cpa]
                    next
                  else
                      ua_cmd = "#{ua_script} "\
                               "--full-output "\
                               "--spec #{ua_specification} "\
                               "--architecture 32bit "\
                               "--file #{over_instr_program} "\
                               "--witness-dir #{program_dir}"

                      ua_stopwatch.start
                      ua_timeout = $adaptive_timebound_map["UltimateAutomizer"]
                      ua_output = `timeout #{ua_timeout} #{ua_cmd}`
                      ua_stopwatch.stop

                      #puts ua_output.yellow
                      if $?.exitstatus == TIMEOUT_STATUS
                        ua_timeouts += 1
                      end
                      File.open("#{program_dir}/Ultimate.log.#{iteration}","w") {|f| f.write(ua_output)}
                      
                      unsupported_operation_message = "- UnsupportedSyntaxResult [Line: "
                      if ua_output.include? unsupported_operation_message
                          instr_program_with_problematic_line = "#{under_instr_program}.problematic.#{iteration}.c"
                          `cp #{under_instr_program} #{instr_program_with_problematic_line}`


                        generalization_stopwatch.start
                            problem_line = ua_output.scan(/UnsupportedSyntaxResult \[Line: (\d+)\]/).flatten.first.to_i
                            message = "  UA found a problem line at: #{problem_line}; prepending error line to instrumented file."
                            $log.write message
                            prepend_error_at_line(over_instr_program, problem_line)
                            prepend_error_at_line(under_instr_program, problem_line)
                            generalization_count += 1
                              ua_output = `timeout #{$options[:ua_timeout]} #{ua_cmd}`
                              if $?.exitstatus == TIMEOUT_STATUS
                                ua_timeouts += 1
                              end
                              File.open("#{program_dir}/Ultimate.log.#{iteration}","w") {|f| f.write(ua_output)}  
                                if ua_output.include? "SyntaxErrorResult [Line:"
                                  message = "UltimateAutomizer reported a syntax error. Aborting."
                                  $log.write message
                                  assert { puts message.red; false }
                                end

                                if ua_output.include? "Result:\nTRUE"
                                  overapprox_result = safe
                                elsif ua_output.include? "Result:\nFALSE"
                                    ua_string = "UltimateAutomizer"
                                    message = "\n"
                                    message += "#{ua_string}'s timebound was: #{$adaptive_timebound_map[ua_string]}"
                                    message += "  but it just found an error after running for #{ua_stopwatch.time_spent} seconds."
                                    message += "  -> setting UA's timebound to #{ua_stopwatch.time_spent.to_i + $timebound_padding.to_i}\n"

                                    $log.write message
                                    $adaptive_timebound_map["UltimateAutomizer"] = ua_stopwatch.time_spent.to_i + $timebound_padding

                                    witness = "#{program_dir}/witness.graphml"
                                    is_cpa = false
                                    abstract_witness_object = get_abstract_witness(witness, is_cpa)
                                    abstract_witness = get_canonical_string(abstract_witness_object)

                                  if $witness_cache.include? abstract_witness
                                    overapprox_result = cached 
                                  else
                                    overapprox_result = error_found 
                                    $witness_cache << abstract_witness
                                      guidance_file = make_guidance_file(abstract_witness_object, program_dir, under_instr_program)

                                  end
                                else
                                  overapprox_result = unknown
                                end

                              ua_stopwatch.reset
                                if conf.first == "UltimateAutomizer"
                                  message = "  UAutomizer result: #{result_string(overapprox_result)}"
                                else
                                  message = "  #{c} result: #{result_string(overapprox_result)}"
                                end
                                $log.write message



                        # Do I classify this chunk as a definite failure? It's a definite overapproximation...
                           if $options[:parallel]
                             run_civl_in_parallel(directives, program_dir, over_instr_program, disjoint_covering)      
                           else
                              bound = 42
                              civl_verify_cmd = "#{civl_jar} verify "\
                                                "-svcomp16 "\
                                                "-timeout=#{$options[:civl_timeout]} "\
                                                "-errorBound=#{bound} "\
                                                "-direct=#{guidance_file} "\
                                                "#{under_instr_program}"


                            definite_failure_stopwatch.start
                            civl_output = `#{civl_verify_cmd}`
                            definite_failure_stopwatch.stop

                            #puts "CIVL output: #{civl_output}".blue
                            if civl_output.include? "Time out."
                              civl_timeouts += 1
                            end
                              if svcomp_error_found(civl_output)
                                $underapprox_result = error_found
                                    summary_regex = 'Violation \d encountered at depth .*?(?=\R\R|\z)'
                                    violation_summaries = civl_output.scan(/#{summary_regex}/m).flatten

                                  violation_summaries.each do |violation|
                                    if is_svcomp_error(violation)
                                      trace_id = extract_trace_id(violation)
                                      svcomp_error_summary = violation
                                      break
                                    end
                                  end

                                  unless svcomp_error_summary.include? "certainty: PROVEABLE"
                                    assert { puts "CIVL cannot prove error with certainty: need to specialize."; false }
                                  end

                              elsif no_relevant_error_found(civl_output)
                                $underapprox_result = safe
                                spurious_error_count += 1
                              elsif timed_out(civl_output)
                                $underapprox_result = unknown # For timeouts
                              else
                                assert { puts "CIVL messed up somehow: investigate. Aborting.".red; false }
                              end

                              message = "  CIVL result: #{result_string($underapprox_result)}"
                              $log.write message

                            if civl_debug
                                civl_out_file = "#{program_dir}/civl.iter#{iteration}.out"
                                under_instr_iter_program = "#{under_instr_program}.#{iteration}.c"
                                unless File.exist? under_instr_iter_program
                                  `cp #{under_instr_program} #{under_instr_iter_program}` 
                                end
                                directive_iter_file = "#{guidance_file}.#{iteration}"
                                `cp #{guidance_file} #{directive_iter_file}`
                                  f = directive_iter_file
                                  # The regular expression just looks for any line containing a '.'
                                  # and replaces this with the named of the debugging file name
                                  debug_c_file = under_instr_iter_program.split('/')[-1] # Don't want absolute path
                                  File.write(f, File.read(f).gsub(/.*\..*/, debug_c_file))


                                # Grab everything up to the directive option
                                civl_verify_cmd_prefix = civl_verify_cmd[/(.*)\s-direct=/,1]
                                civl_debug_verify_cmd = "#{civl_verify_cmd_prefix} "\
                                                        "-direct=#{directive_iter_file} "\
                                                        "#{under_instr_iter_program}"

                                header = "Running:\n\n"\
                                         "  #{civl_debug_verify_cmd}\n\n\n"

                                File.open(civl_out_file, 'w') {|f| f.write(header+civl_output)}

                            end
                           end

                          if $underapprox_result == error_found
                                slice_cmd = "#{civl_jar} replay -id=#{trace_id} -sliceAnalysis #{under_instr_program}"
                                slice_output = `#{slice_cmd}`
                                if civl_debug
                                    message = "\n\n=============== REPLAY =================\n\n"
                                    File.open(civl_out_file, 'a') {|f| f.write(message+slice_output)}

                                end
                                  new_input_counts = parse_input_frequency(slice_output)
                                  $input_counts = update_input_counts($input_counts, new_input_counts)

                                  new_input_types = parse_input_types(slice_output)
                                  $input_types = update_input_types($input_types, new_input_types)

                                  new_symvars_to_lines = parse_read_calls(slice_output)
                                  $symvars_to_lines = update_symvars_to_lines($symvars_to_lines, new_symvars_to_lines)

                                  new_lines_to_vars = make_line_to_var_map(slice_output)
                                  $lines_to_vars = update_lines_to_vars($lines_to_vars, new_lines_to_vars)

                                  sliced_away_count = get_number_sliced_away(slice_output)
                                  $conjuncts_sliced += sliced_away_count

                                  sliced_pc = parse_assumptions(slice_output, $symvars_to_lines)

                                  if sliced_pc.empty?
                                    always_fails = true
                                    break
                                  end
                                  $equiv_partition_stack.push sliced_pc
                                #  lines = parse_instrument_lines(slice_output)

                                    expression_lines = grab_section("SMT TRANSLATIONS", slice_output)
                                    exprs = []
                                    expression_lines.each_line do |l|
                                      tuple = l.split(" maps to ")
                                      c_expr = tuple[0]
                                      smt_expr = tuple[1]
                                      exprs << Expression.new(c_expr, smt_expr)
                                    end

                                    declarations = grab_section("SMT DECLARATIONS", slice_output)
                                    cfc_pair = CfcPair.new(exprs, declarations)
                                    
                                    cfc_pairs << cfc_pair

                                  map = Hash[sliced_pc.zip exprs]
                                  $bounds_map[sliced_pc] = [map, declarations]


                                disjoint_covering[sliced_pc] = [sliced_pc]
                                  message = "\n  Adding new entry to disjoint covering:\n"\
                                              "    #{sliced_pc}\n"
                                  $log.write message



                          elsif $underapprox_result == safe
                              trying_to_block_spurious_errors = true
                              if trying_to_block_spurious_errors
                                    civl_rerun_verify_cmd = "#{civl_jar} verify "\
                                                            "-svcomp16 "\
                                                            "-direct=#{guidance_file} "\
                                                            "-logTraceOnBacktrack "\
                                                            "#{under_instr_program}"
                                    puts "Rerunning CIVL with cmd: #{civl_rerun_verify_cmd}".yellow
                                    civl_rerun_output = `#{civl_rerun_verify_cmd}`

                                    replay_spurious_cmd = "#{civl_jar} replay -logTraceOnBacktrack #{under_instr_program}"
                                    civl_spurious_replay_output = `#{replay_spurious_cmd}`
                                    puts civl_spurious_replay_output.red

                                    new_input_counts = parse_input_frequency(civl_spurious_replay_output)
                                    $input_counts = update_input_counts($input_counts, new_input_counts)

                                    new_input_types = parse_input_types(civl_spurious_replay_output)
                                    $input_types = update_input_types($input_types, new_input_types)

                                    new_symvars_to_lines = parse_read_calls(civl_spurious_replay_output)
                                    $symvars_to_lines = update_symvars_to_lines($symvars_to_lines, new_symvars_to_lines)

                                    new_lines_to_vars = make_line_to_var_map(civl_spurious_replay_output)
                                    $lines_to_vars = update_lines_to_vars($lines_to_vars, new_lines_to_vars)

                                  spurious_pc = parse_assumptions(civl_spurious_replay_output, $symvars_to_lines)

                                $log.write("\n\n  Spurious error PC:\n   #{spurious_pc}\n")
                                unless spurious_pc.empty?
                                  spurious_errors << spurious_pc
                                end
                                if civl_debug
                                    civl_rerun_verify_debug_cmd = "#{civl_jar} verify "\
                                                                  "-svcomp16 "\
                                                                  "-direct=#{directive_iter_file} "\
                                                                  "-logTraceOnBacktrack "\
                                                                  "#{under_instr_iter_program}"
                                    header = "\n\n  ** Rerunning to find spurious error:\n\n  #{civl_rerun_verify_debug_cmd}\n\n"
                                    File.open(civl_out_file, 'a') {|f| f.write(header+civl_rerun_output)}

                                end
                              else
                                  unless just_generalized
                                    overapprox_result = unknown
                                    $underapprox_result = unknown
                                  end
                                    message = "\n  ==== Disjoint Covering after iteration #{iteration} ====\n\n"\
                                                "#{make_bracketed_pairs(disjoint_covering)}"
                                    $log.write message

                                  iteration += 1
                                  $log.write_banner(iteration)
                                    # Don't shuffle while debugging trex
                                    # TODO: make this a shift, so this analysis is deterministic
                                    #configurations.shuffle!


                                next
                              end

                          end

                        generalization_stopwatch.stop

                        possible_failure_stopwatch.stop
                        definite_failure_stopwatch.start
                                problematic_line_guidance_file = "#{guidance_file}.problematic_line"
                                `cp #{guidance_file} #{problematic_line_guidance_file}`
                                rewrite_line_with_string(problematic_line_guidance_file, 1, instr_program_with_problematic_line.split('/').last)

                              bound = 42
                              civl_verify_cmd = "#{civl_jar} verify "\
                                                "-svcomp16 "\
                                                "-timeout=#{$options[:civl_timeout]} "\
                                                "-errorBound=#{bound} "\
                                                "-direct=#{problematic_line_guidance_file} "\
                                                "#{instr_program_with_problematic_line}"


                            civl_output = `#{civl_verify_cmd}`

                            if civl_output.include? "Time out."
                              civl_timeouts += 1
                            end
                              if svcomp_error_found(civl_output)
                                $underapprox_result = error_found
                                    summary_regex = 'Violation \d encountered at depth .*?(?=\R\R|\z)'
                                    violation_summaries = civl_output.scan(/#{summary_regex}/m).flatten

                                  violation_summaries.each do |violation|
                                    if is_svcomp_error(violation)
                                      trace_id = extract_trace_id(violation)
                                      svcomp_error_summary = violation
                                      break
                                    end
                                  end

                                  unless svcomp_error_summary.include? "certainty: PROVEABLE"
                                    assert { puts "CIVL cannot prove error with certainty: need to specialize."; false }
                                  end

                              elsif no_relevant_error_found(civl_output)
                                $underapprox_result = safe
                                spurious_error_count += 1
                              elsif timed_out(civl_output)
                                $underapprox_result = unknown # For timeouts
                              else
                                assert { puts "CIVL messed up somehow: investigate. Aborting.".red; false }
                              end

                              message = "  CIVL result: #{result_string($underapprox_result)}"
                              $log.write message


                          if $underapprox_result == error_found
                              problematic_statement_blocking_clause = $equiv_partition_stack.last.dup
                                slice_cmd = "#{civl_jar} replay -id=#{trace_id} -sliceAnalysis #{instr_program_with_problematic_line}"
                                slice_output = `#{slice_cmd}`
                                if civl_debug
                                    message = "\n\n=============== REPLAY =================\n\n"
                                    File.open(civl_out_file, 'a') {|f| f.write(message+slice_output)}

                                end
                                  $symvars_to_lines = parse_read_calls(slice_output)
                                  problematic_sliced_pc = parse_assumptions(slice_output, $symvars_to_lines)


                              disjoint_covering[problematic_statement_blocking_clause] = problematic_sliced_pc
                              message = "  Disjoint element: #{problematic_statement_blocking_clause} now has an image of:\n    #{problematic_sliced_pc}"
                              $log.write message

                          elsif $underapprox_result == safe
                              trying_to_block_spurious_errors = true
                              if trying_to_block_spurious_errors
                                    civl_rerun_verify_cmd = "#{civl_jar} verify "\
                                                            "-svcomp16 "\
                                                            "-direct=#{guidance_file} "\
                                                            "-logTraceOnBacktrack "\
                                                            "#{under_instr_program}"
                                    puts "Rerunning CIVL with cmd: #{civl_rerun_verify_cmd}".yellow
                                    civl_rerun_output = `#{civl_rerun_verify_cmd}`

                                    replay_spurious_cmd = "#{civl_jar} replay -logTraceOnBacktrack #{under_instr_program}"
                                    civl_spurious_replay_output = `#{replay_spurious_cmd}`
                                    puts civl_spurious_replay_output.red

                                    new_input_counts = parse_input_frequency(civl_spurious_replay_output)
                                    $input_counts = update_input_counts($input_counts, new_input_counts)

                                    new_input_types = parse_input_types(civl_spurious_replay_output)
                                    $input_types = update_input_types($input_types, new_input_types)

                                    new_symvars_to_lines = parse_read_calls(civl_spurious_replay_output)
                                    $symvars_to_lines = update_symvars_to_lines($symvars_to_lines, new_symvars_to_lines)

                                    new_lines_to_vars = make_line_to_var_map(civl_spurious_replay_output)
                                    $lines_to_vars = update_lines_to_vars($lines_to_vars, new_lines_to_vars)

                                  spurious_pc = parse_assumptions(civl_spurious_replay_output, $symvars_to_lines)

                                $log.write("\n\n  Spurious error PC:\n   #{spurious_pc}\n")
                                unless spurious_pc.empty?
                                  spurious_errors << spurious_pc
                                end
                                if civl_debug
                                    civl_rerun_verify_debug_cmd = "#{civl_jar} verify "\
                                                                  "-svcomp16 "\
                                                                  "-direct=#{directive_iter_file} "\
                                                                  "-logTraceOnBacktrack "\
                                                                  "#{under_instr_iter_program}"
                                    header = "\n\n  ** Rerunning to find spurious error:\n\n  #{civl_rerun_verify_debug_cmd}\n\n"
                                    File.open(civl_out_file, 'a') {|f| f.write(header+civl_rerun_output)}

                                end
                              else
                                  unless just_generalized
                                    overapprox_result = unknown
                                    $underapprox_result = unknown
                                  end
                                    message = "\n  ==== Disjoint Covering after iteration #{iteration} ====\n\n"\
                                                "#{make_bracketed_pairs(disjoint_covering)}"
                                    $log.write message

                                  iteration += 1
                                  $log.write_banner(iteration)
                                    # Don't shuffle while debugging trex
                                    # TODO: make this a shift, so this analysis is deterministic
                                    #configurations.shuffle!


                                next
                              end

                          end

                        definite_failure_stopwatch.stop
                        possible_failure_stopwatch.start

                          unless just_generalized
                            upper_characterization = disjoint_covering.keys
                            clauses_to_block = upper_characterization + spurious_errors
                            over_instr_program = instrument(over_instr_program, clauses_to_block, $symvars_to_lines, $input_counts, $input_types)
                            under_instr_program = instrument(under_instr_program, clauses_to_block, $symvars_to_lines, $input_counts, $input_types)
                          end

                          unless just_generalized
                            overapprox_result = unknown
                            $underapprox_result = unknown
                          end
                            message = "\n  ==== Disjoint Covering after iteration #{iteration} ====\n\n"\
                                        "#{make_bracketed_pairs(disjoint_covering)}"
                            $log.write message

                          iteration += 1
                          $log.write_banner(iteration)
                            # Don't shuffle while debugging trex
                            # TODO: make this a shift, so this analysis is deterministic
                            #configurations.shuffle!


                        goto_beginning_of_analysis_loop = true
                        next
                      end

                        if ua_output.include? "SyntaxErrorResult [Line:"
                          message = "UltimateAutomizer reported a syntax error. Aborting."
                          $log.write message
                          assert { puts message.red; false }
                        end

                        if ua_output.include? "Result:\nTRUE"
                          overapprox_result = safe
                        elsif ua_output.include? "Result:\nFALSE"
                            ua_string = "UltimateAutomizer"
                            message = "\n"
                            message += "#{ua_string}'s timebound was: #{$adaptive_timebound_map[ua_string]}"
                            message += "  but it just found an error after running for #{ua_stopwatch.time_spent} seconds."
                            message += "  -> setting UA's timebound to #{ua_stopwatch.time_spent.to_i + $timebound_padding.to_i}\n"

                            $log.write message
                            $adaptive_timebound_map["UltimateAutomizer"] = ua_stopwatch.time_spent.to_i + $timebound_padding

                            witness = "#{program_dir}/witness.graphml"
                            is_cpa = false
                            abstract_witness_object = get_abstract_witness(witness, is_cpa)
                            abstract_witness = get_canonical_string(abstract_witness_object)

                          if $witness_cache.include? abstract_witness
                            overapprox_result = cached 
                          else
                            overapprox_result = error_found 
                            $witness_cache << abstract_witness
                              guidance_file = make_guidance_file(abstract_witness_object, program_dir, under_instr_program)

                          end
                        else
                          overapprox_result = unknown
                        end

                      ua_stopwatch.reset

                  end
                elsif conf.first == "CPA"
                  if $options[:only_ua]
                    next
                  else
                      c = conf[1] # grab configuration flag for CPA
                      if ($options[:single_cpa] && (c != "sv-comp17"))
                        next
                      else
                          cpa_timeout = $adaptive_timebound_map[c]
                          cpa_cmd = "timeout #{cpa_timeout} "\
                                    "#{cpa_script} -#{c} "\
                                    "-timelimit 900 "\
                                    "-spec #{cpa_specification} "\
                                    "-setprop output.path=\"#{cpa_output_dir}\" "\
                                    "#{over_instr_program}"

                          cpa_stopwatch.start
                          cpa_output, cpa_stderr, status = Open3.capture3(cpa_cmd)
                          cpa_stopwatch.stop

                          if cpa_debug
                              config_out_file = "#{program_dir}/#{c}.iter#{iteration}.out"
                              over_instr_iter_program = "#{over_instr_program}.#{iteration}.c"
                              `cp #{over_instr_program} #{over_instr_iter_program}` 

                              cpa_cmd_prefix = cpa_cmd[/(.*)\s/,1] # Remove program name from cmd
                              header = "Running:\n\n"\
                                       "  #{cpa_cmd_prefix} #{over_instr_iter_program}\n\n\n"

                              File.open(config_out_file, 'w') {|f| f.write(header+cpa_output)}

                          end
                          if cpa_stderr.include? "Warning: Analysis interrupted (The CPU-time limit"
                            cpa_timeouts += 1
                          end

                          if cpa_output.include? "Verification result: TRUE."
                            overapprox_result = safe
                          elsif cpa_output.include? "Verification result: FALSE."
                              message =  "\n"
                              message += "CPA config #{c}'s timebound was: #{$adaptive_timebound_map[c]}\n"
                              message += "  but it just found an error after running for #{cpa_stopwatch.time_spent} seconds.\n"
                              message += "  -> setting #{c}'s timebound to #{cpa_stopwatch.time_spent.to_i + $timebound_padding.to_i}\n"

                              $log.write message
                              $adaptive_timebound_map[c] = cpa_stopwatch.time_spent.to_i + $timebound_padding

                              if c.include? "sv-comp17"
                                witness = "#{cpa_output_dir}/violation-witness.graphml"
                              else
                                witness = "#{cpa_output_dir}/witness.graphml"
                                witness_gz = witness+'.gz'
                                `rm #{witness}` if File.exist? witness # from a previous run
                                unless File.exist? witness_gz
                                  assert { puts 'Zipped witness file does not exist in #{cpa_output_dir}. Aborting.'; false }
                                end
                                `gunzip #{witness_gz}`
                              end
                              is_cpa = true
                              abstract_witness_object = get_abstract_witness(witness, is_cpa)
                              abstract_witness = get_canonical_string(abstract_witness_object)

                            if $witness_cache.include? abstract_witness
                              overapprox_result = cached 
                            else
                              overapprox_result = error_found 
                              $witness_cache << abstract_witness
                                guidance_file = make_guidance_file(abstract_witness_object, program_dir, under_instr_program)

                            end
                          else
                            overapprox_result = unknown
                          end

                        cpa_stopwatch.reset
                      end

                  end
                else
                  assert { puts "Unsupported overapproximation tool: #{conf.first}"; false }
                end
                  if conf.first == "UltimateAutomizer"
                    message = "  UAutomizer result: #{result_string(overapprox_result)}"
                  else
                    message = "  #{c} result: #{result_string(overapprox_result)}"
                  end
                  $log.write message

                  unless (overapprox_result == cached || overapprox_result == unknown)
                    break
                  end

                break if goto_beginning_of_analysis_loop
              end
              if goto_beginning_of_analysis_loop
                possible_failure_stopwatch.stop
                next
              end
                message = "\n  -------------\n"\
                            "  Witness cache: #{$witness_cache}\n"\
                            "  -------------\n"
                $log.write message


             end

            possible_failure_stopwatch.stop
          end

         if $options[:parallel]
           break if overapprox_result == safe
           if ( overapprox_result == unknown or overapprox_result == cached )
               if iteration == 1
                 message = "No overapproximators could find an error on the first iteration. Aborting."
                 $log.write message; puts message; abort
               end
               if $options[:parallel]
                 clause_to_generalize = $last_injected_clauses_queue.deq

                 upper = disjoint_covering.keys
                 upper = upper - [clause_to_generalize]

                 $clauses_that_prove_safety = []
                 # returns an array of OverapproximateComputation objects
             #    generalized_clause = binary_search(@portfolio, program_dir, over_instr_program, clause_to_generalize, upper)
                 over_computations = binary_search(@portfolio, program_dir, over_instr_program, clause_to_generalize, upper)
                 generalized_clause = over_computations.map {|c| c.generalized_clause}.compact.first
                 puts "Generalized clause:".red; generalized_clause.each {|c| puts c}
                 $last_injected_clauses_queue.enq generalized_clause
                 if generalized_clause
                   lowest_safety_clause = $clauses_that_prove_safety.max_by {|e| e.size}
                   generalized_clause = lowest_safety_clause
                   update_disjoint_covering(disjoint_covering, generalized_clause, clause_to_generalize)
                   puts "Safety clause lowest in the lattice: #{lowest_safety_clause}"
                   overapprox_result = safe
                   just_generalized = true
                   break
                 else
                   update_disjoint_covering(disjoint_covering, generalized_clause, clause_to_generalize)
                 end
                 assert { puts "Drop in generalization in parallel.".red; false }
                 assert { puts "Need to update disjoint partition map with generalized clause".red; false }
               else
                 generalization_stopwatch.start
                   generalization_count += 1
                   generalized_clause = top_of_lattice
                     disjoint_covering.delete($equiv_partition_stack.last)

                     upper_approximation = disjoint_covering.keys
                     base = find_maximally_intersecting_set(upper_approximation, $equiv_partition_stack.last)
                     candidates = []

                     if $equiv_partition_stack.last
                   #    candidate_clause = $equiv_partition_stack.last - base
                   #    candidates = get_candidate_powerset(candidate_clause)
                       clause_size = $equiv_partition_stack.last.size
                       if clause_size > 3
                           candidates.concat $equiv_partition_stack.last.combination($equiv_partition_stack.last.size-1).to_a

                       elsif clause_size > 2
                           candidates.concat $equiv_partition_stack.last.combination(1).to_a
                           candidates.concat $equiv_partition_stack.last.combination(2).to_a

                       elsif clause_size > 1
                           candidates.concat $equiv_partition_stack.last.combination(1).to_a

                       else
                         generalized_clause = top_of_lattice
                           update_disjoint_covering(disjoint_covering, generalized_clause)

                             analysis_moved_to_top = true
                             $log.write "\n\n  ########## BLOCKING TOP ##########\n"
                             puts "Blocking TOP".green
                             overapprox_result = safe
                             just_generalized = true

                       end
                     else
                       generalized_clause = top_of_lattice
                         update_disjoint_covering(disjoint_covering, generalized_clause)

                           analysis_moved_to_top = true
                           $log.write "\n\n  ########## BLOCKING TOP ##########\n"
                           puts "Blocking TOP".green
                           overapprox_result = safe
                           just_generalized = true

                     end


                   message = "The candidate clauses in this generalization attempt are: #{candidates}"

                   candidates.each do |clause_set|
                     iffy_generalization = $equiv_partition_stack.last - clause_set
                       if iffy_generalization.flatten.empty?
                         generalized_clause = top_of_lattice
                           update_disjoint_covering(disjoint_covering, generalized_clause)

                             analysis_moved_to_top = true
                             $log.write "\n\n  ########## BLOCKING TOP ##########\n"
                             puts "Blocking TOP".green
                             overapprox_result = safe
                             just_generalized = true

                       end

                     unless analysis_moved_to_top
                         upper_characterization = disjoint_covering.keys
                         blocking_clauses = upper_characterization + [iffy_generalization] + spurious_errors
                         puts "Blocking clauses in generalization: #{blocking_clauses}".red
                       # instrumented_program = instrument(instrumented_program, blocking_clauses, $symvars_to_lines, $input_counts, $input_types)
                         over_instr_program = instrument(over_instr_program, blocking_clauses, $symvars_to_lines, $input_counts, $input_types)
                         under_instr_program = instrument(under_instr_program, blocking_clauses, $symvars_to_lines, $input_counts, $input_types)

                         message = "\n  ** Attempting to generalize **\n\n"\
                                     "    Dropping:\n"\
                                     "      #{clause_set}\n"\
                                     "    Tentatively blocking:\n"\
                                     "      #{iffy_generalization}\n"
                         $log.write message

                       
                         goto_beginning_of_analysis_loop = false

                        if $options[:parallel]

                         over_computations = run_in_parallel(@portfolio, over_instr_program, program_dir)

                         if over_computations.select {|c| c.result == "TRUE"}.any?
                           message = "Over approximator found a TRUE result. Exiting."
                           $log.write message
                           overapprox_result = safe
                         else
                           pieces_of_evidence = over_computations.select {|c| c.result == "FALSE"}.map {|c| c.evidence}.uniq  
                           puts "Pieces of evidence: #{pieces_of_evidence}".yellow
                           if new_evidence(pieces_of_evidence, $witness_cache).any?
                             directives = make_directives(pieces_of_evidence, program_dir, over_instr_program)
                             puts "Directives:".blue
                             directives.each {|d| puts "#{d.evidence} #{d.file} \n\n"}
                             overapprox_result = error_found
                       #      string_evidence = pieces_of_evidence.map {|e| e.canonical_string}.uniq
                       #      $witness_cache = $witness_cache + string_evidence
                             $witness_cache = $witness_cache + pieces_of_evidence.uniq
                           else
                             overapprox_result = cached
                           end
                         end

                        else


                         configurations.each do |conf|
                           if conf.first == "UltimateAutomizer"
                             if $options[:only_cpa]
                               next
                             else
                                 ua_cmd = "#{ua_script} "\
                                          "--full-output "\
                                          "--spec #{ua_specification} "\
                                          "--architecture 32bit "\
                                          "--file #{over_instr_program} "\
                                          "--witness-dir #{program_dir}"

                                 ua_stopwatch.start
                                 ua_timeout = $adaptive_timebound_map["UltimateAutomizer"]
                                 ua_output = `timeout #{ua_timeout} #{ua_cmd}`
                                 ua_stopwatch.stop

                                 #puts ua_output.yellow
                                 if $?.exitstatus == TIMEOUT_STATUS
                                   ua_timeouts += 1
                                 end
                                 File.open("#{program_dir}/Ultimate.log.#{iteration}","w") {|f| f.write(ua_output)}
                                 
                                 unsupported_operation_message = "- UnsupportedSyntaxResult [Line: "
                                 if ua_output.include? unsupported_operation_message
                                     instr_program_with_problematic_line = "#{under_instr_program}.problematic.#{iteration}.c"
                                     `cp #{under_instr_program} #{instr_program_with_problematic_line}`


                                   generalization_stopwatch.start
                                       problem_line = ua_output.scan(/UnsupportedSyntaxResult \[Line: (\d+)\]/).flatten.first.to_i
                                       message = "  UA found a problem line at: #{problem_line}; prepending error line to instrumented file."
                                       $log.write message
                                       prepend_error_at_line(over_instr_program, problem_line)
                                       prepend_error_at_line(under_instr_program, problem_line)
                                       generalization_count += 1
                                         ua_output = `timeout #{$options[:ua_timeout]} #{ua_cmd}`
                                         if $?.exitstatus == TIMEOUT_STATUS
                                           ua_timeouts += 1
                                         end
                                         File.open("#{program_dir}/Ultimate.log.#{iteration}","w") {|f| f.write(ua_output)}  
                                           if ua_output.include? "SyntaxErrorResult [Line:"
                                             message = "UltimateAutomizer reported a syntax error. Aborting."
                                             $log.write message
                                             assert { puts message.red; false }
                                           end

                                           if ua_output.include? "Result:\nTRUE"
                                             overapprox_result = safe
                                           elsif ua_output.include? "Result:\nFALSE"
                                               ua_string = "UltimateAutomizer"
                                               message = "\n"
                                               message += "#{ua_string}'s timebound was: #{$adaptive_timebound_map[ua_string]}"
                                               message += "  but it just found an error after running for #{ua_stopwatch.time_spent} seconds."
                                               message += "  -> setting UA's timebound to #{ua_stopwatch.time_spent.to_i + $timebound_padding.to_i}\n"

                                               $log.write message
                                               $adaptive_timebound_map["UltimateAutomizer"] = ua_stopwatch.time_spent.to_i + $timebound_padding

                                               witness = "#{program_dir}/witness.graphml"
                                               is_cpa = false
                                               abstract_witness_object = get_abstract_witness(witness, is_cpa)
                                               abstract_witness = get_canonical_string(abstract_witness_object)

                                             if $witness_cache.include? abstract_witness
                                               overapprox_result = cached 
                                             else
                                               overapprox_result = error_found 
                                               $witness_cache << abstract_witness
                                                 guidance_file = make_guidance_file(abstract_witness_object, program_dir, under_instr_program)

                                             end
                                           else
                                             overapprox_result = unknown
                                           end

                                         ua_stopwatch.reset
                                           if conf.first == "UltimateAutomizer"
                                             message = "  UAutomizer result: #{result_string(overapprox_result)}"
                                           else
                                             message = "  #{c} result: #{result_string(overapprox_result)}"
                                           end
                                           $log.write message



                                   # Do I classify this chunk as a definite failure? It's a definite overapproximation...
                                      if $options[:parallel]
                                        run_civl_in_parallel(directives, program_dir, over_instr_program, disjoint_covering)      
                                      else
                                         bound = 42
                                         civl_verify_cmd = "#{civl_jar} verify "\
                                                           "-svcomp16 "\
                                                           "-timeout=#{$options[:civl_timeout]} "\
                                                           "-errorBound=#{bound} "\
                                                           "-direct=#{guidance_file} "\
                                                           "#{under_instr_program}"


                                       definite_failure_stopwatch.start
                                       civl_output = `#{civl_verify_cmd}`
                                       definite_failure_stopwatch.stop

                                       #puts "CIVL output: #{civl_output}".blue
                                       if civl_output.include? "Time out."
                                         civl_timeouts += 1
                                       end
                                         if svcomp_error_found(civl_output)
                                           $underapprox_result = error_found
                                               summary_regex = 'Violation \d encountered at depth .*?(?=\R\R|\z)'
                                               violation_summaries = civl_output.scan(/#{summary_regex}/m).flatten

                                             violation_summaries.each do |violation|
                                               if is_svcomp_error(violation)
                                                 trace_id = extract_trace_id(violation)
                                                 svcomp_error_summary = violation
                                                 break
                                               end
                                             end

                                             unless svcomp_error_summary.include? "certainty: PROVEABLE"
                                               assert { puts "CIVL cannot prove error with certainty: need to specialize."; false }
                                             end

                                         elsif no_relevant_error_found(civl_output)
                                           $underapprox_result = safe
                                           spurious_error_count += 1
                                         elsif timed_out(civl_output)
                                           $underapprox_result = unknown # For timeouts
                                         else
                                           assert { puts "CIVL messed up somehow: investigate. Aborting.".red; false }
                                         end

                                         message = "  CIVL result: #{result_string($underapprox_result)}"
                                         $log.write message

                                       if civl_debug
                                           civl_out_file = "#{program_dir}/civl.iter#{iteration}.out"
                                           under_instr_iter_program = "#{under_instr_program}.#{iteration}.c"
                                           unless File.exist? under_instr_iter_program
                                             `cp #{under_instr_program} #{under_instr_iter_program}` 
                                           end
                                           directive_iter_file = "#{guidance_file}.#{iteration}"
                                           `cp #{guidance_file} #{directive_iter_file}`
                                             f = directive_iter_file
                                             # The regular expression just looks for any line containing a '.'
                                             # and replaces this with the named of the debugging file name
                                             debug_c_file = under_instr_iter_program.split('/')[-1] # Don't want absolute path
                                             File.write(f, File.read(f).gsub(/.*\..*/, debug_c_file))


                                           # Grab everything up to the directive option
                                           civl_verify_cmd_prefix = civl_verify_cmd[/(.*)\s-direct=/,1]
                                           civl_debug_verify_cmd = "#{civl_verify_cmd_prefix} "\
                                                                   "-direct=#{directive_iter_file} "\
                                                                   "#{under_instr_iter_program}"

                                           header = "Running:\n\n"\
                                                    "  #{civl_debug_verify_cmd}\n\n\n"

                                           File.open(civl_out_file, 'w') {|f| f.write(header+civl_output)}

                                       end
                                      end

                                     if $underapprox_result == error_found
                                           slice_cmd = "#{civl_jar} replay -id=#{trace_id} -sliceAnalysis #{under_instr_program}"
                                           slice_output = `#{slice_cmd}`
                                           if civl_debug
                                               message = "\n\n=============== REPLAY =================\n\n"
                                               File.open(civl_out_file, 'a') {|f| f.write(message+slice_output)}

                                           end
                                             new_input_counts = parse_input_frequency(slice_output)
                                             $input_counts = update_input_counts($input_counts, new_input_counts)

                                             new_input_types = parse_input_types(slice_output)
                                             $input_types = update_input_types($input_types, new_input_types)

                                             new_symvars_to_lines = parse_read_calls(slice_output)
                                             $symvars_to_lines = update_symvars_to_lines($symvars_to_lines, new_symvars_to_lines)

                                             new_lines_to_vars = make_line_to_var_map(slice_output)
                                             $lines_to_vars = update_lines_to_vars($lines_to_vars, new_lines_to_vars)

                                             sliced_away_count = get_number_sliced_away(slice_output)
                                             $conjuncts_sliced += sliced_away_count

                                             sliced_pc = parse_assumptions(slice_output, $symvars_to_lines)

                                             if sliced_pc.empty?
                                               always_fails = true
                                               break
                                             end
                                             $equiv_partition_stack.push sliced_pc
                                           #  lines = parse_instrument_lines(slice_output)

                                               expression_lines = grab_section("SMT TRANSLATIONS", slice_output)
                                               exprs = []
                                               expression_lines.each_line do |l|
                                                 tuple = l.split(" maps to ")
                                                 c_expr = tuple[0]
                                                 smt_expr = tuple[1]
                                                 exprs << Expression.new(c_expr, smt_expr)
                                               end

                                               declarations = grab_section("SMT DECLARATIONS", slice_output)
                                               cfc_pair = CfcPair.new(exprs, declarations)
                                               
                                               cfc_pairs << cfc_pair

                                             map = Hash[sliced_pc.zip exprs]
                                             $bounds_map[sliced_pc] = [map, declarations]


                                           disjoint_covering[sliced_pc] = [sliced_pc]
                                             message = "\n  Adding new entry to disjoint covering:\n"\
                                                         "    #{sliced_pc}\n"
                                             $log.write message



                                     elsif $underapprox_result == safe
                                         trying_to_block_spurious_errors = true
                                         if trying_to_block_spurious_errors
                                               civl_rerun_verify_cmd = "#{civl_jar} verify "\
                                                                       "-svcomp16 "\
                                                                       "-direct=#{guidance_file} "\
                                                                       "-logTraceOnBacktrack "\
                                                                       "#{under_instr_program}"
                                               puts "Rerunning CIVL with cmd: #{civl_rerun_verify_cmd}".yellow
                                               civl_rerun_output = `#{civl_rerun_verify_cmd}`

                                               replay_spurious_cmd = "#{civl_jar} replay -logTraceOnBacktrack #{under_instr_program}"
                                               civl_spurious_replay_output = `#{replay_spurious_cmd}`
                                               puts civl_spurious_replay_output.red

                                               new_input_counts = parse_input_frequency(civl_spurious_replay_output)
                                               $input_counts = update_input_counts($input_counts, new_input_counts)

                                               new_input_types = parse_input_types(civl_spurious_replay_output)
                                               $input_types = update_input_types($input_types, new_input_types)

                                               new_symvars_to_lines = parse_read_calls(civl_spurious_replay_output)
                                               $symvars_to_lines = update_symvars_to_lines($symvars_to_lines, new_symvars_to_lines)

                                               new_lines_to_vars = make_line_to_var_map(civl_spurious_replay_output)
                                               $lines_to_vars = update_lines_to_vars($lines_to_vars, new_lines_to_vars)

                                             spurious_pc = parse_assumptions(civl_spurious_replay_output, $symvars_to_lines)

                                           $log.write("\n\n  Spurious error PC:\n   #{spurious_pc}\n")
                                           unless spurious_pc.empty?
                                             spurious_errors << spurious_pc
                                           end
                                           if civl_debug
                                               civl_rerun_verify_debug_cmd = "#{civl_jar} verify "\
                                                                             "-svcomp16 "\
                                                                             "-direct=#{directive_iter_file} "\
                                                                             "-logTraceOnBacktrack "\
                                                                             "#{under_instr_iter_program}"
                                               header = "\n\n  ** Rerunning to find spurious error:\n\n  #{civl_rerun_verify_debug_cmd}\n\n"
                                               File.open(civl_out_file, 'a') {|f| f.write(header+civl_rerun_output)}

                                           end
                                         else
                                             unless just_generalized
                                               overapprox_result = unknown
                                               $underapprox_result = unknown
                                             end
                                               message = "\n  ==== Disjoint Covering after iteration #{iteration} ====\n\n"\
                                                           "#{make_bracketed_pairs(disjoint_covering)}"
                                               $log.write message

                                             iteration += 1
                                             $log.write_banner(iteration)
                                               # Don't shuffle while debugging trex
                                               # TODO: make this a shift, so this analysis is deterministic
                                               #configurations.shuffle!


                                           next
                                         end

                                     end

                                   generalization_stopwatch.stop

                                   possible_failure_stopwatch.stop
                                   definite_failure_stopwatch.start
                                           problematic_line_guidance_file = "#{guidance_file}.problematic_line"
                                           `cp #{guidance_file} #{problematic_line_guidance_file}`
                                           rewrite_line_with_string(problematic_line_guidance_file, 1, instr_program_with_problematic_line.split('/').last)

                                         bound = 42
                                         civl_verify_cmd = "#{civl_jar} verify "\
                                                           "-svcomp16 "\
                                                           "-timeout=#{$options[:civl_timeout]} "\
                                                           "-errorBound=#{bound} "\
                                                           "-direct=#{problematic_line_guidance_file} "\
                                                           "#{instr_program_with_problematic_line}"


                                       civl_output = `#{civl_verify_cmd}`

                                       if civl_output.include? "Time out."
                                         civl_timeouts += 1
                                       end
                                         if svcomp_error_found(civl_output)
                                           $underapprox_result = error_found
                                               summary_regex = 'Violation \d encountered at depth .*?(?=\R\R|\z)'
                                               violation_summaries = civl_output.scan(/#{summary_regex}/m).flatten

                                             violation_summaries.each do |violation|
                                               if is_svcomp_error(violation)
                                                 trace_id = extract_trace_id(violation)
                                                 svcomp_error_summary = violation
                                                 break
                                               end
                                             end

                                             unless svcomp_error_summary.include? "certainty: PROVEABLE"
                                               assert { puts "CIVL cannot prove error with certainty: need to specialize."; false }
                                             end

                                         elsif no_relevant_error_found(civl_output)
                                           $underapprox_result = safe
                                           spurious_error_count += 1
                                         elsif timed_out(civl_output)
                                           $underapprox_result = unknown # For timeouts
                                         else
                                           assert { puts "CIVL messed up somehow: investigate. Aborting.".red; false }
                                         end

                                         message = "  CIVL result: #{result_string($underapprox_result)}"
                                         $log.write message


                                     if $underapprox_result == error_found
                                         problematic_statement_blocking_clause = $equiv_partition_stack.last.dup
                                           slice_cmd = "#{civl_jar} replay -id=#{trace_id} -sliceAnalysis #{instr_program_with_problematic_line}"
                                           slice_output = `#{slice_cmd}`
                                           if civl_debug
                                               message = "\n\n=============== REPLAY =================\n\n"
                                               File.open(civl_out_file, 'a') {|f| f.write(message+slice_output)}

                                           end
                                             $symvars_to_lines = parse_read_calls(slice_output)
                                             problematic_sliced_pc = parse_assumptions(slice_output, $symvars_to_lines)


                                         disjoint_covering[problematic_statement_blocking_clause] = problematic_sliced_pc
                                         message = "  Disjoint element: #{problematic_statement_blocking_clause} now has an image of:\n    #{problematic_sliced_pc}"
                                         $log.write message

                                     elsif $underapprox_result == safe
                                         trying_to_block_spurious_errors = true
                                         if trying_to_block_spurious_errors
                                               civl_rerun_verify_cmd = "#{civl_jar} verify "\
                                                                       "-svcomp16 "\
                                                                       "-direct=#{guidance_file} "\
                                                                       "-logTraceOnBacktrack "\
                                                                       "#{under_instr_program}"
                                               puts "Rerunning CIVL with cmd: #{civl_rerun_verify_cmd}".yellow
                                               civl_rerun_output = `#{civl_rerun_verify_cmd}`

                                               replay_spurious_cmd = "#{civl_jar} replay -logTraceOnBacktrack #{under_instr_program}"
                                               civl_spurious_replay_output = `#{replay_spurious_cmd}`
                                               puts civl_spurious_replay_output.red

                                               new_input_counts = parse_input_frequency(civl_spurious_replay_output)
                                               $input_counts = update_input_counts($input_counts, new_input_counts)

                                               new_input_types = parse_input_types(civl_spurious_replay_output)
                                               $input_types = update_input_types($input_types, new_input_types)

                                               new_symvars_to_lines = parse_read_calls(civl_spurious_replay_output)
                                               $symvars_to_lines = update_symvars_to_lines($symvars_to_lines, new_symvars_to_lines)

                                               new_lines_to_vars = make_line_to_var_map(civl_spurious_replay_output)
                                               $lines_to_vars = update_lines_to_vars($lines_to_vars, new_lines_to_vars)

                                             spurious_pc = parse_assumptions(civl_spurious_replay_output, $symvars_to_lines)

                                           $log.write("\n\n  Spurious error PC:\n   #{spurious_pc}\n")
                                           unless spurious_pc.empty?
                                             spurious_errors << spurious_pc
                                           end
                                           if civl_debug
                                               civl_rerun_verify_debug_cmd = "#{civl_jar} verify "\
                                                                             "-svcomp16 "\
                                                                             "-direct=#{directive_iter_file} "\
                                                                             "-logTraceOnBacktrack "\
                                                                             "#{under_instr_iter_program}"
                                               header = "\n\n  ** Rerunning to find spurious error:\n\n  #{civl_rerun_verify_debug_cmd}\n\n"
                                               File.open(civl_out_file, 'a') {|f| f.write(header+civl_rerun_output)}

                                           end
                                         else
                                             unless just_generalized
                                               overapprox_result = unknown
                                               $underapprox_result = unknown
                                             end
                                               message = "\n  ==== Disjoint Covering after iteration #{iteration} ====\n\n"\
                                                           "#{make_bracketed_pairs(disjoint_covering)}"
                                               $log.write message

                                             iteration += 1
                                             $log.write_banner(iteration)
                                               # Don't shuffle while debugging trex
                                               # TODO: make this a shift, so this analysis is deterministic
                                               #configurations.shuffle!


                                           next
                                         end

                                     end

                                   definite_failure_stopwatch.stop
                                   possible_failure_stopwatch.start

                                     unless just_generalized
                                       upper_characterization = disjoint_covering.keys
                                       clauses_to_block = upper_characterization + spurious_errors
                                       over_instr_program = instrument(over_instr_program, clauses_to_block, $symvars_to_lines, $input_counts, $input_types)
                                       under_instr_program = instrument(under_instr_program, clauses_to_block, $symvars_to_lines, $input_counts, $input_types)
                                     end

                                     unless just_generalized
                                       overapprox_result = unknown
                                       $underapprox_result = unknown
                                     end
                                       message = "\n  ==== Disjoint Covering after iteration #{iteration} ====\n\n"\
                                                   "#{make_bracketed_pairs(disjoint_covering)}"
                                       $log.write message

                                     iteration += 1
                                     $log.write_banner(iteration)
                                       # Don't shuffle while debugging trex
                                       # TODO: make this a shift, so this analysis is deterministic
                                       #configurations.shuffle!


                                   goto_beginning_of_analysis_loop = true
                                   next
                                 end

                                   if ua_output.include? "SyntaxErrorResult [Line:"
                                     message = "UltimateAutomizer reported a syntax error. Aborting."
                                     $log.write message
                                     assert { puts message.red; false }
                                   end

                                   if ua_output.include? "Result:\nTRUE"
                                     overapprox_result = safe
                                   elsif ua_output.include? "Result:\nFALSE"
                                       ua_string = "UltimateAutomizer"
                                       message = "\n"
                                       message += "#{ua_string}'s timebound was: #{$adaptive_timebound_map[ua_string]}"
                                       message += "  but it just found an error after running for #{ua_stopwatch.time_spent} seconds."
                                       message += "  -> setting UA's timebound to #{ua_stopwatch.time_spent.to_i + $timebound_padding.to_i}\n"

                                       $log.write message
                                       $adaptive_timebound_map["UltimateAutomizer"] = ua_stopwatch.time_spent.to_i + $timebound_padding

                                       witness = "#{program_dir}/witness.graphml"
                                       is_cpa = false
                                       abstract_witness_object = get_abstract_witness(witness, is_cpa)
                                       abstract_witness = get_canonical_string(abstract_witness_object)

                                     if $witness_cache.include? abstract_witness
                                       overapprox_result = cached 
                                     else
                                       overapprox_result = error_found 
                                       $witness_cache << abstract_witness
                                         guidance_file = make_guidance_file(abstract_witness_object, program_dir, under_instr_program)

                                     end
                                   else
                                     overapprox_result = unknown
                                   end

                                 ua_stopwatch.reset

                             end
                           elsif conf.first == "CPA"
                             if $options[:only_ua]
                               next
                             else
                                 c = conf[1] # grab configuration flag for CPA
                                 if ($options[:single_cpa] && (c != "sv-comp17"))
                                   next
                                 else
                                     cpa_timeout = $adaptive_timebound_map[c]
                                     cpa_cmd = "timeout #{cpa_timeout} "\
                                               "#{cpa_script} -#{c} "\
                                               "-timelimit 900 "\
                                               "-spec #{cpa_specification} "\
                                               "-setprop output.path=\"#{cpa_output_dir}\" "\
                                               "#{over_instr_program}"

                                     cpa_stopwatch.start
                                     cpa_output, cpa_stderr, status = Open3.capture3(cpa_cmd)
                                     cpa_stopwatch.stop

                                     if cpa_debug
                                         config_out_file = "#{program_dir}/#{c}.iter#{iteration}.out"
                                         over_instr_iter_program = "#{over_instr_program}.#{iteration}.c"
                                         `cp #{over_instr_program} #{over_instr_iter_program}` 

                                         cpa_cmd_prefix = cpa_cmd[/(.*)\s/,1] # Remove program name from cmd
                                         header = "Running:\n\n"\
                                                  "  #{cpa_cmd_prefix} #{over_instr_iter_program}\n\n\n"

                                         File.open(config_out_file, 'w') {|f| f.write(header+cpa_output)}

                                     end
                                     if cpa_stderr.include? "Warning: Analysis interrupted (The CPU-time limit"
                                       cpa_timeouts += 1
                                     end

                                     if cpa_output.include? "Verification result: TRUE."
                                       overapprox_result = safe
                                     elsif cpa_output.include? "Verification result: FALSE."
                                         message =  "\n"
                                         message += "CPA config #{c}'s timebound was: #{$adaptive_timebound_map[c]}\n"
                                         message += "  but it just found an error after running for #{cpa_stopwatch.time_spent} seconds.\n"
                                         message += "  -> setting #{c}'s timebound to #{cpa_stopwatch.time_spent.to_i + $timebound_padding.to_i}\n"

                                         $log.write message
                                         $adaptive_timebound_map[c] = cpa_stopwatch.time_spent.to_i + $timebound_padding

                                         if c.include? "sv-comp17"
                                           witness = "#{cpa_output_dir}/violation-witness.graphml"
                                         else
                                           witness = "#{cpa_output_dir}/witness.graphml"
                                           witness_gz = witness+'.gz'
                                           `rm #{witness}` if File.exist? witness # from a previous run
                                           unless File.exist? witness_gz
                                             assert { puts 'Zipped witness file does not exist in #{cpa_output_dir}. Aborting.'; false }
                                           end
                                           `gunzip #{witness_gz}`
                                         end
                                         is_cpa = true
                                         abstract_witness_object = get_abstract_witness(witness, is_cpa)
                                         abstract_witness = get_canonical_string(abstract_witness_object)

                                       if $witness_cache.include? abstract_witness
                                         overapprox_result = cached 
                                       else
                                         overapprox_result = error_found 
                                         $witness_cache << abstract_witness
                                           guidance_file = make_guidance_file(abstract_witness_object, program_dir, under_instr_program)

                                       end
                                     else
                                       overapprox_result = unknown
                                     end

                                   cpa_stopwatch.reset
                                 end

                             end
                           else
                             assert { puts "Unsupported overapproximation tool: #{conf.first}"; false }
                           end
                             if conf.first == "UltimateAutomizer"
                               message = "  UAutomizer result: #{result_string(overapprox_result)}"
                             else
                               message = "  #{c} result: #{result_string(overapprox_result)}"
                             end
                             $log.write message

                             unless (overapprox_result == cached || overapprox_result == unknown)
                               break
                             end

                           break if goto_beginning_of_analysis_loop
                         end
                         if goto_beginning_of_analysis_loop
                           possible_failure_stopwatch.stop
                           next
                         end
                           message = "\n  -------------\n"\
                                       "  Witness cache: #{$witness_cache}\n"\
                                       "  -------------\n"
                           $log.write message


                        end

                     end
                     if ( overapprox_result == safe )
                       generalized_clause = iffy_generalization
                         update_disjoint_covering(disjoint_covering, generalized_clause)

                       break
                     elsif ( overapprox_result == error_found )
                        if $options[:parallel]
                          run_civl_in_parallel(directives, program_dir, over_instr_program, disjoint_covering)      
                        else
                           bound = 42
                           civl_verify_cmd = "#{civl_jar} verify "\
                                             "-svcomp16 "\
                                             "-timeout=#{$options[:civl_timeout]} "\
                                             "-errorBound=#{bound} "\
                                             "-direct=#{guidance_file} "\
                                             "#{under_instr_program}"


                         definite_failure_stopwatch.start
                         civl_output = `#{civl_verify_cmd}`
                         definite_failure_stopwatch.stop

                         #puts "CIVL output: #{civl_output}".blue
                         if civl_output.include? "Time out."
                           civl_timeouts += 1
                         end
                           if svcomp_error_found(civl_output)
                             $underapprox_result = error_found
                                 summary_regex = 'Violation \d encountered at depth .*?(?=\R\R|\z)'
                                 violation_summaries = civl_output.scan(/#{summary_regex}/m).flatten

                               violation_summaries.each do |violation|
                                 if is_svcomp_error(violation)
                                   trace_id = extract_trace_id(violation)
                                   svcomp_error_summary = violation
                                   break
                                 end
                               end

                               unless svcomp_error_summary.include? "certainty: PROVEABLE"
                                 assert { puts "CIVL cannot prove error with certainty: need to specialize."; false }
                               end

                           elsif no_relevant_error_found(civl_output)
                             $underapprox_result = safe
                             spurious_error_count += 1
                           elsif timed_out(civl_output)
                             $underapprox_result = unknown # For timeouts
                           else
                             assert { puts "CIVL messed up somehow: investigate. Aborting.".red; false }
                           end

                           message = "  CIVL result: #{result_string($underapprox_result)}"
                           $log.write message

                         if civl_debug
                             civl_out_file = "#{program_dir}/civl.iter#{iteration}.out"
                             under_instr_iter_program = "#{under_instr_program}.#{iteration}.c"
                             unless File.exist? under_instr_iter_program
                               `cp #{under_instr_program} #{under_instr_iter_program}` 
                             end
                             directive_iter_file = "#{guidance_file}.#{iteration}"
                             `cp #{guidance_file} #{directive_iter_file}`
                               f = directive_iter_file
                               # The regular expression just looks for any line containing a '.'
                               # and replaces this with the named of the debugging file name
                               debug_c_file = under_instr_iter_program.split('/')[-1] # Don't want absolute path
                               File.write(f, File.read(f).gsub(/.*\..*/, debug_c_file))


                             # Grab everything up to the directive option
                             civl_verify_cmd_prefix = civl_verify_cmd[/(.*)\s-direct=/,1]
                             civl_debug_verify_cmd = "#{civl_verify_cmd_prefix} "\
                                                     "-direct=#{directive_iter_file} "\
                                                     "#{under_instr_iter_program}"

                             header = "Running:\n\n"\
                                      "  #{civl_debug_verify_cmd}\n\n\n"

                             File.open(civl_out_file, 'w') {|f| f.write(header+civl_output)}

                         end
                        end

                       if $underapprox_result == error_found
                         generalized_clause = iffy_generalization
                           update_disjoint_covering(disjoint_covering, generalized_clause)

                         $equiv_partition_stack.pop
                             slice_cmd = "#{civl_jar} replay -id=#{trace_id} -sliceAnalysis #{under_instr_program}"
                             slice_output = `#{slice_cmd}`
                             if civl_debug
                                 message = "\n\n=============== REPLAY =================\n\n"
                                 File.open(civl_out_file, 'a') {|f| f.write(message+slice_output)}

                             end
                               new_input_counts = parse_input_frequency(slice_output)
                               $input_counts = update_input_counts($input_counts, new_input_counts)

                               new_input_types = parse_input_types(slice_output)
                               $input_types = update_input_types($input_types, new_input_types)

                               new_symvars_to_lines = parse_read_calls(slice_output)
                               $symvars_to_lines = update_symvars_to_lines($symvars_to_lines, new_symvars_to_lines)

                               new_lines_to_vars = make_line_to_var_map(slice_output)
                               $lines_to_vars = update_lines_to_vars($lines_to_vars, new_lines_to_vars)

                               sliced_away_count = get_number_sliced_away(slice_output)
                               $conjuncts_sliced += sliced_away_count

                               sliced_pc = parse_assumptions(slice_output, $symvars_to_lines)

                               if sliced_pc.empty?
                                 always_fails = true
                                 break
                               end
                               $equiv_partition_stack.push sliced_pc
                             #  lines = parse_instrument_lines(slice_output)

                                 expression_lines = grab_section("SMT TRANSLATIONS", slice_output)
                                 exprs = []
                                 expression_lines.each_line do |l|
                                   tuple = l.split(" maps to ")
                                   c_expr = tuple[0]
                                   smt_expr = tuple[1]
                                   exprs << Expression.new(c_expr, smt_expr)
                                 end

                                 declarations = grab_section("SMT DECLARATIONS", slice_output)
                                 cfc_pair = CfcPair.new(exprs, declarations)
                                 
                                 cfc_pairs << cfc_pair

                               map = Hash[sliced_pc.zip exprs]
                               $bounds_map[sliced_pc] = [map, declarations]


                             disjoint_covering[sliced_pc] = [sliced_pc]
                               message = "\n  Adding new entry to disjoint covering:\n"\
                                           "    #{sliced_pc}\n"
                               $log.write message



                         break
                       end
                     end
                     #if ( overapprox_result == safe || overapprox_result == error_found )
                     #  generalized_clause = iffy_generalization
                     #  just_generalized = true
                     #  break
                     #end
                   end

                   if ( overapprox_result == safe )
                     break
                   elsif ( ! ( overapprox_result == error_found && $underapprox_result == error_found ) )
                     # If our generalization scheme didn't work, we punt.
                     generalized_clause = top_of_lattice
                       update_disjoint_covering(disjoint_covering, generalized_clause)

                         analysis_moved_to_top = true
                         $log.write "\n\n  ########## BLOCKING TOP ##########\n"
                         puts "Blocking TOP".green
                         overapprox_result = safe
                         just_generalized = true

                     break
                   end

                 generalization_stopwatch.stop
               end

           end
            if $options[:parallel]
              run_civl_in_parallel(directives, program_dir, over_instr_program, disjoint_covering)      
            else
               bound = 42
               civl_verify_cmd = "#{civl_jar} verify "\
                                 "-svcomp16 "\
                                 "-timeout=#{$options[:civl_timeout]} "\
                                 "-errorBound=#{bound} "\
                                 "-direct=#{guidance_file} "\
                                 "#{under_instr_program}"


             definite_failure_stopwatch.start
             civl_output = `#{civl_verify_cmd}`
             definite_failure_stopwatch.stop

             #puts "CIVL output: #{civl_output}".blue
             if civl_output.include? "Time out."
               civl_timeouts += 1
             end
               if svcomp_error_found(civl_output)
                 $underapprox_result = error_found
                     summary_regex = 'Violation \d encountered at depth .*?(?=\R\R|\z)'
                     violation_summaries = civl_output.scan(/#{summary_regex}/m).flatten

                   violation_summaries.each do |violation|
                     if is_svcomp_error(violation)
                       trace_id = extract_trace_id(violation)
                       svcomp_error_summary = violation
                       break
                     end
                   end

                   unless svcomp_error_summary.include? "certainty: PROVEABLE"
                     assert { puts "CIVL cannot prove error with certainty: need to specialize."; false }
                   end

               elsif no_relevant_error_found(civl_output)
                 $underapprox_result = safe
                 spurious_error_count += 1
               elsif timed_out(civl_output)
                 $underapprox_result = unknown # For timeouts
               else
                 assert { puts "CIVL messed up somehow: investigate. Aborting.".red; false }
               end

               message = "  CIVL result: #{result_string($underapprox_result)}"
               $log.write message

             if civl_debug
                 civl_out_file = "#{program_dir}/civl.iter#{iteration}.out"
                 under_instr_iter_program = "#{under_instr_program}.#{iteration}.c"
                 unless File.exist? under_instr_iter_program
                   `cp #{under_instr_program} #{under_instr_iter_program}` 
                 end
                 directive_iter_file = "#{guidance_file}.#{iteration}"
                 `cp #{guidance_file} #{directive_iter_file}`
                   f = directive_iter_file
                   # The regular expression just looks for any line containing a '.'
                   # and replaces this with the named of the debugging file name
                   debug_c_file = under_instr_iter_program.split('/')[-1] # Don't want absolute path
                   File.write(f, File.read(f).gsub(/.*\..*/, debug_c_file))


                 # Grab everything up to the directive option
                 civl_verify_cmd_prefix = civl_verify_cmd[/(.*)\s-direct=/,1]
                 civl_debug_verify_cmd = "#{civl_verify_cmd_prefix} "\
                                         "-direct=#{directive_iter_file} "\
                                         "#{under_instr_iter_program}"

                 header = "Running:\n\n"\
                          "  #{civl_debug_verify_cmd}\n\n\n"

                 File.open(civl_out_file, 'w') {|f| f.write(header+civl_output)}

             end
            end

         else
           if overapprox_result == safe
             break
           elsif overapprox_result == error_found
              if $options[:parallel]
                run_civl_in_parallel(directives, program_dir, over_instr_program, disjoint_covering)      
              else
                 bound = 42
                 civl_verify_cmd = "#{civl_jar} verify "\
                                   "-svcomp16 "\
                                   "-timeout=#{$options[:civl_timeout]} "\
                                   "-errorBound=#{bound} "\
                                   "-direct=#{guidance_file} "\
                                   "#{under_instr_program}"


               definite_failure_stopwatch.start
               civl_output = `#{civl_verify_cmd}`
               definite_failure_stopwatch.stop

               #puts "CIVL output: #{civl_output}".blue
               if civl_output.include? "Time out."
                 civl_timeouts += 1
               end
                 if svcomp_error_found(civl_output)
                   $underapprox_result = error_found
                       summary_regex = 'Violation \d encountered at depth .*?(?=\R\R|\z)'
                       violation_summaries = civl_output.scan(/#{summary_regex}/m).flatten

                     violation_summaries.each do |violation|
                       if is_svcomp_error(violation)
                         trace_id = extract_trace_id(violation)
                         svcomp_error_summary = violation
                         break
                       end
                     end

                     unless svcomp_error_summary.include? "certainty: PROVEABLE"
                       assert { puts "CIVL cannot prove error with certainty: need to specialize."; false }
                     end

                 elsif no_relevant_error_found(civl_output)
                   $underapprox_result = safe
                   spurious_error_count += 1
                 elsif timed_out(civl_output)
                   $underapprox_result = unknown # For timeouts
                 else
                   assert { puts "CIVL messed up somehow: investigate. Aborting.".red; false }
                 end

                 message = "  CIVL result: #{result_string($underapprox_result)}"
                 $log.write message

               if civl_debug
                   civl_out_file = "#{program_dir}/civl.iter#{iteration}.out"
                   under_instr_iter_program = "#{under_instr_program}.#{iteration}.c"
                   unless File.exist? under_instr_iter_program
                     `cp #{under_instr_program} #{under_instr_iter_program}` 
                   end
                   directive_iter_file = "#{guidance_file}.#{iteration}"
                   `cp #{guidance_file} #{directive_iter_file}`
                     f = directive_iter_file
                     # The regular expression just looks for any line containing a '.'
                     # and replaces this with the named of the debugging file name
                     debug_c_file = under_instr_iter_program.split('/')[-1] # Don't want absolute path
                     File.write(f, File.read(f).gsub(/.*\..*/, debug_c_file))


                   # Grab everything up to the directive option
                   civl_verify_cmd_prefix = civl_verify_cmd[/(.*)\s-direct=/,1]
                   civl_debug_verify_cmd = "#{civl_verify_cmd_prefix} "\
                                           "-direct=#{directive_iter_file} "\
                                           "#{under_instr_iter_program}"

                   header = "Running:\n\n"\
                            "  #{civl_debug_verify_cmd}\n\n\n"

                   File.open(civl_out_file, 'w') {|f| f.write(header+civl_output)}

               end
              end

             if $underapprox_result == error_found
               slice_stopwatch.start
                   slice_cmd = "#{civl_jar} replay -id=#{trace_id} -sliceAnalysis #{under_instr_program}"
                   slice_output = `#{slice_cmd}`
                   if civl_debug
                       message = "\n\n=============== REPLAY =================\n\n"
                       File.open(civl_out_file, 'a') {|f| f.write(message+slice_output)}

                   end
                     new_input_counts = parse_input_frequency(slice_output)
                     $input_counts = update_input_counts($input_counts, new_input_counts)

                     new_input_types = parse_input_types(slice_output)
                     $input_types = update_input_types($input_types, new_input_types)

                     new_symvars_to_lines = parse_read_calls(slice_output)
                     $symvars_to_lines = update_symvars_to_lines($symvars_to_lines, new_symvars_to_lines)

                     new_lines_to_vars = make_line_to_var_map(slice_output)
                     $lines_to_vars = update_lines_to_vars($lines_to_vars, new_lines_to_vars)

                     sliced_away_count = get_number_sliced_away(slice_output)
                     $conjuncts_sliced += sliced_away_count

                     sliced_pc = parse_assumptions(slice_output, $symvars_to_lines)

                     if sliced_pc.empty?
                       always_fails = true
                       break
                     end
                     $equiv_partition_stack.push sliced_pc
                   #  lines = parse_instrument_lines(slice_output)

                       expression_lines = grab_section("SMT TRANSLATIONS", slice_output)
                       exprs = []
                       expression_lines.each_line do |l|
                         tuple = l.split(" maps to ")
                         c_expr = tuple[0]
                         smt_expr = tuple[1]
                         exprs << Expression.new(c_expr, smt_expr)
                       end

                       declarations = grab_section("SMT DECLARATIONS", slice_output)
                       cfc_pair = CfcPair.new(exprs, declarations)
                       
                       cfc_pairs << cfc_pair

                     map = Hash[sliced_pc.zip exprs]
                     $bounds_map[sliced_pc] = [map, declarations]


                   disjoint_covering[sliced_pc] = [sliced_pc]
                     message = "\n  Adding new entry to disjoint covering:\n"\
                                 "    #{sliced_pc}\n"
                     $log.write message



               slice_stopwatch.stop
             elsif $underapprox_result == safe
                 trying_to_block_spurious_errors = true
                 if trying_to_block_spurious_errors
                       civl_rerun_verify_cmd = "#{civl_jar} verify "\
                                               "-svcomp16 "\
                                               "-direct=#{guidance_file} "\
                                               "-logTraceOnBacktrack "\
                                               "#{under_instr_program}"
                       puts "Rerunning CIVL with cmd: #{civl_rerun_verify_cmd}".yellow
                       civl_rerun_output = `#{civl_rerun_verify_cmd}`

                       replay_spurious_cmd = "#{civl_jar} replay -logTraceOnBacktrack #{under_instr_program}"
                       civl_spurious_replay_output = `#{replay_spurious_cmd}`
                       puts civl_spurious_replay_output.red

                       new_input_counts = parse_input_frequency(civl_spurious_replay_output)
                       $input_counts = update_input_counts($input_counts, new_input_counts)

                       new_input_types = parse_input_types(civl_spurious_replay_output)
                       $input_types = update_input_types($input_types, new_input_types)

                       new_symvars_to_lines = parse_read_calls(civl_spurious_replay_output)
                       $symvars_to_lines = update_symvars_to_lines($symvars_to_lines, new_symvars_to_lines)

                       new_lines_to_vars = make_line_to_var_map(civl_spurious_replay_output)
                       $lines_to_vars = update_lines_to_vars($lines_to_vars, new_lines_to_vars)

                     spurious_pc = parse_assumptions(civl_spurious_replay_output, $symvars_to_lines)

                   $log.write("\n\n  Spurious error PC:\n   #{spurious_pc}\n")
                   unless spurious_pc.empty?
                     spurious_errors << spurious_pc
                   end
                   if civl_debug
                       civl_rerun_verify_debug_cmd = "#{civl_jar} verify "\
                                                     "-svcomp16 "\
                                                     "-direct=#{directive_iter_file} "\
                                                     "-logTraceOnBacktrack "\
                                                     "#{under_instr_iter_program}"
                       header = "\n\n  ** Rerunning to find spurious error:\n\n  #{civl_rerun_verify_debug_cmd}\n\n"
                       File.open(civl_out_file, 'a') {|f| f.write(header+civl_rerun_output)}

                   end
                 else
                     unless just_generalized
                       overapprox_result = unknown
                       $underapprox_result = unknown
                     end
                       message = "\n  ==== Disjoint Covering after iteration #{iteration} ====\n\n"\
                                   "#{make_bracketed_pairs(disjoint_covering)}"
                       $log.write message

                     iteration += 1
                     $log.write_banner(iteration)
                       # Don't shuffle while debugging trex
                       # TODO: make this a shift, so this analysis is deterministic
                       #configurations.shuffle!


                   next
                 end

             end
           else
               if iteration == 1
                 message = "No overapproximators could find an error on the first iteration. Aborting."
                 $log.write message; puts message; abort
               end
               if $options[:parallel]
                 clause_to_generalize = $last_injected_clauses_queue.deq

                 upper = disjoint_covering.keys
                 upper = upper - [clause_to_generalize]

                 $clauses_that_prove_safety = []
                 # returns an array of OverapproximateComputation objects
             #    generalized_clause = binary_search(@portfolio, program_dir, over_instr_program, clause_to_generalize, upper)
                 over_computations = binary_search(@portfolio, program_dir, over_instr_program, clause_to_generalize, upper)
                 generalized_clause = over_computations.map {|c| c.generalized_clause}.compact.first
                 puts "Generalized clause:".red; generalized_clause.each {|c| puts c}
                 $last_injected_clauses_queue.enq generalized_clause
                 if generalized_clause
                   lowest_safety_clause = $clauses_that_prove_safety.max_by {|e| e.size}
                   generalized_clause = lowest_safety_clause
                   update_disjoint_covering(disjoint_covering, generalized_clause, clause_to_generalize)
                   puts "Safety clause lowest in the lattice: #{lowest_safety_clause}"
                   overapprox_result = safe
                   just_generalized = true
                   break
                 else
                   update_disjoint_covering(disjoint_covering, generalized_clause, clause_to_generalize)
                 end
                 assert { puts "Drop in generalization in parallel.".red; false }
                 assert { puts "Need to update disjoint partition map with generalized clause".red; false }
               else
                 generalization_stopwatch.start
                   generalization_count += 1
                   generalized_clause = top_of_lattice
                     disjoint_covering.delete($equiv_partition_stack.last)

                     upper_approximation = disjoint_covering.keys
                     base = find_maximally_intersecting_set(upper_approximation, $equiv_partition_stack.last)
                     candidates = []

                     if $equiv_partition_stack.last
                   #    candidate_clause = $equiv_partition_stack.last - base
                   #    candidates = get_candidate_powerset(candidate_clause)
                       clause_size = $equiv_partition_stack.last.size
                       if clause_size > 3
                           candidates.concat $equiv_partition_stack.last.combination($equiv_partition_stack.last.size-1).to_a

                       elsif clause_size > 2
                           candidates.concat $equiv_partition_stack.last.combination(1).to_a
                           candidates.concat $equiv_partition_stack.last.combination(2).to_a

                       elsif clause_size > 1
                           candidates.concat $equiv_partition_stack.last.combination(1).to_a

                       else
                         generalized_clause = top_of_lattice
                           update_disjoint_covering(disjoint_covering, generalized_clause)

                             analysis_moved_to_top = true
                             $log.write "\n\n  ########## BLOCKING TOP ##########\n"
                             puts "Blocking TOP".green
                             overapprox_result = safe
                             just_generalized = true

                       end
                     else
                       generalized_clause = top_of_lattice
                         update_disjoint_covering(disjoint_covering, generalized_clause)

                           analysis_moved_to_top = true
                           $log.write "\n\n  ########## BLOCKING TOP ##########\n"
                           puts "Blocking TOP".green
                           overapprox_result = safe
                           just_generalized = true

                     end


                   message = "The candidate clauses in this generalization attempt are: #{candidates}"

                   candidates.each do |clause_set|
                     iffy_generalization = $equiv_partition_stack.last - clause_set
                       if iffy_generalization.flatten.empty?
                         generalized_clause = top_of_lattice
                           update_disjoint_covering(disjoint_covering, generalized_clause)

                             analysis_moved_to_top = true
                             $log.write "\n\n  ########## BLOCKING TOP ##########\n"
                             puts "Blocking TOP".green
                             overapprox_result = safe
                             just_generalized = true

                       end

                     unless analysis_moved_to_top
                         upper_characterization = disjoint_covering.keys
                         blocking_clauses = upper_characterization + [iffy_generalization] + spurious_errors
                         puts "Blocking clauses in generalization: #{blocking_clauses}".red
                       # instrumented_program = instrument(instrumented_program, blocking_clauses, $symvars_to_lines, $input_counts, $input_types)
                         over_instr_program = instrument(over_instr_program, blocking_clauses, $symvars_to_lines, $input_counts, $input_types)
                         under_instr_program = instrument(under_instr_program, blocking_clauses, $symvars_to_lines, $input_counts, $input_types)

                         message = "\n  ** Attempting to generalize **\n\n"\
                                     "    Dropping:\n"\
                                     "      #{clause_set}\n"\
                                     "    Tentatively blocking:\n"\
                                     "      #{iffy_generalization}\n"
                         $log.write message

                       
                         goto_beginning_of_analysis_loop = false

                        if $options[:parallel]

                         over_computations = run_in_parallel(@portfolio, over_instr_program, program_dir)

                         if over_computations.select {|c| c.result == "TRUE"}.any?
                           message = "Over approximator found a TRUE result. Exiting."
                           $log.write message
                           overapprox_result = safe
                         else
                           pieces_of_evidence = over_computations.select {|c| c.result == "FALSE"}.map {|c| c.evidence}.uniq  
                           puts "Pieces of evidence: #{pieces_of_evidence}".yellow
                           if new_evidence(pieces_of_evidence, $witness_cache).any?
                             directives = make_directives(pieces_of_evidence, program_dir, over_instr_program)
                             puts "Directives:".blue
                             directives.each {|d| puts "#{d.evidence} #{d.file} \n\n"}
                             overapprox_result = error_found
                       #      string_evidence = pieces_of_evidence.map {|e| e.canonical_string}.uniq
                       #      $witness_cache = $witness_cache + string_evidence
                             $witness_cache = $witness_cache + pieces_of_evidence.uniq
                           else
                             overapprox_result = cached
                           end
                         end

                        else


                         configurations.each do |conf|
                           if conf.first == "UltimateAutomizer"
                             if $options[:only_cpa]
                               next
                             else
                                 ua_cmd = "#{ua_script} "\
                                          "--full-output "\
                                          "--spec #{ua_specification} "\
                                          "--architecture 32bit "\
                                          "--file #{over_instr_program} "\
                                          "--witness-dir #{program_dir}"

                                 ua_stopwatch.start
                                 ua_timeout = $adaptive_timebound_map["UltimateAutomizer"]
                                 ua_output = `timeout #{ua_timeout} #{ua_cmd}`
                                 ua_stopwatch.stop

                                 #puts ua_output.yellow
                                 if $?.exitstatus == TIMEOUT_STATUS
                                   ua_timeouts += 1
                                 end
                                 File.open("#{program_dir}/Ultimate.log.#{iteration}","w") {|f| f.write(ua_output)}
                                 
                                 unsupported_operation_message = "- UnsupportedSyntaxResult [Line: "
                                 if ua_output.include? unsupported_operation_message
                                     instr_program_with_problematic_line = "#{under_instr_program}.problematic.#{iteration}.c"
                                     `cp #{under_instr_program} #{instr_program_with_problematic_line}`


                                   generalization_stopwatch.start
                                       problem_line = ua_output.scan(/UnsupportedSyntaxResult \[Line: (\d+)\]/).flatten.first.to_i
                                       message = "  UA found a problem line at: #{problem_line}; prepending error line to instrumented file."
                                       $log.write message
                                       prepend_error_at_line(over_instr_program, problem_line)
                                       prepend_error_at_line(under_instr_program, problem_line)
                                       generalization_count += 1
                                         ua_output = `timeout #{$options[:ua_timeout]} #{ua_cmd}`
                                         if $?.exitstatus == TIMEOUT_STATUS
                                           ua_timeouts += 1
                                         end
                                         File.open("#{program_dir}/Ultimate.log.#{iteration}","w") {|f| f.write(ua_output)}  
                                           if ua_output.include? "SyntaxErrorResult [Line:"
                                             message = "UltimateAutomizer reported a syntax error. Aborting."
                                             $log.write message
                                             assert { puts message.red; false }
                                           end

                                           if ua_output.include? "Result:\nTRUE"
                                             overapprox_result = safe
                                           elsif ua_output.include? "Result:\nFALSE"
                                               ua_string = "UltimateAutomizer"
                                               message = "\n"
                                               message += "#{ua_string}'s timebound was: #{$adaptive_timebound_map[ua_string]}"
                                               message += "  but it just found an error after running for #{ua_stopwatch.time_spent} seconds."
                                               message += "  -> setting UA's timebound to #{ua_stopwatch.time_spent.to_i + $timebound_padding.to_i}\n"

                                               $log.write message
                                               $adaptive_timebound_map["UltimateAutomizer"] = ua_stopwatch.time_spent.to_i + $timebound_padding

                                               witness = "#{program_dir}/witness.graphml"
                                               is_cpa = false
                                               abstract_witness_object = get_abstract_witness(witness, is_cpa)
                                               abstract_witness = get_canonical_string(abstract_witness_object)

                                             if $witness_cache.include? abstract_witness
                                               overapprox_result = cached 
                                             else
                                               overapprox_result = error_found 
                                               $witness_cache << abstract_witness
                                                 guidance_file = make_guidance_file(abstract_witness_object, program_dir, under_instr_program)

                                             end
                                           else
                                             overapprox_result = unknown
                                           end

                                         ua_stopwatch.reset
                                           if conf.first == "UltimateAutomizer"
                                             message = "  UAutomizer result: #{result_string(overapprox_result)}"
                                           else
                                             message = "  #{c} result: #{result_string(overapprox_result)}"
                                           end
                                           $log.write message



                                   # Do I classify this chunk as a definite failure? It's a definite overapproximation...
                                      if $options[:parallel]
                                        run_civl_in_parallel(directives, program_dir, over_instr_program, disjoint_covering)      
                                      else
                                         bound = 42
                                         civl_verify_cmd = "#{civl_jar} verify "\
                                                           "-svcomp16 "\
                                                           "-timeout=#{$options[:civl_timeout]} "\
                                                           "-errorBound=#{bound} "\
                                                           "-direct=#{guidance_file} "\
                                                           "#{under_instr_program}"


                                       definite_failure_stopwatch.start
                                       civl_output = `#{civl_verify_cmd}`
                                       definite_failure_stopwatch.stop

                                       #puts "CIVL output: #{civl_output}".blue
                                       if civl_output.include? "Time out."
                                         civl_timeouts += 1
                                       end
                                         if svcomp_error_found(civl_output)
                                           $underapprox_result = error_found
                                               summary_regex = 'Violation \d encountered at depth .*?(?=\R\R|\z)'
                                               violation_summaries = civl_output.scan(/#{summary_regex}/m).flatten

                                             violation_summaries.each do |violation|
                                               if is_svcomp_error(violation)
                                                 trace_id = extract_trace_id(violation)
                                                 svcomp_error_summary = violation
                                                 break
                                               end
                                             end

                                             unless svcomp_error_summary.include? "certainty: PROVEABLE"
                                               assert { puts "CIVL cannot prove error with certainty: need to specialize."; false }
                                             end

                                         elsif no_relevant_error_found(civl_output)
                                           $underapprox_result = safe
                                           spurious_error_count += 1
                                         elsif timed_out(civl_output)
                                           $underapprox_result = unknown # For timeouts
                                         else
                                           assert { puts "CIVL messed up somehow: investigate. Aborting.".red; false }
                                         end

                                         message = "  CIVL result: #{result_string($underapprox_result)}"
                                         $log.write message

                                       if civl_debug
                                           civl_out_file = "#{program_dir}/civl.iter#{iteration}.out"
                                           under_instr_iter_program = "#{under_instr_program}.#{iteration}.c"
                                           unless File.exist? under_instr_iter_program
                                             `cp #{under_instr_program} #{under_instr_iter_program}` 
                                           end
                                           directive_iter_file = "#{guidance_file}.#{iteration}"
                                           `cp #{guidance_file} #{directive_iter_file}`
                                             f = directive_iter_file
                                             # The regular expression just looks for any line containing a '.'
                                             # and replaces this with the named of the debugging file name
                                             debug_c_file = under_instr_iter_program.split('/')[-1] # Don't want absolute path
                                             File.write(f, File.read(f).gsub(/.*\..*/, debug_c_file))


                                           # Grab everything up to the directive option
                                           civl_verify_cmd_prefix = civl_verify_cmd[/(.*)\s-direct=/,1]
                                           civl_debug_verify_cmd = "#{civl_verify_cmd_prefix} "\
                                                                   "-direct=#{directive_iter_file} "\
                                                                   "#{under_instr_iter_program}"

                                           header = "Running:\n\n"\
                                                    "  #{civl_debug_verify_cmd}\n\n\n"

                                           File.open(civl_out_file, 'w') {|f| f.write(header+civl_output)}

                                       end
                                      end

                                     if $underapprox_result == error_found
                                           slice_cmd = "#{civl_jar} replay -id=#{trace_id} -sliceAnalysis #{under_instr_program}"
                                           slice_output = `#{slice_cmd}`
                                           if civl_debug
                                               message = "\n\n=============== REPLAY =================\n\n"
                                               File.open(civl_out_file, 'a') {|f| f.write(message+slice_output)}

                                           end
                                             new_input_counts = parse_input_frequency(slice_output)
                                             $input_counts = update_input_counts($input_counts, new_input_counts)

                                             new_input_types = parse_input_types(slice_output)
                                             $input_types = update_input_types($input_types, new_input_types)

                                             new_symvars_to_lines = parse_read_calls(slice_output)
                                             $symvars_to_lines = update_symvars_to_lines($symvars_to_lines, new_symvars_to_lines)

                                             new_lines_to_vars = make_line_to_var_map(slice_output)
                                             $lines_to_vars = update_lines_to_vars($lines_to_vars, new_lines_to_vars)

                                             sliced_away_count = get_number_sliced_away(slice_output)
                                             $conjuncts_sliced += sliced_away_count

                                             sliced_pc = parse_assumptions(slice_output, $symvars_to_lines)

                                             if sliced_pc.empty?
                                               always_fails = true
                                               break
                                             end
                                             $equiv_partition_stack.push sliced_pc
                                           #  lines = parse_instrument_lines(slice_output)

                                               expression_lines = grab_section("SMT TRANSLATIONS", slice_output)
                                               exprs = []
                                               expression_lines.each_line do |l|
                                                 tuple = l.split(" maps to ")
                                                 c_expr = tuple[0]
                                                 smt_expr = tuple[1]
                                                 exprs << Expression.new(c_expr, smt_expr)
                                               end

                                               declarations = grab_section("SMT DECLARATIONS", slice_output)
                                               cfc_pair = CfcPair.new(exprs, declarations)
                                               
                                               cfc_pairs << cfc_pair

                                             map = Hash[sliced_pc.zip exprs]
                                             $bounds_map[sliced_pc] = [map, declarations]


                                           disjoint_covering[sliced_pc] = [sliced_pc]
                                             message = "\n  Adding new entry to disjoint covering:\n"\
                                                         "    #{sliced_pc}\n"
                                             $log.write message



                                     elsif $underapprox_result == safe
                                         trying_to_block_spurious_errors = true
                                         if trying_to_block_spurious_errors
                                               civl_rerun_verify_cmd = "#{civl_jar} verify "\
                                                                       "-svcomp16 "\
                                                                       "-direct=#{guidance_file} "\
                                                                       "-logTraceOnBacktrack "\
                                                                       "#{under_instr_program}"
                                               puts "Rerunning CIVL with cmd: #{civl_rerun_verify_cmd}".yellow
                                               civl_rerun_output = `#{civl_rerun_verify_cmd}`

                                               replay_spurious_cmd = "#{civl_jar} replay -logTraceOnBacktrack #{under_instr_program}"
                                               civl_spurious_replay_output = `#{replay_spurious_cmd}`
                                               puts civl_spurious_replay_output.red

                                               new_input_counts = parse_input_frequency(civl_spurious_replay_output)
                                               $input_counts = update_input_counts($input_counts, new_input_counts)

                                               new_input_types = parse_input_types(civl_spurious_replay_output)
                                               $input_types = update_input_types($input_types, new_input_types)

                                               new_symvars_to_lines = parse_read_calls(civl_spurious_replay_output)
                                               $symvars_to_lines = update_symvars_to_lines($symvars_to_lines, new_symvars_to_lines)

                                               new_lines_to_vars = make_line_to_var_map(civl_spurious_replay_output)
                                               $lines_to_vars = update_lines_to_vars($lines_to_vars, new_lines_to_vars)

                                             spurious_pc = parse_assumptions(civl_spurious_replay_output, $symvars_to_lines)

                                           $log.write("\n\n  Spurious error PC:\n   #{spurious_pc}\n")
                                           unless spurious_pc.empty?
                                             spurious_errors << spurious_pc
                                           end
                                           if civl_debug
                                               civl_rerun_verify_debug_cmd = "#{civl_jar} verify "\
                                                                             "-svcomp16 "\
                                                                             "-direct=#{directive_iter_file} "\
                                                                             "-logTraceOnBacktrack "\
                                                                             "#{under_instr_iter_program}"
                                               header = "\n\n  ** Rerunning to find spurious error:\n\n  #{civl_rerun_verify_debug_cmd}\n\n"
                                               File.open(civl_out_file, 'a') {|f| f.write(header+civl_rerun_output)}

                                           end
                                         else
                                             unless just_generalized
                                               overapprox_result = unknown
                                               $underapprox_result = unknown
                                             end
                                               message = "\n  ==== Disjoint Covering after iteration #{iteration} ====\n\n"\
                                                           "#{make_bracketed_pairs(disjoint_covering)}"
                                               $log.write message

                                             iteration += 1
                                             $log.write_banner(iteration)
                                               # Don't shuffle while debugging trex
                                               # TODO: make this a shift, so this analysis is deterministic
                                               #configurations.shuffle!


                                           next
                                         end

                                     end

                                   generalization_stopwatch.stop

                                   possible_failure_stopwatch.stop
                                   definite_failure_stopwatch.start
                                           problematic_line_guidance_file = "#{guidance_file}.problematic_line"
                                           `cp #{guidance_file} #{problematic_line_guidance_file}`
                                           rewrite_line_with_string(problematic_line_guidance_file, 1, instr_program_with_problematic_line.split('/').last)

                                         bound = 42
                                         civl_verify_cmd = "#{civl_jar} verify "\
                                                           "-svcomp16 "\
                                                           "-timeout=#{$options[:civl_timeout]} "\
                                                           "-errorBound=#{bound} "\
                                                           "-direct=#{problematic_line_guidance_file} "\
                                                           "#{instr_program_with_problematic_line}"


                                       civl_output = `#{civl_verify_cmd}`

                                       if civl_output.include? "Time out."
                                         civl_timeouts += 1
                                       end
                                         if svcomp_error_found(civl_output)
                                           $underapprox_result = error_found
                                               summary_regex = 'Violation \d encountered at depth .*?(?=\R\R|\z)'
                                               violation_summaries = civl_output.scan(/#{summary_regex}/m).flatten

                                             violation_summaries.each do |violation|
                                               if is_svcomp_error(violation)
                                                 trace_id = extract_trace_id(violation)
                                                 svcomp_error_summary = violation
                                                 break
                                               end
                                             end

                                             unless svcomp_error_summary.include? "certainty: PROVEABLE"
                                               assert { puts "CIVL cannot prove error with certainty: need to specialize."; false }
                                             end

                                         elsif no_relevant_error_found(civl_output)
                                           $underapprox_result = safe
                                           spurious_error_count += 1
                                         elsif timed_out(civl_output)
                                           $underapprox_result = unknown # For timeouts
                                         else
                                           assert { puts "CIVL messed up somehow: investigate. Aborting.".red; false }
                                         end

                                         message = "  CIVL result: #{result_string($underapprox_result)}"
                                         $log.write message


                                     if $underapprox_result == error_found
                                         problematic_statement_blocking_clause = $equiv_partition_stack.last.dup
                                           slice_cmd = "#{civl_jar} replay -id=#{trace_id} -sliceAnalysis #{instr_program_with_problematic_line}"
                                           slice_output = `#{slice_cmd}`
                                           if civl_debug
                                               message = "\n\n=============== REPLAY =================\n\n"
                                               File.open(civl_out_file, 'a') {|f| f.write(message+slice_output)}

                                           end
                                             $symvars_to_lines = parse_read_calls(slice_output)
                                             problematic_sliced_pc = parse_assumptions(slice_output, $symvars_to_lines)


                                         disjoint_covering[problematic_statement_blocking_clause] = problematic_sliced_pc
                                         message = "  Disjoint element: #{problematic_statement_blocking_clause} now has an image of:\n    #{problematic_sliced_pc}"
                                         $log.write message

                                     elsif $underapprox_result == safe
                                         trying_to_block_spurious_errors = true
                                         if trying_to_block_spurious_errors
                                               civl_rerun_verify_cmd = "#{civl_jar} verify "\
                                                                       "-svcomp16 "\
                                                                       "-direct=#{guidance_file} "\
                                                                       "-logTraceOnBacktrack "\
                                                                       "#{under_instr_program}"
                                               puts "Rerunning CIVL with cmd: #{civl_rerun_verify_cmd}".yellow
                                               civl_rerun_output = `#{civl_rerun_verify_cmd}`

                                               replay_spurious_cmd = "#{civl_jar} replay -logTraceOnBacktrack #{under_instr_program}"
                                               civl_spurious_replay_output = `#{replay_spurious_cmd}`
                                               puts civl_spurious_replay_output.red

                                               new_input_counts = parse_input_frequency(civl_spurious_replay_output)
                                               $input_counts = update_input_counts($input_counts, new_input_counts)

                                               new_input_types = parse_input_types(civl_spurious_replay_output)
                                               $input_types = update_input_types($input_types, new_input_types)

                                               new_symvars_to_lines = parse_read_calls(civl_spurious_replay_output)
                                               $symvars_to_lines = update_symvars_to_lines($symvars_to_lines, new_symvars_to_lines)

                                               new_lines_to_vars = make_line_to_var_map(civl_spurious_replay_output)
                                               $lines_to_vars = update_lines_to_vars($lines_to_vars, new_lines_to_vars)

                                             spurious_pc = parse_assumptions(civl_spurious_replay_output, $symvars_to_lines)

                                           $log.write("\n\n  Spurious error PC:\n   #{spurious_pc}\n")
                                           unless spurious_pc.empty?
                                             spurious_errors << spurious_pc
                                           end
                                           if civl_debug
                                               civl_rerun_verify_debug_cmd = "#{civl_jar} verify "\
                                                                             "-svcomp16 "\
                                                                             "-direct=#{directive_iter_file} "\
                                                                             "-logTraceOnBacktrack "\
                                                                             "#{under_instr_iter_program}"
                                               header = "\n\n  ** Rerunning to find spurious error:\n\n  #{civl_rerun_verify_debug_cmd}\n\n"
                                               File.open(civl_out_file, 'a') {|f| f.write(header+civl_rerun_output)}

                                           end
                                         else
                                             unless just_generalized
                                               overapprox_result = unknown
                                               $underapprox_result = unknown
                                             end
                                               message = "\n  ==== Disjoint Covering after iteration #{iteration} ====\n\n"\
                                                           "#{make_bracketed_pairs(disjoint_covering)}"
                                               $log.write message

                                             iteration += 1
                                             $log.write_banner(iteration)
                                               # Don't shuffle while debugging trex
                                               # TODO: make this a shift, so this analysis is deterministic
                                               #configurations.shuffle!


                                           next
                                         end

                                     end

                                   definite_failure_stopwatch.stop
                                   possible_failure_stopwatch.start

                                     unless just_generalized
                                       upper_characterization = disjoint_covering.keys
                                       clauses_to_block = upper_characterization + spurious_errors
                                       over_instr_program = instrument(over_instr_program, clauses_to_block, $symvars_to_lines, $input_counts, $input_types)
                                       under_instr_program = instrument(under_instr_program, clauses_to_block, $symvars_to_lines, $input_counts, $input_types)
                                     end

                                     unless just_generalized
                                       overapprox_result = unknown
                                       $underapprox_result = unknown
                                     end
                                       message = "\n  ==== Disjoint Covering after iteration #{iteration} ====\n\n"\
                                                   "#{make_bracketed_pairs(disjoint_covering)}"
                                       $log.write message

                                     iteration += 1
                                     $log.write_banner(iteration)
                                       # Don't shuffle while debugging trex
                                       # TODO: make this a shift, so this analysis is deterministic
                                       #configurations.shuffle!


                                   goto_beginning_of_analysis_loop = true
                                   next
                                 end

                                   if ua_output.include? "SyntaxErrorResult [Line:"
                                     message = "UltimateAutomizer reported a syntax error. Aborting."
                                     $log.write message
                                     assert { puts message.red; false }
                                   end

                                   if ua_output.include? "Result:\nTRUE"
                                     overapprox_result = safe
                                   elsif ua_output.include? "Result:\nFALSE"
                                       ua_string = "UltimateAutomizer"
                                       message = "\n"
                                       message += "#{ua_string}'s timebound was: #{$adaptive_timebound_map[ua_string]}"
                                       message += "  but it just found an error after running for #{ua_stopwatch.time_spent} seconds."
                                       message += "  -> setting UA's timebound to #{ua_stopwatch.time_spent.to_i + $timebound_padding.to_i}\n"

                                       $log.write message
                                       $adaptive_timebound_map["UltimateAutomizer"] = ua_stopwatch.time_spent.to_i + $timebound_padding

                                       witness = "#{program_dir}/witness.graphml"
                                       is_cpa = false
                                       abstract_witness_object = get_abstract_witness(witness, is_cpa)
                                       abstract_witness = get_canonical_string(abstract_witness_object)

                                     if $witness_cache.include? abstract_witness
                                       overapprox_result = cached 
                                     else
                                       overapprox_result = error_found 
                                       $witness_cache << abstract_witness
                                         guidance_file = make_guidance_file(abstract_witness_object, program_dir, under_instr_program)

                                     end
                                   else
                                     overapprox_result = unknown
                                   end

                                 ua_stopwatch.reset

                             end
                           elsif conf.first == "CPA"
                             if $options[:only_ua]
                               next
                             else
                                 c = conf[1] # grab configuration flag for CPA
                                 if ($options[:single_cpa] && (c != "sv-comp17"))
                                   next
                                 else
                                     cpa_timeout = $adaptive_timebound_map[c]
                                     cpa_cmd = "timeout #{cpa_timeout} "\
                                               "#{cpa_script} -#{c} "\
                                               "-timelimit 900 "\
                                               "-spec #{cpa_specification} "\
                                               "-setprop output.path=\"#{cpa_output_dir}\" "\
                                               "#{over_instr_program}"

                                     cpa_stopwatch.start
                                     cpa_output, cpa_stderr, status = Open3.capture3(cpa_cmd)
                                     cpa_stopwatch.stop

                                     if cpa_debug
                                         config_out_file = "#{program_dir}/#{c}.iter#{iteration}.out"
                                         over_instr_iter_program = "#{over_instr_program}.#{iteration}.c"
                                         `cp #{over_instr_program} #{over_instr_iter_program}` 

                                         cpa_cmd_prefix = cpa_cmd[/(.*)\s/,1] # Remove program name from cmd
                                         header = "Running:\n\n"\
                                                  "  #{cpa_cmd_prefix} #{over_instr_iter_program}\n\n\n"

                                         File.open(config_out_file, 'w') {|f| f.write(header+cpa_output)}

                                     end
                                     if cpa_stderr.include? "Warning: Analysis interrupted (The CPU-time limit"
                                       cpa_timeouts += 1
                                     end

                                     if cpa_output.include? "Verification result: TRUE."
                                       overapprox_result = safe
                                     elsif cpa_output.include? "Verification result: FALSE."
                                         message =  "\n"
                                         message += "CPA config #{c}'s timebound was: #{$adaptive_timebound_map[c]}\n"
                                         message += "  but it just found an error after running for #{cpa_stopwatch.time_spent} seconds.\n"
                                         message += "  -> setting #{c}'s timebound to #{cpa_stopwatch.time_spent.to_i + $timebound_padding.to_i}\n"

                                         $log.write message
                                         $adaptive_timebound_map[c] = cpa_stopwatch.time_spent.to_i + $timebound_padding

                                         if c.include? "sv-comp17"
                                           witness = "#{cpa_output_dir}/violation-witness.graphml"
                                         else
                                           witness = "#{cpa_output_dir}/witness.graphml"
                                           witness_gz = witness+'.gz'
                                           `rm #{witness}` if File.exist? witness # from a previous run
                                           unless File.exist? witness_gz
                                             assert { puts 'Zipped witness file does not exist in #{cpa_output_dir}. Aborting.'; false }
                                           end
                                           `gunzip #{witness_gz}`
                                         end
                                         is_cpa = true
                                         abstract_witness_object = get_abstract_witness(witness, is_cpa)
                                         abstract_witness = get_canonical_string(abstract_witness_object)

                                       if $witness_cache.include? abstract_witness
                                         overapprox_result = cached 
                                       else
                                         overapprox_result = error_found 
                                         $witness_cache << abstract_witness
                                           guidance_file = make_guidance_file(abstract_witness_object, program_dir, under_instr_program)

                                       end
                                     else
                                       overapprox_result = unknown
                                     end

                                   cpa_stopwatch.reset
                                 end

                             end
                           else
                             assert { puts "Unsupported overapproximation tool: #{conf.first}"; false }
                           end
                             if conf.first == "UltimateAutomizer"
                               message = "  UAutomizer result: #{result_string(overapprox_result)}"
                             else
                               message = "  #{c} result: #{result_string(overapprox_result)}"
                             end
                             $log.write message

                             unless (overapprox_result == cached || overapprox_result == unknown)
                               break
                             end

                           break if goto_beginning_of_analysis_loop
                         end
                         if goto_beginning_of_analysis_loop
                           possible_failure_stopwatch.stop
                           next
                         end
                           message = "\n  -------------\n"\
                                       "  Witness cache: #{$witness_cache}\n"\
                                       "  -------------\n"
                           $log.write message


                        end

                     end
                     if ( overapprox_result == safe )
                       generalized_clause = iffy_generalization
                         update_disjoint_covering(disjoint_covering, generalized_clause)

                       break
                     elsif ( overapprox_result == error_found )
                        if $options[:parallel]
                          run_civl_in_parallel(directives, program_dir, over_instr_program, disjoint_covering)      
                        else
                           bound = 42
                           civl_verify_cmd = "#{civl_jar} verify "\
                                             "-svcomp16 "\
                                             "-timeout=#{$options[:civl_timeout]} "\
                                             "-errorBound=#{bound} "\
                                             "-direct=#{guidance_file} "\
                                             "#{under_instr_program}"


                         definite_failure_stopwatch.start
                         civl_output = `#{civl_verify_cmd}`
                         definite_failure_stopwatch.stop

                         #puts "CIVL output: #{civl_output}".blue
                         if civl_output.include? "Time out."
                           civl_timeouts += 1
                         end
                           if svcomp_error_found(civl_output)
                             $underapprox_result = error_found
                                 summary_regex = 'Violation \d encountered at depth .*?(?=\R\R|\z)'
                                 violation_summaries = civl_output.scan(/#{summary_regex}/m).flatten

                               violation_summaries.each do |violation|
                                 if is_svcomp_error(violation)
                                   trace_id = extract_trace_id(violation)
                                   svcomp_error_summary = violation
                                   break
                                 end
                               end

                               unless svcomp_error_summary.include? "certainty: PROVEABLE"
                                 assert { puts "CIVL cannot prove error with certainty: need to specialize."; false }
                               end

                           elsif no_relevant_error_found(civl_output)
                             $underapprox_result = safe
                             spurious_error_count += 1
                           elsif timed_out(civl_output)
                             $underapprox_result = unknown # For timeouts
                           else
                             assert { puts "CIVL messed up somehow: investigate. Aborting.".red; false }
                           end

                           message = "  CIVL result: #{result_string($underapprox_result)}"
                           $log.write message

                         if civl_debug
                             civl_out_file = "#{program_dir}/civl.iter#{iteration}.out"
                             under_instr_iter_program = "#{under_instr_program}.#{iteration}.c"
                             unless File.exist? under_instr_iter_program
                               `cp #{under_instr_program} #{under_instr_iter_program}` 
                             end
                             directive_iter_file = "#{guidance_file}.#{iteration}"
                             `cp #{guidance_file} #{directive_iter_file}`
                               f = directive_iter_file
                               # The regular expression just looks for any line containing a '.'
                               # and replaces this with the named of the debugging file name
                               debug_c_file = under_instr_iter_program.split('/')[-1] # Don't want absolute path
                               File.write(f, File.read(f).gsub(/.*\..*/, debug_c_file))


                             # Grab everything up to the directive option
                             civl_verify_cmd_prefix = civl_verify_cmd[/(.*)\s-direct=/,1]
                             civl_debug_verify_cmd = "#{civl_verify_cmd_prefix} "\
                                                     "-direct=#{directive_iter_file} "\
                                                     "#{under_instr_iter_program}"

                             header = "Running:\n\n"\
                                      "  #{civl_debug_verify_cmd}\n\n\n"

                             File.open(civl_out_file, 'w') {|f| f.write(header+civl_output)}

                         end
                        end

                       if $underapprox_result == error_found
                         generalized_clause = iffy_generalization
                           update_disjoint_covering(disjoint_covering, generalized_clause)

                         $equiv_partition_stack.pop
                             slice_cmd = "#{civl_jar} replay -id=#{trace_id} -sliceAnalysis #{under_instr_program}"
                             slice_output = `#{slice_cmd}`
                             if civl_debug
                                 message = "\n\n=============== REPLAY =================\n\n"
                                 File.open(civl_out_file, 'a') {|f| f.write(message+slice_output)}

                             end
                               new_input_counts = parse_input_frequency(slice_output)
                               $input_counts = update_input_counts($input_counts, new_input_counts)

                               new_input_types = parse_input_types(slice_output)
                               $input_types = update_input_types($input_types, new_input_types)

                               new_symvars_to_lines = parse_read_calls(slice_output)
                               $symvars_to_lines = update_symvars_to_lines($symvars_to_lines, new_symvars_to_lines)

                               new_lines_to_vars = make_line_to_var_map(slice_output)
                               $lines_to_vars = update_lines_to_vars($lines_to_vars, new_lines_to_vars)

                               sliced_away_count = get_number_sliced_away(slice_output)
                               $conjuncts_sliced += sliced_away_count

                               sliced_pc = parse_assumptions(slice_output, $symvars_to_lines)

                               if sliced_pc.empty?
                                 always_fails = true
                                 break
                               end
                               $equiv_partition_stack.push sliced_pc
                             #  lines = parse_instrument_lines(slice_output)

                                 expression_lines = grab_section("SMT TRANSLATIONS", slice_output)
                                 exprs = []
                                 expression_lines.each_line do |l|
                                   tuple = l.split(" maps to ")
                                   c_expr = tuple[0]
                                   smt_expr = tuple[1]
                                   exprs << Expression.new(c_expr, smt_expr)
                                 end

                                 declarations = grab_section("SMT DECLARATIONS", slice_output)
                                 cfc_pair = CfcPair.new(exprs, declarations)
                                 
                                 cfc_pairs << cfc_pair

                               map = Hash[sliced_pc.zip exprs]
                               $bounds_map[sliced_pc] = [map, declarations]


                             disjoint_covering[sliced_pc] = [sliced_pc]
                               message = "\n  Adding new entry to disjoint covering:\n"\
                                           "    #{sliced_pc}\n"
                               $log.write message



                         break
                       end
                     end
                     #if ( overapprox_result == safe || overapprox_result == error_found )
                     #  generalized_clause = iffy_generalization
                     #  just_generalized = true
                     #  break
                     #end
                   end

                   if ( overapprox_result == safe )
                     break
                   elsif ( ! ( overapprox_result == error_found && $underapprox_result == error_found ) )
                     # If our generalization scheme didn't work, we punt.
                     generalized_clause = top_of_lattice
                       update_disjoint_covering(disjoint_covering, generalized_clause)

                         analysis_moved_to_top = true
                         $log.write "\n\n  ########## BLOCKING TOP ##########\n"
                         puts "Blocking TOP".green
                         overapprox_result = safe
                         just_generalized = true

                     break
                   end

                 generalization_stopwatch.stop
               end

           end
         end
          unless just_generalized
            upper_characterization = disjoint_covering.keys
            clauses_to_block = upper_characterization + spurious_errors
            over_instr_program = instrument(over_instr_program, clauses_to_block, $symvars_to_lines, $input_counts, $input_types)
            under_instr_program = instrument(under_instr_program, clauses_to_block, $symvars_to_lines, $input_counts, $input_types)
          end

          unless just_generalized
            overapprox_result = unknown
            $underapprox_result = unknown
          end
            message = "\n  ==== Disjoint Covering after iteration #{iteration} ====\n\n"\
                        "#{make_bracketed_pairs(disjoint_covering)}"
            $log.write message

          iteration += 1
          $log.write_banner(iteration)
            # Don't shuffle while debugging trex
            # TODO: make this a shift, so this analysis is deterministic
            #configurations.shuffle!


      end

      analysis_stopwatch.stop
      if $options[:smt]
        unless analysis_moved_to_top
          
            smt_dir = program_dir+"/smt_files"
            if File.exist? smt_dir
              `rm #{smt_dir}/*`
            else
              `mkdir #{smt_dir}`
            end

            smt_files = []

            disjoint_covering.each_with_index do |(upper, lower), i|
              file_str = make_smt_string(upper, lower, $bounds_map)
              file_name = "cfc_pair_#{i}.smt"
              file_path = smt_dir+'/'+file_name
              File.write(file_path, file_str)
              smt_files << file_path
            end

            PCP_DIR = File.expand_path('./tools/PathConditionsProbability', File.dirname(__FILE__))
            pcp_script = PCP_DIR+"/startPCP"
            puts "Running PCP:".red
            smt_files.each {|f| puts `#{pcp_script} <#{f}`}

        end
      end
        partitions_without_gap = 0
        disjoint_covering.each do |upper_bound, lower_bound|
          if lower_bound.size == 1
            if upper_bound == lower_bound.first
              partitions_without_gap += 1
            end
          end
        end


      msg = "\n===============================================\n"\
              "================= CFC RESULTS =================\n"\
              "===============================================\n"\
              "\n"\
              "\n"\
              "  Analysis time: #{analysis_stopwatch.time_spent}\n"\
              "  Possible failure time: #{possible_failure_stopwatch.time_spent}\n"\
              "  Generalization time: #{generalization_stopwatch.time_spent}\n"\
              "  Definite failure time: #{definite_failure_stopwatch.time_spent}\n"\
              "  Slice time: #{slice_stopwatch.time_spent}\n"\
              "\n"\
              "  UA timeouts: #{ua_timeouts} (#{$options[:ua_timeout]} seconds)\n"\
              "  CPA timeouts: #{cpa_timeouts} (#{$options[:cpa_timeout]} seconds)\n"\
              "  CIVL timeouts: #{civl_timeouts} (#{$options[:civl_timeout]} seconds)\n"\
              "\n"\
              "  Iterations: #{iteration}\n"\
              "  Generalizations: #{generalization_count}\n"\
              "  Spurious errors: #{spurious_error_count}\n"\
              "\n"\
              "  Conjuncts sliced: #{$conjuncts_sliced}\n"\
              "  Total partitions: #{disjoint_covering.size}\n"\
              "  Partitions without gap: #{partitions_without_gap}\n"\
              "\n"\
              "  Final disjoint covering:\n"\
              "#{make_bracketed_pairs(disjoint_covering)}"
      if analysis_moved_to_top
        msg += "  *** Analysis moved to top ***\n"
      end
      if always_fails
        msg += "  *** All executions lead to an error ***\n"
      end
      msg += "\n\n"

      $log.write msg
      if ( $options[:prettify] )
        puts prettify(msg)
      else
        puts msg
      end


