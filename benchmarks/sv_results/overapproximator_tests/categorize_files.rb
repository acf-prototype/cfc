unless ARGV.size == 2
  puts "Expecting two arguments: list_file, directory"; abort
end

require 'fileutils'

list_file = ARGV[0]
dir = ARGV[1]

files = File.readlines("./#{list_file}")
files = files.map {|name| name.strip}
files = files.map {|name| name.gsub(".out","")}

path = "#{Dir.pwd}/#{dir}"
target_dir = ""
if list_file.include? "success"
  target_dir = "#{path}/SUCCESS"
elsif list_file.include? "fail"
  target_dir = "#{path}/FAIL"
elsif list_file.include? "unknown"
  target_dir = "#{path}/UNKNOWN"
else
  puts "Unexpected list_file: #{list_file}"; abort
end
FileUtils.mkdir target_dir unless File.exist? target_dir

ext = ""; prefix = ""
if dir == "scripts/"
  ext = "rb"; prefix = "single-overs__"
elsif dir == "sandhills_scripts/"
  ext = "script"; prefix = "single-overs__"
elsif dir == "output/"
  ext = "out"
elsif dir == "error/"
  ext = "err"
else
  puts "Unexpected directory: #{dir}"; abort
end

files.each do |name|
  file_path = "#{path}/#{prefix}#{name}.#{ext}"
  FileUtils.mv(file_path, target_dir) if File.exist? file_path
end
