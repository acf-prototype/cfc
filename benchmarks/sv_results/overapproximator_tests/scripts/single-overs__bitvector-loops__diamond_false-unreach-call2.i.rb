require 'open3'

found_error = false
error_finding_config = ""

configs = [["CPA", "sv-comp17"], ["CPA", "sv-comp16-bam"], ["CPA", "sv-comp17-k-induction"], ["UltimateAutomizer"]]

commands = ["timeout 68 /lustre/work/esquared/mgerrard/cfc/analyzers/cpachecker/scripts/cpa.sh -sv-comp17 -timelimit 900 -spec /lustre/work/esquared/mgerrard/cfc/analyzers/cpachecker/config/specification/sv-comp-reachability.spc ../../../sv_subset_falsified_by_cpa_or_ua_transform/c/bitvector-loops/diamond_false-unreach-call2.i", "timeout 68 /lustre/work/esquared/mgerrard/cfc/analyzers/cpachecker/scripts/cpa.sh -sv-comp16-bam -timelimit 900 -spec /lustre/work/esquared/mgerrard/cfc/analyzers/cpachecker/config/specification/sv-comp-reachability.spc ../../../sv_subset_falsified_by_cpa_or_ua_transform/c/bitvector-loops/diamond_false-unreach-call2.i", "timeout 68 /lustre/work/esquared/mgerrard/cfc/analyzers/cpachecker/scripts/cpa.sh -sv-comp17-k-induction -timelimit 900 -spec /lustre/work/esquared/mgerrard/cfc/analyzers/cpachecker/config/specification/sv-comp-reachability.spc ../../../sv_subset_falsified_by_cpa_or_ua_transform/c/bitvector-loops/diamond_false-unreach-call2.i", "timeout 68 /lustre/work/esquared/mgerrard/cfc/analyzers/uautomizer/Ultimate.py --full-output --spec /lustre/work/esquared/mgerrard/cfc/analyzers/uautomizer/PropertyUnreachCall.prp --architecture 32bit --file ../../../sv_subset_falsified_by_cpa_or_ua_transform/c/bitvector-loops/diamond_false-unreach-call2.i"]
commands.each_with_index do |cmd, i|
  puts "Trying to run: "+cmd
  stdout, stderr, status = Open3.capture3(cmd)

  if stdout.include? "FALSE"
    found_error = true
    error_finding_config = configs[i].last
  end
  
  break if found_error
end
  
if found_error
  puts "
  SUCCESS with "+error_finding_config+": ../../sv_subset_falsified_by_cpa_or_ua_transform/c/bitvector-loops/diamond_false-unreach-call2.i"
else
  puts "
  FAIL: ../../sv_subset_falsified_by_cpa_or_ua_transform/c/bitvector-loops/diamond_false-unreach-call2.i"
end

