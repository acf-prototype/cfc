def convert_to_hr_min_sec ( t )
  # Solution: https://gist.github.com/shunchu/3175001
  return Time.at(t).utc.strftime("%H:%M:%S")
end

def get_script_time ( time )
  single_padding = 60 # seconds
  num_of_tools = 4
  # Ask for double the total time on Sandhills
  time = 2*((time+single_padding)*num_of_tools)
  return convert_to_hr_min_sec(time)
end

def get_script_path ( base_name )
  return "#{Dir.pwd}/scripts/single-overs__#{base_name}.rb"
end

def get_base_name ( program )
  return program.split('/c/').last.split('/').join('__')
end

STDOUT_DIR = "/work/esquared/mgerrard/cfc/benchmarks/sv_results/overapproximator_tests/output"
STDERR_DIR = "/work/esquared/mgerrard/cfc/benchmarks/sv_results/overapproximator_tests/error"

def get_script_string ( config )
  time = get_script_time(config.time.to_i)
  base_name = get_base_name(config.program)
  script_path = get_script_path(base_name)
  
  stdout_file = "#{STDOUT_DIR}/#{base_name}.out"
  stderr_file = "#{STDERR_DIR}/#{base_name}.err"

return <<-SCRIPT
#!/bin/sh
#SBATCH --time=#{time}           # Run time in hh:mm:ss
#SBATCH --mem-per-cpu=1024       # Maximum memory required per CPU (in megabytes)
#SBATCH --job-name=#{base_name}
#SBATCH --error=#{stderr_file}
#SBATCH --output=#{stdout_file}
#SBATCH --partition=esquared

module load ruby/2.0
module load java/1.8
module load python/2.7
module load compiler/gcc/4.9

shopt -s extglob
LD_LIBRARY_PATH=/work/esquared/mgerrard/local/provers/cvc3/lib:$LD_LIBRARY_PATH
PATH=/work/esquared/mgerrard/local/bin:$PATH
export UA_PATH="/work/esquared/mgerrard/cfc/analyzers/uautomizer" 
export PATH=$UA_PATH:$PATH

ruby #{script_path}
SCRIPT
end

SANDHILLS_SCRIPT_DIR = "/work/esquared/mgerrard/cfc/benchmarks/sv_results/overapproximator_tests/sandhills_scripts/"

def make_sandhills_script_for_single_overapprox_test ( config )
  script_string = get_script_string(config)
  base_name = get_base_name(config.program)
  sandhills_script_path = "#{SANDHILLS_SCRIPT_DIR}/single-overs__#{base_name}.script"
  File.write(sandhills_script_path, script_string)
end
