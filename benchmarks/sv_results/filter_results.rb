require 'nokogiri'

def reports_error(n)
  return (n.css("column[title='status']").first['value'] == "false(unreach-call)")
end

def error_exists(n)
  return (n.css("column[title='category']").first['value'] == "correct")
end

def found_error(n)
  return (reports_error(n) and error_exists(n))
end

def get_program_name(n)
  relative_path = n["name"]
  sv_path = relative_path.split('/')[1..-1].join('/')
  return sv_path
end

def get_cpu_time(n)
  e = n.css("column[title=cputime]")
  time = e.first['value'][0..-2] # strip 's' at the end
  return time.to_i
end

def get_tool_name(doc)
  return doc.css("result").first["benchmarkname"]
end

class SvResult
  attr_reader :program, :tool_name, :cpu_time
  def initialize ( p, t, c )
    @program = p; @tool_name = t; @cpu_time = c
  end
end

cpa_results_dir = "./CPA"
ua_results_dir = "./UA"

falsified_results = []

tool_dirs = [cpa_results_dir, ua_results_dir]
tool_dirs.each do |tool_dir|
  Dir.foreach(tool_dir) do |category_xml|
    next if category_xml.start_with? '.'
    path = tool_dir+'/'+category_xml
    doc = File.open(path) { |f| Nokogiri::XML(f) }
    tool_name = get_tool_name(doc)

    result_nodes = doc.css("run")
    result_nodes.each do |r|
      if found_error(r)
        program = get_program_name(r)
        cpu_time = get_cpu_time(r)
        falsified_results << SvResult.new(program,tool_name,cpu_time)
      end
    end
  end
end

falsified_results.sort! {|a,b| a.program <=> b.program}

falsified_programs = falsified_results.map {|e| e.program}
falsified_programs.uniq!
falsified_programs_file = "falsified_programs"
File.open(falsified_programs_file, "w+") {|f| f.puts(falsified_programs)}

require 'fileutils'

falsified_programs.each do |orig_program|
  copy_program = orig_program.gsub("sv-benchmarks", "sv_subset_falsified_by_cpa_or_ua")
  unless File.exist? copy_program
    FileUtils.mkdir_p(File.dirname(copy_program))
    FileUtils.cp(orig_program, copy_program)
  end
end

file_str = ""
curr_program = ""
falsified_results.each do |result|
  unless curr_program == result.program
    curr_program = result.program
    file_str += "#{curr_program}:\n"
  end
  file_str += "  #{result.tool_name} #{result.cpu_time}\n"
end
file_path = "programs_tool-names_timeouts"
File.write(file_path, file_str)

file_str = ""
falsified_programs.each do |program|
  rs = falsified_results.select {|r| r.program == program}
  cpu_times = rs.map {|r| r.cpu_time}
  max_cpu_time = cpu_times.max
  fastest_tool = rs.min_by(&:cpu_time).tool_name
  file_str += "#{program} #{cpu_times.max} #{fastest_tool}\n"
end
file_path = "program_max-cpu-time_fastest-tool"
File.write(file_path, file_str)
