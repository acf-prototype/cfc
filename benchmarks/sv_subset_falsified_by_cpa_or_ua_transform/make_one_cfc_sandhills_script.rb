STDOUT_DIR = "/work/esquared/mgerrard/cfc/benchmarks/sv_subset_falsified_by_cpa_or_ua_transform/output"
STDERR_DIR = "/work/esquared/mgerrard/cfc/benchmarks/sv_subset_falsified_by_cpa_or_ua_transform/error"
CFC = "/work/esquared/mgerrard/cfc/src/cfc.rb"

def convert_to_hr_min_sec ( t )
  # Solution: https://gist.github.com/shunchu/3175001
  return Time.at(t).utc.strftime("%H:%M:%S")
end

def get_script_timeout ( time )
  single_padding = 60 # seconds
  num_of_tools = 4
  # Ask for double the total time on Sandhills
  time = 2*((time.to_i+single_padding)*num_of_tools)
  return convert_to_hr_min_sec(time)
end

def get_base_name ( program )
  return program.split('/c/').last.split('/').join('__')
end

def make_script ( program, timeout, tool )
  max_time = 50000 # Time based on longest running of successful results
  script_timeout = convert_to_hr_min_sec(max_time)
  base_name = get_base_name(program)
  padding = 60
  #  timeout = timeout + padding
  timeout = 900 # SV-COMP timeout -- how many can go through?

  memory = 4096
  stdout_file = "#{STDOUT_DIR}/#{base_name}.out"
  stderr_file = "#{STDERR_DIR}/#{base_name}.err"

return <<-SCRIPT
#!/bin/sh
#SBATCH --time=#{script_timeout}           # Run time in hh:mm:ss
#SBATCH --mem-per-cpu=#{memory}       # Maximum memory required per CPU (in megabytes)
#SBATCH --job-name=#{base_name}
#SBATCH --error=#{stderr_file}
#SBATCH --output=#{stdout_file}
#SBATCH --partition=esquared

module load ruby/2.0
module load java/1.8
module load python/2.7
module load compiler/gcc/4.9

shopt -s extglob
LD_LIBRARY_PATH=/work/esquared/mgerrard/local/provers/cvc3/lib:$LD_LIBRARY_PATH
PATH=/work/esquared/mgerrard/local/bin:$PATH
export UA_PATH="/work/esquared/mgerrard/cfc/analyzers/uautomizer" 
export PATH=$UA_PATH:$PATH

ruby #{CFC} --first_tool #{tool} --ua_timeout #{timeout} --cpa_timeout #{timeout} #{program}
SCRIPT
end

SANDHILLS_SCRIPT_DIR = "/work/esquared/mgerrard/cfc/benchmarks/sv_subset_falsified_by_cpa_or_ua_transform/scripts"

def make_one_cfc_sandhills_script ( program, timeout, tool )
  script_string = make_script(program, timeout, tool)
  base_name = get_base_name(program)
  sandhills_script_path = "#{SANDHILLS_SCRIPT_DIR}/#{base_name}.script"
  File.write(sandhills_script_path, script_string)
end
