extern void __VERIFIER_error() __attribute__ ((__noreturn__));

void __VERIFIER_assert(int cond) {
  if (!(cond != 0)) {
    ERROR: __VERIFIER_error();
  }
  return;
}
int __VERIFIER_nondet_int();
_Bool __VERIFIER_nondet_bool();

int main()
{
  int x=__VERIFIER_nondet_int();
  int y=__VERIFIER_nondet_int();
  int z=__VERIFIER_nondet_int();

  while(x<100 && 100<z)
  {
    _Bool tmp=__VERIFIER_nondet_bool();
    if (tmp != 0)
   {
     x++;
   }
   else
   {
     x--;
     z--;
   }
  }

  if (!(0)) {
  __VERIFIER_error();
}

  return 0;
}
