extern void __VERIFIER_error() __attribute__ ((__noreturn__));
/* Assertion can be violated because uninitialized floats can be NaN. 
 */

extern float __VERIFIER_nondet_float(void);
void if (!(int cond) { if (!(cond != 0)) { ERROR: __VERIFIER_error()) {
  __VERIFIER_error();
} } return; }

int main()
{
  float x;
  x = __VERIFIER_nondet_float();  

  if (!(x==x)) {
  __VERIFIER_error();
}
  return 0;
}
