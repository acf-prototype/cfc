extern void __VERIFIER_error() __attribute__ ((__noreturn__));
void if (!(int cond) { if (!(cond != 0)) { ERROR: __VERIFIER_error()) {
  __VERIFIER_error();
} } return; }

int main()
{
  double x = 1.0/7.0;
  long long res = 0;

  int i = 1;
  while(x != 0)
  {
    res += ((int)(x * 10) % 10) * (i * 10);
    x = (x * 10) - (int) x * 10;
    i++;
  }

  if (!(res > 56430)) {
  __VERIFIER_error();
}
  return 0;
}
