#!/bin/bash
cp -R -u -p sv_subset_falsified_by_cpa_or_ua sv_subset_falsified_by_cpa_or_ua_transform
cd sv_subset_falsified_by_cpa_or_ua_transform

# rewrite if statements
find ./ -type f -exec sed -i -- 's/if\s*(\s*\([_a-zA-Z][_a-zA-Z0-9]\{0,30\}\)\s*)/if (\1 != 0)/g' {} \;
find ./ -type f -exec sed -i -- 's/if\s*(\s*!(\s*\([_a-zA-Z][_a-zA-Z0-9]\{0,30\}\)\s*)\s*)/if (!(\1 != 0))/g' {} \;
find ./ -type f -exec sed -i -- 's/if\s*(\s*!\([_a-zA-Z][_a-zA-Z0-9]\{0,30\}\)\s*)/if (!(\1 != 0))/g' {} \;

# rewrite while statements
find ./ -type f -exec sed -i -- 's/while\s*(\s*\([_a-zA-Z][_a-zA-Z0-9]\{0,30\}\)\s*)/while (\1 != 0)/g' {} \;
find ./ -type f -exec sed -i -- 's/while\s*(\s*!(\s*\([_a-zA-Z][_a-zA-Z0-9]\{0,30\}\)\s*)\s*)/while (!(\1 != 0))/g' {} \;
find ./ -type f -exec sed -i -- 's/while\s*(\s*!\([_a-zA-Z][_a-zA-Z0-9]\{0,30\}\)\s*)/while (!(\1 != 0))/g' {} \;

# rewrite assert statements
find ./ -type f -exec sed -i -- 's/__VERIFIER_assert(\s*\(.*\)\s*);/if (!(\1)) {\n  __VERIFIER_error();\n}/g' {} \;

