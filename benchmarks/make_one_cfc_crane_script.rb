STDOUT_DIR = "/work/esquared/mgerrard/cfc/benchmarks/ase_benchmarks/output"
STDERR_DIR = "/work/esquared/mgerrard/cfc/benchmarks/ase_benchmarks/error"
CFC = "/work/esquared/mgerrard/cfc/src/cfc.rb"

def convert_to_hr_min_sec ( t )
  # Solution: https://gist.github.com/shunchu/3175001
  return Time.at(t).utc.strftime("%H:%M:%S")
end

def get_base_name ( program )
  return program.split('/').last
end

def make_script ( program )
  max_time = 50000 # Time based on longest running of successful results
  script_timeout = convert_to_hr_min_sec(max_time)
  base_name = get_base_name(program)
  timeout = 900 # SV-COMP timeout -- how many can go through?

  memory = 4096
  stdout_file = "#{STDOUT_DIR}/#{base_name}.out"
  stderr_file = "#{STDERR_DIR}/#{base_name}.err"

return <<-SCRIPT
#!/bin/sh
#SBATCH --time=#{script_timeout}           # Run time in hh:mm:ss
#SBATCH --mem-per-cpu=#{memory}       # Maximum memory required per CPU (in megabytes)
#SBATCH --job-name=#{base_name}
#SBATCH --error=#{stderr_file}
#SBATCH --output=#{stdout_file}
#SBATCH --partition=esquared

module load ruby/2.1
module load python/2.7
module load java/1.8
module load compiler/gcc/4.9

shopt -s extglob
LD_LIBRARY_PATH=/work/esquared/mgerrard/local/provers/cvc3/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/work/esquared/mgerrard/local/lib
PATH=/work/esquared/mgerrard/local/bin:$PATH
export UA_PATH="/work/esquared/mgerrard/cfc/analyzers/uautomizer" 
export PATH=$UA_PATH:$PATH

ruby #{CFC} --ua_timeout #{timeout} --cpa_timeout #{timeout} #{program}
SCRIPT
end

ASE_SCRIPT_DIR = "/work/esquared/mgerrard/cfc/benchmarks/ase_benchmarks/scripts"

def make_one_cfc_sandhills_script ( program )
  script_string = make_script(program)
  base_name = get_base_name(program)
  script_path = "#{ASE_SCRIPT_DIR}/#{base_name}.script"
  File.write(script_path, script_string)
end

###########

program = ARGV[0]
make_one_cfc_sandhills_script(program)
